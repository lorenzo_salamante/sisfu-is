/* 
 * @Author: Lorenzo Salamante - lorenzosalamante@gmail.com
 * Team Istruktura - http://www.istruktura.com
 */

$(document).ready(function () {
    console.log("ready!");

    var current_address = $('#current_address');

    var permanent_address = $('#permanent_address');
    var father_address = $('#father_address');
    var mother_address = $('#mother_address');
    var guardian_address = $('#guardian_address');
    
    $('.same-1').on('click', function () {
        permanent_address.val(getCurrentAddress());
    });
    $('.same-2').on('click', function () {
        father_address.val(getCurrentAddress());
    });
    $('.same-3').on('click', function () {
        mother_address.val(getCurrentAddress());
    });
    $('.same-4').on('click', function () {
        guardian_address.val(getCurrentAddress());
    });
    $('#unlock').on('click', function () {
        $('input').prop('disabled', false);
        $('select').prop('disabled', false);
        $('textarea').prop('disabled', false);
        $('#btn-edit').prop('disabled', false);
        $('.same-1').attr('disabled', false);
        $('.same-2').attr('disabled', false);
        $('.same-3').attr('disabled', false);
        $('.same-4').attr('disabled', false);
    });

    function getCurrentAddress() {
        return current_address.val();
    }
});