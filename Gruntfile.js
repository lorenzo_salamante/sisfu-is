/* 
 * @Author: Lorenzo Salamante - lorenzosalamante@gmail.com
 * Team Istruktura - http://www.istruktura.com
 */
module.exports = function (grunt) {
    grunt.initConfig({
        autoprefixer: {
            dist: {
                files: {
                    // output : watched
                    'assets/css/custom.css': 'assets/css/custom.css'
                }
            }
        },
        watch: {
            styles: {
                //watched
                files: ['assets/css/custom.css'],
                tasks: ['autoprefixer']
            }
        }
    });
    grunt.loadNpmTasks('grunt-autoprefixer');
    grunt.loadNpmTasks('grunt-contrib-watch');
};