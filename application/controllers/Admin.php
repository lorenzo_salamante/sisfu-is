<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');

        /* cache control */
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
    }

    /*     * *default functin, redirects to login page if no admin logged in yet** */

    public function index() {
        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url() . 'index.php?login', 'refresh');
        if ($this->session->userdata('admin_login') == 1)
            redirect(base_url() . 'index.php?admin/dashboard', 'refresh');
    }

    /*     * *ADMIN DASHBOARD** */

    function dashboard() {
        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url(), 'refresh');
        $page_data['page_name'] = 'dashboard';
        $page_data['page_title'] = get_phrase('dashboard');
        $this->load->view('backend/index', $page_data);
    }

    /*     * **MANAGE STUDENTS CLASSWISE**** */

    function student_add() {
        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url(), 'refresh');

        $page_data['page_name'] = 'student_add';
        $page_data['page_title'] = get_phrase('admit_new_student');
        $this->load->view('backend/index', $page_data);
    }

    function student_transcript($param1 = '', $param2 = '', $param3 = '') {
        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url(), 'refresh');

        $page_data['page_name'] = 'student_transcript';
        $page_data['page_title'] = get_phrase('student_transcript');
        $this->load->view('backend/index', $page_data);
    }

    function student_edit($param1 = '', $param2 = '', $param3 = '') {
        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url(), 'refresh');

        $page_data['page_name'] = 'student_edit';
        $page_data['page_title'] = get_phrase('manage_student');
        $this->load->view('backend/index', $page_data);
    }

    function student_assess($param1 = '', $param2 = '', $param3 = '') {
        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url(), 'refresh');

        $page_data['page_name'] = 'student_assess';
        $page_data['page_title'] = get_phrase('assess_student');
        $this->load->view('backend/index', $page_data);
    }

    function student_sog($param1 = '', $param2 = '', $param3 = '') {
        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url(), 'refresh');

        $page_data['page_name'] = 'student_sog';
        $page_data['page_title'] = get_phrase('statement_of_grades');
        $this->load->view('backend/index', $page_data);
    }

    function student_bulk_add($param1 = '') {
        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url(), 'refresh');

        if ($param1 == 'import_excel') {
            move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/student_import.xlsx');
// Importing excel sheet for bulk student uploads

            include 'simplexlsx.class.php';

            $xlsx = new SimpleXLSX('uploads/student_import.xlsx');

            list($num_cols, $num_rows) = $xlsx->dimension();
            $f = 0;
            foreach ($xlsx->rows() as $r) {
// Ignore the inital name row of excel file
                if ($f == 0) {
                    $f++;
                    continue;
                }
                for ($i = 0; $i < $num_cols; $i++) {
                    if ($i == 0)
                        $data['name'] = $r[$i];
                    else if ($i == 1)
                        $data['birthday'] = $r[$i];
                    else if ($i == 2)
                        $data['sex'] = $r[$i];
                    else if ($i == 3)
                        $data['address'] = $r[$i];
                    else if ($i == 4)
                        $data['phone'] = $r[$i];
                    else if ($i == 5)
                        $data['email'] = $r[$i];
                    else if ($i == 6)
                        $data['password'] = $r[$i];
                    else if ($i == 7)
                        $data['roll'] = $r[$i];
                }
                $data['class_id'] = $this->input->post('class_id');

                $this->db->insert('student', $data);
//print_r($data);
            }
            redirect(base_url() . 'index.php?admin/student_information/' . $this->input->post('class_id'), 'refresh');
        }
        $page_data['page_name'] = 'student_bulk_add';
        $page_data['page_title'] = get_phrase('add_bulk_student');
        $this->load->view('backend/index', $page_data);
    }

    function student_information($class_id = '') {
        if ($this->session->userdata('admin_login') != 1)
            redirect('login', 'refresh');

        $page_data['page_name'] = 'student_information';
        $page_data['page_title'] = get_phrase('student_information') . " - " . get_phrase('class') . " : " .
                $this->crud_model->get_class_name($class_id);
        $page_data['class_id'] = $class_id;
        $this->load->view('backend/index', $page_data);
    }

    function student_marksheet($class_id = '') {
        if ($this->session->userdata('admin_login') != 1)
            redirect('login', 'refresh');

        $page_data['page_name'] = 'student_marksheet';
        $page_data['page_title'] = get_phrase('student_marksheet') . " - " . get_phrase('class') . " : " .
                $this->crud_model->get_class_name($class_id);
        $page_data['class_id'] = $class_id;
        $this->load->view('backend/index', $page_data);
    }

#STUDENT STUFFS

    function student($param1 = '', $param2 = '', $param3 = '', $param4 = '', $param5 = '') {
        if ($this->session->userdata('admin_login') != 1)
            redirect('login', 'refresh');

        $this->load->model('Student_model');

        if ($param1 == 'create') {
//            NAME FIELDS
            $data['student_no'] = $this->input->post('student_no');
            $data['last_name'] = $this->input->post('last_name');
            $data['first_name'] = $this->input->post('first_name');
            $data['middle_name'] = $this->input->post('middle_name');
            $data['suffix'] = $this->input->post('suffix');
            $data['nickname'] = $this->input->post('nickname');

            $data['admission_date'] = date('Y-m-d');
            $data['admitted_by'] = $_SESSION['username'];

            $data['department_id'] = $this->input->post('department_id');
            $data['course_id'] = $this->input->post('course_id');
            $data['scholarship_id'] = $this->input->post('scholarship_id');
            $data['discount_id'] = $this->input->post('discount_id');

            $data['status_id'] = $this->input->post('status_id');

            $data['nationality_id'] = $this->input->post('nationality_id');
            $data['sex'] = $this->input->post('sex');
            $data['birth_date'] = $this->input->post('birth_date');

            $data['current_address'] = $this->input->post('current_address');
            $data['permanent_address'] = $this->input->post('permanent_address');
            $data['email'] = $this->input->post('email');
            $data['contact_no'] = $this->input->post('contact_no');

            $data['visa_id'] = $this->input->post('visa_id');
            $data['visa_expiration'] = $this->input->post('visa_expiration');

            $data['remarks'] = $this->input->post('remarks');
            $data['photo'] = $this->input->post('photo');

            $data['is_active'] = 1;

            $data['degree_id'] = $this->input->post('degree_id');
            $data['degree_year_completed'] = $this->input->post('degree_year_completed');
            $data['other_school_id'] = $this->input->post('other_school_id');
            $data['other_school_year_completed'] = $this->input->post('other_school_year_completed');
            $data['high_school_id'] = $this->input->post('high_school_id');
            $data['high_school_year_completed'] = $this->input->post('high_school_year_completed');

            $this->db->insert('sisfu_students', $data);
            $student_id = $this->db->insert_id();

#PARENTS
            $father['parent_name'] = $this->input->post('father_name');
            $father['parent_address'] = $this->input->post('father_address');
            $father['parent_contact'] = $this->input->post('father_contact');
            $father['parent_email'] = $this->input->post('father_email');
            $father['parent_code'] = $this->input->post('father_code');
            $father['student_id'] = $student_id;

            $this->db->insert('sisfu_parents', $father);

            $mother['parent_name'] = $this->input->post('mother_name');
            $mother['parent_address'] = $this->input->post('mother_address');
            $mother['parent_contact'] = $this->input->post('mother_contact');
            $mother['parent_email'] = $this->input->post('mother_email');
            $mother['parent_code'] = $this->input->post('mother_code');
            $mother['student_id'] = $student_id;

            $this->db->insert('sisfu_parents', $mother);

            $guardian['parent_name'] = $this->input->post('guardian_name');
            $guardian['parent_address'] = $this->input->post('guardian_address');
            $guardian['parent_contact'] = $this->input->post('guardian_contact');
            $guardian['parent_email'] = $this->input->post('guardian_email');
            $guardian['parent_code'] = $this->input->post('guardian_code');
            $guardian['student_id'] = $student_id;

            $this->db->insert('sisfu_parents', $guardian);

#CHECKLISTS
#CHECKLIST FOR FRESHMAN
            $checklist['checklist_name'] = '500-word essay';
            $checklist['checklist_type'] = '1';
            $checklist['checklist_code'] = 'fresh-1';
            $checklist['student_id'] = $student_id;
            $this->db->insert('sisfu_checklist', $checklist);

            $checklist['checklist_name'] = 'Copy of birth certificate and/or passport';
            $checklist['checklist_type'] = '1';
            $checklist['checklist_code'] = 'fresh-2';
            $checklist['student_id'] = $student_id;
            $this->db->insert('sisfu_checklist', $checklist);

            $checklist['checklist_name'] = 'Four (4) 2&quot; x 2&quot; pictures';
            $checklist['checklist_type'] = '1';
            $checklist['checklist_code'] = 'fresh-3';
            $checklist['student_id'] = $student_id;
            $this->db->insert('sisfu_checklist', $checklist);

            $checklist['checklist_name'] = 'High School Diploma';
            $checklist['checklist_type'] = '1';
            $checklist['checklist_code'] = 'fresh-4';
            $checklist['student_id'] = $student_id;
            $this->db->insert('sisfu_checklist', $checklist);

            $checklist['checklist_name'] = 'High School Report Card (F138)';
            $checklist['checklist_type'] = '1';
            $checklist['checklist_code'] = 'fresh-5';
            $checklist['student_id'] = $student_id;
            $this->db->insert('sisfu_checklist', $checklist);

            $checklist['checklist_name'] = 'Official Transcript of Secondary Education (F137)';
            $checklist['checklist_type'] = '1';
            $checklist['checklist_code'] = 'fresh-6';
            $checklist['student_id'] = $student_id;
            $this->db->insert('sisfu_checklist', $checklist);

            $checklist['checklist_name'] = 'Payment ofadmission and application fees';
            $checklist['checklist_type'] = '1';
            $checklist['checklist_code'] = 'fresh-7';
            $checklist['student_id'] = $student_id;
            $this->db->insert('sisfu_checklist', $checklist);

            $checklist['checklist_name'] = 'Recommendation Letter from a Professor or Teacher';
            $checklist['checklist_type'] = '1';
            $checklist['checklist_code'] = 'fresh-8';
            $checklist['student_id'] = $student_id;
            $this->db->insert('sisfu_checklist', $checklist);

            $checklist['checklist_name'] = 'Recommendation Letter from a School Principal or Guidance Counselor';
            $checklist['checklist_type'] = '1';
            $checklist['checklist_code'] = 'fresh-9';
            $checklist['student_id'] = $student_id;
            $this->db->insert('sisfu_checklist', $checklist);

#CHECKLIST FOR TRANSFEREE
            $checklist['checklist_name'] = '500-word essay';
            $checklist['checklist_type'] = '2';
            $checklist['checklist_code'] = 'trans-1';
            $checklist['student_id'] = $student_id;
            $this->db->insert('sisfu_checklist', $checklist);

            $checklist['checklist_name'] = 'Certificate of Honorable Dismissal';
            $checklist['checklist_type'] = '2';
            $checklist['checklist_code'] = 'trans-2';
            $checklist['student_id'] = $student_id;
            $this->db->insert('sisfu_checklist', $checklist);

            $checklist['checklist_name'] = 'Copy of birth certificate and/or passport';
            $checklist['checklist_type'] = '2';
            $checklist['checklist_code'] = 'trans-3';
            $checklist['student_id'] = $student_id;
            $this->db->insert('sisfu_checklist', $checklist);

            $checklist['checklist_name'] = 'Course Description';
            $checklist['checklist_type'] = '2';
            $checklist['checklist_code'] = 'trans-4';
            $checklist['student_id'] = $student_id;
            $this->db->insert('sisfu_checklist', $checklist);

            $checklist['checklist_name'] = 'Four (4) 2&quot; x 2&quot; pictures';
            $checklist['checklist_type'] = '2';
            $checklist['checklist_code'] = 'trans-5';
            $checklist['student_id'] = $student_id;
            $this->db->insert('sisfu_checklist', $checklist);

            $checklist['checklist_name'] = 'Official Transcript of Secondary Education (F137)';
            $checklist['checklist_type'] = '2';
            $checklist['checklist_code'] = 'trans-6';
            $checklist['student_id'] = $student_id;
            $this->db->insert('sisfu_checklist', $checklist);

            $checklist['checklist_name'] = 'Payment of admission and application fees';
            $checklist['checklist_type'] = '2';
            $checklist['checklist_code'] = 'trans-7';
            $checklist['student_id'] = $student_id;
            $this->db->insert('sisfu_checklist', $checklist);

            $checklist['checklist_name'] = 'Recommendation Letter from a Professor or Teacher';
            $checklist['checklist_type'] = '2';
            $checklist['checklist_code'] = 'trans-8';
            $checklist['student_id'] = $student_id;
            $this->db->insert('sisfu_checklist', $checklist);

            $checklist['checklist_name'] = 'Recommendation Letter from School Principal or Guidance Counselor';
            $checklist['checklist_type'] = '2';
            $checklist['checklist_code'] = 'trans-9';
            $checklist['student_id'] = $student_id;
            $this->db->insert('sisfu_checklist', $checklist);

#CHECKLIST FOR FOREIGN STUDENT
            $checklist['checklist_name'] = '500-word essay';
            $checklist['checklist_type'] = '3';
            $checklist['checklist_code'] = 'for-1';
            $checklist['student_id'] = $student_id;
            $this->db->insert('sisfu_checklist', $checklist);

            $checklist['checklist_name'] = 'Copy of Alien Certificate or Registration(ACR)';
            $checklist['checklist_type'] = '3';
            $checklist['checklist_code'] = 'for-2';
            $checklist['student_id'] = $student_id;
            $this->db->insert('sisfu_checklist', $checklist);

            $checklist['checklist_name'] = 'Copy of birth certificate and/or passport';
            $checklist['checklist_type'] = '3';
            $checklist['checklist_code'] = 'for-3';
            $checklist['student_id'] = $student_id;
            $this->db->insert('sisfu_checklist', $checklist);

            $checklist['checklist_name'] = 'Copy of Student Visa and/or Special Study Permit';
            $checklist['checklist_type'] = '3';
            $checklist['checklist_code'] = 'for-4';
            $checklist['student_id'] = $student_id;
            $this->db->insert('sisfu_checklist', $checklist);

            $checklist['checklist_name'] = 'Four (4) 2&quot; x 2&quot; pictures';
            $checklist['checklist_type'] = '3';
            $checklist['checklist_code'] = 'for-5';
            $checklist['student_id'] = $student_id;
            $this->db->insert('sisfu_checklist', $checklist);

            $checklist['checklist_name'] = 'High School Diploma';
            $checklist['checklist_type'] = '3';
            $checklist['checklist_code'] = 'for-6';
            $checklist['student_id'] = $student_id;
            $this->db->insert('sisfu_checklist', $checklist);

            $checklist['checklist_name'] = 'High School Report Card (F138)';
            $checklist['checklist_type'] = '3';
            $checklist['checklist_code'] = 'for-7';
            $checklist['student_id'] = $student_id;
            $this->db->insert('sisfu_checklist', $checklist);

            $checklist['checklist_name'] = 'Official Transcript of Secondary Education (F137)';
            $checklist['checklist_type'] = '3';
            $checklist['checklist_code'] = 'for-8';
            $checklist['student_id'] = $student_id;
            $this->db->insert('sisfu_checklist', $checklist);

            $checklist['checklist_name'] = 'Payment of admission and application fees';
            $checklist['checklist_type'] = '3';
            $checklist['checklist_code'] = 'for-9';
            $checklist['student_id'] = $student_id;
            $this->db->insert('sisfu_checklist', $checklist);

            $checklist['checklist_name'] = 'Recommendation Letter from a Professor or Teacher';
            $checklist['checklist_type'] = '3';
            $checklist['checklist_code'] = 'for-10';
            $checklist['student_id'] = $student_id;
            $this->db->insert('sisfu_checklist', $checklist);

            $checklist['checklist_name'] = 'Recommendation Letter from School Principal or Guidance Counselor';
            $checklist['checklist_type'] = '3';
            $checklist['checklist_code'] = 'for-11';
            $checklist['student_id'] = $student_id;
            $this->db->insert('sisfu_checklist', $checklist);

#CHECKLIST FOR GRADUATE SCHOOL
            $checklist['checklist_name'] = 'Copy of birth certificate and/or passport';
            $checklist['checklist_type'] = '4';
            $checklist['checklist_code'] = 'grad-1';
            $checklist['student_id'] = $student_id;
            $this->db->insert('sisfu_checklist', $checklist);

            $checklist['checklist_name'] = 'Certificate of Honorable Dismissal';
            $checklist['checklist_type'] = '4';
            $checklist['checklist_code'] = 'grad-2';
            $checklist['student_id'] = $student_id;
            $this->db->insert('sisfu_checklist', $checklist);

            $checklist['checklist_name'] = 'Four (4) 2&quot; x 2&quot; pictures';
            $checklist['checklist_type'] = '4';
            $checklist['checklist_code'] = 'grad-3';
            $checklist['student_id'] = $student_id;
            $this->db->insert('sisfu_checklist', $checklist);

            $checklist['checklist_name'] = 'Copy of Student Visa and/or Special Study Permit';
            $checklist['checklist_type'] = '4';
            $checklist['checklist_code'] = 'grad-4';
            $checklist['student_id'] = $student_id;
            $this->db->insert('sisfu_checklist', $checklist);

            $checklist['checklist_name'] = 'Copy of Alien Certificate or Registration(ACR)';
            $checklist['checklist_type'] = '4';
            $checklist['checklist_code'] = 'grad-5';
            $checklist['student_id'] = $student_id;
            $this->db->insert('sisfu_checklist', $checklist);

            $checklist['checklist_name'] = 'Payment of admission and application fees';
            $checklist['checklist_type'] = '4';
            $checklist['checklist_code'] = 'grad-6';
            $checklist['student_id'] = $student_id;
            $this->db->insert('sisfu_checklist', $checklist);

            $checklist['checklist_name'] = 'Recommendation Letters from Employer and Immediate Supervisor';
            $checklist['checklist_type'] = '4';
            $checklist['checklist_code'] = 'grad-7';
            $checklist['student_id'] = $student_id;
            $this->db->insert('sisfu_checklist', $checklist);

            $checklist['checklist_name'] = 'Complete Curriculum Vitae / Resume';
            $checklist['checklist_type'] = '4';
            $checklist['checklist_code'] = 'grad-8';
            $checklist['student_id'] = $student_id;
            $this->db->insert('sisfu_checklist', $checklist);

            $checklist['checklist_name'] = 'Enrollment Agreement';
            $checklist['checklist_type'] = '4';
            $checklist['checklist_code'] = 'grad-9';
            $checklist['student_id'] = $student_id;
            $this->db->insert('sisfu_checklist', $checklist);

            $checklist['checklist_name'] = 'Completed & Signed Application for Admission';
            $checklist['checklist_type'] = '4';
            $checklist['checklist_code'] = 'grad-10';
            $checklist['student_id'] = $student_id;
            $this->db->insert('sisfu_checklist', $checklist);

            $checklist['checklist_name'] = 'A copy of Bachelor & Master Diplomas with English Translation';
            $checklist['checklist_type'] = '4';
            $checklist['checklist_code'] = 'grad-11';
            $checklist['student_id'] = $student_id;
            $this->db->insert('sisfu_checklist', $checklist);

            $checklist['checklist_name'] = 'A copy of Bachelor & Master Transcripts with English Translation';
            $checklist['checklist_type'] = '4';
            $checklist['checklist_code'] = 'grad-12';
            $checklist['student_id'] = $student_id;
            $this->db->insert('sisfu_checklist', $checklist);

#CHECKLIST FOR FOREIGN / TRANSFEREE
            $checklist['checklist_name'] = '500-word essay';
            $checklist['checklist_type'] = '5';
            $checklist['checklist_code'] = 'tf-1';
            $checklist['student_id'] = $student_id;
            $this->db->insert('sisfu_checklist', $checklist);

            $checklist['checklist_name'] = 'Certificate of Honorable Dismissal';
            $checklist['checklist_type'] = '5';
            $checklist['checklist_code'] = 'tf-2';
            $checklist['student_id'] = $student_id;
            $this->db->insert('sisfu_checklist', $checklist);

            $checklist['checklist_name'] = 'Copy of birth certificate and/or passport';
            $checklist['checklist_type'] = '5';
            $checklist['checklist_code'] = 'tf-3';
            $checklist['student_id'] = $student_id;
            $this->db->insert('sisfu_checklist', $checklist);

            $checklist['checklist_name'] = 'Course Description';
            $checklist['checklist_type'] = '5';
            $checklist['checklist_code'] = 'tf-4';
            $checklist['student_id'] = $student_id;
            $this->db->insert('sisfu_checklist', $checklist);

            $checklist['checklist_name'] = 'Four (4) 2&quot; x 2&quot; pictures';
            $checklist['checklist_type'] = '5';
            $checklist['checklist_code'] = 'tf-5';
            $checklist['student_id'] = $student_id;
            $this->db->insert('sisfu_checklist', $checklist);

            $checklist['checklist_name'] = 'Official Transcript of Secondary Education (F137)';
            $checklist['checklist_type'] = '5';
            $checklist['checklist_code'] = 'tf-6';
            $checklist['student_id'] = $student_id;
            $this->db->insert('sisfu_checklist', $checklist);

            $checklist['checklist_name'] = 'Payment of admission and application fees';
            $checklist['checklist_type'] = '5';
            $checklist['checklist_code'] = 'tf-7';
            $checklist['student_id'] = $student_id;
            $this->db->insert('sisfu_checklist', $checklist);

            $checklist['checklist_name'] = 'Recommendation Letter from a Professor or Teacher';
            $checklist['checklist_type'] = '5';
            $checklist['checklist_code'] = 'tf-8';
            $checklist['student_id'] = $student_id;
            $this->db->insert('sisfu_checklist', $checklist);

            $checklist['checklist_name'] = 'Recommendation Letter from School Principal or Guidance Counselor';
            $checklist['checklist_type'] = '5';
            $checklist['checklist_code'] = 'tf-9';
            $checklist['student_id'] = $student_id;
            $this->db->insert('sisfu_checklist', $checklist);

            $checklist['checklist_name'] = 'Copy of Alien Certification or Registration (ACR)';
            $checklist['checklist_type'] = '5';
            $checklist['checklist_code'] = 'tf-10';
            $checklist['student_id'] = $student_id;
            $this->db->insert('sisfu_checklist', $checklist);

            $checklist['checklist_name'] = 'Copy of Student Visa and/or Special Study Permit';
            $checklist['checklist_type'] = '5';
            $checklist['checklist_code'] = 'tf-11';
            $checklist['student_id'] = $student_id;
            $this->db->insert('sisfu_checklist', $checklist);

            $checklist['checklist_name'] = 'High School Diploma';
            $checklist['checklist_type'] = '5';
            $checklist['checklist_code'] = 'tf-12';
            $checklist['student_id'] = $student_id;
            $this->db->insert('sisfu_checklist', $checklist);

            $checklist['checklist_name'] = 'High Report Card (F138)';
            $checklist['checklist_type'] = '5';
            $checklist['checklist_code'] = 'tf-13';
            $checklist['student_id'] = $student_id;
            $this->db->insert('sisfu_checklist', $checklist);
#END FOREIGN / TRANSFEREE

            move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/student_image/' . $student_id . '.jpg');

            $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
            redirect(base_url() . 'index.php?admin/student_add/', 'refresh');
//            $this->email_model->account_opening_email('student', $data['email']); //SEND EMAIL ACCOUNT OPENING EMAIL
        }
        if ($param1 == 'do_update') {

            $data['student_no'] = $this->input->post('student_no');
            $data['last_name'] = $this->input->post('last_name');
            $data['first_name'] = $this->input->post('first_name');
            $data['middle_name'] = $this->input->post('middle_name');
            $data['suffix'] = $this->input->post('suffix');
            $data['nickname'] = $this->input->post('nickname');
            $data['level_id'] = $this->input->post('level_id');

            $data['department_id'] = $this->input->post('department_id');
            $data['course_id'] = $this->input->post('course_id');
            $data['scholarship_id'] = $this->input->post('scholarship_id');
            $data['discount_id'] = $this->input->post('discount_id');
//            $data['mentor_id'] = $this->input->post('mentor_id');

            $data['status_id'] = $this->input->post('status_id');

            $data['nationality_id'] = $this->input->post('nationality_id');
            $data['sex'] = $this->input->post('sex');
            $data['birth_date'] = $this->input->post('birth_date');

            $data['current_address'] = $this->input->post('current_address');
            $data['permanent_address'] = $this->input->post('permanent_address');
            $data['email'] = $this->input->post('email');
            $data['contact_no'] = $this->input->post('contact_no');

            $data['visa_id'] = $this->input->post('visa_id');
            $data['visa_expiration'] = $this->input->post('visa_expiration');

            $data['remarks'] = $this->input->post('remarks');
            $data['photo'] = $this->input->post('photo');

            $data['degree_id'] = $this->input->post('degree_id');
            $data['degree_year_completed'] = $this->input->post('degree_year_completed');
            $data['other_school_id'] = $this->input->post('other_school_id');
            $data['other_school_year_completed'] = $this->input->post('other_school_year_completed');
            $data['high_school_id'] = $this->input->post('high_school_id');
            $data['high_school_year_completed'] = $this->input->post('high_school_year_completed');

            $this->db->where('student_id', $param2);
            $this->db->update('sisfu_students', $data);

#PARENTS
            $this->db->flush_cache();
            $father['parent_name'] = $this->input->post('father_name');
            $father['parent_address'] = $this->input->post('father_address');
            $father['parent_contact'] = $this->input->post('father_contact');
            $father['parent_email'] = $this->input->post('father_email');

            $this->db->where('student_id', $param2);
            $this->db->where('parent_code', $this->input->post('father_code'));
            $this->db->update('sisfu_parents', $father);

            $this->db->flush_cache();
            $mother['parent_name'] = $this->input->post('mother_name');
            $mother['parent_address'] = $this->input->post('mother_address');
            $mother['parent_contact'] = $this->input->post('mother_contact');
            $mother['parent_email'] = $this->input->post('mother_email');

            $this->db->where('student_id', $param2);
            $this->db->where('parent_code', $this->input->post('mother_code'));
            $this->db->update('sisfu_parents', $mother);

            $this->db->flush_cache();
            $guardian['parent_name'] = $this->input->post('guardian_name');
            $guardian['parent_address'] = $this->input->post('guardian_address');
            $guardian['parent_contact'] = $this->input->post('guardian_contact');
            $guardian['parent_email'] = $this->input->post('guardian_email');

            $this->db->where('student_id', $param2);
            $this->db->where('parent_code', $this->input->post('guardian_code'));
            $this->db->update('sisfu_parents', $guardian);


            move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/student_image/' . $param2 . '.jpg');
            $this->crud_model->clear_cache();
            $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
            redirect(base_url() . 'index.php?admin/student_edit/' . $param2, 'refresh');
        }
        if ($param1 == 'do_checklist') {

            $fresh_arr = [];
            $trans_arr = [];
            $for_arr = [];
            $grad_arr = [];
            $tf_arr = [];
            $fresh_arr_full = ['fresh-1', 'fresh-2', 'fresh-3', 'fresh-4', 'fresh-5', 'fresh-6', 'fresh-7', 'fresh-8', 'fresh-9'];
            $trans_arr_full = ['trans-1', 'trans-2', 'trans-3', 'trans-4', 'trans-5', 'trans-6', 'trans-7', 'trans-8', 'trans-9'];
            $for_arr_full = ['for-1', 'for-2', 'for-3', 'for-4', 'for-5', 'for-6', 'for-7', 'for-8', 'for-9', 'for-10', 'for-11'];
            $grad_arr_full = ['grad-1', 'grad-2', 'grad-3', 'grad-4', 'grad-5', 'grad-6', 'grad-7', 'grad-8', 'grad-9', 'grad-10', 'grad-11', 'grad-12'];
            $tf_arr_full = ['tf-1', 'tf-2', 'tf-3', 'tf-4', 'tf-5', 'tf-6', 'tf-7', 'tf-8', 'tf-9', 'tf-10', 'tf-11', 'tf-12', 'tf-13'];
#FRESHMAN
            foreach ($this->input->post('freshman') as $freshman):
                $data['is_checked'] = 1;

                $this->db->where('student_id', $param2);
                $this->db->where('checklist_type', 1);
                $this->db->where('checklist_code', $freshman);
                $this->db->update('sisfu_checklist', $data);
                array_push($fresh_arr, $freshman);
            endforeach;

            foreach ($fresh_arr as $row):
                foreach ($fresh_arr_full as $innerRow):
                    if (($key = array_search($row, $fresh_arr_full)) !== false) {
                        unset($fresh_arr_full[$key]);
                    }
                endforeach;
            endforeach;

            foreach ($fresh_arr_full as $row):
                $data['is_checked'] = 0;

                $this->db->where('student_id', $param2);
                $this->db->where('checklist_type', 1);
                $this->db->where('checklist_code', $row);
                $this->db->update('sisfu_checklist', $data);
            endforeach;
#END FRESHMAN
#TRANSFEREE
            foreach ($this->input->post('transferee') as $transferee):
                $data['is_checked'] = 1;

                $this->db->where('student_id', $param2);
                $this->db->where('checklist_type', 2); #type 2 = transferee
                $this->db->where('checklist_code', $transferee);
                $this->db->update('sisfu_checklist', $data);
                array_push($trans_arr, $transferee);
            endforeach;

            foreach ($trans_arr as $row):
                foreach ($trans_arr_full as $innerRow):
                    if (($key = array_search($row, $trans_arr_full)) !== false) {
                        unset($trans_arr_full[$key]);
                    }
                endforeach;
            endforeach;

            foreach ($trans_arr_full as $row):
                $data['is_checked'] = 0;

                $this->db->where('student_id', $param2);
                $this->db->where('checklist_type', 2); #type 2
                $this->db->where('checklist_code', $row);
                $this->db->update('sisfu_checklist', $data);
            endforeach;
#END TRANSFEREE
#FOREIGN
            foreach ($this->input->post('foreign') as $foreign):
                $data['is_checked'] = 1;

                $this->db->where('student_id', $param2);
                $this->db->where('checklist_type', 3); #type 3 = foreign
                $this->db->where('checklist_code', $foreign);
                $this->db->update('sisfu_checklist', $data);
                array_push($for_arr, $foreign);
            endforeach;

            foreach ($for_arr as $row):
                foreach ($for_arr_full as $innerRow):
                    if (($key = array_search($row, $for_arr_full)) !== false) {
                        unset($for_arr_full[$key]);
                    }
                endforeach;
            endforeach;

            foreach ($for_arr_full as $row):
                $data['is_checked'] = 0;

                $this->db->where('student_id', $param2);
                $this->db->where('checklist_type', 3);
                $this->db->where('checklist_code', $row);
                $this->db->update('sisfu_checklist', $data);
            endforeach;
#END FOREIGN
#GRAD
            foreach ($this->input->post('graduate') as $graduate):
                $data['is_checked'] = 1;

                $this->db->where('student_id', $param2);
                $this->db->where('checklist_type', 4); #type 3 = Graduate
                $this->db->where('checklist_code', $graduate);
                $this->db->update('sisfu_checklist', $data);
                array_push($grad_arr, $graduate);
            endforeach;

            foreach ($grad_arr as $row):
                foreach ($grad_arr_full as $innerRow):
                    if (($key = array_search($row, $grad_arr_full)) !== false) {
                        unset($grad_arr_full[$key]);
                    }
                endforeach;
            endforeach;

            foreach ($grad_arr_full as $row):
                $data['is_checked'] = 0;

                $this->db->where('student_id', $param2);
                $this->db->where('checklist_type', 4);
                $this->db->where('checklist_code', $row);
                $this->db->update('sisfu_checklist', $data);
            endforeach;
#END GRAD
#TF
            foreach ($this->input->post('tf') as $tf):
                $data['is_checked'] = 1;

                $this->db->where('student_id', $param2);
                $this->db->where('checklist_type', 5); #type 3 = Graduate
                $this->db->where('checklist_code', $tf);
                $this->db->update('sisfu_checklist', $data);
                array_push($tf_arr, $tf);
            endforeach;

            foreach ($tf_arr as $row):
                foreach ($tf_arr_full as $innerRow):
                    if (($key = array_search($row, $tf_arr_full)) !== false) {
                        unset($tf_arr_full[$key]);
                    }
                endforeach;
            endforeach;

            foreach ($tf_arr_full as $row):
                $data['is_checked'] = 0;

                $this->db->where('student_id', $param2);
                $this->db->where('checklist_type', 5);
                $this->db->where('checklist_code', $row);
                $this->db->update('sisfu_checklist', $data);
            endforeach;
#END TF

            $this->crud_model->clear_cache();
            $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
            redirect(base_url() . 'index.php?admin/student/', 'refresh');
        }
        if ($param1 == 'assess') {

            $num_of_subjs = $this->input->post('subject_count');

            $subjs = '';

            for ($i = 1; $i <= $num_of_subjs; $i++):
                $h = 'hidden_subj_id-' . $i;
                $subjs .= $this->input->post($h) . ",";
            endfor;

            $subjects = rtrim($subjs, ",");

            $data['enrolled_subjects'] = $subjects;

            $this->db->where('student_id', $param2);
            $this->db->update('sisfu_students', $data);


            $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
            redirect(base_url() . 'index.php?admin/student_assess/' . $param2, 'refresh');
        }
        if ($param1 == 'do_post___') {
            $q = $this->db->query('SELECT MAX(level_id) FROM sisfu_transcript WHERE student_id =' . $param2 . '');
            $num_rows = $q->num_rows();

            if ($num_rows > 0) {
                $max_level_id = $q->result();
            } else {
                $max_level_id = 0;
            }

            if ($this->input->post('post_level_id') == 0 || $max_level_id > 0) {
                $data['student_id'] = $param2;
                $data['enrolled_subjects'] = $this->input->post('post_enrolled_subjects');
                $data['balance'] = $this->input->post('post_balance');
                $data['level_id'] = $this->input->post('post_level_id');
                $this->db->insert('sisfu_transcript', $data);

                $stud['enrolled_subjects'] = NULL;
                $stud['level_id'] = $this->input->post('post_level_id') + 1;
                $this->db->where('student_id', $param2);
                $this->db->update('sisfu_students', $stud);


                $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
                redirect(base_url() . 'index.php?admin/student_assess/' . $param2, 'refresh');
            } else {
                $this->session->set_flashdata('flash_message', 'Unknown Error Occurred');
                redirect(base_url() . 'index.php?admin/student_assess/' . $param2, 'refresh');
            }
        }
        if ($param1 == 'do_post_all') {
//            $student = $this->Student_Model->get_all_students();
//            
//            foreach($student as $s){
//                if((strlen($s['enrolled_subjects']) > 0) || ($s['enrolled_subjects'] != ' ')){
//                    
//                }
//            }
            show_404();
        }

        if ($param1 == 'do_post') {
            $student_no = $this->db->get_where('sisfu_students', array('student_id' => $param2))->row()->student_no;

            $tuition['student_no'] = $student_no;
            $tuition['term_code'] = $this->input->post('post_term_code');
            $tuition['acad_year'] = $this->input->post('post_acad_year');
            $tuition['year_level'] = $this->input->post('post_year_level');
            $tuition['tuition'] = $this->input->post('post_tuition');
            $tuition['discount_pct'] = $this->input->post('post_discount_pct');

            $tuition['ir_fee'] = $this->input->post('post_ir_fee');
            $tuition['misc_fee'] = $this->input->post('post_misc_fee');
            $tuition['admin_fee'] = $this->input->post('post_admin_fee');
            $tuition['application_fee'] = $this->input->post('post_application_fee');
            $tuition['math_fee'] = $this->input->post('post_math_fee');
            $tuition['tutorial_fee'] = $this->input->post('post_tutorial_fee');
            $tuition['rswl_fee'] = $this->input->post('post_rswl_fee');
            $tuition['esl_fee'] = $this->input->post('post_esl_fee');
            $tuition['other_fee'] = $this->input->post('post_other_fee');
            $tuition['other_text'] = $this->input->post('post_other_text');
            $tuition['grad_fee_php'] = $this->input->post('post_grad_fee');
            $tuition['b_add_on'] = $this->input->post('post_b_add_on');
            $tuition['c_add_on'] = $this->input->post('post_c_add_on');
            $tuition['grand_total'] = $this->input->post('post_grand_total');
            $tuition['currency'] = $this->input->post('post_currency');

            $tuition['posted_by'] = $_SESSION['username'];
            $tuition['post_date'] = date('Y-m-d');

            $this->db->insert('sisfu_tuition', $tuition);

            $enrolled_subjs = $this->input->post('post_enrolled_subjects');

            $subjs_arr = explode(",", $enrolled_subjs);

            foreach ($subjs_arr as $subject_id) {
                $grade['student_no'] = $student_no;
                $grade['subject_code'] = $this->db->get_where('sisfu_subjects', array('subject_id' => $subject_id))->row()->subject_code;
                $grade['grade_alpha'] = "NGY";
                $grade['posted_by'] = $_SESSION['username'];
                $grade['post_date'] = date('Y-m-d');
                $grade['acad_year'] = $this->input->post('post_acad_year');
                $grade['term_code'] = $this->input->post('post_term_code');
                $this->db->insert('sisfu_grades', $grade);
                $this->db->flush_cache();
            }

            $student['enrolled_subjects'] = NULL;

            $this->db->where('student_id', $param2);
            $this->db->update('sisfu_students', $student);

            $this->session->set_flashdata('flash_message', get_phrase('posted_successfully'));
            redirect(base_url() . 'index.php?admin/student_assess/' . $param2, 'refresh');
        }

        if ($param1 == '___do_script3') {
            
        }
        if ($param1 == '___do_script2') {

            $r = $this->db->get('sisfu_terms')->result_array();
            foreach ($r as $row):
                $data['acad_year'] = $row['acad_year'];
                $data['term_code'] = $row['term_code'];
                $this->db->where('term_id', $row['term_id']);
                $this->db->update('sisfu_grades', $data);
            endforeach;
        }
        if ($param1 == '___do_script') {


            for ($i = 1389; $i > 699; $i--) {
                $student_id = $i;
#PARENTS
                $father['parent_name'] = $this->input->post('father_name');
                $father['parent_address'] = $this->input->post('father_address');
                $father['parent_contact'] = $this->input->post('father_contact');
                $father['parent_email'] = $this->input->post('father_email');
                $father['parent_code'] = $this->input->post('father_code');
                $father['student_id'] = $student_id;

                $this->db->insert('sisfu_parents', $father);

                $mother['parent_name'] = $this->input->post('mother_name');
                $mother['parent_address'] = $this->input->post('mother_address');
                $mother['parent_contact'] = $this->input->post('mother_contact');
                $mother['parent_email'] = $this->input->post('mother_email');
                $mother['parent_code'] = $this->input->post('mother_code');
                $mother['student_id'] = $student_id;

                $this->db->insert('sisfu_parents', $mother);

                $guardian['parent_name'] = $this->input->post('guardian_name');
                $guardian['parent_address'] = $this->input->post('guardian_address');
                $guardian['parent_contact'] = $this->input->post('guardian_contact');
                $guardian['parent_email'] = $this->input->post('guardian_email');
                $guardian['parent_code'] = $this->input->post('guardian_code');
                $guardian['student_id'] = $student_id;

                $this->db->insert('sisfu_parents', $guardian);

#CHECKLISTS
#CHECKLIST FOR FRESHMAN
                $checklist['checklist_name'] = '500-word essay';
                $checklist['checklist_type'] = '1';
                $checklist['checklist_code'] = 'fresh-1';
                $checklist['student_id'] = $student_id;
                $this->db->insert('sisfu_checklist', $checklist);

                $checklist['checklist_name'] = 'Copy of birth certificate and/or passport';
                $checklist['checklist_type'] = '1';
                $checklist['checklist_code'] = 'fresh-2';
                $checklist['student_id'] = $student_id;
                $this->db->insert('sisfu_checklist', $checklist);

                $checklist['checklist_name'] = 'Four (4) 2&quot; x 2&quot; pictures';
                $checklist['checklist_type'] = '1';
                $checklist['checklist_code'] = 'fresh-3';
                $checklist['student_id'] = $student_id;
                $this->db->insert('sisfu_checklist', $checklist);

                $checklist['checklist_name'] = 'High School Diploma';
                $checklist['checklist_type'] = '1';
                $checklist['checklist_code'] = 'fresh-4';
                $checklist['student_id'] = $student_id;
                $this->db->insert('sisfu_checklist', $checklist);

                $checklist['checklist_name'] = 'High School Report Card (F138)';
                $checklist['checklist_type'] = '1';
                $checklist['checklist_code'] = 'fresh-5';
                $checklist['student_id'] = $student_id;
                $this->db->insert('sisfu_checklist', $checklist);

                $checklist['checklist_name'] = 'Official Transcript of Secondary Education (F137)';
                $checklist['checklist_type'] = '1';
                $checklist['checklist_code'] = 'fresh-6';
                $checklist['student_id'] = $student_id;
                $this->db->insert('sisfu_checklist', $checklist);

                $checklist['checklist_name'] = 'Payment ofadmission and application fees';
                $checklist['checklist_type'] = '1';
                $checklist['checklist_code'] = 'fresh-7';
                $checklist['student_id'] = $student_id;
                $this->db->insert('sisfu_checklist', $checklist);

                $checklist['checklist_name'] = 'Recommendation Letter from a Professor or Teacher';
                $checklist['checklist_type'] = '1';
                $checklist['checklist_code'] = 'fresh-8';
                $checklist['student_id'] = $student_id;
                $this->db->insert('sisfu_checklist', $checklist);

                $checklist['checklist_name'] = 'Recommendation Letter from a School Principal or Guidance Counselor';
                $checklist['checklist_type'] = '1';
                $checklist['checklist_code'] = 'fresh-9';
                $checklist['student_id'] = $student_id;
                $this->db->insert('sisfu_checklist', $checklist);

#CHECKLIST FOR TRANSFEREE
                $checklist['checklist_name'] = '500-word essay';
                $checklist['checklist_type'] = '2';
                $checklist['checklist_code'] = 'trans-1';
                $checklist['student_id'] = $student_id;
                $this->db->insert('sisfu_checklist', $checklist);

                $checklist['checklist_name'] = 'Certificate of Honorable Dismissal';
                $checklist['checklist_type'] = '2';
                $checklist['checklist_code'] = 'trans-2';
                $checklist['student_id'] = $student_id;
                $this->db->insert('sisfu_checklist', $checklist);

                $checklist['checklist_name'] = 'Copy of birth certificate and/or passport';
                $checklist['checklist_type'] = '2';
                $checklist['checklist_code'] = 'trans-3';
                $checklist['student_id'] = $student_id;
                $this->db->insert('sisfu_checklist', $checklist);

                $checklist['checklist_name'] = 'Course Description';
                $checklist['checklist_type'] = '2';
                $checklist['checklist_code'] = 'trans-4';
                $checklist['student_id'] = $student_id;
                $this->db->insert('sisfu_checklist', $checklist);

                $checklist['checklist_name'] = 'Four (4) 2&quot; x 2&quot; pictures';
                $checklist['checklist_type'] = '2';
                $checklist['checklist_code'] = 'trans-5';
                $checklist['student_id'] = $student_id;
                $this->db->insert('sisfu_checklist', $checklist);

                $checklist['checklist_name'] = 'Official Transcript of Secondary Education (F137)';
                $checklist['checklist_type'] = '2';
                $checklist['checklist_code'] = 'trans-6';
                $checklist['student_id'] = $student_id;
                $this->db->insert('sisfu_checklist', $checklist);

                $checklist['checklist_name'] = 'Payment of admission and application fees';
                $checklist['checklist_type'] = '2';
                $checklist['checklist_code'] = 'trans-7';
                $checklist['student_id'] = $student_id;
                $this->db->insert('sisfu_checklist', $checklist);

                $checklist['checklist_name'] = 'Recommendation Letter from a Professor or Teacher';
                $checklist['checklist_type'] = '2';
                $checklist['checklist_code'] = 'trans-8';
                $checklist['student_id'] = $student_id;
                $this->db->insert('sisfu_checklist', $checklist);

                $checklist['checklist_name'] = 'Recommendation Letter from School Principal or Guidance Counselor';
                $checklist['checklist_type'] = '2';
                $checklist['checklist_code'] = 'trans-9';
                $checklist['student_id'] = $student_id;
                $this->db->insert('sisfu_checklist', $checklist);

#CHECKLIST FOR FOREIGN STUDENT
                $checklist['checklist_name'] = '500-word essay';
                $checklist['checklist_type'] = '3';
                $checklist['checklist_code'] = 'for-1';
                $checklist['student_id'] = $student_id;
                $this->db->insert('sisfu_checklist', $checklist);

                $checklist['checklist_name'] = 'Copy of Alien Certificate or Registration(ACR)';
                $checklist['checklist_type'] = '3';
                $checklist['checklist_code'] = 'for-2';
                $checklist['student_id'] = $student_id;
                $this->db->insert('sisfu_checklist', $checklist);

                $checklist['checklist_name'] = 'Copy of birth certificate and/or passport';
                $checklist['checklist_type'] = '3';
                $checklist['checklist_code'] = 'for-3';
                $checklist['student_id'] = $student_id;
                $this->db->insert('sisfu_checklist', $checklist);

                $checklist['checklist_name'] = 'Copy of Student Visa and/or Special Study Permit';
                $checklist['checklist_type'] = '3';
                $checklist['checklist_code'] = 'for-4';
                $checklist['student_id'] = $student_id;
                $this->db->insert('sisfu_checklist', $checklist);

                $checklist['checklist_name'] = 'Four (4) 2&quot; x 2&quot; pictures';
                $checklist['checklist_type'] = '3';
                $checklist['checklist_code'] = 'for-5';
                $checklist['student_id'] = $student_id;
                $this->db->insert('sisfu_checklist', $checklist);

                $checklist['checklist_name'] = 'High School Diploma';
                $checklist['checklist_type'] = '3';
                $checklist['checklist_code'] = 'for-6';
                $checklist['student_id'] = $student_id;
                $this->db->insert('sisfu_checklist', $checklist);

                $checklist['checklist_name'] = 'High School Report Card (F138)';
                $checklist['checklist_type'] = '3';
                $checklist['checklist_code'] = 'for-7';
                $checklist['student_id'] = $student_id;
                $this->db->insert('sisfu_checklist', $checklist);

                $checklist['checklist_name'] = 'Official Transcript of Secondary Education (F137)';
                $checklist['checklist_type'] = '3';
                $checklist['checklist_code'] = 'for-8';
                $checklist['student_id'] = $student_id;
                $this->db->insert('sisfu_checklist', $checklist);

                $checklist['checklist_name'] = 'Payment of admission and application fees';
                $checklist['checklist_type'] = '3';
                $checklist['checklist_code'] = 'for-9';
                $checklist['student_id'] = $student_id;
                $this->db->insert('sisfu_checklist', $checklist);

                $checklist['checklist_name'] = 'Recommendation Letter from a Professor or Teacher';
                $checklist['checklist_type'] = '3';
                $checklist['checklist_code'] = 'for-10';
                $checklist['student_id'] = $student_id;
                $this->db->insert('sisfu_checklist', $checklist);

                $checklist['checklist_name'] = 'Recommendation Letter from School Principal or Guidance Counselor';
                $checklist['checklist_type'] = '3';
                $checklist['checklist_code'] = 'for-11';
                $checklist['student_id'] = $student_id;
                $this->db->insert('sisfu_checklist', $checklist);

#CHECKLIST FOR GRADUATE SCHOOL
                $checklist['checklist_name'] = 'Copy of birth certificate and/or passport';
                $checklist['checklist_type'] = '4';
                $checklist['checklist_code'] = 'grad-1';
                $checklist['student_id'] = $student_id;
                $this->db->insert('sisfu_checklist', $checklist);

                $checklist['checklist_name'] = 'Certificate of Honorable Dismissal';
                $checklist['checklist_type'] = '4';
                $checklist['checklist_code'] = 'grad-2';
                $checklist['student_id'] = $student_id;
                $this->db->insert('sisfu_checklist', $checklist);

                $checklist['checklist_name'] = 'Four (4) 2&quot; x 2&quot; pictures';
                $checklist['checklist_type'] = '4';
                $checklist['checklist_code'] = 'grad-3';
                $checklist['student_id'] = $student_id;
                $this->db->insert('sisfu_checklist', $checklist);

                $checklist['checklist_name'] = 'Copy of Student Visa and/or Special Study Permit';
                $checklist['checklist_type'] = '4';
                $checklist['checklist_code'] = 'grad-4';
                $checklist['student_id'] = $student_id;
                $this->db->insert('sisfu_checklist', $checklist);

                $checklist['checklist_name'] = 'Copy of Alien Certificate or Registration(ACR)';
                $checklist['checklist_type'] = '4';
                $checklist['checklist_code'] = 'grad-5';
                $checklist['student_id'] = $student_id;
                $this->db->insert('sisfu_checklist', $checklist);

                $checklist['checklist_name'] = 'Payment of admission and application fees';
                $checklist['checklist_type'] = '4';
                $checklist['checklist_code'] = 'grad-6';
                $checklist['student_id'] = $student_id;
                $this->db->insert('sisfu_checklist', $checklist);

                $checklist['checklist_name'] = 'Recommendation Letters from Employer and Immediate Supervisor';
                $checklist['checklist_type'] = '4';
                $checklist['checklist_code'] = 'grad-7';
                $checklist['student_id'] = $student_id;
                $this->db->insert('sisfu_checklist', $checklist);

                $checklist['checklist_name'] = 'Complete Curriculum Vitae / Resume';
                $checklist['checklist_type'] = '4';
                $checklist['checklist_code'] = 'grad-8';
                $checklist['student_id'] = $student_id;
                $this->db->insert('sisfu_checklist', $checklist);

                $checklist['checklist_name'] = 'Enrollment Agreement';
                $checklist['checklist_type'] = '4';
                $checklist['checklist_code'] = 'grad-9';
                $checklist['student_id'] = $student_id;
                $this->db->insert('sisfu_checklist', $checklist);

                $checklist['checklist_name'] = 'Completed & Signed Application for Admission';
                $checklist['checklist_type'] = '4';
                $checklist['checklist_code'] = 'grad-10';
                $checklist['student_id'] = $student_id;
                $this->db->insert('sisfu_checklist', $checklist);

                $checklist['checklist_name'] = 'A copy of Bachelor & Master Diplomas with English Translation';
                $checklist['checklist_type'] = '4';
                $checklist['checklist_code'] = 'grad-11';
                $checklist['student_id'] = $student_id;
                $this->db->insert('sisfu_checklist', $checklist);

                $checklist['checklist_name'] = 'A copy of Bachelor & Master Transcripts with English Translation';
                $checklist['checklist_type'] = '4';
                $checklist['checklist_code'] = 'grad-12';
                $checklist['student_id'] = $student_id;
                $this->db->insert('sisfu_checklist', $checklist);

#CHECKLIST FOR FOREIGN / TRANSFEREE
                $checklist['checklist_name'] = '500-word essay';
                $checklist['checklist_type'] = '5';
                $checklist['checklist_code'] = 'tf-1';
                $checklist['student_id'] = $student_id;
                $this->db->insert('sisfu_checklist', $checklist);

                $checklist['checklist_name'] = 'Certificate of Honorable Dismissal';
                $checklist['checklist_type'] = '5';
                $checklist['checklist_code'] = 'tf-2';
                $checklist['student_id'] = $student_id;
                $this->db->insert('sisfu_checklist', $checklist);

                $checklist['checklist_name'] = 'Copy of birth certificate and/or passport';
                $checklist['checklist_type'] = '5';
                $checklist['checklist_code'] = 'tf-3';
                $checklist['student_id'] = $student_id;
                $this->db->insert('sisfu_checklist', $checklist);

                $checklist['checklist_name'] = 'Course Description';
                $checklist['checklist_type'] = '5';
                $checklist['checklist_code'] = 'tf-4';
                $checklist['student_id'] = $student_id;
                $this->db->insert('sisfu_checklist', $checklist);

                $checklist['checklist_name'] = 'Four (4) 2&quot; x 2&quot; pictures';
                $checklist['checklist_type'] = '5';
                $checklist['checklist_code'] = 'tf-5';
                $checklist['student_id'] = $student_id;
                $this->db->insert('sisfu_checklist', $checklist);

                $checklist['checklist_name'] = 'Official Transcript of Secondary Education (F137)';
                $checklist['checklist_type'] = '5';
                $checklist['checklist_code'] = 'tf-6';
                $checklist['student_id'] = $student_id;
                $this->db->insert('sisfu_checklist', $checklist);

                $checklist['checklist_name'] = 'Payment of admission and application fees';
                $checklist['checklist_type'] = '5';
                $checklist['checklist_code'] = 'tf-7';
                $checklist['student_id'] = $student_id;
                $this->db->insert('sisfu_checklist', $checklist);

                $checklist['checklist_name'] = 'Recommendation Letter from a Professor or Teacher';
                $checklist['checklist_type'] = '5';
                $checklist['checklist_code'] = 'tf-8';
                $checklist['student_id'] = $student_id;
                $this->db->insert('sisfu_checklist', $checklist);

                $checklist['checklist_name'] = 'Recommendation Letter from School Principal or Guidance Counselor';
                $checklist['checklist_type'] = '5';
                $checklist['checklist_code'] = 'tf-9';
                $checklist['student_id'] = $student_id;
                $this->db->insert('sisfu_checklist', $checklist);

                $checklist['checklist_name'] = 'Copy of Alien Certification or Registration (ACR)';
                $checklist['checklist_type'] = '5';
                $checklist['checklist_code'] = 'tf-10';
                $checklist['student_id'] = $student_id;
                $this->db->insert('sisfu_checklist', $checklist);

                $checklist['checklist_name'] = 'Copy of Student Visa and/or Special Study Permit';
                $checklist['checklist_type'] = '5';
                $checklist['checklist_code'] = 'tf-11';
                $checklist['student_id'] = $student_id;
                $this->db->insert('sisfu_checklist', $checklist);

                $checklist['checklist_name'] = 'High School Diploma';
                $checklist['checklist_type'] = '5';
                $checklist['checklist_code'] = 'tf-12';
                $checklist['student_id'] = $student_id;
                $this->db->insert('sisfu_checklist', $checklist);

                $checklist['checklist_name'] = 'High Report Card (F138)';
                $checklist['checklist_type'] = '5';
                $checklist['checklist_code'] = 'tf-13';
                $checklist['student_id'] = $student_id;
                $this->db->insert('sisfu_checklist', $checklist);
#END FOREIGN / TRANSFEREE
            }




//            $num_of_subjs = $this->input->post('subject_count');
//
//            $subjs = '';
//
//            for ($i = 1; $i <= $num_of_subjs; $i++):
//                $h = 'hidden_subj_id-' . $i;
//                $subjs .= $this->input->post($h) . ",";
//            endfor;
//
//            $subjects = rtrim($subjs, ",");
//
//            $data['enrolled_subjects'] = $subjects;
//
//            $this->db->where('student_id', $param2);
//            $this->db->update('sisfu_students', $data);
//
//
//            $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
//            redirect(base_url() . 'index.php?admin/student_assess/' . $param2, 'refresh');
        }
        if ($param1 == 'delete') {
            $this->db->where('student_id', $param2);
            $this->db->delete('sisfu_students');
            $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
            redirect(base_url() . 'index.php?admin/student/', 'refresh');
        }
        if ($param1 == 'transcript_save') {

            $r_length = $this->input->post('r_length');
            for ($i = 0; $i < $r_length; $i++) {
                $this->db->start_cache();
                $this->db->where('grade_id', $this->input->post('grade_id' . $i));
                $grade['grade_alpha'] = $this->input->post('grade_alpha' . $i);
                $grade['subject_code'] = $this->input->post('new_subject' . $i);
                $grade['posted_by'] = $_SESSION['username'];
                $grade['post_date'] = date('Y-m-d');
                $this->db->update('sisfu_grades', $grade);
                $this->db->stop_cache();
                $this->db->flush_cache();
            }

            $this->session->set_flashdata('flash_message', get_phrase('grade_updated'));
            redirect(base_url() . 'index.php?admin/student_transcript/' . $param2, 'refresh');
        }
        if ($param1 == 'transcript_add') {
            $data['acad_year'] = $param4;
            $data['term_code'] = $param3;
            $data['student_no'] = $this->db->get_where('sisfu_students', array('student_id' => $param2))->row()->student_no;
            $data['subject_code'] = $this->input->post('subject_code');
            $data['grade_alpha'] = 'NGY';
            $data['posted_by'] = $_SESSION['username'];
            $data['post_date'] = date('Y-m-d');

            $this->db->insert('sisfu_grades', $data);

            $this->session->set_flashdata('flash_message', get_phrase('grade_updated'));
            redirect(base_url() . 'index.php?admin/student_transcript/' . $param2, 'refresh');
        }
//        if ($param1 == 'update_grades') {
//
//            $r_length = $this->input->post('r_length');
//            for ($i = 0; $i < $r_length; $i++) {
//                $this->db->start_cache();
//                $this->db->where('grade_id', $this->input->post('grade_id' . $i));
//                $grade['grade_alpha'] = $this->input->post('grade_alpha' . $i);
//                $this->db->update('sisfu_grades', $grade);
//                $this->db->stop_cache();
//                $this->db->flush_cache();
//            }
//
//            $this->session->set_flashdata('flash_message', get_phrase('grade_updated'));
//            redirect(base_url() . 'index.php?admin/student_transcript/' . $param2, 'refresh');
//        }
        if ($param1 == 'delete_grade') {
            $this->db->where('grade_id', $param3);
            $this->db->delete('sisfu_grades');
            $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
            redirect(base_url() . 'index.php?admin/student_transcript/' . $param2, 'refresh');
        }
//        if ($param1 == 'replace_grade') {
//            $data['subject_code'] = $_SESSION['new_subject'];
//            $data['posted_by'] = $_SESSION['username'];
//            $data['post_date'] = date('Y-m-d');
//
//            $this->db->where('grade_id', $param3);
//            $this->db->update('sisfu_grades', $data);
//            $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
//            redirect(base_url() . 'index.php?admin/student_transcript/' . $param2, 'refresh');
//        }
        $page_data['student'] = $this->Student_model->get_all_students();
        $page_data['page_name'] = 'student';
        $page_data['page_title'] = get_phrase('manage_students');
        $this->load->view('backend/index', $page_data);
    }

    /*     * **MANAGE PARENTS CLASSWISE**** */

    function parent($param1 = '', $param2 = '', $param3 = '')
    {
    if($this->  session->userdata('admin_login') != 1)
        redirect('login', 'refresh');
    if ($param1 == 'create') {
        $data['name'] = $this->input->post('name');
        $data['email'] = $this->input->post('email');
        $data['password'] = $this->input->post('password');
        $data['phone'] = $this->input->post('phone');
        $data['address'] = $this->input->post('address');
        $data['profession'] = $this->input->post('profession');
        $this->db->insert('parent', $data);
        $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
        $this->email_model->account_opening_email('parent', $data['email']); //SEND EMAIL ACCOUNT OPENING EMAIL
        redirect(base_url() . 'index.php?admin/parent/', 'refresh');
    }
    if ($param1 == 'edit') {
        $data['name'] = $this->input->post('name');
        $data['email'] = $this->input->post('email');
        $data['phone'] = $this->input->post('phone');
        $data['address'] = $this->input->post('address');
        $data['profession'] = $this->input->post('profession');
        $this->db->where('parent_id', $param2);
        $this->db->update('parent', $data);
        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/parent/', 'refresh');
    }
    if ($param1 == 'delete') {
        $this->db->where('parent_id', $param2);
        $this->db->delete('parent');
        $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
        redirect(base_url() . 'index.php?admin/parent/', 'refresh');
    }
    $page_data['page_title'] = get_phrase('all_parents');
    $page_data['page_name'] = 'parent';
    $this->load->view('backend/index', $page_data);
}

/* * **MANAGE TEACHERS**** */

function teacher($param1 = '', $param2 = '', $param3 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');
    if ($param1 == 'create') {
        $data['name'] = $this->input->post('name');
        $data['birthday'] = $this->input->post('birthday');
        $data['sex'] = $this->input->post('sex');
        $data['address'] = $this->input->post('address');
        $data['phone'] = $this->input->post('phone');
        $data['email'] = $this->input->post('email');
        $data['password'] = $this->input->post('password');
        $this->db->insert('teacher', $data);
        $teacher_id = $this->db->insert_id();
        move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/teacher_image/' . $teacher_id . '.jpg');
        $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
        $this->email_model->account_opening_email('teacher', $data['email']); //SEND EMAIL ACCOUNT OPENING EMAIL
        redirect(base_url() . 'index.php?admin/teacher/', 'refresh');
    }
    if ($param1 == 'do_update') {
        $data['name'] = $this->input->post('name');
        $data['birthday'] = $this->input->post('birthday');
        $data['sex'] = $this->input->post('sex');
        $data['address'] = $this->input->post('address');
        $data['phone'] = $this->input->post('phone');
        $data['email'] = $this->input->post('email');

        $this->db->where('teacher_id', $param2);
        $this->db->update('teacher', $data);
        move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/teacher_image/' . $param2 . '.jpg');
        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/teacher/', 'refresh');
    } else if ($param1 == 'personal_profile') {
        $page_data['personal_profile'] = true;
        $page_data['current_teacher_id'] = $param2;
    } else if ($param1 == 'edit') {
        $page_data['edit_data'] = $this->db->get_where('teacher', array(
                    'teacher_id' => $param2
                ))->result_array();
    }
    if ($param1 == 'delete') {
        $this->db->where('teacher_id', $param2);
        $this->db->delete('teacher');
        $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
        redirect(base_url() . 'index.php?admin/teacher/', 'refresh');
    }
    $page_data['teachers'] = $this->db->get('teacher')->result_array();
    $page_data['page_name'] = 'teacher';
    $page_data['page_title'] = get_phrase('manage_teacher');
    $this->load->view('backend/index', $page_data);
}

/* * **MANAGE SUBJECTS**** */

//
//function subject($param1 = '', $param2 = '', $param3 = '') {
//    if ($this->session->userdata('admin_login') != 1)
//        redirect(base_url(), 'refresh');
//    if ($param1 == 'create') {
//        $data['name'] = $this->input->post('name');
//        $data['class_id'] = $this->input->post('class_id');
//        $data['teacher_id'] = $this->input->post('teacher_id');
//        $this->db->insert('subject', $data);
//        $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
//        redirect(base_url() . 'index.php?admin/subject/' . $data['class_id'], 'refresh');
//    }
//    if ($param1 == 'do_update') {
//        $data['name'] = $this->input->post('name');
//        $data['class_id'] = $this->input->post('class_id');
//        $data['teacher_id'] = $this->input->post('teacher_id');
//
//        $this->db->where('subject_id', $param2);
//        $this->db->update('subject', $data);
//        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
//        redirect(base_url() . 'index.php?admin/subject/' . $data['class_id'], 'refresh');
//    } else if ($param1 == 'edit') {
//        $page_data['edit_data'] = $this->db->get_where('subject', array(
//                    'subject_id' => $param2
//                ))->result_array();
//    }
//    if ($param1 == 'delete') {
//        $this->db->where('subject_id', $param2);
//        $this->db->delete('subject');
//        $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
//        redirect(base_url() . 'index.php?admin/subject/' . $param3, 'refresh');
//    }
//    $page_data['class_id'] = $param1;
//    $page_data['subjects'] = $this->db->get_where('subject', array('class_id' => $param1))->result_array();
//    $page_data['page_name'] = 'subject';
//    $page_data['page_title'] = get_phrase('manage_subject');
//    $this->load->view('backend/index', $page_data);
//}

/* * **MANAGE CLASSES**** */

function classes($param1 = '', $param2 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');
    if ($param1 == 'create') {
        $data['name'] = $this->input->post('name');
        $data['name_numeric'] = $this->input->post('name_numeric');
        $data['teacher_id'] = $this->input->post('teacher_id');
        $this->db->insert('class', $data);
        $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
        redirect(base_url() . 'index.php?admin/classes/', 'refresh');
    }
    if ($param1 == 'do_update') {
        $data['name'] = $this->input->post('name');
        $data['name_numeric'] = $this->input->post('name_numeric');
        $data['teacher_id'] = $this->input->post('teacher_id');

        $this->db->where('class_id', $param2);
        $this->db->update('class', $data);
        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/classes/', 'refresh');
    } else if ($param1 == 'edit') {
        $page_data['edit_data'] = $this->db->get_where('class', array(
                    'class_id' => $param2
                ))->result_array();
    }
    if ($param1 == 'delete') {
        $this->db->where('class_id', $param2);
        $this->db->delete('class');
        $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
        redirect(base_url() . 'index.php?admin/classes/', 'refresh');
    }
    $page_data['classes'] = $this->db->get('class')->result_array();
    $page_data['page_name'] = 'class';
    $page_data['page_title'] = get_phrase('manage_class');
    $this->load->view('backend/index', $page_data);
}

/* * **MANAGE SECTIONS**** */

function section($class_id = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');
// detect the first class
    if ($class_id == '')
        $class_id = $this->db->get('class')->first_row()->class_id;

    $page_data['page_name'] = 'section';
    $page_data['page_title'] = get_phrase('manage_sections');
    $page_data['class_id'] = $class_id;
    $this->load->view('backend/index', $page_data);
}

function sections($param1 = '', $param2 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');
    if ($param1 == 'create') {
        $data['name'] = $this->input->post('name');
        $data['nick_name'] = $this->input->post('nick_name');
        $data['class_id'] = $this->input->post('class_id');
        $data['teacher_id'] = $this->input->post('teacher_id');
        $this->db->insert('section', $data);
        $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
        redirect(base_url() . 'index.php?admin/section/' . $data['class_id'], 'refresh');
    }

    if ($param1 == 'edit') {
        $data['name'] = $this->input->post('name');
        $data['nick_name'] = $this->input->post('nick_name');
        $data['class_id'] = $this->input->post('class_id');
        $data['teacher_id'] = $this->input->post('teacher_id');
        $this->db->where('section_id', $param2);
        $this->db->update('section', $data);
        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/section/' . $data['class_id'], 'refresh');
    }

    if ($param1 == 'delete') {
        $this->db->where('section_id', $param2);
        $this->db->delete('section');
        $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
        redirect(base_url() . 'index.php?admin/section', 'refresh');
    }
}

function get_class_section($class_id) {
    $sections = $this->db->get_where('section', array(
                'class_id' => $class_id
            ))->result_array();
    foreach ($sections as $row) {
        echo '<option value="' . $row['section_id'] . '">' . $row['name'] . '</option>';
    }
}

function get_class_subject($class_id) {
    $subjects = $this->db->get_where('subject', array(
                'class_id' => $class_id
            ))->result_array();
    foreach ($subjects as $row) {
        echo '<option value="' . $row['subject_id'] . '">' . $row['name'] . '</option>';
    }
}

/* * **MANAGE EXAMS**** */

function exam($param1 = '', $param2 = '', $param3 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');
    if ($param1 == 'create') {
        $data['name'] = $this->input->post('name');
        $data['date'] = $this->input->post('date');
        $data['comment'] = $this->input->post('comment');
        $this->db->insert('exam', $data);
        $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
        redirect(base_url() . 'index.php?admin/exam/', 'refresh');
    }
    if ($param1 == 'edit' && $param2 == 'do_update') {
        $data['name'] = $this->input->post('name');
        $data['date'] = $this->input->post('date');
        $data['comment'] = $this->input->post('comment');

        $this->db->where('exam_id', $param3);
        $this->db->update('exam', $data);
        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/exam/', 'refresh');
    } else if ($param1 == 'edit') {
        $page_data['edit_data'] = $this->db->get_where('exam', array(
                    'exam_id' => $param2
                ))->result_array();
    }
    if ($param1 == 'delete') {
        $this->db->where('exam_id', $param2);
        $this->db->delete('exam');
        $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
        redirect(base_url() . 'index.php?admin/exam/', 'refresh');
    }
    $page_data['exams'] = $this->db->get('exam')->result_array();
    $page_data['page_name'] = 'exam';
    $page_data['page_title'] = get_phrase('manage_exam');
    $this->load->view('backend/index', $page_data);
}

/* * **** SEND EXAM MARKS VIA SMS ******* */

function exam_marks_sms($param1 = '', $param2 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');

    if ($param1 == 'send_sms') {

        $exam_id = $this->input->post('exam_id');
        $class_id = $this->input->post('class_id');
        $receiver = $this->input->post('receiver');

// get all the students of the selected class
        $students = $this->db->get_where('student', array(
                    'class_id' => $class_id
                ))->result_array();
// get the marks of the student for selected exam
        foreach ($students as $row) {
            if ($receiver == 'student')
                $receiver_phone = $row['phone'];
            if ($receiver == 'parent' && $row['parent_id'] != '')
                $receiver_phone = $this->db->get_where('parent', array('parent_id' => $row['parent_id']))->row()->phone;


            $this->db->where('exam_id', $exam_id);
            $this->db->where('student_id', $row['student_id']);
            $marks = $this->db->get('mark')->result_array();
            $message = '';
            foreach ($marks as $row2) {
                $subject = $this->db->get_where('subject', array('subject_id' => $row2['subject_id']))->row()->name;
                $mark_obtained = $row2['mark_obtained'];
                $message .= $row2['student_id'] . $subject . ' : ' . $mark_obtained . ' , ';
            }
// send sms
            $this->sms_model->send_sms($message, $receiver_phone);
        }
        $this->session->set_flashdata('flash_message', get_phrase('message_sent'));
        redirect(base_url() . 'index.php?admin/exam_marks_sms', 'refresh');
    }

    $page_data['page_name'] = 'exam_marks_sms';
    $page_data['page_title'] = get_phrase('send_marks_by_sms');
    $this->load->view('backend/index', $page_data);
}

/* * **MANAGE EXAM MARKS**** */

function marks($exam_id = '', $class_id = '', $subject_id = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');

    if ($this->input->post('operation') == 'selection') {
        $page_data['exam_id'] = $this->input->post('exam_id');
        $page_data['class_id'] = $this->input->post('class_id');
        $page_data['subject_id'] = $this->input->post('subject_id');

        if ($page_data['exam_id'] > 0 && $page_data['class_id'] > 0 && $page_data['subject_id'] > 0) {
            redirect(base_url() . 'index.php?admin/marks/' . $page_data['exam_id'] . '/' . $page_data['class_id'] . '/' . $page_data['subject_id'], 'refresh');
        } else {
            $this->session->set_flashdata('mark_message', 'Choose exam, class and subject');
            redirect(base_url() . 'index.php?admin/marks/', 'refresh');
        }
    }
    if ($this->input->post('operation') == 'update') {
        $data['mark_obtained'] = $this->input->post('mark_obtained');
        $data['comment'] = $this->input->post('comment');

        $this->db->where('mark_id', $this->input->post('mark_id'));
        $this->db->update('mark', $data);
        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/marks/' . $this->input->post('exam_id') . '/' . $this->input->post('class_id') . '/' . $this->input->post('subject_id'), 'refresh');
    }
    $page_data['exam_id'] = $exam_id;
    $page_data['class_id'] = $class_id;
    $page_data['subject_id'] = $subject_id;

    $page_data['page_info'] = 'Exam marks';

    $page_data['page_name'] = 'marks';
    $page_data['page_title'] = get_phrase('manage_exam_marks');
    $this->load->view('backend/index', $page_data);
}

/* * **MANAGE GRADES**** */

function grade($param1 = '', $param2 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');
    if ($param1 == 'create') {
        $data['name'] = $this->input->post('name');
        $data['grade_point'] = $this->input->post('grade_point');
        $data['mark_from'] = $this->input->post('mark_from');
        $data['mark_upto'] = $this->input->post('mark_upto');
        $data['comment'] = $this->input->post('comment');
        $this->db->insert('grade', $data);
        $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
        redirect(base_url() . 'index.php?admin/grade/', 'refresh');
    }
    if ($param1 == 'do_update') {
        $data['name'] = $this->input->post('name');
        $data['grade_point'] = $this->input->post('grade_point');
        $data['mark_from'] = $this->input->post('mark_from');
        $data['mark_upto'] = $this->input->post('mark_upto');
        $data['comment'] = $this->input->post('comment');

        $this->db->where('grade_id', $param2);
        $this->db->update('grade', $data);
        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/grade/', 'refresh');
    } else if ($param1 == 'edit') {
        $page_data['edit_data'] = $this->db->get_where('grade', array(
                    'grade_id' => $param2
                ))->result_array();
    }
    if ($param1 == 'delete') {
        $this->db->where('grade_id', $param2);
        $this->db->delete('grade');
        $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
        redirect(base_url() . 'index.php?admin/grade/', 'refresh');
    }
    $page_data['grades'] = $this->db->get('grade')->result_array();
    $page_data['page_name'] = 'grade';
    $page_data['page_title'] = get_phrase('manage_grade');
    $this->load->view('backend/index', $page_data);
}

/* * ********MANAGING CLASS ROUTINE***************** */

function class_routine($param1 = '', $param2 = '', $param3 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');
    if ($param1 == 'create') {
        $data['class_id'] = $this->input->post('class_id');
        $data['subject_id'] = $this->input->post('subject_id');
        $data['time_start'] = $this->input->post('time_start') + (12 * ($this->input->post('starting_ampm') - 1));
        $data['time_end'] = $this->input->post('time_end') + (12 * ($this->input->post('ending_ampm') - 1));
        $data['day'] = $this->input->post('day');
        $this->db->insert('class_routine', $data);
        $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
        redirect(base_url() . 'index.php?admin/class_routine/', 'refresh');
    }
    if ($param1 == 'do_update') {
        $data['class_id'] = $this->input->post('class_id');
        $data['subject_id'] = $this->input->post('subject_id');
        $data['time_start'] = $this->input->post('time_start') + (12 * ($this->input->post('starting_ampm') - 1));
        $data['time_end'] = $this->input->post('time_end') + (12 * ($this->input->post('ending_ampm') - 1));
        $data['day'] = $this->input->post('day');

        $this->db->where('class_routine_id', $param2);
        $this->db->update('class_routine', $data);
        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/class_routine/', 'refresh');
    } else if ($param1 == 'edit') {
        $page_data['edit_data'] = $this->db->get_where('class_routine', array(
                    'class_routine_id' => $param2
                ))->result_array();
    }
    if ($param1 == 'delete') {
        $this->db->where('class_routine_id', $param2);
        $this->db->delete('class_routine');
        $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
        redirect(base_url() . 'index.php?admin/class_routine/', 'refresh');
    }
    $page_data['page_name'] = 'class_routine';
    $page_data['page_title'] = get_phrase('manage_class_routine');
    $this->load->view('backend/index', $page_data);
}

/* * **** DAILY ATTENDANCE **************** */

function manage_attendance($date = '', $month = '', $year = '', $class_id = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect('login', 'refresh');

    if ($_POST) {
// Loop all the students of $class_id
        $students = $this->db->get_where('student', array('class_id' => $class_id))->result_array();
        foreach ($students as $row) {
            $attendance_status = $this->input->post('status_' . $row['student_id']);

            $this->db->where('student_id', $row['student_id']);
            $this->db->where('date', $this->input->post('date'));

            $this->db->update('attendance', array('status' => $attendance_status));
        }

        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/manage_attendance/' . $date . '/' . $month . '/' . $year . '/' . $class_id, 'refresh');
    }
    $page_data['date'] = $date;
    $page_data['month'] = $month;
    $page_data['year'] = $year;
    $page_data['class_id'] = $class_id;

    $page_data['page_name'] = 'manage_attendance';
    $page_data['page_title'] = get_phrase('manage_daily_attendance');
    $this->load->view('backend/index', $page_data);
}

function attendance_selector() {
    redirect(base_url() . 'index.php?admin/manage_attendance/' . $this->input->post('date') . '/' .
            $this->input->post('month') . '/' .
            $this->input->post('year') . '/' .
            $this->input->post('class_id'), 'refresh');
}

/* * ****MANAGE BILLING / INVOICES WITH STATUS**** */

function invoice($param1 = '', $param2 = '', $param3 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');

    if ($param1 == 'create') {
        $data['student_id'] = $this->input->post('student_id');
        $data['title'] = $this->input->post('title');
        $data['description'] = $this->input->post('description');
        $data['amount'] = $this->input->post('amount');
        $data['amount_paid'] = $this->input->post('amount_paid');
        $data['due'] = $data['amount'] - $data['amount_paid'];
        $data['status'] = $this->input->post('status');
        $data['creation_timestamp'] = strtotime($this->input->post('date'));

        $this->db->insert('invoice', $data);
        $invoice_id = $this->db->insert_id();

        $data2['invoice_id'] = $invoice_id;
        $data2['student_id'] = $this->input->post('student_id');
        $data2['title'] = $this->input->post('title');
        $data2['description'] = $this->input->post('description');
        $data2['payment_type'] = 'income';
        $data2['method'] = $this->input->post('method');
        $data2['amount'] = $this->input->post('amount_paid');
        $data2['timestamp'] = strtotime($this->input->post('date'));

        $this->db->insert('payment', $data2);

        $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
        redirect(base_url() . 'index.php?admin/invoice', 'refresh');
    }
    if ($param1 == 'do_update') {
        $data['student_id'] = $this->input->post('student_id');
        $data['title'] = $this->input->post('title');
        $data['description'] = $this->input->post('description');
        $data['amount'] = $this->input->post('amount');
        $data['status'] = $this->input->post('status');
        $data['creation_timestamp'] = strtotime($this->input->post('date'));

        $this->db->where('invoice_id', $param2);
        $this->db->update('invoice', $data);
        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/invoice', 'refresh');
    } else if ($param1 == 'edit') {
        $page_data['edit_data'] = $this->db->get_where('invoice', array(
                    'invoice_id' => $param2
                ))->result_array();
    }
    if ($param1 == 'take_payment') {
        $data['invoice_id'] = $this->input->post('invoice_id');
        $data['student_id'] = $this->input->post('student_id');
        $data['title'] = $this->input->post('title');
        $data['description'] = $this->input->post('description');
        $data['payment_type'] = 'income';
        $data['method'] = $this->input->post('method');
        $data['amount'] = $this->input->post('amount');
        $data['timestamp'] = strtotime($this->input->post('timestamp'));
        $this->db->insert('payment', $data);

        $data2['amount_paid'] = $this->input->post('amount');
        $this->db->where('invoice_id', $param2);
        $this->db->set('amount_paid', 'amount_paid + ' . $data2['amount_paid'], FALSE);
        $this->db->set('due', 'due - ' . $data2['amount_paid'], FALSE);
        $this->db->update('invoice');

        $this->session->set_flashdata('flash_message', get_phrase('payment_successfull'));
        redirect(base_url() . 'index.php?admin/invoice', 'refresh');
    }

    if ($param1 == 'delete') {
        $this->db->where('invoice_id', $param2);
        $this->db->delete('invoice');
        $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
        redirect(base_url() . 'index.php?admin/invoice', 'refresh');
    }
    $page_data['page_name'] = 'invoice';
    $page_data['page_title'] = get_phrase('manage_invoice/payment');
    $this->db->order_by('creation_timestamp', 'desc');
    $page_data['invoices'] = $this->db->get('invoice')->result_array();
    $this->load->view('backend/index', $page_data);
}

/* * ********ACCOUNTING******************* */

function income($param1 = '', $param2 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect('login', 'refresh');
    $page_data['page_name'] = 'income';
    $page_data['page_title'] = get_phrase('incomes');
    $this->db->order_by('creation_timestamp', 'desc');
    $page_data['invoices'] = $this->db->get('invoice')->result_array();
    $this->load->view('backend/index', $page_data);
}

function expense($param1 = '', $param2 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect('login', 'refresh');
    if ($param1 == 'create') {
        $data['title'] = $this->input->post('title');
        $data['expense_category_id'] = $this->input->post('expense_category_id');
        $data['description'] = $this->input->post('description');
        $data['payment_type'] = 'expense';
        $data['method'] = $this->input->post('method');
        $data['amount'] = $this->input->post('amount');
        $data['timestamp'] = strtotime($this->input->post('timestamp'));
        $this->db->insert('payment', $data);
        $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
        redirect(base_url() . 'index.php?admin/expense', 'refresh');
    }

    if ($param1 == 'edit') {
        $data['title'] = $this->input->post('title');
        $data['expense_category_id'] = $this->input->post('expense_category_id');
        $data['description'] = $this->input->post('description');
        $data['payment_type'] = 'expense';
        $data['method'] = $this->input->post('method');
        $data['amount'] = $this->input->post('amount');
        $data['timestamp'] = strtotime($this->input->post('timestamp'));
        $this->db->where('payment_id', $param2);
        $this->db->update('payment', $data);
        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/expense', 'refresh');
    }

    if ($param1 == 'delete') {
        $this->db->where('payment_id', $param2);
        $this->db->delete('payment');
        $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
        redirect(base_url() . 'index.php?admin/expense', 'refresh');
    }

    $page_data['page_name'] = 'expense';
    $page_data['page_title'] = get_phrase('expenses');
    $this->load->view('backend/index', $page_data);
}

function expense_category($param1 = '', $param2 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect('login', 'refresh');
    if ($param1 == 'create') {
        $data['name'] = $this->input->post('name');
        $this->db->insert('expense_category', $data);
        $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
        redirect(base_url() . 'index.php?admin/expense_category');
    }
    if ($param1 == 'edit') {
        $data['name'] = $this->input->post('name');
        $this->db->where('expense_category_id', $param2);
        $this->db->update('expense_category', $data);
        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/expense_category');
    }
    if ($param1 == 'delete') {
        $this->db->where('expense_category_id', $param2);
        $this->db->delete('expense_category');
        $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
        redirect(base_url() . 'index.php?admin/expense_category');
    }

    $page_data['page_name'] = 'expense_category';
    $page_data['page_title'] = get_phrase('expense_category');
    $this->load->view('backend/index', $page_data);
}

/* * ********MANAGE LIBRARY / BOOKS******************* */

function book($param1 = '', $param2 = '', $param3 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect('login', 'refresh');
    if ($param1 == 'create') {
        $data['name'] = $this->input->post('name');
        $data['description'] = $this->input->post('description');
        $data['price'] = $this->input->post('price');
        $data['author'] = $this->input->post('author');
        $data['class_id'] = $this->input->post('class_id');
        $data['status'] = $this->input->post('status');
        $this->db->insert('book', $data);
        $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
        redirect(base_url() . 'index.php?admin/book', 'refresh');
    }
    if ($param1 == 'do_update') {
        $data['name'] = $this->input->post('name');
        $data['description'] = $this->input->post('description');
        $data['price'] = $this->input->post('price');
        $data['author'] = $this->input->post('author');
        $data['class_id'] = $this->input->post('class_id');
        $data['status'] = $this->input->post('status');

        $this->db->where('book_id', $param2);
        $this->db->update('book', $data);
        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/book', 'refresh');
    } else if ($param1 == 'edit') {
        $page_data['edit_data'] = $this->db->get_where('book', array(
                    'book_id' => $param2
                ))->result_array();
    }
    if ($param1 == 'delete') {
        $this->db->where('book_id', $param2);
        $this->db->delete('book');
        $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
        redirect(base_url() . 'index.php?admin/book', 'refresh');
    }
    $page_data['books'] = $this->db->get('book')->result_array();
    $page_data['page_name'] = 'book';
    $page_data['page_title'] = get_phrase('manage_library_books');
    $this->load->view('backend/index', $page_data);
}

/* * ********MANAGE TRANSPORT / VEHICLES / ROUTES******************* */

function transport($param1 = '', $param2 = '', $param3 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect('login', 'refresh');
    if ($param1 == 'create') {
        $data['route_name'] = $this->input->post('route_name');
        $data['number_of_vehicle'] = $this->input->post('number_of_vehicle');
        $data['description'] = $this->input->post('description');
        $data['route_fare'] = $this->input->post('route_fare');
        $this->db->insert('transport', $data);
        $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
        redirect(base_url() . 'index.php?admin/transport', 'refresh');
    }
    if ($param1 == 'do_update') {
        $data['route_name'] = $this->input->post('route_name');
        $data['number_of_vehicle'] = $this->input->post('number_of_vehicle');
        $data['description'] = $this->input->post('description');
        $data['route_fare'] = $this->input->post('route_fare');

        $this->db->where('transport_id', $param2);
        $this->db->update('transport', $data);
        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/transport', 'refresh');
    } else if ($param1 == 'edit') {
        $page_data['edit_data'] = $this->db->get_where('transport', array(
                    'transport_id' => $param2
                ))->result_array();
    }
    if ($param1 == 'delete') {
        $this->db->where('transport_id', $param2);
        $this->db->delete('transport');
        $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
        redirect(base_url() . 'index.php?admin/transport', 'refresh');
    }
    $page_data['transports'] = $this->db->get('transport')->result_array();
    $page_data['page_name'] = 'transport';
    $page_data['page_title'] = get_phrase('manage_transport');
    $this->load->view('backend/index', $page_data);
}

/* * ********MANAGE DORMITORY / HOSTELS / ROOMS ******************* */

function dormitory($param1 = '', $param2 = '', $param3 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect('login', 'refresh');
    if ($param1 == 'create') {
        $data['name'] = $this->input->post('name');
        $data['number_of_room'] = $this->input->post('number_of_room');
        $data['description'] = $this->input->post('description');
        $this->db->insert('dormitory', $data);
        $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
        redirect(base_url() . 'index.php?admin/dormitory', 'refresh');
    }
    if ($param1 == 'do_update') {
        $data['name'] = $this->input->post('name');
        $data['number_of_room'] = $this->input->post('number_of_room');
        $data['description'] = $this->input->post('description');

        $this->db->where('dormitory_id', $param2);
        $this->db->update('dormitory', $data);
        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/dormitory', 'refresh');
    } else if ($param1 == 'edit') {
        $page_data['edit_data'] = $this->db->get_where('dormitory', array(
                    'dormitory_id' => $param2
                ))->result_array();
    }
    if ($param1 == 'delete') {
        $this->db->where('dormitory_id', $param2);
        $this->db->delete('dormitory');
        $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
        redirect(base_url() . 'index.php?admin/dormitory', 'refresh');
    }
    $page_data['dormitories'] = $this->db->get('dormitory')->result_array();
    $page_data['page_name'] = 'dormitory';
    $page_data['page_title'] = get_phrase('manage_dormitory');
    $this->load->view('backend/index', $page_data);
}

/* * *MANAGE EVENT / NOTICEBOARD, WILL BE SEEN BY ALL ACCOUNTS DASHBOARD* */

function noticeboard($param1 = '', $param2 = '', $param3 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');

    if ($param1 == 'create') {
        $data['notice_title'] = $this->input->post('notice_title');
        $data['notice'] = $this->input->post('notice');
        $data['create_timestamp'] = strtotime($this->input->post('create_timestamp'));
        $this->db->insert('noticeboard', $data);

        $check_sms_send = $this->input->post('check_sms');

        if ($check_sms_send == 1) {
// sms sending configurations

            $parents = $this->db->get('parent')->result_array();
            $students = $this->db->get('student')->result_array();
            $teachers = $this->db->get('teacher')->result_array();
            $date = $this->input->post('create_timestamp');
            $message = $data['notice_title'] . ' ';
            $message .= get_phrase('on') . ' ' . $date;
            foreach ($parents as $row) {
                $reciever_phone = $row['phone'];
                $this->sms_model->send_sms($message, $reciever_phone);
            }
            foreach ($students as $row) {
                $reciever_phone = $row['phone'];
                $this->sms_model->send_sms($message, $reciever_phone);
            }
            foreach ($teachers as $row) {
                $reciever_phone = $row['phone'];
                $this->sms_model->send_sms($message, $reciever_phone);
            }
        }

        $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
        redirect(base_url() . 'index.php?admin/noticeboard/', 'refresh');
    }
    if ($param1 == 'do_update') {
        $data['notice_title'] = $this->input->post('notice_title');
        $data['notice'] = $this->input->post('notice');
        $data['create_timestamp'] = strtotime($this->input->post('create_timestamp'));
        $this->db->where('notice_id', $param2);
        $this->db->update('noticeboard', $data);

        $check_sms_send = $this->input->post('check_sms');

        if ($check_sms_send == 1) {
// sms sending configurations

            $parents = $this->db->get('parent')->result_array();
            $students = $this->db->get('student')->result_array();
            $teachers = $this->db->get('teacher')->result_array();
            $date = $this->input->post('create_timestamp');
            $message = $data['notice_title'] . ' ';
            $message .= get_phrase('on') . ' ' . $date;
            foreach ($parents as $row) {
                $reciever_phone = $row['phone'];
                $this->sms_model->send_sms($message, $reciever_phone);
            }
            foreach ($students as $row) {
                $reciever_phone = $row['phone'];
                $this->sms_model->send_sms($message, $reciever_phone);
            }
            foreach ($teachers as $row) {
                $reciever_phone = $row['phone'];
                $this->sms_model->send_sms($message, $reciever_phone);
            }
        }

        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/noticeboard/', 'refresh');
    } else if ($param1 == 'edit') {
        $page_data['edit_data'] = $this->db->get_where('noticeboard', array(
                    'notice_id' => $param2
                ))->result_array();
    }
    if ($param1 == 'delete') {
        $this->db->where('notice_id', $param2);
        $this->db->delete('noticeboard');
        $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
        redirect(base_url() . 'index.php?admin/noticeboard/', 'refresh');
    }
    $page_data['page_name'] = 'noticeboard';
    $page_data['page_title'] = get_phrase('manage_noticeboard');
    $page_data['notices'] = $this->db->get('noticeboard')->result_array();
    $this->load->view('backend/index', $page_data);
}

/* private messaging */

function message($param1 = 'message_home', $param2 = '', $param3 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');

    if ($param1 == 'send_new') {
        $message_thread_code = $this->crud_model->send_new_private_message();
        $this->session->set_flashdata('flash_message', get_phrase('message_sent!'));
        redirect(base_url() . 'index.php?admin/message/message_read/' . $message_thread_code, 'refresh');
    }

    if ($param1 == 'send_reply') {
        $this->crud_model->send_reply_message($param2);  //$param2 = message_thread_code
        $this->session->set_flashdata('flash_message', get_phrase('message_sent!'));
        redirect(base_url() . 'index.php?admin/message/message_read/' . $param2, 'refresh');
    }

    if ($param1 == 'message_read') {
        $page_data['current_message_thread_code'] = $param2;  // $param2 = message_thread_code
        $this->crud_model->mark_thread_messages_read($param2);
    }

    $page_data['message_inner_page_name'] = $param1;
    $page_data['page_name'] = 'message';
    $page_data['page_title'] = get_phrase('private_messaging');
    $this->load->view('backend/index', $page_data);
}

/* * ***SITE/SYSTEM SETTINGS******** */

function system_settings($param1 = '', $param2 = '', $param3 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url() . 'index.php?login', 'refresh');

    if ($param1 == 'do_update') {

        $data['description'] = $this->input->post('system_name');
        $this->db->where('type', 'system_name');
        $this->db->update('settings', $data);

        $data['description'] = $this->input->post('system_title');
        $this->db->where('type', 'system_title');
        $this->db->update('settings', $data);

        $data['description'] = $this->input->post('address');
        $this->db->where('type', 'address');
        $this->db->update('settings', $data);

        $data['description'] = $this->input->post('phone');
        $this->db->where('type', 'phone');
        $this->db->update('settings', $data);

        $data['description'] = $this->input->post('paypal_email');
        $this->db->where('type', 'paypal_email');
        $this->db->update('settings', $data);

        $data['description'] = $this->input->post('currency');
        $this->db->where('type', 'currency');
        $this->db->update('settings', $data);

        $data['description'] = $this->input->post('system_email');
        $this->db->where('type', 'system_email');
        $this->db->update('settings', $data);

        $data['description'] = $this->input->post('language');
        $this->db->where('type', 'language');
        $this->db->update('settings', $data);

        $data['description'] = $this->input->post('text_align');
        $this->db->where('type', 'text_align');
        $this->db->update('settings', $data);

        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/system_settings/', 'refresh');
    }
    if ($param1 == 'upload_logo') {
        move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/logo.png');
        $this->session->set_flashdata('flash_message', get_phrase('settings_updated'));
        redirect(base_url() . 'index.php?admin/system_settings/', 'refresh');
    }
    if ($param1 == 'change_skin') {
        $data['description'] = $param2;
        $this->db->where('type', 'skin_colour');
        $this->db->update('settings', $data);
        $this->session->set_flashdata('flash_message', get_phrase('theme_selected'));
        redirect(base_url() . 'index.php?admin/system_settings/', 'refresh');
    }
    if ($param1 == 'do_other') {
        #REF NOS
        $reg['ref_no'] = $this->input->post('reg_no');
        $this->db->where('ref_code', 'REGCARD');
        $this->db->update('sisfu_reference', $reg);

        $ref['ref_no'] = $this->input->post('ref_no');
        $this->db->where('ref_code', 'LOA');
        $this->db->update('sisfu_reference', $ref);

        #SETTINGS
        $data['description'] = $this->input->post('active_term');
        $this->db->where('type', 'active_term');
        $this->db->update('settings', $data);

        $data['description'] = $this->input->post('commencement_date');
        $this->db->where('type', 'commencement_date');
        $this->db->update('settings', $data);

        $data['description'] = $this->input->post('requirements_due');
        $this->db->where('type', 'requirements_due');
        $this->db->update('settings', $data);

        $data['description'] = $this->input->post('orientation_date');
        $this->db->where('type', 'orientation_date');
        $this->db->update('settings', $data);

        $tuition['description'] = $this->input->post('tuition');
        $this->db->where('type', 'tuition');
        $this->db->update('settings', $tuition);

        $last['description'] = $this->input->post('last_no');
        $this->db->where('type', 'last_no');
        $this->db->update('settings', $last);

        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/system_settings/', 'refresh');
    }

    $page_data['page_name'] = 'system_settings';
    $page_data['page_title'] = get_phrase('system_settings');
    $page_data['settings'] = $this->db->get('settings')->result_array();
    $this->load->view('backend/index', $page_data);
}

#CURRICULUM

function curriculum($param1 = '', $param2 = '', $param3 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect('login', 'refresh');

    if ($param1 == 'create') {

        $data['curriculum_name'] = $this->input->post('curriculum_name');
        $data['curriculum_code'] = $this->input->post('curriculum_code');
        $data['curriculum_term'] = $this->input->post('curriculum_term');

        $this->db->insert('sisfu_curriculum', $data);

        $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
        redirect(base_url() . 'index.php?admin/curriculum/', 'refresh');
    }
    if ($param1 == 'edit') {
        $data['curriculum_name'] = $this->input->post('curriculum_name');
        $data['curriculum_code'] = $this->input->post('curriculum_code');
        $data['curriculum_term'] = $this->input->post('curriculum_term');

        $this->db->where('curriculum_id', $param2);
        $this->db->update('sisfu_curriculum', $data);
        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/curriculum/', 'refresh');
    }
    if ($param1 == 'add_subject') {

        $this->db->where('curriculum_id', $param2);
        $result = $this->db->get('sisfu_curriculum')->result_array();
        foreach ($result as $row):
            $subjs = $row['subjects'];
        endforeach;

        $subjs .= "," . $param3;

        $data['subjects'] = $subjs;

        $this->db->where('curriculum_id', $param2);
        $this->db->update('sisfu_curriculum', $data);
        $this->session->set_flashdata('flash_message', get_phrase('subject_added'));
        redirect(base_url() . 'index.php?admin/curriculum/', 'refresh');
    }
    if ($param1 == 'delete') {
        $this->db->where('curriculum_id', $param2);
        $this->db->delete('sisfu_curriculum');
        $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
        redirect(base_url() . 'index.php?admin/curriculum/', 'refresh');
    }
    if ($param1 == 'delete_subj_from') {
        $this->db->where('curriculum_id', $param2);
        $result = $this->db->get('sisfu_curriculum')->result_array();

        foreach ($result as $row) {
            $subjects = split(',', $row['subjects']);
        }

        foreach ($subjects as $innerRow):
            if (($key = array_search($param3, $subjects)) !== false) {
                unset($subjects[$key]);
            }
        endforeach;

        foreach ($subjects as $row) {
            $string .= $row . ",";
        }

        $new_string = rtrim($string, ',');

        $data['subjects'] = $new_string;

        $this->db->where('curriculum_id', $param2);
        $this->db->update('sisfu_curriculum', $data);

        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/curriculum/', 'refresh');
    }

    $page_data['page_name'] = 'curriculum';
    $page_data['page_title'] = get_phrase('manage_curricula');
    $this->load->view('backend/index', $page_data);
}

#ISO FORMS

function forms($param1 = '', $param2 = '', $param3 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect('login', 'refresh');

    $this->load->model('Maintenance_model');

    if ($param1 == 'create') {
        $this->Maintenance_model->add_new_form();

        $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
        redirect(base_url() . 'index.php?admin/forms/', 'refresh');
    }
    if ($param1 == 'edit') {
        $this->Maintenance_model->edit_due($param2);

        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/forms/', 'refresh');
    }
    if ($param1 == 'delete') {
        $this->Maintenance_model->delete_form($param2);

        $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
        redirect(base_url() . 'index.php?admin/forms/', 'refresh');
    }

    $page_data['result'] = $this->Maintenance_model->get_all_forms();
    $page_data['page_name'] = 'forms';
    $page_data['page_title'] = get_phrase('manage_ISO_forms');
    $this->load->view('backend/index', $page_data);
}

#DUES

function dues($param1 = '', $param2 = '', $param3 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect('login', 'refresh');

    $this->load->model('Maintenance_model');

    if ($param1 == 'create') {
        $this->Maintenance_model->add_new_due();

        $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
        redirect(base_url() . 'index.php?admin/dues/', 'refresh');
    }
    if ($param1 == 'edit') {
        $this->Maintenance_model->edit_form($param2);

        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/dues/', 'refresh');
    }
    if ($param1 == 'delete') {
        $this->Maintenance_model->delete_due($param2);

        $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
        redirect(base_url() . 'index.php?admin/dues/', 'refresh');
    }

    $page_data['result'] = $this->Maintenance_model->get_all_dues();
    $page_data['page_name'] = 'dues';
    $page_data['page_title'] = get_phrase('manage_dues');
    $this->load->view('backend/index', $page_data);
}

#NATIONALITIES

function nationalities($param1 = '', $param2 = '', $param3 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect('login', 'refresh');

    if ($param1 == 'create') {
        $data['nationality_name'] = $this->input->post('nationality_name');

        $this->db->insert('sisfu_nationalities_meta', $data);

        $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
        redirect(base_url() . 'index.php?admin/nationalities/', 'refresh');
    }
    if ($param1 == 'edit') {
        $data['nationality_name'] = $this->input->post('nationality_name');

        $this->db->where('nationality_id', $param2);
        $this->db->update('sisfu_nationalities_meta', $data);

        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/nationalities/', 'refresh');
    }
    if ($param1 == 'delete') {
        $this->db->where('nationality_id', $param2);
        $this->db->delete('sisfu_nationalities_meta');
        $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
        redirect(base_url() . 'index.php?admin/nationalities/', 'refresh');
    }

    $page_data['page_name'] = 'nationalities';
    $page_data['page_title'] = get_phrase('manage_nationalities');
    $this->load->view('backend/index', $page_data);
}

#STATUSES

function status($param1 = '', $param2 = '', $param3 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect('login', 'refresh');

    if ($param1 == 'create') {
        $data['status_name'] = $this->input->post('status_name');
        $data['status_category'] = $this->input->post('status_category');

        $this->db->insert('sisfu_status_meta', $data);

        $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
        redirect(base_url() . 'index.php?admin/status/', 'refresh');
    }
    if ($param1 == 'edit') {
        $data['status_name'] = $this->input->post('status_name');
        $data['status_category'] = $this->input->post('status_category');

        $this->db->where('status_id', $param2);
        $this->db->update('sisfu_status_meta', $data);

        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/status/', 'refresh');
    }
    if ($param1 == 'delete') {
        $this->db->where('status_id', $param2);
        $this->db->delete('sisfu_status_meta');
        $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
        redirect(base_url() . 'index.php?admin/status/', 'refresh');
    }

    $page_data['page_name'] = 'status';
    $page_data['page_title'] = get_phrase('manage_statuses');
    $this->load->view('backend/index', $page_data);
}

#SCHOLARSHIPS

function scholarships($param1 = '', $param2 = '', $param3 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect('login', 'refresh');

    if ($param1 == 'create') {
        $data['scholarship_name'] = $this->input->post('scholarship_name');
        $data['discount_value'] = $this->input->post('discount_value');

        $this->db->insert('sisfu_scholarships_meta', $data);

        $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
        redirect(base_url() . 'index.php?admin/scholarships/', 'refresh');
    }
    if ($param1 == 'edit') {
        $data['scholarship_name'] = $this->input->post('scholarship_name');
        $data['discount_value'] = $this->input->post('discount_value');

        $this->db->where('scholarship_id', $param2);
        $this->db->update('sisfu_scholarships_meta', $data);

        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/scholarships/', 'refresh');
    }
    if ($param1 == 'delete') {
        $this->db->where('scholarship_id', $param2);
        $this->db->delete('sisfu_scholarships_meta');
        $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
        redirect(base_url() . 'index.php?admin/scholarships/', 'refresh');
    }

    $page_data['page_name'] = 'scholarships';
    $page_data['page_title'] = get_phrase('manage_scholarships');
    $this->load->view('backend/index', $page_data);
}

#DISCOUNTS

function discounts($param1 = '', $param2 = '', $param3 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect('login', 'refresh');

    if ($param1 == 'create') {
        $data['discount_name'] = $this->input->post('discount_name');
        $data['discount_value'] = $this->input->post('discount_value');

        $this->db->insert('sisfu_discounts_meta', $data);

        $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
        redirect(base_url() . 'index.php?admin/discounts/', 'refresh');
    }
    if ($param1 == 'edit') {
        $data['discount_name'] = $this->input->post('discount_name');
        $data['discount_value'] = $this->input->post('discount_value');

        $this->db->where('discount_id', $param2);
        $this->db->update('sisfu_discounts_meta', $data);

        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/discounts/', 'refresh');
    }
    if ($param1 == 'delete') {
        $this->db->where('discount_id', $param2);
        $this->db->delete('sisfu_discounts_meta');
        $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
        redirect(base_url() . 'index.php?admin/discounts/', 'refresh');
    }

    $page_data['page_name'] = 'discounts';
    $page_data['page_title'] = get_phrase('manage_discounts');
    $this->load->view('backend/index', $page_data);
}

#SCHOOLS

function departments($param1 = '', $param2 = '', $param3 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect('login', 'refresh');

    if ($param1 == 'create') {
        $data['department_name'] = $this->input->post('department_name');
        $data['department_code'] = $this->input->post('department_code');

        $this->db->insert('sisfu_departments', $data);

        $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
        redirect(base_url() . 'index.php?admin/departments/', 'refresh');
    }
    if ($param1 == 'edit') {
        $data['department_name'] = $this->input->post('department_name');
        $data['department_code'] = $this->input->post('department_code');

        $this->db->where('department_id', $param2);
        $this->db->update('sisfu_departments', $data);

        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/departments/', 'refresh');
    }
    if ($param1 == 'delete') {
        $this->db->where('department_id', $param2);
        $this->db->delete('sisfu_departments');
        $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
        redirect(base_url() . 'index.php?admin/departments/', 'refresh');
    }

    $page_data['page_name'] = 'departments';
    $page_data['page_title'] = get_phrase('manage_schools');
    $this->load->view('backend/index', $page_data);
}

#SUBJS

function subjects($param1 = '', $param2 = '', $param3 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect('login', 'refresh');

    if ($param1 == 'create') {

        $data['subject_code'] = $this->input->post('subject_code');
        $data['partner_code'] = $this->input->post('partner_code');
        $data['subject_name'] = $this->input->post('subject_name');
        $data['department_id'] = $this->input->post('department_id');
        $data['units'] = $this->input->post('units');
        $data['unit_level'] = $this->input->post('unit_level');

        $this->db->insert('sisfu_subjects', $data);

        $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
        redirect(base_url() . 'index.php?admin/subjects/', 'refresh');
    }
    if ($param1 == 'edit') {
        $data['subject_code'] = $this->input->post('subject_code');
        $data['partner_code'] = $this->input->post('partner_code');
        $data['subject_name'] = $this->input->post('subject_name');
        $data['department_id'] = $this->input->post('department_id');
        $data['units'] = $this->input->post('units');
        $data['unit_level'] = $this->input->post('unit_level');

        $this->db->where('subject_id', $param2);
        $this->db->update('sisfu_subjects', $data);
        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/subjects/', 'refresh');
    }
    if ($param1 == 'delete') {
        $this->db->where('subject_id', $param2);
        $this->db->delete('sisfu_subjects');
        $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
        redirect(base_url() . 'index.php?admin/subjects/', 'refresh');
    }

    $page_data['page_name'] = 'subjects';
    $page_data['page_title'] = get_phrase('manage_subjects');
    $this->load->view('backend/index', $page_data);
}

#USERS

function users($param1 = '', $param2 = '', $param3 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect('login', 'refresh');

    if ($param1 == 'create') {

        $data['username'] = $this->input->post('username');
        $data['password'] = md5($this->input->post('password'));
        $data['email'] = $this->input->post('email');
        $data['access_level'] = $this->input->post('access_level');

        $data['creation_date'] = date('Y-m-d');
        $data['created_by'] = $_SESSION['username'];

        $this->db->insert('admin', $data);

        $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
        redirect(base_url() . 'index.php?admin/users/' . $data['class_id'], 'refresh');
    }
    if ($param1 == 'edit') {
        $data['username'] = $this->input->post('username');
        $data['password'] = md5($this->input->post('password'));
        $data['email'] = $this->input->post('email');
        $data['access_level'] = $this->input->post('access_level');

        $this->db->where('admin_id', $param2);
        $this->db->update('admin', $data);
        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/users/', 'refresh');
    }
    if ($param1 == 'delete') {
        $this->db->where('admin_id', $param2);
        $this->db->delete('admin');

        $this->db->where('student_id', $param2);
        $this->db->delete('sisfu_parents');

        $this->db->where('student_id', $param2);
        $this->db->delete('sisfu_checklist');

        $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
        redirect(base_url() . 'index.php?admin/users/', 'refresh');
    }

    $page_data['page_name'] = 'users';
    $page_data['page_title'] = get_phrase('manage_users');
    $this->load->view('backend/index', $page_data);
}

#STUDENTS( STUDENTS STUDENTS()
//function students($param1 = '', $param2 = '', $param3 = '') {
//    if ($this->session->userdata('admin_login') != 1)
//        redirect('login', 'refresh');
//    if (false) {
//
////    if ($param1 == 'create') {
////
////        $data['username'] = $this->input->post('username');
////        $data['password'] = md5($this->input->post('password'));
////        $data['email'] = $this->input->post('email');
////        $data['access_level'] = $this->input->post('access_level');
////
////        $data['creation_date'] = date('Y-m-d');
////        $data['created_by'] = $_SESSION['username'];
////
////        $this->db->insert('admin', $data);
////
////        $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
////
////        redirect(base_url() . 'index.php?admin/users/' . $data['class_id'], 'refresh');
////
//////        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
//////        redirect(base_url() . 'index.php?admin/users/', 'refresh');
////    }
////    if ($param1 == 'edit') {
////        $data['name'] = $this->input->post('name');
////        $data['email'] = $this->input->post('email');
////        $data['phone'] = $this->input->post('phone');
////        $data['address'] = $this->input->post('address');
////        $data['profession'] = $this->input->post('profession');
////        $this->db->where('parent_id', $param2);
////        $this->db->update('parent', $data);
////        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
////        redirect(base_url() . 'index.php?admin/parent/', 'refresh');
////    }
////    if ($param1 == 'delete') {
////        $this->db->where('admin_id', $param2);
////        $this->db->delete('admin');
////        $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
////        redirect(base_url() . 'index.php?admin/users/', 'refresh');
////    }
//    }
//
//    $page_data['page_name'] = 'students';
//    $page_data['page_title'] = get_phrase('manage_students');
//    $this->load->view('backend/index', $page_data);
//}
//function user_add($param1 = '', $param2 = '', $param3 = '') {
//    if ($this->session->userdata('admin_login') != 1)
//        redirect('login', 'refresh');
//
//    if ($param1 == 'create') {
//        $data['username'] = $this->input->post('username');
//        $data['password'] = md5($this->input->post('password'));
//        $data['email'] = $this->input->post('email');
//        $data['access_level'] = $this->input->post('access_level');
//
//        $data['creation_date'] = date('Y-m-d');
//        $data['created_by'] = $_SESSION['username'];
//
//        $this->db->insert('admin', $data);
//
//        $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
//
//        redirect(base_url() . 'index.php?admin/add_users/' . $data['class_id'], 'refresh');
//    }
//
//    $page_data['page_name'] = 'user_add';
//    $page_data['page_title'] = get_phrase('add_users');
//    $this->load->view('backend/index', $page_data);
//}

/* * ***SMS SETTINGS******** */

function sms_settings($param1 = '', $param2 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url() . 'index.php?login', 'refresh');
    if ($param1 == 'clickatell') {

        $data['description'] = $this->input->post('clickatell_user');
        $this->db->where('type', 'clickatell_user');
        $this->db->update('settings', $data);

        $data['description'] = $this->input->post('clickatell_password');
        $this->db->where('type', 'clickatell_password');
        $this->db->update('settings', $data);

        $data['description'] = $this->input->post('clickatell_api_id');
        $this->db->where('type', 'clickatell_api_id');
        $this->db->update('settings', $data);

        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/sms_settings/', 'refresh');
    }

    if ($param1 == 'twilio') {

        $data['description'] = $this->input->post('twilio_account_sid');
        $this->db->where('type', 'twilio_account_sid');
        $this->db->update('settings', $data);

        $data['description'] = $this->input->post('twilio_auth_token');
        $this->db->where('type', 'twilio_auth_token');
        $this->db->update('settings', $data);

        $data['description'] = $this->input->post('twilio_sender_phone_number');
        $this->db->where('type', 'twilio_sender_phone_number');
        $this->db->update('settings', $data);

        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/sms_settings/', 'refresh');
    }

    if ($param1 == 'active_service') {

        $data['description'] = $this->input->post('active_sms_service');
        $this->db->where('type', 'active_sms_service');
        $this->db->update('settings', $data);

        $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
        redirect(base_url() . 'index.php?admin/sms_settings/', 'refresh');
    }

    $page_data['page_name'] = 'sms_settings';
    $page_data['page_title'] = get_phrase('sms_settings');
    $page_data['settings'] = $this->db->get('settings')->result_array();
    $this->load->view('backend/index', $page_data);
}

/* * ***LANGUAGE SETTINGS******** */

function manage_language($param1 = '', $param2 = '', $param3 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url() . 'index.php?login', 'refresh');

    if ($param1 == 'edit_phrase') {
        $page_data['edit_profile'] = $param2;
    }
    if ($param1 == 'update_phrase') {
        $language = $param2;
        $total_phrase = $this->input->post('total_phrase');
        for ($i = 1; $i < $total_phrase; $i++) {
//$data[$language]	=	$this->input->post('phrase').$i;
            $this->db->where('phrase_id', $i);
            $this->db->update('language', array($language => $this->input->post('phrase' . $i)));
        }
        redirect(base_url() . 'index.php?admin/manage_language/edit_phrase/' . $language, 'refresh');
    }
    if ($param1 == 'do_update') {
        $language = $this->input->post('language');
        $data[$language] = $this->input->post('phrase');
        $this->db->where('phrase_id', $param2);
        $this->db->update('language', $data);
        $this->session->set_flashdata('flash_message', get_phrase('settings_updated'));
        redirect(base_url() . 'index.php?admin/manage_language/', 'refresh');
    }
    if ($param1 == 'add_phrase') {
        $data['phrase'] = $this->input->post('phrase');
        $this->db->insert('language', $data);
        $this->session->set_flashdata('flash_message', get_phrase('settings_updated'));
        redirect(base_url() . 'index.php?admin/manage_language/', 'refresh');
    }
    if ($param1 == 'add_language') {
        $language = $this->input->post('language');
        $this->load->dbforge();
        $fields = array(
            $language => array(
                'type' => 'LONGTEXT'
            )
        );
        $this->dbforge->add_column('language', $fields);

        $this->session->set_flashdata('flash_message', get_phrase('settings_updated'));
        redirect(base_url() . 'index.php?admin/manage_language/', 'refresh');
    }
    if ($param1 == 'delete_language') {
        $language = $param2;
        $this->load->dbforge();
        $this->dbforge->drop_column('language', $language);
        $this->session->set_flashdata('flash_message', get_phrase('settings_updated'));

        redirect(base_url() . 'index.php?admin/manage_language/', 'refresh');
    }
    $page_data['page_name'] = 'manage_language';
    $page_data['page_title'] = get_phrase('manage_language');
//$page_data['language_phrases'] = $this->db->get('language')->result_array();
    $this->load->view('backend/index', $page_data);
}

/* * ***BACKUP / RESTORE / DELETE DATA PAGE********* */

function backup_restore($operation = '', $type = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url(), 'refresh');

    if ($operation == 'create') {
        $this->crud_model->create_backup($type);
    }
    if ($operation == 'restore') {
        $this->crud_model->restore_backup();
        $this->session->set_flashdata('backup_message', 'Backup Restored');
        redirect(base_url() . 'index.php?admin/backup_restore/', 'refresh');
    }
    if ($operation == 'delete') {
        $this->crud_model->truncate($type);
        $this->session->set_flashdata('backup_message', 'Data removed');
        redirect(base_url() . 'index.php?admin/backup_restore/', 'refresh');
    }

    $page_data['page_info'] = 'Create backup / restore from backup';
    $page_data['page_name'] = 'backup_restore';
    $page_data['page_title'] = get_phrase('manage_backup_restore');
    $this->load->view('backend/index', $page_data);
}

/* * ****MANAGE OWN PROFILE AND CHANGE PASSWORD** */

function manage_profile($param1 = '', $param2 = '', $param3 = '') {
    if ($this->session->userdata('admin_login') != 1)
        redirect(base_url() . 'index.php?login', 'refresh');
    if ($param1 == 'update_profile_info') {
        $data['username'] = $this->input->post('username');
        $data['email'] = $this->input->post('email');

        $this->db->where('admin_id', $this->session->userdata('admin_id'));
        $this->db->update('admin', $data);
        move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/admin_image/' . $this->session->userdata('admin_id') . '.jpg');
        $this->session->set_flashdata('flash_message', get_phrase('account_updated'));
        redirect(base_url() . 'index.php?admin/manage_profile/', 'refresh');
    }
    if ($param1 == 'change_password') {
        $data['password'] = md5($this->input->post('password'));
        $data['new_password'] = md5($this->input->post('new_password'));
        $data['confirm_new_password'] = md5($this->input->post('confirm_new_password'));

        $current_password = $this->db->get_where('admin', array(
                    'admin_id' => $this->session->userdata('admin_id')
                ))->row()->password;
        if ($current_password == $data['password'] && $data['new_password'] == $data['confirm_new_password']) {
            $this->db->where('admin_id', $this->session->userdata('admin_id'));
            $this->db->update('admin', array(
                'password' => $data['new_password']
            ));
            $this->session->set_flashdata('flash_message', get_phrase('password_updated'));
        } else {
            $this->session->set_flashdata('flash_message', get_phrase('password_mismatch'));
        }
        redirect(base_url() . 'index.php?admin/manage_profile/', 'refresh');
    }
    $page_data['page_name'] = 'manage_profile';
    $page_data['page_title'] = get_phrase('manage_profile');
    $page_data['edit_data'] = $this->db->get_where('admin', array(
                'admin_id' => $this->session->userdata('admin_id')
            ))->result_array();
    $this->load->view('backend/index', $page_data);
}

function ajax() {

    $curriculum_id = $_POST['curriculum_id'];

    $this->db->where('curriculum_id', $curriculum_id);
    $curriculum_arr = $this->db->get('sisfu_curriculum')->result_array();
    foreach ($curriculum_arr as $row):

        $subjects = explode(",", $row['subjects']);

    endforeach;

    $subjects_arr = [];

    for ($i = 0; $i < count($subjects); $i++):
        $this->db->where('subject_id', $subjects[$i]);
        $a = $this->db->get('sisfu_subjects')->result_array();
        array_push($subjects_arr, $a);
//        array_push($subjects_arr, $i);
    endfor;

    echo json_encode($subjects_arr);
}

function ajax_get_courses() {

    $department_id = $_POST['department_id'];

    $this->db->where('department_id', $department_id);
    $subjects_arr = $this->db->get('sisfu_courses')->result_array();

    echo json_encode($subjects_arr);
}

function loa_freshman($param1 = '', $param2 = '', $param3 = '') {

    $page_data['page_name'] = 'loa_freshman';
    $page_data['page_title'] = get_phrase('letter_of_acceptance_freshman');
    $page_data['settings'] = $this->db->get('settings')->result_array();
    $this->load->view('backend/index', $page_data);
}

function do_print_2($param1 = '', $param2 = '', $param3 = '') {
    $this->load->view('backend/admin/letter_1');
}

function loa_scholar($param1 = '', $param2 = '', $param3 = '') {
    $page_data['page_name'] = 'loa_scholar';
    $page_data['page_title'] = get_phrase('unconditional_letter_of_acceptance_with_scholarship');
    $page_data['settings'] = $this->db->get('settings')->result_array();
    $this->load->view('backend/index', $page_data);
}

function loa_unconditional($param1 = '', $param2 = '', $param3 = '') {
    $page_data['page_name'] = 'loa_unconditional';
    $page_data['page_title'] = get_phrase('unconditional_letter_of_acceptance');
    $page_data['settings'] = $this->db->get('settings')->result_array();
    $this->load->view('backend/index', $page_data);
}

function reg_card($param1 = '', $param2 = '', $param3 = '') {
//    $this->load->view('backend/admin/registration_card');
    $page_data['page_name'] = 'registration_card';
    $page_data['page_title'] = get_phrase('registration_card');
    $page_data['settings'] = $this->db->get('settings')->result_array();
    $this->load->view('backend/index', $page_data);
}

function server_processing_students() {

    $this->datatables->select('student_id, student_no, last_name');
    $this->datatables->from('sisfu_students');
    echo $this->datatables->generate();
}

function ajax_increment_ref_no() {
    $ref_no = $this->db->get_where('sisfu_reference', array(
                'ref_code' => 'LOA'
            ))->row()->ref_no;

    $ref_no = $ref_no + 1;

    $ref['ref_no'] = $ref_no;
    $this->db->where('ref_code', 'LOA');
    $this->db->update('sisfu_reference', $ref);
    return $this->db->num_rows();
}

function ajax_increment_reg_no() {
    $reg_no = $this->db->get_where('sisfu_reference', array(
                'ref_code' => 'REGCARD'
            ))->row()->ref_no;

    $reg_no = $reg_no + 1;

    $reg['ref_no'] = $reg_no;
    $this->db->where('ref_code', 'REGCARD');
    $this->db->update('sisfu_reference', $reg);
    return $this->db->num_rows();
}

function ajax_reg_card() {
    $_SESSION['student_id'] = $_POST['student_id'];

    $_SESSION['a_total'] = $_POST['planA_total'];
    $_SESSION['b_total'] = $_POST['planB_total'];
    $_SESSION['c_total'] = $_POST['planC_total'];


    $_SESSION['net_tuition'] = $_POST['net_tuition'];
    $_SESSION['tr_count'] = $_POST['tr_count'] - 1;

    $_SESSION['b_total'] = $_POST['planB_total'];
    $_SESSION['b_down'] = $_POST['b_down'];
    $_SESSION['b_give'] = $_POST['b_give'];

    $_SESSION['c_total'] = $_POST['planC_total'];
    $_SESSION['c_down'] = $_POST['c_down'];
    $_SESSION['c_give'] = $_POST['c_give'];

    $_SESSION['tuition'] = $_POST['tuition'];
    $_SESSION['misc'] = $_POST['misc'];
    $_SESSION['ir_fee'] = $_POST['ir_fee'];
    $_SESSION['other'] = $_POST['other'];
    $_SESSION['other_text'] = $_POST['modal_other_text'];
    $_SESSION['grad'] = $_POST['ajax_grad'];
    $_SESSION['b_add_on'] = $_POST['b_add_on'];
    $_SESSION['c_add_on'] = $_POST['c_add_on'];

    $_SESSION['last_url'] = $_POST['last_url'];
}

function ajax_new_subject() {
    $_SESSION['new_subject'] = $_POST['new_subject'];
}

}
