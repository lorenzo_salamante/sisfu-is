<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary" data-collapsed="0">
            <div class="panel-heading">
                <div class="panel-title"><span class="primary-color">
                        <i class="entypo-plus-circled"></i>
                        <?php echo get_phrase('view_subjects'); ?></span>
                </div>
            </div>
            <div class="panel-body">
                <table class="table table-bordered datatable hidden-print" id="">
                    <thead>
                        <tr>
                            <th><div><?php echo get_phrase('subject_name'); ?></div></th>
                    <th><div><?php echo get_phrase('subject_code') ?></div></th>
                    <th><div><?php echo get_phrase('options') ?></div></th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php
                        $this->db->where('curriculum_id', $param2);
                        $result = $this->db->get('sisfu_curriculum')->result_array();
                        foreach ($result as $row):

                            $subjects = $row['subjects'];
                            $subjects_arr = explode(',', $subjects);
                            ?>

                        <?php endforeach; ?>
                        <tr>
                            <?php
                            foreach ($subjects_arr as $row):
                                $this->db->where('subject_id', $row);
                                $result = $this->db->get('sisfu_subjects')->result_array();
                                foreach ($result as $subject):
                                    ?>
                                    <td><?php echo $subject['subject_name'] ?></td>
                                    <td><?php echo $subject['subject_code'] ?></td>
                                    <td>
                                        <a href="#" onclick="confirm_modal('<?php echo base_url(); ?>index.php?admin/curriculum/delete_subj_from/<?php echo $param2 . '/' . $subject['subject_id'] ?>');">
                                            <i class="fa fa-trash-o"></i>
                                            <?php echo get_phrase('delete'); ?>
                                        </a>
                                    </td>
                                </tr>

                            <?php endforeach; ?>
                        <?php endforeach; ?>

                    </tbody>

                </table>
            </div>
        </div>
    </div>
</div>