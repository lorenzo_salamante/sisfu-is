<?php
foreach ($edit_data as $row):
    ?>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary" data-collapsed="0">
                <div class="panel-heading">
                    <div class="panel-title"><span class="primary-color">
                            <i class="entypo-plus-circled"></i>
                            <?php echo get_phrase('edit_ISO_form'); ?></span>
                    </div>
                </div>
                <div class="panel-body">

                    <?php echo form_open(base_url() . 'index.php?admin/dues/edit/' . $param2, array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data')); ?>

                    <div class="form-group">
                        <label for="active_term" class="col-sm-3 control-label"><?php echo get_phrase('term_number'); ?> </label>
                        <div class="col-sm-7">
                            <input type="number" min="0" class="form-control" id="active_term" name="active_term" value="<?php echo $row['active_term']; ?>" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                        </div> 
                    </div>

                    <div class="form-group">
                        <label for="due_first" class="col-sm-3 control-label"><?php echo get_phrase('due_first'); ?> </label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="due_first" name="due_first" value="<?php echo $row['due_first']; ?>" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                        </div> 
                    </div>

                    <div class="form-group">
                        <label for="due_second" class="col-sm-3 control-label"><?php echo get_phrase('due_second'); ?> </label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="due_second" name="due_second" value="<?php echo $row['due_second']; ?>" data-validate="required" placeholder=""data-message-required="<?php echo get_phrase('value_required'); ?>">
                        </div> 
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-5">
                            <button type="submit" class="btn btn-default"><?php echo get_phrase('save'); ?></button>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
<?php endforeach;