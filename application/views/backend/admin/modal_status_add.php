<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary" data-collapsed="0">
            <div class="panel-heading">
                <div class="panel-title"><span class="primary-color">
                    <i class="entypo-plus-circled"></i>
                    <?php echo get_phrase('add_new_status'); ?></span>
                </div>
            </div>
            <div class="panel-body">

                <?php echo form_open(base_url() . 'index.php?admin/status/create/', array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data')); ?>
                
                <div class="form-group">
                    <label for="status_name" class="col-sm-3 control-label"><?php echo get_phrase('status_name'); ?> </label>

                    <div class="col-sm-7">
                        <input type="text" class="form-control" id="status_name" name="status_name" value="" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                    </div> 
                </div>
                
                <div class="form-group">
                    <label for="status_category" class="col-sm-3 control-label"><?php echo get_phrase('is_active'); ?> </label>

                    <div class="col-sm-7">
                        <input type="number" min='0' max='1' class="form-control" id="status_category" name="status_category" value="" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                    </div> 
                </div>
                
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-default"><?php echo get_phrase('add_status'); ?></button>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>