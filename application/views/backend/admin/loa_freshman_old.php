<?php $student_id = $this->uri->segment(3, 0); ?>

<style>
    
</style>
<body >
    <?php
    $this->db->where('student_id', $student_id);
    $result = $this->db->get('sisfu_students')->result_array();
    foreach ($result as $row):
        ?>

        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 print-header">
                    <center>
                        <img id="print-heading" src="<?php echo base_url() . 'assets/images/letter-heading.jpg' ?>" style="    height:155px;
                             "/>

                    </center>

                </div>

            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div style="margin-left: 70px;
                         font-size: 13px">
                         <?php echo date('d F Y') ?>
                    </div>
                    <div style="margin-left: 70%; position: absolute;
                         font-size: 13px;
                         margin-top:-17px">
                        Office of the Registrar<br>
                        Ref No. ################
                    </div>
                    <div style="    margin-left: 70px;
                         margin-top: 10px;
                         font-weight: bold;
                         font-size: 13px;">
                         <?php
                         echo 'Mr. ' . $row['first_name'] . ' ' . $row['middle_name'] . ' ' . $row['last_name'];
                         ?>
                    </div>
                    <div style="    margin-left: 70px;
                         font-weight: bold;
                         font-size: 13px;">
                         <?php
//                     echo $row['current_address'];
                         ?>
                        221B Baker Street,<br>London, England
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div style="  text-align:center;
                         font-weight: bold;
                         font-size: 13px;
                         margin-top:4px">
                        CONDITIONAL LETTER OF ACCEPTANCE
                    </div>
                </div>
                <div class="col-sm-12">
                    <div style=" 
                         margin-left: 70px;

                         font-size: 13px;
                         margin-top:4px">
                         <?php
                         echo 'Dear Mr. <strong>' . $row['last_name'] . '</strong>';
                         ?>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div style=" text-align: justify;
                         margin-left: 70px;
                         width: 83%;
                         font-size: 13px;
                         margin-top:10px">
                        Congratulations! It is my distinct pleasure to inform you that you have been accepted into Southville 
                        International School affiliated with Foreign Universities for undergraduate study in 

                        <?php
                        $this->db->where('course_id', $row['course_id']);
                        $result = $this->db->get('sisfu_courses')->result_array();
                        foreach ($result as $course):
                            ?>
                            <span style="text-decoration:underline; font-weight:bold"> <?php echo $course['degree_name'] ?> </span>
                            for the School Year 
                            <?php
                            ?>
                            2015-2016
                            <?php
                            ?>
                            2<superscript>nd</superscript> trimester, commencing on #########, conditional upon the completion of the following requirements:
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div style=" text-align: justify;
                         margin-left: 70px;
                         width: 80%;
                         font-size: 13px;
                         margin-top:18px">
                        <ul>
                            <li>Submission of the following academic records</li>
                            <ul>
                                <li>Recommendation Letter from School Principal or Guidance Counselor</li>
                                <li>Recommendation Letter from a Professor or Teacher</li>
                                <li>Official Transcript of Secondary Education (F137)</li>
                                <li>High School Report Card (F138) </li>
                                <li>Copy of High School Diploma</li>
                            </ul>
                            <li>Submission of the following:</li>
                            <ul>
                                <li>Four (4) 2” x  2” pictures</li>
                                <li>Copy of birth certificate and/or passport</li>
                                <li>500-word essay</li>
                            </ul>
                            <li>Payment of admission and application fees</li>

                        </ul>
                    </div>
                    <div style=" text-align: justify;
                         margin-left: 70px;
                         width: 83%;
                         font-size: 13px;
                         margin-top:18px">
                        <p>
                            All requirements must be submitted on or before 15 May 2015. Once these have been received by Southville 
                            you will receive an Unconditional Letter of Acceptance. International Registration Fees (IRFs) therefore 
                            will be paid by all students. The period of registration is valid for five years from date of enrolment.
                        </p>
                        <p>
                            The academic program is taught at British University standard. You will have the opportunity to learn with 
                            students from all around the world in one of the most focused academic institutions in the Philippines and it 
                            is our hope that you will contribute your special talents to the rich diversity that defines the Southville 
                            student body. You are required to attend the College Preparatory Program, scheduled on June 2015 as a 
                            requirement for all incoming students.
                        </p>
                        <p>
                            It is essential that you take the TOEIC test for appropriate placement and assistance. The Marketing 
                            Department shall coordinate with you for the scheduled testing date.
                        </p>
                        <p>
                            On behalf of the entire faculty and management, congratulations on your conditional admission to Southville 
                            International School affiliated with Foreign Universities.
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6" style="margin-left: 70px;
                     margin-top: 10px;
                     font-size: 13px;">
                    <p>Sincerely</p>

                    <p><span style="font-weight: bold;">Ms. Caroline F. Mediodia</span><br>
                        Registrar</p>
                </div>
                <div class="col-md-6" style="    margin-top: -65px;
    font-size: 13px;
    margin-left: 65%;">
                    <p>Noted by:</p>

                    <p><span style="font-weight: bold;">Mr. Amir Tohid</span><br>
                        Administrative Head<br>
                        School of Business and Computing
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="print-footer col-sm-12" style="font-size: 11px;
                     line-height: 3px;
                     text-align: right;
                     margin-right: 10px;
                     bottom: 8px;
                     position: absolute;
                     right: 1%;">
                    <img id="print-heading" src="<?php echo base_url() . 'assets/images/letter-footer.png' ?>" style="    height: 12px;
                         margin-bottom: -5px;"/>

                    <p>Lima corner Luxembourg Street BF International, Las Piñas City, Philippines 1741</p>
                    <p class="" >+632-820-918   |   +632-820-6774</p>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</body>
<script>
    $('document').ready(function(){
        window.print();
    });
</script>
