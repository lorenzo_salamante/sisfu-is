<div class="">
    <?php $student_id = $this->uri->segment(4, 0); 
    $scholarship_id = $this->db->get_where('sisfu_students', array(
                'student_id' => $student_id
            ))->row()->scholarship_id;
    ?>
    <div class="row hidden-print">
        <div class="col-md-12">
            <div class="panel panel-primary" data-collapsed="0">
                <div class="panel-body">
                    <!--                            <div class="panel-heading">
                                                    <div class="panel-title">
                                                        <i class="entypo-plus-circled"></i>
                    <?php // echo get_phrase('freshman_requirements'); ?>
                                                    </div>
                                                </div>-->

                    <?php echo form_open(base_url() . 'index.php?admin/student/do_checklist/' . $student_id, array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data')); ?>

                    <div>
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#panel1" aria-controls="panel1" role="tab" data-toggle="tab">Freshman</a></li>
                            <li role="presentation"><a href="#panel2" aria-controls="panel2" role="tab" data-toggle="tab">Transferee</a></li>
                            <li role="presentation"><a href="#panel3" aria-controls="panel3" role="tab" data-toggle="tab">Foreign</a></li>
                            <li role="presentation"><a href="#panel4" aria-controls="panel4" role="tab" data-toggle="tab">Graduate</a></li>
                            <li role="presentation"><a href="#panel5" aria-controls="panel5" role="tab" data-toggle="tab">Foreign & Transferee</a></li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="panel1">
                                <div class="row">
                                    <center class="">
                                        
                                        <?php if($scholarship_id > 0):?>
                                        <div class="col-sm-12">
                                            <a href="<?php echo base_url() . 'index.php?admin/loa_scholar/' . $student_id ?>" class=" form-control btn btn-blue" id="print_fresh" target="_blank"><i class="fa fa-print"></i> Preview Unconditional LoA w/ Scholarship</a>
                                        </div>
                                        <?php else:?>
                                        <div class="col-sm-12">
                                            <a href="<?php echo base_url() . 'index.php?admin/loa_unconditional/' . $student_id ?>" class=" form-control btn btn-blue" id="print_fresh" target="_blank"><i class="fa fa-print"></i> Preview Unconditional LoA</a>
                                        </div>
                                        <?php endif;?>
                                        <hr>
                                    </center>
                                </div>

                                <?php
                                $this->db->where('student_id', $student_id);
                                $this->db->where('checklist_type', 1);
                                $freshman = $this->db->get('sisfu_checklist')->result_array();
                                $x = 1;
                                foreach ($freshman as $row):
                                    ?>
                                    <div class="form-group">
                                        <label for="" class="col-sm-7 control-label"><?php echo $row['checklist_name'] ?></label>
                                        <div class="col-sm-3">
                                            <input type="checkbox" class="form-control" id="fresh-<?php echo $row['checklist_id'] ?>" name="freshman[]" value="fresh-<?php echo $x ?>"
                                                   <?php echo ($row['is_checked'] == 1) ? 'checked' : ''; ?>>
                                        </div>
                                    </div>

                                    <?php
                                    $x++;
                                endforeach;
                                ?>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="panel2">
                                <div class="row">
                                    <center class="">
                                        <?php if($scholarship_id > 0):?>
                                        <div class="col-sm-12">
                                            <a href="<?php echo base_url() . 'index.php?admin/loa_scholar/' . $student_id ?>" class=" form-control btn btn-blue" id="print_fresh" target="_blank"><i class="fa fa-print"></i> Preview Unconditional LoA w/ Scholarship</a>
                                        </div>
                                        <?php else:?>
                                        <div class="col-sm-12">
                                            <a href="<?php echo base_url() . 'index.php?admin/loa_unconditional/' . $student_id ?>" class=" form-control btn btn-blue" id="print_fresh" target="_blank"><i class="fa fa-print"></i> Preview Unconditional LoA</a>
                                        </div>
                                        <?php endif;?>
                                        <hr>
                                    </center>
                                </div>
                                <?php
                                $this->db->where('student_id', $student_id);
                                $this->db->where('checklist_type', 2);
                                $transferee = $this->db->get('sisfu_checklist')->result_array();
                                $x = 1;
                                foreach ($transferee as $row):
                                    ?>
                                    <div class="form-group">
                                        <label for="" class="col-sm-7 control-label"><?php echo $row['checklist_name'] ?></label>
                                        <div class="col-sm-3">
                                            <input type="checkbox" class="form-control" id="trans-<?php echo $row['checklist_id'] ?>" name="transferee[]" value="trans-<?php echo $x ?>"
                                                   <?php echo ($row['is_checked'] == 1) ? 'checked' : ''; ?>>
                                        </div>
                                    </div>

                                    <?php
                                    $x++;
                                endforeach;
                                ?>

                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="panel3">
                                <div class="row">
                                    <center class="">
                                        
                                        <?php if($scholarship_id > 0):?>
                                        <div class="col-sm-12">
                                            <a href="<?php echo base_url() . 'index.php?admin/loa_scholar/' . $student_id ?>" class=" form-control btn btn-blue" id="print_fresh" target="_blank"><i class="fa fa-print"></i> Preview Unconditional LoA w/ Scholarship</a>
                                        </div>
                                        <?php else:?>
                                        <div class="col-sm-12">
                                            <a href="<?php echo base_url() . 'index.php?admin/loa_unconditional/' . $student_id ?>" class=" form-control btn btn-blue" id="print_fresh" target="_blank"><i class="fa fa-print"></i> Preview Unconditional LoA</a>
                                        </div>
                                        <?php endif;?>
                                        <hr>
                                    </center>
                                </div>
                                <?php
                                $this->db->where('student_id', $student_id);
                                $this->db->where('checklist_type', 3);
                                $foreign = $this->db->get('sisfu_checklist')->result_array();
                                $x = 1;
                                foreach ($foreign as $row):
                                    ?>
                                    <div class="form-group">
                                        <label for="" class="col-sm-7 control-label"><?php echo $row['checklist_name'] ?></label>
                                        <div class="col-sm-3">
                                            <input type="checkbox" class="form-control" id="for-<?php echo $row['checklist_id'] ?>" name="foreign[]" value="for-<?php echo $x ?>"
                                                   <?php echo ($row['is_checked'] == 1) ? 'checked' : ''; ?>>
                                        </div>
                                    </div>

                                    <?php
                                    $x++;
                                endforeach;
                                ?>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="panel4">
                                <div class="row">
                                    <center class="">
                                        
                                        <?php if($scholarship_id > 0):?>
                                        <div class="col-sm-12">
                                            <a href="<?php echo base_url() . 'index.php?admin/loa_scholar/' . $student_id ?>" class=" form-control btn btn-blue" id="print_fresh" target="_blank"><i class="fa fa-print"></i> Preview Unconditional LoA w/ Scholarship</a>
                                        </div>
                                        <?php else:?>
                                        <div class="col-sm-12">
                                            <a href="<?php echo base_url() . 'index.php?admin/loa_unconditional/' . $student_id ?>" class=" form-control btn btn-blue" id="print_fresh" target="_blank"><i class="fa fa-print"></i> Preview Unconditional LoA</a>
                                        </div>
                                        <?php endif;?>
                                        <hr>
                                    </center>
                                </div>
                                <?php
                                $this->db->where('student_id', $student_id);
                                $this->db->where('checklist_type', 4);
                                $result = $this->db->get('sisfu_checklist')->result_array();
                                $x = 1;
                                foreach ($result as $row):
                                    ?>
                                    <div class="form-group">
                                        <label for="" class="col-sm-7 control-label"><?php echo $row['checklist_name'] ?></label>
                                        <div class="col-sm-3">
                                            <input type="checkbox" class="form-control" id="for-<?php echo $row['checklist_id'] ?>" name="graduate[]" value="grad-<?php echo $x ?>"
                                                   <?php echo ($row['is_checked'] == 1) ? 'checked' : ''; ?>>
                                        </div>
                                    </div>

                                    <?php
                                    $x++;
                                endforeach;
                                ?>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="panel5">
                                <div class="row">
                                    <center class="">
                                        
                                        <?php if($scholarship_id > 0):?>
                                        <div class="col-sm-12">
                                            <a href="<?php echo base_url() . 'index.php?admin/loa_scholar/' . $student_id ?>" class=" form-control btn btn-blue" id="print_fresh" target="_blank"><i class="fa fa-print"></i> Preview Unconditional LoA w/ Scholarship</a>
                                        </div>
                                        <?php else:?>
                                        <div class="col-sm-12">
                                            <a href="<?php echo base_url() . 'index.php?admin/loa_unconditional/' . $student_id ?>" class=" form-control btn btn-blue" id="print_fresh" target="_blank"><i class="fa fa-print"></i> Preview Unconditional LoA</a>
                                        </div>
                                        <?php endif;?>
                                        <hr>
                                    </center>
                                </div>
                                <?php
                                $this->db->where('student_id', $student_id);
                                $this->db->where('checklist_type', 5);
                                $result = $this->db->get('sisfu_checklist')->result_array();
                                $x = 1;
                                foreach ($result as $row):
                                    ?>
                                    <div class="form-group">
                                        <label for="" class="col-sm-7 control-label"><?php echo $row['checklist_name'] ?></label>
                                        <div class="col-sm-3">
                                            <input type="checkbox" class="form-control" id="tf-<?php echo $row['checklist_id'] ?>" name="tf[]" value="tf-<?php echo $x ?>"
                                                   <?php echo ($row['is_checked'] == 1) ? 'checked' : ''; ?>>
                                        </div>
                                    </div>

                                    <?php
                                    $x++;
                                endforeach;
                                ?>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-5 col-sm-5">
                            <button type="submit" class="btn btn-default" id="save"><?php echo get_phrase('save'); ?></button>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
</script>