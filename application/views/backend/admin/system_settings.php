<hr />

<div class="row">
    <?php echo form_open(base_url() . 'index.php?admin/system_settings/do_update', array('class' => 'form-horizontal form-groups-bordered validate', 'target' => '_top'));
    ?>
    <div class="col-md-6">

        <div class="panel panel-primary" >

            <div class="panel-heading">
                <div class="panel-title">
                    <?php echo get_phrase('system_settings'); ?>
                </div>
            </div>

            <div class="panel-body">

                <div class="form-group">
                    <label  class="col-sm-3 control-label"><?php echo get_phrase('system_name'); ?></label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="system_name" 
                               value="<?php echo $this->db->get_where('settings', array('type' => 'system_name'))->row()->description; ?>">
                    </div>
                </div>

                <div class="form-group">
                    <label  class="col-sm-3 control-label"><?php echo get_phrase('system_title'); ?></label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="system_title" 
                               value="<?php echo $this->db->get_where('settings', array('type' => 'system_title'))->row()->description; ?>">
                    </div>
                </div>

                <div class="form-group">
                    <label  class="col-sm-3 control-label"><?php echo get_phrase('address'); ?></label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="address" 
                               value="<?php echo $this->db->get_where('settings', array('type' => 'address'))->row()->description; ?>">
                    </div>
                </div>

                <div class="form-group">
                    <label  class="col-sm-3 control-label"><?php echo get_phrase('phone'); ?></label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="phone" 
                               value="<?php echo $this->db->get_where('settings', array('type' => 'phone'))->row()->description; ?>">
                    </div>
                </div>

                <!--                <div class="form-group">
                                    <label  class="col-sm-3 control-label"><?php echo get_phrase('paypal_email'); ?></label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="paypal_email" 
                                               value="<?php echo $this->db->get_where('settings', array('type' => 'paypal_email'))->row()->description; ?>">
                                    </div>
                                </div>
                
                                <div class="form-group">
                                    <label  class="col-sm-3 control-label"><?php echo get_phrase('currency'); ?></label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="currency" 
                                               value="<?php echo $this->db->get_where('settings', array('type' => 'currency'))->row()->description; ?>">
                                    </div>
                                </div>-->

                <!--                <div class="form-group">
                                    <label  class="col-sm-3 control-label"><?php echo get_phrase('system_email'); ?></label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="system_email" 
                                               value="<?php echo $this->db->get_where('settings', array('type' => 'system_email'))->row()->description; ?>">
                                    </div>
                                </div>
                
                                <div class="form-group">
                                    <label  class="col-sm-3 control-label"><?php echo get_phrase('language'); ?></label>
                                    <div class="col-sm-9">
                                        <select name="language" class="form-control">
                <?php
                $fields = $this->db->list_fields('language');
                foreach ($fields as $field) {
                    if ($field == 'phrase_id' || $field == 'phrase')
                        continue;

                    $current_default_language = $this->db->get_where('settings', array('type' => 'language'))->row()->description;
                    ?>
                                                                <option value="<?php echo $field; ?>"
                    <?php if ($current_default_language == $field) echo 'selected'; ?>> <?php echo $field; ?> </option>
                    <?php
                }
                ?>
                                        </select>
                                    </div>
                                </div>
                
                                <div class="form-group">
                                    <label  class="col-sm-3 control-label"><?php echo get_phrase('text_align'); ?></label>
                                    <div class="col-sm-9">
                                        <select name="text_align" class="form-control">
                <?php $text_align = $this->db->get_where('settings', array('type' => 'text_align'))->row()->description; ?>
                                            <option value="left-to-right" <?php if ($text_align == 'left-to-right') echo 'selected'; ?>> left-to-right</option>
                                            <option value="right-to-left" <?php if ($text_align == 'right-to-left') echo 'selected'; ?>> right-to-left</option>
                                        </select>
                                    </div>
                                </div>-->

                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9">
                        <button type="submit" class="btn btn-info"><?php echo get_phrase('save'); ?></button>
                    </div>
                </div>

            </div>

            <?php echo form_close(); ?>
        </div>

        <div class="panel panel-primary" >

            <div class="panel-heading">
                <div class="panel-title">
                    <?php echo get_phrase('theme_settings'); ?>
                </div>
            </div>

            <div class="panel-body">

                <div class="gallery-env">

                    <div class="col-sm-4">
                        <article class="album">
                            <header>
                                <a href="#" id="default">
                                    <img src="assets/images/skins/default.png"
                                         <?php if ($skin == 'default') echo 'style="background-color: black; opacity: 0.3;"'; ?> />
                                </a>
                                <a href="#" class="album-options" id="default">
                                    <i class="entypo-check"></i>
                                    <?php echo get_phrase('default'); ?>
                                </a>
                            </header>
                        </article>
                    </div>

                </div>
                <center>
                    <div class="label label-primary" style="font-size: 12px;">
                        <i class="entypo-check"></i> <?php echo get_phrase('select_a_theme_to_make_changes'); ?>
                    </div>
                </center>
            </div>

        </div>
    </div>

    <?php
    $skin = $this->db->get_where('settings', array(
                'type' => 'skin_colour'
            ))->row()->description;
    $commencement_date = $this->db->get_where('settings', array(
                'type' => 'commencement_date'
            ))->row()->description;
    $requirements_due = $this->db->get_where('settings', array(
                'type' => 'requirements_due'
            ))->row()->description;
    $orientation_date = $this->db->get_where('settings', array(
                'type' => 'orientation_date'
            ))->row()->description;
    $ref_no = $this->db->get_where('sisfu_reference', array(
                'ref_code' => 'LOA'
            ))->row()->ref_no;
    $reg_no = $this->db->get_where('sisfu_reference', array(
                'ref_code' => 'REGCARD'
            ))->row()->ref_no;
    $active_term = $this->db->get_where('settings', array(
                'type' => 'active_term'
            ))->row()->description;
    $tuition = $this->db->get_where('settings', array(
                'type' => 'tuition'
            ))->row()->description;
    $last_no = $this->db->get_where('settings', array(
                'type' => 'last_no'
            ))->row()->description;
    ?>

    <div class="col-md-6">
        <?php echo form_open(base_url() . 'index.php?admin/system_settings/do_other', array('class' => 'form-horizontal form-groups-bordered validate', 'target' => '_top'));
        ?>

        <div class="panel panel-primary" >

            <div class="panel-heading">
                <div class="panel-title">
                    <?php echo get_phrase('other_settings'); ?>
                </div>
            </div>

            <div class="panel-body">
                <div class="col-sm-9">
                    <div class="form-group">
                        <label class="col-sm-6 control-label"><?php echo get_phrase('base_tuition_fee'); ?></label>
                        <div class="col-sm-6">
                            <input class='form-control' type='number' min="0" name='tuition' value='<?php echo $tuition ?>' />
                        </div>
                    </div>
                </div>
                <div class="col-sm-9">
                    <div class="form-group">
                        <label class="col-sm-6 control-label"><?php echo get_phrase('last_student_num'); ?></label>
                        <div class="col-sm-6">
                            <input class='form-control' type='number' min="0" name='last_no' value='<?php echo $last_no ?>' />
                        </div>
                    </div>
                </div>
                <div class="col-sm-9">
                    <div class="form-group">
                        <label class="col-sm-6 control-label"><?php echo get_phrase('current_term'); ?></label>
                        <div class="col-sm-6">
                            <select name="active_term" class="form-control">
                                <option value="1" <?php echo ($active_term == 1 ? 'selected' : "") ?> >First term</option>
                                <option value="2" <?php echo ($active_term == 2 ? 'selected' : "") ?> >Second term</option>
                                <option value="3" <?php echo ($active_term == 3 ? 'selected' : "") ?> >Third term</option>
                                <option value="S" <?php echo ($active_term == 'S' ? 'selected' : "") ?> >Summer</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-sm-9">
                    <div class="form-group">
                        <label class="col-sm-6 control-label"><?php echo get_phrase('commencement_date'); ?></label>
                        <div class="col-sm-6">
                            <input class='form-control' type='date' name='commencement_date' value='<?php echo $commencement_date ?>' />
                        </div>
                    </div>
                </div>
                <div class="col-sm-9">
                    <div class="form-group">
                        <label class="col-sm-6 control-label"><?php echo get_phrase('requirements_due'); ?></label>
                        <div class="col-sm-6">
                            <input class='form-control' type='date' name='requirements_due' value='<?php echo $requirements_due ?>' />
                        </div>
                    </div>
                </div>
                <div class="col-sm-9">
                    <div class="form-group">
                        <label class="col-sm-6 control-label"><?php echo get_phrase('orientation_date'); ?></label>
                        <div class="col-sm-6">
                            <input class='form-control' type='date' name='orientation_date' value='<?php echo $orientation_date ?>' />
                        </div>
                    </div>
                </div>
                <div class="col-sm-9">
                    <div class="form-group">
                        <label class="col-sm-6 control-label"><?php echo get_phrase('LOA_reference_no'); ?></label>
                        <div class="col-sm-6">
                            <input class='form-control' type='text' name='ref_no' value='<?php echo $ref_no ?>' />
                        </div>
                    </div>
                </div>
                <div class="col-sm-9">
                    <div class="form-group">
                        <label class="col-sm-6 control-label"><?php echo get_phrase('registration_card_reference_no'); ?></label>
                        <div class="col-sm-6">
                            <input class='form-control' type='text' name='reg_no' value='<?php echo $reg_no ?>' />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-1 col-sm-9">
                        <button type="submit" class="btn btn-info"><?php echo get_phrase('save'); ?></button>
                    </div>
                </div>
            </div>


        </div>

        <?php echo form_close(); ?>

    </div>

</div>

<script type="text/javascript">
    $(".gallery-env").on('click', 'a', function () {
        skin = this.id;
        $.ajax({
            url: '<?php echo base_url(); ?>index.php?admin/system_settings/change_skin/' + skin,
            success: window.location = '<?php echo base_url(); ?>index.php?admin/system_settings/'
        });
    });
</script>