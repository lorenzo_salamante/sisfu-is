<?php
$active_term = $this->db->get_where('settings', array(
            'type' => 'active_term'
        ))->row()->description;
$due_first = $this->db->get_where('sisfu_dues', array(
            'active_term' => $active_term
        ))->row()->due_first;
$due_second = $this->db->get_where('sisfu_dues', array(
            'active_term' => $active_term
        ))->row()->due_second;
$reg_no = substr($this->db->get_where('sisfu_reference', array('ref_code' => 'REGCARD'))->row()->ref_no, 6);

$this->db->where('student_id', $_SESSION['student_id']);
$r = $this->db->get('sisfu_students')->result_array();
foreach ($r as $row):
    ?>
    <div class="reg-card visible-print ">

        <div class="reg-no reg-no-1">Reg. No. <?php echo $reg_no ?></div>
        <div class="reg-no reg-no-2">Reg. No. <?php echo $reg_no ?></div>
        <div class="reg-no reg-no-3">Reg. No. <?php echo $reg_no ?></div>

        <table class='payment-schedule payment-schedule-1'>
            <tbody>
                <tr>
                    <td>&nbsp;</td>
                    <td colspan='2'>
                        Schedule of Payment
                    </td>
                </tr>
                <!--PLAN A-->
                <tr>
                    <td>&#9744;</td>
                    <td>Plan A </td>
                    <td class='grad-fee'>
                        <span class="<?php
                        if ($_SESSION['grad'] == 0) {
                            echo "hidden";
                        }
                        ?>">Grad Fee (PhP) <?php echo $_SESSION['grad'] ?></span>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class='td-num bold'>Total:</td>
                    <td class='td-num bold'>$<?php echo $_SESSION['a_total'] ?></td>
                </tr>
                <!--PLAN B-->
                <tr>
                    <td>&#9744;</td>
                    <td>Plan B </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>1st Inst.:</td>
                    <td class='td-num'><?php echo $_SESSION['b_down']; ?></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>2nd Inst.:</td>
                    <td class='td-num'><?php echo $_SESSION['b_give']; ?></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class='td-num bold'>Total:</td>
                    <td class='td-num bold'>$<?php echo $_SESSION['b_total'] ?></td>
                </tr>
                <!--PLAN C-->
                <tr>
                    <td>&#9744;</td>
                    <td>Plan C </td>
                    <td>&nbsp;</td>

                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>1st Inst.:</td>
                    <td class='td-num'><?php echo $_SESSION['c_down']; ?></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>2nd Inst.:</td>
                    <td class='td-num'><?php echo $_SESSION['c_give']; ?></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>3rd Inst.:</td>
                    <td class='td-num'><?php echo $_SESSION['c_give']; ?></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class='td-num bold'>Total:</td>
                    <td class='td-num bold'>$<?php echo $_SESSION['c_total'] ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="3"><hr style='border-color:black'></td>
                </tr>
                <tr>
                    <td class='underline'>Note:</td>
                    <td>&nbsp;</td>
                    <td class='underline'>Due Date:</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>Plan A / 1st Inst.</td>
                    <td>Upon Enrollment</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>2nd Inst.</td>
                    <td><?php
                        $d1 = new DateTime($due_first);
                        $d1 = $d1->format('M d, Y');
                        echo $d1;
                        ?></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>3rd Inst.</td>
                    <td><?php
                        $d2 = new DateTime($due_second);
                        $d2 = $d2->format('M d, Y');
                        echo $d2;
                        ?></td>
                </tr>
            </tbody>
        </table>
        <table class='payment-schedule payment-schedule-2'>
            <tbody>
                <tr>
                    <td>&nbsp;</td>
                    <td colspan='2'>
                        Schedule of Payment
                    </td>
                </tr>
                <!--PLAN A-->
                <tr>
                    <td>&#9744;</td>
                    <td>Plan A </td>
                    <td class='grad-fee'>
                        <span class="<?php
                        if ($_SESSION['grad'] == 0) {
                            echo "hidden";
                        }
                        ?>">Grad Fee (PhP) <?php echo $_SESSION['grad'] ?></span>    
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class='td-num bold'>Total:</td>
                    <td class='td-num bold'>$<?php echo $_SESSION['a_total'] ?></td>
                </tr>
                <!--PLAN B-->
                <tr>
                    <td>&#9744;</td>
                    <td>Plan B </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>1st Inst.:</td>
                    <td class='td-num'><?php echo $_SESSION['b_down']; ?></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>2nd Inst.:</td>
                    <td class='td-num'><?php echo $_SESSION['b_give']; ?></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class='td-num bold'>Total:</td>
                    <td class='td-num bold'>$<?php echo $_SESSION['b_total'] ?></td>
                </tr>
                <!--PLAN C-->
                <tr>
                    <td>&#9744;</td>
                    <td>Plan C </td>
                    <td>&nbsp;</td>

                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>1st Inst.:</td>
                    <td class='td-num'><?php echo $_SESSION['c_down']; ?></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>2nd Inst.:</td>
                    <td class='td-num'><?php echo $_SESSION['c_give']; ?></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>3rd Inst.:</td>
                    <td class='td-num'><?php echo $_SESSION['c_give']; ?></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class='td-num bold'>Total:</td>
                    <td class='td-num bold'>$<?php echo $_SESSION['c_total'] ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="3"><hr style='border-color:black'></td>
                </tr>
                <tr>
                    <td class='underline'>Note:</td>
                    <td>&nbsp;</td>
                    <td class='underline'>Due Date:</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>Plan A / 1st Inst.</td>
                    <td>Upon Enrollment</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>2nd Inst.</td>
                    <td><?php
                        $d1 = new DateTime($due_first);
                        $d1 = $d1->format('M d, Y');
                        echo $d1;
                        ?></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>3rd Inst.</td>
                    <td><?php
                        $d2 = new DateTime($due_second);
                        $d2 = $d2->format('M d, Y');
                        echo $d2;
                        ?></td>
                </tr>
            </tbody>
        </table>

        <table class='payment-schedule payment-schedule-3'>
            <tbody>
                <tr>
                    <td>&nbsp;</td>
                    <td colspan='2'>
                        Schedule of Payment
                    </td>
                </tr>
                <!--PLAN A-->
                <tr>
                    <td>&#9744;</td>
                    <td>Plan A </td>
                    <td class='grad-fee'>
                        <span class="<?php
                        if ($_SESSION['grad'] == 0) {
                            echo "hidden";
                        }
                        ?>">Grad Fee (PhP) <?php echo $_SESSION['grad'] ?></span>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class='td-num bold'>Total:</td>
                    <td class='td-num bold'>$<?php echo $_SESSION['a_total'] ?></td>
                </tr>
                <!--PLAN B-->
                <tr>
                    <td>&#9744;</td>
                    <td>Plan B </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>1st Inst.:</td>
                    <td class='td-num'><?php echo $_SESSION['b_down']; ?></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>2nd Inst.:</td>
                    <td class='td-num'><?php echo $_SESSION['b_give']; ?></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class='td-num bold'>Total:</td>
                    <td class='td-num bold'>$<?php echo $_SESSION['b_total'] ?></td>
                </tr>
                <!--PLAN C-->
                <tr>
                    <td>&#9744;</td>
                    <td>Plan C </td>
                    <td>&nbsp;</td>

                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>1st Inst.:</td>
                    <td class='td-num'><?php echo $_SESSION['c_down']; ?></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>2nd Inst.:</td>
                    <td class='td-num'><?php echo $_SESSION['c_give']; ?></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>3rd Inst.:</td>
                    <td class='td-num'><?php echo $_SESSION['c_give']; ?></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class='td-num bold'>Total:</td>
                    <td class='td-num bold'>$<?php echo $_SESSION['c_total'] ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="3"><hr style='border-color:black'></td>
                </tr>
                <tr>
                    <td class='underline'>Note:</td>
                    <td>&nbsp;</td>
                    <td class='underline'>Due Date:</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>Plan A / 1st Inst.</td>
                    <td>Upon Enrollment</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>2nd Inst.</td>
                    <td><?php
                        $d1 = new DateTime($due_first);
                        $d1 = $d1->format('M d, Y');
                        echo $d1;
                        ?></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>3rd Inst.</td>
                    <td><?php
                        $d2 = new DateTime($due_second);
                        $d2 = $d2->format('M d, Y');
                        echo $d2;
                        ?></td>
                </tr>
            </tbody>
        </table>

        <table class='fee-breakdown'>
            <tbody>
                <tr>
                    <td class='underline'>Breakdown of Fees:</td>
                    <td></td>
                </tr>
                <tr>
                    <td>Tuition Fee: US$</td>
                    <td><?php echo $_SESSION['tuition'] ?></td>
                </tr>
                <tr>
                    <td>Net Total Tuition Fee:</td>
                    <td><?php echo $_SESSION['net_tuition'] ?></td>
                </tr>
                <tr>
                    <td>Registration Fee:</td>
                    <td><?php echo $_SESSION['ir_fee'] ?></td>
                </tr>
                <tr>
                    <td>Miscellaneous Fee:</td>
                    <td><?php echo $_SESSION['misc'] ?></td>
                </tr>
                <!--SPECIAL ZONE-->
                <tr>
                    <td>Other: <?php echo $_SESSION['other_text']; ?></td>
                    <td><?php echo $_SESSION['other'] ?></td>
                </tr>
                <tr class='<?php
                if ($_SESSION['grad'] == 0) {
                    echo "hidden";
                }
                ?>'>
                    <td>Grad Fee (PhP):</td>
                    <td><?php echo $_SESSION['grad'] ?></td>
                </tr>
                <!--/ SPECIAL ZONE-->
                <tr>
                    <td>Add On (Plan B): US$</td>
                    <td><?php echo $_SESSION['b_add_on'] ?></td>
                </tr>
                <tr>
                    <td>Add On (Plan C): US$</td>
                    <td><?php echo $_SESSION['c_add_on'] ?></td>
                </tr>
                <tr>
                    <td>Type of Payment</td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="2" class='center-text'>&#9744; Cash &#9744; Check &#9744; Credit Card</td>
                </tr>
            </tbody>    
        </table>

        <table class='fee-breakdown bottom-breakdown'>
            <tbody>
                <tr>
                    <td>No. of subjects enrolled:</td>
                    <td><?php echo $_SESSION['tr_count'] ?></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>Tuition Fee: US$</td>
                    <td><?php echo $_SESSION['tuition'] ?></td>
                </tr>
                <tr>
                    <td>Net Total Tuition Fee:</td>
                    <td><?php echo $_SESSION['net_tuition'] ?></td>
                </tr>
                <tr>
                    <td>Registration Fee:</td>
                    <td><?php echo $_SESSION['ir_fee'] ?></td>
                </tr>
                <tr>
                    <td>Miscellaneous Fee:</td>
                    <td><?php echo $_SESSION['misc'] ?></td>
                </tr>
                <!--SPECIAL ZONE-->
                <tr>
                    <td>Other: <?php echo $_SESSION['other_text']; ?></td>
                    <td><?php echo $_SESSION['other'] ?></td>
                </tr>
                <tr class='<?php
                if ($_SESSION['grad'] == 0) {
                    echo "hidden";
                }
                ?>'>
                    <td>Grad Fee (PhP):</td>
                    <td><?php echo $_SESSION['grad'] ?></td>
                </tr>
                <!--/ SPECIAL ZONE-->
                <tr>
                    <td>Add On (Plan B): US$</td>
                    <td><?php echo $_SESSION['b_add_on'] ?></td>
                </tr>
                <tr>
                    <td>Add On (Plan C): US$</td>
                    <td><?php echo $_SESSION['c_add_on'] ?></td>
                </tr>
                <tr>
                    <td>Type of Payment</td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="2" class='center-text'>&#9744; Cash &#9744; Check &#9744; Credit Card</td>
                </tr>
            </tbody>    
        </table>



        <table class="student-info card-1">
            <tr>
                <td>Name of Student:</td>
                <td>
                    <span style='font-size:10px;'>
                        <?php
                        $full_name = $row['last_name'] . ', ' . $row['first_name'] . ' ' . $row['middle_name'];
                        echo strtoupper($full_name);
                        ?>
                    </span>
                </td>
                <td class="spacer"></td>
                <td>School Year:</td>
                <td>
                    <?php
                    $this_year = date('Y');
                    $last_year = date("Y", strtotime("-1 year", time()));
                    echo $school_year = $last_year . '-' . $this_year;
                    ?>
                </td>
    <!--                <td>Reg. No.</td>
                <td>
                <?php
                echo $reg_no = $this->db->get_where('sisfu_reference', array('ref_code' => 'REGCARD'))->row()->ref_no;
                ?>
                </td>-->
            </tr>
            <tr>
                <td>Student No.:</td>
                <td><?php echo $row['student_no'] ?></td>
                <td class="spacer"></td>
                <td>Term / Year Level:</td>
                <td>
                    <?php
                    echo $active_term . ' / ';
                    $level = $this->db->get_where('sisfu_levels_meta', array(
                                'level_id' => $row['level_id']
                            ))->row()->description;

                    if ($row['level_id'] == 0) {
                        echo $level = 'New Enrollee';
                    } else {

                        echo $level;
                    }
                    ?>
                </td>
            </tr>
            <tr>
                <td>Status / Scholarship:</td>
                <td class="td-compact">
                    <?php
                    echo $status = $this->db->get_where('sisfu_status_meta', array('status_id' => $row['status_id']))->row()->status_name;
                    echo ' / ';
                    $scholarship = $this->db->get_where('sisfu_scholarships_meta', array('scholarship_id' => $row['scholarship_id']))->row()->scholarship_name;
                    if ($row['scholarship_id'] == 0) {
                        echo $scholarship = 'No Scholarship';
                    } else {
                        if (strlen($scholarship) > 28) {
                            $scholarship = substr($scholarship, 0, 25);
                            $scholarship .= '...';
                            echo $scholarship;
                        } else {
                            echo $scholarship;
                        }
                    }
                    ?>
                </td>
                <td class="spacer"></td>
                <td>Course:</td>
                <td class="td-compact">
                        
                    <?php
                    echo $course = $this->db->get_where('sisfu_courses', array('course_id' => $row['course_id']))->row()->course_name;
                    ?>
                </td>
            </tr>
        </table>
        <!--SUBJECTS-->
        <table class="card-table-1 card-table">
            <thead>
                <tr class="tr-centered">
                    <th>Subjects Enrolled</th>
                    <th>Units</th>
                    <th>Day</th>
                    <th>Time</th>
                    <th>Rm</th>
                    <th>Sig</th>
                </tr>
            </thead>
            <tbody>
                <?php
                for ($x = 1; $x < 11; $x++):

                    $subjs_arr = explode(",", $row['enrolled_subjects']);
                    if ($subjs_arr[0] == '') {
                        $subjs_len = 0;
                    } else {
                        $subjs_len = count($subjs_arr);
                    }
                    $this->db->where('subject_id', $subjs_arr[$x - 1]);
                    $q = $this->db->get('sisfu_subjects');
                    $result = $q->result_array();

                    if ($q->num_rows() == 0) {
                        ?>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <?php
                    } else {
                        foreach ($result as $row2):
                            ?>
                            <tr>
                                <td><?php echo '&nbsp' . $x . '. ' . $row2['subject_name'] ?></td>
                                <td><?php echo $row2['units'] ?></td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <?php
                        endforeach;
                    }
                endfor;
                ?>
            </tbody>
        </table>
        <table class="student-info card-2">
            <tr>
                <td>Name of Student:</td>
                <td>
                    <span style='font-size:10px;'>
                        <?php
                        echo strtoupper($full_name);
                        ?>
                    </span>
                </td>
                <td class="spacer"></td>
                <td>School Year:</td>
                <td>
                    <?php
                    $this_year = date('Y');
                    $last_year = date("Y", strtotime("-1 year", time()));
                    echo $school_year = $last_year . '-' . $this_year;
                    ?>
                </td>
    <!--                <td>Reg. No.</td>
                <td>
                <?php
                echo $reg_no = $this->db->get_where('sisfu_reference', array('ref_code' => 'REGCARD'))->row()->ref_no;
                ?>
                </td>-->
            </tr>
            <tr>
                <td>Student No.:</td>
                <td><?php echo $row['student_no'] ?></td>
                <td class="spacer"></td>
                <td>Term / Year Level:</td>
                <td>
                    <?php
                    echo $active_term . ' / ';
                    $level = $this->db->get_where('sisfu_levels_meta', array(
                                'level_id' => $row['level_id']
                            ))->row()->description;

                    if ($row['level_id'] == 0) {
                        echo $level = 'New Enrollee';
                    } else {

                        echo $level;
                    }
                    ?>
                </td>
            </tr>
            <tr>
                <td>Status / Scholarship:</td>
                <td class="td-compact">
                    <?php
                    echo $status = $this->db->get_where('sisfu_status_meta', array('status_id' => $row['status_id']))->row()->status_name;
                    echo ' / ';
                    $scholarship = $this->db->get_where('sisfu_scholarships_meta', array('scholarship_id' => $row['scholarship_id']))->row()->scholarship_name;
                    if ($row['scholarship_id'] == 0) {
                        echo $scholarship = 'No Scholarship';
                    } else {

                        if (strlen($scholarship) > 28) {
                            $scholarship = substr($scholarship, 0, 25);
                            $scholarship .= '...';
                            echo $scholarship;
                        } else {
                            echo $scholarship;
                        }
                    }
                    ?>
                </td>
                <td class="spacer"></td>
                <td>Course:</td>
                <td class="td-compact">
                    <?php
                    echo $course = $this->db->get_where('sisfu_courses', array('course_id' => $row['course_id']))->row()->course_name;
                    ?>
                </td>
            </tr>
        </table>
        <!--SUBJECTS-->
        <table class="card-table-2 card-table">
            <thead>
                <tr class="tr-centered">
                    <th>Subjects Enrolled</th>
                    <th>Units</th>
                    <th>Day</th>
                    <th>Time</th>
                    <th>Rm</th>
                    <th>Sig</th>
                </tr>
            </thead>
            <tbody>
                <?php
                for ($x = 1; $x < 11; $x++):

                    $subjs_arr = explode(",", $row['enrolled_subjects']);
                    if ($subjs_arr[0] == '') {
                        $subjs_len = 0;
                    } else {
                        $subjs_len = count($subjs_arr);
                    }
                    $this->db->where('subject_id', $subjs_arr[$x - 1]);
                    $q = $this->db->get('sisfu_subjects');
                    $result = $q->result_array();

                    if ($q->num_rows() == 0) {
                        ?>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <?php
                    } else {
                        foreach ($result as $row2):
                            ?>
                            <tr>
                                <td><?php echo '&nbsp' . $x . '. ' . $row2['subject_name'] ?></td>
                                <td><?php echo $row2['units'] ?></td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <?php
                        endforeach;
                    }
                endfor;
                ?>
            </tbody>
        </table>

        <!--CARD 3--> 

        <table class="student-info card-3">
            <tr>
                <td>Name of Student:</td>
                <td>
                    <span style='font-size:10px;'>
                        <?php
                        echo strtoupper($full_name);
                        ?>
                    </span>
                </td>
                <td class="spacer"></td>
                <td>School Year:</td>
                <td>
                    <?php
                    $this_year = date('Y');
                    $last_year = date("Y", strtotime("-1 year", time()));
                    echo $school_year = $last_year . '-' . $this_year;
                    ?>
                </td>
    <!--                <td>Reg. No.</td>
                <td>
                <?php
                echo $reg_no = $this->db->get_where('sisfu_reference', array('ref_code' => 'REGCARD'))->row()->ref_no;
                ?>
                </td>-->
            </tr>
            <tr>
                <td>Student No.:</td>
                <td><?php echo $row['student_no'] ?></td>
                <td class="spacer"></td>
                <td>Term / Year Level:</td>
                <td>
                    <?php
                    echo $active_term . ' / ';
                    $level = $this->db->get_where('sisfu_levels_meta', array(
                                'level_id' => $row['level_id']
                            ))->row()->description;

                    if ($row['level_id'] == 0) {
                        echo $level = 'New Enrollee';
                    } else {

                        echo $level;
                    }
                    ?>
                </td>
            </tr>
            <tr>
                <td>Status / Scholarship:</td>
                <td class="td-compact">
                    <?php
                    echo $status = $this->db->get_where('sisfu_status_meta', array('status_id' => $row['status_id']))->row()->status_name;
                    echo ' / ';
                    $scholarship = $this->db->get_where('sisfu_scholarships_meta', array('scholarship_id' => $row['scholarship_id']))->row()->scholarship_name;
                    if ($row['scholarship_id'] == 0) {
                        echo $scholarship = 'No Scholarship';
                    } else {

                        if (strlen($scholarship) > 28) {
                            $scholarship = substr($scholarship, 0, 25);
                            $scholarship .= '...';
                            echo $scholarship;
                        } else {
                            echo $scholarship;
                        }
                    }
                    ?>
                </td>
                <td class="spacer"></td>
                <td>Course:</td>
                <td class="td-compact">
                    <?php
                    echo $course = $this->db->get_where('sisfu_courses', array('course_id' => $row['course_id']))->row()->course_name;
                    ?>
                </td>
            </tr>
        </table>
        <table class="card-table-3 card-table">
            <thead>
                <tr class="tr-centered">
                    <th>Subjects Enrolled</th>
                    <th>Units</th>
                </tr>
            </thead>
            <tbody>
                <?php
                for ($x = 1; $x < 11; $x++):

                    $subjs_arr = explode(",", $row['enrolled_subjects']);
                    if ($subjs_arr[0] == '') {
                        $subjs_len = 0;
                    } else {
                        $subjs_len = count($subjs_arr);
                    }
                    $this->db->where('subject_id', $subjs_arr[$x - 1]);
                    $q = $this->db->get('sisfu_subjects');
                    $result = $q->result_array();

                    if ($q->num_rows() == 0) {
                        ?>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <?php
                    } else {
                        foreach ($result as $row2):
                            ?>
                            <tr>
                                <td><?php echo '&nbsp' . $x . '. ' . $row2['subject_name'] ?></td>
                                <td><?php echo $row2['units'] ?></td>
                            </tr>
                            <?php
                        endforeach;
                    }
                endfor;
                ?>
            </tbody>
        </table>
    </div>
    <div class="hidden-print">
        <div class="well well-info">
            <i class="fa fa-info-circle"></i> <?php echo get_phrase('print_config') ?><br>
            <br>
            <strong>Paper size:</strong> 8 1/2 x 14in (LEGAL)<br>
            <strong>Margins:</strong> None
        </div>
        <div>
            <a href="<?php echo base_url() . 'index.php?admin/student_assess/' . $row['student_id'] ?>" class="btn btn-info"><i class="fa fa-arrow-left"></i> Back to <?php echo $full_name ?>'s assessment</a> 
            <button class="btn btn-primary" id="print"><i class="fa fa-print"></i> Print preview</button>
        </div>
    </div>
<?php endforeach; ?>
<script>
    $(document).ready(function () {
        $('#print').on('click', function () {
            window.print();
        });

        var beforePrint = function () {
            console.log('Functionality to run before printing.');
        };
        var afterPrint = function () {
            $.ajax({
                url: 'index.php?Admin/ajax_increment_reg_no', // define here controller then function name
                type: 'post',
                data: {data: 0}, // pass here your date variable into controller
                success: function (data) {
                    console.log('ajax request successful');
                    console.log(data);
                }
            });


            console.log('Functionality to run after printing');
        };

        if (window.matchMedia) {
            var mediaQueryList = window.matchMedia('print');
            mediaQueryList.addListener(function (mql) {
                if (mql.matches) {
                    beforePrint();
                } else {
                    afterPrint();
                }
            });
        }

        window.onbeforeprint = beforePrint;
        window.onafterprint = afterPrint;
    });
</script>