<?php
$edit_data = $this->db->get_where('admin', array('admin_id' => $param2))->result_array();
foreach ($edit_data as $row):
    ?>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary" data-collapsed="0">
                <div class="panel-heading">
                    <div class="panel-title"><span class="primary-color">
                        <i class="entypo-plus-circled"></i>
                        <?php echo get_phrase('edit_user'); ?></span>
                    </div>
                </div>
                <div class="panel-body">

                    <?php echo form_open(base_url() . 'index.php?admin/users/edit/' . $param2 , array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data')); ?>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('username'); ?></label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="username" 
                                   value="<?php echo $row['username']; ?>" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('email'); ?></label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="email" 
                                   value="<?php echo $row['email']; ?>" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="password" class="col-sm-3 control-label"><?php echo get_phrase('new_password'); ?></label>
                        <div class="col-sm-7">
                            <div class="input-group">
                                <input type="text" class="form-control" id="password" name="password" value="" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                                <span class="input-group-addon generate-password"><i class="fa fa-cog"></i> Generate Password</span>
                            </div> 
                        </div>
                    </div>
                    <!--ACCESS LEVEL-->
                    <div class="form-group">
                        <label for="access_level" class="col-sm-3 control-label"><?php echo get_phrase('access_level'); ?></label>

                        <div class="col-sm-7">
                            <select name="access_level" class="form-control" id="access_level" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                                <option value=""><?php echo get_phrase('select'); ?></option>
                                <?php
                                $visas = $this->db->get('sisfu_access_levels')->result_array();
                                foreach ($visas as $row2):
                                    ?>
                                    <option value="<?php echo $row2['access_level']; ?>"
                                            <?php echo ($row['access_level'] == $row2['access_level'] ? 'selected' :  '');?>
                                            >
                                        <?php echo $row2['access_level'] . ' - ' . $row2['definition']; ?>
                                    </option>
                                    <?php
                                endforeach;
                                ?>
                            </select>
                        </div> 
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-5">
                            <button type="submit" class="btn btn-default"><?php echo get_phrase('save'); ?></button>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
<?php endforeach; ?>

<script>
    $(document).ready(function () {
        var password = $('#password');

        function randomString(length, chars) {
            var result = '';
            for (var i = length; i > 0; --i)
                result += chars[Math.floor(Math.random() * chars.length)];
            return result;
        }

        $('.generate-password').on('click', function () {
            var newpass = randomString(8, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
            password.val(newpass);
        });

    });
</script>