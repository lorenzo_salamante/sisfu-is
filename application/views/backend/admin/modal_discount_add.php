<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary" data-collapsed="0">
            <div class="panel-heading">
                <div class="panel-title"><span class="primary-color">
                    <i class="entypo-plus-circled"></i>
                    <?php echo get_phrase('add_new_discount'); ?></span>
                </div>
            </div>
            <div class="panel-body">

                <?php echo form_open(base_url() . 'index.php?admin/discounts/create/', array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data')); ?>

                <div class="form-group">
                    <label for="discount_name" class="col-sm-3 control-label"><?php echo get_phrase('discount_name'); ?></label>

                    <div class="col-sm-7">
                        <input type="text" class="form-control" id="discount_name" name="discount_name" value="" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                    </div> 
                </div>

                <div class="form-group">
                    <label for="discount_value" class="col-sm-3 control-label"><?php echo get_phrase('discount_value'); ?></label>

                    <div class="col-sm-7">
                        <div class="input-group">
                            <input type="number" min="0" step="0.01" class="form-control" id="discount_value" name="discount_value" value="" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                            <span class="input-group-addon">%</span>
                        </div> 
                    </div> 
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-default"><?php echo get_phrase('add_discount'); ?></button>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        var password = $('#password');

        function randomString(length, chars) {
            var result = '';
            for (var i = length; i > 0; --i)
                result += chars[Math.floor(Math.random() * chars.length)];
            return result;
        }

        $('.generate-password').on('click', function () {
            var newpass = randomString(8, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
            password.val(newpass);
        });

    });
</script>