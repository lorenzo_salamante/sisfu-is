<?php
$edit_data = $this->db->get_where('sisfu_nationalities_meta', array('nationality_id' => $param2))->result_array();
foreach ($edit_data as $row):
    ?>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary" data-collapsed="0">
            <div class="panel-heading">
                <div class="panel-title"><span class="primary-color">
                    <i class="entypo-plus-circled"></i>
                    <?php echo get_phrase('edit_nationality'); ?></span>
                </div>
            </div>
            <div class="panel-body">

                <?php echo form_open(base_url() . 'index.php?admin/nationalities/edit/' . $param2, array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data')); ?>

                <div class="form-group">
                    <label for="nationality_name" class="col-sm-3 control-label"><?php echo get_phrase('nationality'); ?></label>

                    <div class="col-sm-7">
                        <input type="text" class="form-control" id="nationality_name" name="nationality_name" value="<?php echo $row['nationality_name'] ?>" data-validate="required" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                    </div> 
                </div>
                
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-default"><?php echo get_phrase('save'); ?></button>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
<?php endforeach;?>