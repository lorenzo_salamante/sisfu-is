
<a href="#" onclick="showAjaxModal('<?php echo base_url(); ?>index.php?modal/popup/modal_curriculum_add/');" 
   class="btn btn-primary pull-right">
    <i class="entypo-plus-circled"></i>
    <?php echo get_phrase('create_new_curriculum'); ?>
</a> 
<br><br>
<table class="table table-bordered datatable table-hover" id="table_export">
    <thead>
        <tr>
            <th>ID</th>
            <th><div><?php echo get_phrase('curriculum_name'); ?></div></th>
<th><div><?php echo get_phrase('curriculum_code'); ?></div></th>
<th><div><?php echo get_phrase('term'); ?></div></th>
<th><div><?php echo get_phrase('subjects'); ?></div></th>
<th><?php echo get_phrase('options'); ?></th>
</tr>
</thead>
<tbody>
    <?php
    $result = $this->db->get('sisfu_curriculum')->result_array();
    foreach ($result as $row):
        ?>
        <tr>
            <td><?php echo $row['curriculum_id'] ?></td>
            <td><?php echo $row['curriculum_name']; ?></td>
            <td><?php echo $row['curriculum_code']; ?></td>
            <td><?php echo $row['curriculum_term']; ?></td>
            <td>
                    <a href="#" onclick="showAjaxModal('<?php echo base_url(); ?>index.php?modal/popup/modal_curriculum_subjects/<?php echo $row['curriculum_id']; ?>');" 
                       class="btn btn-default btn-sm">
                        <i class="fa fa-list"></i>
                        <?php echo get_phrase('view_subjects'); ?>
                    </a> 
                    <a href="#" onclick="showAjaxModal('<?php echo base_url(); ?>index.php?modal/popup/modal_curriculum_add_subject/<?php echo $row['curriculum_id']; ?>');" 
                       class="btn btn-default btn-sm ">
                        <i class="fa fa-plus-square"></i>
                        <?php echo get_phrase('add_subjects'); ?>
                    </a> 
            </td>
            <td>
                <div class="btn-group">
                    <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                        Action <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-default pull-right" role="menu">

                        <!--user EDITING LINK--> 
                        <li>
                            <a href="#" onclick="showAjaxModal('<?php echo base_url(); ?>index.php?modal/popup/modal_curriculum_edit/<?php echo $row['curriculum_id']; ?>');">
                                <i class="fa fa-pencil"></i>
                                <?php echo get_phrase('edit'); ?>
                            </a>
                        </li>
                        <li class="divider"></li>

                        <!--user DELETION LINK--> 
                        <li>
                            <a href="#" onclick="confirm_modal('<?php echo base_url(); ?>index.php?admin/curriculum/delete/<?php echo $row['curriculum_id']; ?>');">
                                <i class="fa fa-trash-o"></i>
                                <?php echo get_phrase('delete'); ?>
                            </a>
                        </li>
                    </ul>
                </div>
            </td>

        </tr>

    <?php endforeach; ?>
</tbody>
</table>



<!-----  DATA TABLE EXPORT CONFIGURATIONS ---->                      
<script type="text/javascript">

    jQuery(document).ready(function ($)
    {
        var datatable = $("#table_export").dataTable({
            "sPaginationType": "bootstrap",
            "sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-3 col-left'i><'col-xs-9 col-right'p>>",
            "oTableTools": {
                "aButtons": [
                    {
                        "sExtends": "xls",
                        "mColumns": [1, 2, 3, 4, 5]
                    },
                    {
                        "sExtends": "pdf",
                        "mColumns": [1, 2, 3, 4, 5]
                    },
                    {
                        "sExtends": "print",
                        "fnSetText": "Press 'esc' to return",
                        "fnClick": function (nButton, oConfig) {
                            datatable.fnSetColumnVis(5, false);

                            this.fnPrint(true, oConfig);

                            window.print();

                            $(window).keyup(function (e) {
                                if (e.which == 27) {
                                    datatable.fnSetColumnVis(5, true);
                                }
                            });
                        },
                    },
                ]
            },
        });

        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });
    });

</script>