<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary" data-collapsed="0">
            <div class="panel-heading">
                <div class="panel-title" >
                    <span class="primary-color"><i class="entypo-plus-circled"></i>
                        <?php echo get_phrase('admission_form'); ?></span>
                </div>
            </div>
            <div class="panel-body">
                <?php echo form_open(base_url() . 'index.php?admin/student/create/', array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data')); ?>

                <?php
                $this->db->where('type', 'active_term');
                $settings = $this->db->get('settings')->result_array();
                foreach ($settings as $row):
                    $active_term = $row['description'];
                endforeach;

//                $query = 'SELECT * FROM sisfu_students WHERE student_id = (SELECT MAX(student_id) FROM sisfu_students)';
//                $last = $this->db->query($query)->result_array();
//                $last_id = 1;
//                foreach ($last as $row):
//                    $last_id = $row['student_id'] + 1;
//                endforeach;
                
                $last_id = $this->db->get_where('settings', array('type' => 'last_no'))->row()->description;
                ?>
                <!--NAMES-->
                <!--XXXXXXXXXXXXXXXX-->
                <div class="form-group">
                    <label for="student_no" class="col-sm-1 control-label"><?php echo get_phrase('student_no'); ?></label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="student_no" name="student_no" value="<?php echo date('y'); ?>-<?php echo $active_term ?>-<?php
                        if ($last_id < 10) {
                            echo('000' . $last_id);
                        } else if ($last_id < 100) {
                            echo('00' . $last_id);
                        } else if ($last_id < 1000) {
                            echo('0' . $last_id);
                        } else if ($last_id < 10000) {
                            echo('' . $last_id);
                        }
                        ?>">
                    </div>
                    <label for="field-1" class="col-sm-5 control-label"><?php echo get_phrase('photo'); ?></label>
                    <div class="col-sm-1">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-new thumbnail" style="width: 100px; height: 100px;" data-trigger="fileinput">
                                <img src="http://placehold.it/200x200" alt="...">
                            </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
                            <div>
                                <span class="btn btn-white btn-file">
                                    <span class="fileinput-new">Select image</span>
                                    <span class="fileinput-exists">Change</span>
                                    <input type="file" name="userfile" accept="image/*">
                                </span>
                                <a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <!--LAST NAME-->
                    <label for="last_name" class="col-sm-1 control-label"><?php echo get_phrase('last_name'); ?> <span class="required">*</span></label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="last_name" name="last_name" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" value="" autofocus>
                    </div>

                    <!--FIRST NAME-->
                    <label for="first_name" class="col-sm-1 control-label"><?php echo get_phrase('first_name'); ?> <span class="required">*</span></label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="first_name" name="first_name" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" value="" >
                    </div> 

                </div>
                <div class="form-group">
                    <!--MIDDLE NAME-->
                    <label for="middle_name" class="col-sm-1 control-label"><?php echo get_phrase('middle_name'); ?></label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" id="middle_name" name="middle_name" value="" >
                    </div> 

                    <!--NICKNAME-->
                    <label for="nickname" class="col-sm-1 control-label"><?php echo get_phrase('nickname'); ?></label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" id="nickname" name="nickname" value="" >
                    </div> 

                    <!--SUFFIX-->
                    <label for="suffix" class="col-sm-1 control-label"><?php echo get_phrase('suffix'); ?></label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" id="suffix" name="suffix" value="" >
                    </div> 
                </div>

                <div class="form-group">
                    <label for="department_id" class="col-sm-1 control-label"><?php echo get_phrase('school'); ?> <span class="required">*</span></label>

                    <!--DEPARTMENT-->
                    <div class="col-sm-4">
                        <select name="department_id" class="form-control" data-validate="required" id="department_id" 
                                data-message-required="<?php echo get_phrase('value_required'); ?>">
                            <option value=""><?php echo get_phrase('select'); ?></option>
                            <?php
                            $departments = $this->db->get('sisfu_departments')->result_array();
                            foreach ($departments as $row):
                                ?>
                                <option value="<?php echo $row['department_id']; ?>">
                                    <?php echo $row['department_name']; ?>
                                </option>
                                <?php
                            endforeach;
                            ?>
                        </select>
                    </div> 

                    <!--COURSES-->
                    <label for="course_id" class="col-sm-1 control-label"><?php echo get_phrase('course'); ?> <span class="required">*</span></label>

                    <div class="col-sm-3">
                        <select name="course_id" class="form-control" data-validate="required" id="course_id" 
                                data-message-required="<?php echo get_phrase('value_required'); ?>">
                            <option value="">Please choose a school first<?php // echo get_phrase('select');  ?></option>
                            <?php
//                            $courses = $this->db->get('sisfu_courses')->result_array();
//                            foreach ($courses as $row):
                            ?>
                                <!-- <option value="<?php // echo $row['course_id'];  ?>"> -->
                            <?php // echo $row['course_name']; ?>
                            <!--</option>-->
                            <?php
//                            endforeach;
                            ?>
                        </select>
                    </div> 
                </div>

                <div class="form-group">

                    <!--STATUS/ENTRY LEVEL-->
                    <label for="status_id" class="col-sm-1 control-label"><?php echo get_phrase('status'); ?> <span class="required">*</span></label>

                    <div class="col-sm-2">
                        <select name="status_id" class="form-control" data-validate="required" id="status_id" 
                                data-message-required="<?php echo get_phrase('value_required'); ?>">
                            <option value=""><?php echo get_phrase('select'); ?></option>
                            <?php
                            $status = $this->db->get('sisfu_status_meta')->result_array();
                            foreach ($status as $row):
                                ?>
                                <option value="<?php echo $row['status_id']; ?>">
                                    <?php echo $row['status_name']; ?>
                                </option>
                                <?php
                            endforeach;
                            ?>
                        </select>
                    </div> 
                    <!--DISCOUNT-->
                    <label for="discount_id" class="col-sm-1 control-label"><?php echo get_phrase('discount'); ?></label>
                    <div class="col-sm-3">
                        <select name="discount_id" class="form-control" id="discount_id">
                            <option value=""><?php echo get_phrase('no_discount'); ?></option>
                            <?php
                            $discounts = $this->db->get('sisfu_discounts_meta')->result_array();
                            foreach ($discounts as $row):
                                ?>
                                <option value="<?php echo $row['discount_id']; ?>">
                                    <?php echo $row['discount_name']; ?>
                                </option>
                                <?php
                            endforeach;
                            ?>
                        </select>
                    </div> 
                    <div class="if_scholar" style="display:none;">
                        <label for="scholarship_id" class="col-sm-1 control-label"><?php echo get_phrase('scholarship'); ?></label>
                        <!--SCHOLARSHIPS-->
                        <div class="col-sm-3">
                            <select name="scholarship_id" class="form-control" id="scholarship_id">
                                <option value=""><?php echo get_phrase('no_discount'); ?></option>
                                <?php
                                $scholarships = $this->db->get('sisfu_scholarships_meta')->result_array();
                                foreach ($scholarships as $row):
                                    ?>
                                    <option value="<?php echo $row['scholarship_id']; ?>">
                                        <?php echo $row['scholarship_name']; ?>
                                    </option>
                                    <?php
                                endforeach;
                                ?>
                            </select>
                        </div> 
                    </div> 

                </div>

                <div class="panel-heading">
                    <div class="panel-title" >
                        <span class="primary-color"><i class="entypo-plus-circled"></i>
                            <?php echo get_phrase('previous_education'); ?></span>
                    </div>
                </div>
                <div class="form-group" style="padding:0">
                </div>

                <!--PREVIOUS EDUCATION-->
                <div class="form-group">
                    <!--DEGREES-->
                    <label for="degree_id" class="col-sm-2 control-label"><?php echo get_phrase('highest_education'); ?> <span class="required">*</span></label>
                    <div class="col-sm-4">
                        <select name="degree_id" class="form-control" id="degree_id" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>"
                                >
                            <option value=""><?php echo get_phrase('select'); ?></option>
                            <?php
                            $degrees = $this->db->get('sisfu_degrees_meta')->result_array();
                            foreach ($degrees as $row):
                                ?>
                                <option value="<?php echo $row['degree_id']; ?>">
                                    <?php echo $row['degree_name'] . ' (' . $row['degree_abbrv'] . ')'; ?>
                                </option>
                                <?php
                            endforeach;
                            ?>
                        </select>
                    </div> 
                    <!--DEGREE YEAR COMPLETED-->
                    <label for="degree_year_completed" class="col-sm-1 control-label"><?php echo get_phrase('degree_year_completed'); ?> <span class="required">*</span></label>

                    <div class="col-sm-4">
                        <input type="number" class="form-control" id="degree_year_completed" name="degree_year_completed" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" value=""  >
                    </div> 
                </div>


                <!--LAST SCHOOL-->
                <div class="form-group if_transferee" style="display:none;">
                    <label for="other_school_id" class="col-sm-2 control-label"><?php echo get_phrase('other_school_id'); ?></label>
                    <div class="col-sm-4">
                        <select name="other_school_id" class="form-control" id="other_school_id" 
                                >
                            <option value=""><?php echo get_phrase('select'); ?></option>
                            <?php
                            $other_schools = $this->db->get('sisfu_other_schools')->result_array();
                            foreach ($other_schools as $row):
                                ?>
                                <option value="<?php echo $row['other_school_id']; ?>">
                                    <?php echo $row['other_school_name']; ?>
                                </option>
                                <?php
                            endforeach;
                            ?>
                        </select>
                    </div> 
                    <!--OTHER SCHOOL YEAR COMPLETED-->
                    <label for="other_school_year_completed" class="col-sm-1 control-label"><?php echo get_phrase('other_school_year_completed'); ?></label>

                    <div class="col-sm-4">
                        <input type="number" class="form-control" id="other_school_year_completed" name="other_school_year_completed" value="" >
                    </div> 
                </div>

                <!--HIGH SCHOOL-->
                <div class="form-group">
                    <label for="high_school_id" class="col-sm-2 control-label"><?php echo get_phrase('high_school_id'); ?> <span class="required">*</span></label>
                    <div class="col-sm-4">
                        <select name="high_school_id" class="form-control" id="high_school_id" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                            <option value=""><?php echo get_phrase('select'); ?></option>
                            <?php
                            $high_schools = $this->db->get('sisfu_other_schools')->result_array();
                            foreach ($high_schools as $row):
                                ?>
                                <option value="<?php echo $row['other_school_id']; ?>">
                                    <?php echo $row['other_school_name']; ?>
                                </option>
                                <?php
                            endforeach;
                            ?>
                        </select>
                    </div> 
                    <!--HIGH SCHOOL YEAR COMPLETED-->
                    <label for="high_school_year_completed" class="col-sm-1 control-label"><?php echo get_phrase('high_school_year_completed'); ?> <span class="required">*</span></label>

                    <div class="col-sm-4">
                        <input type="number" class="form-control" id="high_school_year_completed" name="high_school_year_completed" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" value=""  >
                    </div> 
                </div>

                <div class="panel-heading">
                    <div class="panel-title" >
                        <span class="primary-color"><i class="entypo-plus-circled"></i>
                            <?php echo get_phrase('personal_information'); ?></span>
                    </div>
                </div>
                <div class="form-group" style="padding:0">
                </div>

                <!--PERSONAL INFO-->
                <!--NATIONALITY-->
                <div class="form-group">
                    <label for="nationality_id" class="col-sm-1 control-label"><?php echo get_phrase('nationality'); ?> <span class="required">*</span></label>
                    <div class="col-sm-2">
                        <select name="nationality_id" class="form-control" id="nationality_id" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                            <option value=""><?php echo get_phrase('select'); ?></option>
                            <?php
                            $nationalities = $this->db->get('sisfu_nationalities_meta')->result_array();
                            foreach ($nationalities as $row):
                                ?>
                                <option value="<?php echo $row['nationality_id']; ?>">
                                    <?php echo $row['nationality_name']; ?>
                                </option>
                                <?php
                            endforeach;
                            ?>
                        </select>
                    </div> 

                    <!--SEX-->
                    <label for="sex" class="col-sm-1 control-label"><?php echo get_phrase('sex'); ?> <span class="required">*</span></label>

                    <div class="col-sm-2">
                        <select name="sex" id="sex" class="form-control" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                            <option value=""><?php echo get_phrase('select'); ?></option>
                            <option value="M"><?php echo get_phrase('male'); ?></option>
                            <option value="F"><?php echo get_phrase('female'); ?></option>
                        </select>
                    </div> 
                    <!--BIRTH DATE-->
                    <label for="birth_date" class="col-sm-1 control-label"><?php echo get_phrase('birthday'); ?> <span class="required">*</span></label>

                    <div class="col-sm-2">
                        <input type="date" class="form-control" name="birth_date" id="birth_date" value="" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                    </div> 

                </div>

                <div class="panel-heading">
                    <div class="panel-title" >
                        <span class="primary-color"><i class="entypo-plus-circled"></i>
                            <?php echo get_phrase('contact_information'); ?></span>
                    </div>
                </div>
                <div class="form-group" style="padding:0">
                </div>

                <!--CONTACT INFO-->
                <!--CURRENT ADDRESS-->
                <div class="form-group">
                    <label for="current_address" class="col-sm-2 control-label"><?php echo get_phrase('current_address'); ?> <span class="required">*</span></label>

                    <div class="col-sm-7">
                        <input type="text" class="form-control" id="current_address" name="current_address" value="" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                    </div> 
                </div>
                <!--PERMANENT ADDRESS-->
                <div class="form-group">
                    <label for="permanent_address" class="col-sm-2 control-label"><?php echo get_phrase('permanent_address'); ?> <span class="required">*</span></label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control" id="permanent_address" name="permanent_address" value="" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                    </div> 
                    <div class="col-sm-1">
                        <a class="btn btn-primary same-1"> <i class="entypo-plus-circled"></i> Same as <?php echo get_phrase('current_address'); ?></a>
                    </div> 
                </div>

                <div class="form-group">
                    <!--EMAIL ADDRESS-->
                    <label for="email" class="col-sm-1 control-label"><?php echo get_phrase('email'); ?> <span class="required">*</span></label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="email" name="email" value="" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                    </div> 

                    <!--CONTACT NO-->
                    <label for="contact_no" class="col-sm-1 control-label"><?php echo get_phrase('contact_no'); ?> <span class="required">*</span></label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="contact_no" name="contact_no" value="" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                    </div> 

                </div>
                <div class="form-group">

                    <!--VISA TYPE-->
                    <label for="visa_id" class="col-sm-1 control-label"><?php echo get_phrase('visa_id'); ?> </label>

                    <div class="col-sm-3">
                        <select name="visa_id" class="form-control" id="visa_id">
                            <option value=""><?php echo get_phrase('select'); ?></option>
                            <?php
                            $visas = $this->db->get('sisfu_visa_meta')->result_array();
                            foreach ($visas as $row):
                                ?>
                                <option value="<?php echo $row['visa_id']; ?>">
                                    <?php echo $row['visa_name']; ?>
                                </option>
                                <?php
                            endforeach;
                            ?>
                        </select>
                    </div> 
                    <!--VISA EXPIRATION DATE-->
                    <label for="visa_expiration" class="col-sm-1 control-label"><?php echo get_phrase('visa_expiration'); ?></label>

                    <div class="col-sm-4">
                        <input type="text" class="form-control datepicker" name="visa_expiration" id="visa_expiration" value="" data-start-view="2">
                    </div> 
                </div> 

                <div class="panel-heading">
                    <div class="panel-title" >
                        <span class="primary-color"><i class="entypo-plus-circled"></i>
                            <?php echo get_phrase('father_information'); ?></span>
                    </div>
                </div>
                <div class="form-group" style="padding:0">
                </div>
                <!--FATHER INFO-->
                <div class="form-group">
                    <!--NAME-->
                    <label for="father_name" class="col-sm-1 control-label"><?php echo get_phrase('father_name'); ?></label>

                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="father_name" name="father_name" value="" >
                    </div> 
                    <!--ADDRESS-->
                    <label for="father_address" class="col-sm-1 control-label"><?php echo get_phrase('father_address'); ?></label>

                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="father_address" name="father_address" value="" >
                    </div> 
                    <div class="col-sm-1">
                        <a class="btn btn-primary same-2"> <i class="entypo-plus-circled"></i> Same as <?php echo get_phrase('current_address'); ?></a>
                    </div> 

                </div>
                <!--CONTACT NO-->
                <div class="form-group">
                    <label for="father_contact" class="col-sm-1 control-label"><?php echo get_phrase('father_contact'); ?></label>

                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="father_contact" name="father_contact" value="">
                    </div> 

                    <!--EMAIL ADDRESS-->
                    <label for="father_email" class="col-sm-2 control-label"><?php echo get_phrase('father_email'); ?></label>

                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="father_email" name="father_email" value="" placeholder="email@example.com">
                    </div> 
                </div>
                <input type="hidden" id="father_code" name="father_code" value="1" >
                <div class="panel-heading">
                    <div class="panel-title" >
                        <span class="primary-color"><i class="entypo-plus-circled"></i>
                            <?php echo get_phrase('mother_information'); ?></span>
                    </div>
                </div>
                <div class="form-group" style="padding:0">
                </div>
                <!--MOTHER INFO-->
                <!--NAME-->
                <div class="form-group">
                    <label for="mother_name" class="col-sm-1 control-label"><?php echo get_phrase('mother_name'); ?></label>

                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="mother_name" name="mother_name" value="">
                    </div> 
                    <!--ADDRESS-->
                    <label for="mother_address" class="col-sm-1 control-label"><?php echo get_phrase('mother_address'); ?></label>

                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="mother_address" name="mother_address" value="">
                    </div> 
                    <div class="col-sm-1">
                        <a class="btn btn-primary same-3"> <i class="entypo-plus-circled"></i> Same as <?php echo get_phrase('current_address'); ?></a>
                    </div> 
                </div>
                <div class="form-group">
                    <!--CONTACT NO-->
                    <label for="mother_contact" class="col-sm-1 control-label"><?php echo get_phrase('mother_contact'); ?></label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="mother_contact" name="mother_contact" value="">
                    </div> 

                    <!--EMAIL ADDRESS-->
                    <label for="mother_email" class="col-sm-2 control-label"><?php echo get_phrase('mother_email'); ?></label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="mother_email" name="mother_email" value="" placeholder="email@example.com">
                    </div> 
                </div>
                <input type="hidden" id="mother_code" name="mother_code" value="2">
                <div class="panel-heading">
                    <div class="panel-title" >
                        <span class="primary-color"><i class="entypo-plus-circled"></i>
                            <?php echo get_phrase('guardian_information'); ?></span>
                    </div>
                </div>
                <div class="form-group" style="padding:0">
                </div>
                <!--GUARDIAN INFO-->
                <div class="form-group">
                    <!--NAME-->
                    <label for="guardian_name" class="col-sm-1 control-label"><?php echo get_phrase('guardian_name'); ?></label>

                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="guardian_name" name="guardian_name" value="">
                    </div> 
                    <!--ADDRESS-->
                    <label for="guardian_address" class="col-sm-1 control-label"><?php echo get_phrase('guardian_address'); ?></label>

                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="guardian_address" name="guardian_address" value="">
                    </div> 
                    <div class="col-sm-1">
                        <a class="btn btn-primary same-4"> <i class="entypo-plus-circled"></i> Same as <?php echo get_phrase('current_address'); ?></a>
                    </div> 
                </div>
                <!--CONTACT NO-->
                <div class="form-group">
                    <label for="guardian_contact" class="col-sm-1 control-label"><?php echo get_phrase('guardian_contact'); ?></label>

                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="guardian_contact" name="guardian_contact" value="">
                    </div> 
                    <!--EMAIL ADDRESS-->
                    <label for="guardian_email" class="col-sm-2 control-label"><?php echo get_phrase('guardian_email'); ?></label>

                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="guardian_email" name="guardian_email" value="" placeholder="email@example.com">
                    </div> 
                </div>
                <input type="hidden" id="guardian_code" name="guardian_code" value="3">

                <!--REMARKS-->
                <div class="form-group">
                    <label for="remarks" class="col-sm-1 control-label"><?php echo get_phrase('remarks'); ?></label>

                    <div class="col-sm-8">
                        <textarea class="form-control" id="remarks" name="remarks" value="" style="resize:vertical"> </textarea>
                    </div> 
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-1 col-sm-5">
                        <button type="submit" class="btn btn-info"><?php echo get_phrase('admit_student'); ?></button>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    function get_class_sections(class_id) {

        $.ajax({
            url: '<?php echo base_url(); ?>index.php?admin/get_class_section/' + class_id,
            success: function (response)
            {
                jQuery('#section_selector_holder').html(response);
            }
        });

    }

</script>

<script>

    $(document).ready(function () {
        var status_id = $('#status_id');
        var if_transferee = $('.if_transferee');
        var if_scholar = $('.if_scholar');
        var other_school_id = $('#other_school_id');
        var department_id = $('#department_id');
        var course_id = $('#course_id');
        var other_school_year_completed = $('#other_school_year_completed');

        status_id.on('change', function () {
            if ($('#status_id option:selected').text().trim() == "Transferee") {
                if_transferee.show('slow');
                hide_scholar();
            }
            else if ($('#status_id option:selected').text().trim() == "Scholar") {
                if_scholar.show('slow');
                hide_transferee();
            }
            else {
                hide_transferee();
                hide_scholar();
            }
        });

        function hide_transferee() {
            if_transferee.hide('slow');
            $('#other_school_id option:selected').removeProp('selected');
            other_school_year_completed.attr('value', '');
            other_school_year_completed.val('');
        }
        function hide_scholar() {
            if_scholar.hide('slow');
            $('#scholarship_id option:selected').removeProp('selected');
        }

        department_id.on("change", function () {

            $.ajax({
                url: 'index.php?Admin/ajax_get_courses', // define here controller then function name
                type: 'post',
                data: {department_id: department_id.val()}, // pass here your date variable into controller
                success: function (data) {
                    var data = (JSON.parse(data)); //courses array
                    course_id.empty();

                    if (data.length === 0) {
                        course_id.append('<option>No course(s) available</option>')
                    }
                    else {

                        course_id.append('<option>Select</option>')
                        for (var i = 0; i < data.length; i++) {
                            course_id.append('<option value=' + data[i].course_id + '>' + data[i].course_name + '</option>');
                        }

                    }

                }
            });

        });

    });
</script>