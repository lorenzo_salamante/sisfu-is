<div class="row">
    <div class="col-md-8">
        <div class="row">
            <!-- CALENDAR-->
            <div class="col-md-12 col-xs-12">    
                <div class="panel panel-primary " data-collapsed="0">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <i class="fa fa-calendar"></i>
                            <?php echo get_phrase('event_schedule'); ?>
                        </div>
                    </div>
                    <div class="panel-body" style="padding:0px;">
                        <div class="calendar-env">
                            <div class="calendar-body">
                                <div id="notice_calendar"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="row">
            <div class="col-md-12">

                <div class="tile-stats tile-cyan">
                    <div class="icon"><i class="fa fa-group"></i></div>

                    <?php
                    $q = 0;

                    $this->db->where('status_id', '1');
                    $q += $this->db->count_all_results('sisfu_students');

                    $this->db->where('status_id', '2');
                    $q += $this->db->count_all_results('sisfu_students');

                    $this->db->where('status_id', '3');
                    $q += $this->db->count_all_results('sisfu_students');

                    $this->db->where('status_id', '4');
                    $q += $this->db->count_all_results('sisfu_students');

                    $this->db->where('status_id', '6');
                    $q += $this->db->count_all_results('sisfu_students');

                    $this->db->where('status_id', '7');
                    $q += $this->db->count_all_results('sisfu_students');

                    $this->db->where('status_id', '8');
                    $q += $this->db->count_all_results('sisfu_students');

                    $this->db->where('status_id', '9');
                    $q += $this->db->count_all_results('sisfu_students');

                    $this->db->where('status_id', '10');
                    $q += $this->db->count_all_results('sisfu_students');

                    $this->db->where('status_id', '11');
                    $q += $this->db->count_all_results('sisfu_students');

                    $this->db->where('status_id', '12');
                    $q += $this->db->count_all_results('sisfu_students');

                    $this->db->where('status_id', '15');
                    $q += $this->db->count_all_results('sisfu_students');

                    $this->db->where('status_id', '16');
                    $q += $this->db->count_all_results('sisfu_students');
                    ?>

                    <div class="num" data-start="0" data-end="<?php echo $q; ?>" 
                         data-postfix="" data-duration="1500" data-delay="0">0</div>

                    <h3><?php echo get_phrase('active_students'); ?></h3>
                    <p>Total active students in this term</p>
                </div>
            </div>
            <div class="col-md-12">

                <div class="tile-stats tile-green">
                    <div class="icon"><i class="fa fa-group"></i></div>

                    <?php
                    $this->db->not_like('nationality_name', 'Filipino');
                    $nationality_arr = $this->db->get('sisfu_nationalities_meta');

                    $q2 = 0;
                    foreach ($nationality_arr->result_array() as $row) {
                        $this->db->where('nationality_id', $row['nationality_id']);
                        $q2 += $this->db->count_all_results('sisfu_students');
                    }
                    ?>
                    <div class="num" data-start="0" data-end="<?php echo $q2; ?>" 
                         data-postfix="" data-duration="1500" data-delay="0">0</div>

                    <h3><?php echo get_phrase('foreign_students'); ?></h3>
                    <p>Total active foreign students in this term</p>
                </div>
            </div>
            <div class="col-md-12">

                <div class="tile-stats tile-plum">
                    <div class="icon"><i class="fa fa-group"></i></div>

                    <?php
                    $this->db->where('scholarship_id >', 0);
                    $q3 = $this->db->count_all_results('sisfu_students');
                    ?>
                    <div class="num" data-start="0" data-end="<?php echo $q3; ?>" 
                         data-postfix="" data-duration="1500" data-delay="0">0</div>

                    <h3><?php echo get_phrase('scholars'); ?></h3>
                    <p>Total active scholars in this term</p>
                </div>
            </div>
            <div class="col-md-12">

                <div class="tile-stats tile-red">
                    <div class="icon"><i class="fa fa-group"></i></div>
                    <div class="num" data-start="0" data-end="<?php echo $this->db->count_all('sisfu_students'); ?>"
                         data-postfix="" data-duration="1500" data-delay="0">0</div>
             <!--<div class="num" data-start="0" data-end="<?php // echo count($q)      ?>"-->


                    <h3><?php echo get_phrase('students'); ?></h3>
                    <p>Total student records in the database</p>
                </div>

            </div>
            <!--            <div class="col-md-12">
                        
                            <div class="tile-stats tile-aqua">
                                <div class="icon"><i class="entypo-user"></i></div>
                                <div class="num" data-start="0" data-end="<?php // echo $this->db->count_all('parent');      ?>" 
                                            data-postfix="" data-duration="500" data-delay="0">0</div>
                                
                                <h3><?php // echo get_phrase('parents');      ?></h3>
                               <p>Total parents</p>
                            </div>
                            
                        </div>-->
            <!--            <div class="col-md-12">
                        
                            <div class="tile-stats tile-green">
                                <div class="icon"><i class="entypo-users"></i></div>
                                <div class="num" data-start="0" data-end="<?php // echo $this->db->count_all('teacher');      ?>" 
                                            data-postfix="" data-duration="800" data-delay="0">0</div>
                                
                                <h3><?php // echo get_phrase('teacher');      ?></h3>
                               <p>Total teachers</p>
                            </div>
                            
                        </div>-->
            <!--            <div class="col-md-12">
                        
                            <div class="tile-stats tile-blue">
                                <div class="icon"><i class="entypo-chart-bar"></i></div>
            <?php
//							$check	=	array(	'date' => date('Y-m-d') , 'status' => '1' );
//							$query = $this->db->get_where('attendance' , $check);
//							$present_today		=	$query->num_rows();
            ?>
                                <div class="num" data-start="0" data-end="<?php // echo $present_today;    ?>" 
                                            data-postfix="" data-duration="500" data-delay="0">0</div>
                                
                                <h3><?php // echo get_phrase('attendance');      ?></h3>
                               <p>Total present student today</p>
                            </div>
                            
                        </div>-->
        </div>
    </div>

</div>



<script>
    $(document).ready(function () {

        var calendar = $('#notice_calendar');

        $('#notice_calendar').fullCalendar({
            header: {
                left: 'title',
                right: 'today prev,next'
            },
            //defaultView: 'basicWeek',

            editable: false,
            firstDay: 1,
            height: 530,
            droppable: false,
            events: [
<?php
$notices = $this->db->get('noticeboard')->result_array();
foreach ($notices as $row):
    ?>
                    {
                        title: "<?php echo $row['notice_title']; ?>",
                        start: new Date(<?php echo date('Y', $row['create_timestamp']); ?>, <?php echo date('m', $row['create_timestamp']) - 1; ?>, <?php echo date('d', $row['create_timestamp']); ?>),
                        end: new Date(<?php echo date('Y', $row['create_timestamp']); ?>, <?php echo date('m', $row['create_timestamp']) - 1; ?>, <?php echo date('d', $row['create_timestamp']); ?>)
                    },
    <?php
endforeach
?>

            ]
        });
    });
</script>


