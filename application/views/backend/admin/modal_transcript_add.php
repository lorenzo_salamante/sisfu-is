<?php
$student_id = $this->uri->segment(4, 0);
$term_code = $this->uri->segment(5, 0);
$acad_year = $this->uri->segment(6, 0);

?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary" data-collapsed="0">
            <div class="panel-heading">
                <div class="panel-title"><span class="primary-color">
                    <i class="entypo-plus-circled"></i>
                    <?php echo get_phrase('add_new_subject'); ?></span>
                </div>
            </div>
            <div class="panel-body">

                <?php echo form_open(base_url() . 'index.php?admin/student/transcript_add/' . $student_id . '/' . $term_code . '/' . $acad_year, array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data')); ?>
                
                <div class="form-group">
                    <label for="subject_code" class="col-sm-3 control-label"><?php echo get_phrase('subject'); ?> </label>

                    <div class="col-sm-7">
                       
                        
                        <select name="subject_code" class="form-control" data-validate="required" id="subject_code">
                            <?php
                            $this->db->order_by("subject_name", "asc");
                            $subjects = $this->db->get('sisfu_subjects')->result_array();
                            foreach ($subjects as $row):
                                ?>
                                <option value="<?php echo $row['subject_code']; ?>" >
                                    <?php echo $row['subject_name']; ?> - <?php echo $row['subject_code']; ?>
                                </option>
                                <?php
                            endforeach;
                            ?>
                        </select>
                        
                        
                    </div> 
                </div>
                
                
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-default"><?php echo get_phrase('add_subject'); ?></button>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>