<?php
$edit_data = $this->db->get_where('sisfu_subjects', array('subject_id' => $param2))->result_array();
foreach ($edit_data as $row):
    ?>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary" data-collapsed="0">
            <div class="panel-heading">
                <div class="panel-title"><span class="primary-color">
                    <i class="entypo-plus-circled"></i>
                    <?php echo get_phrase('edit_subject'); ?></span>
                </div>
            </div>
            <div class="panel-body">

                <?php echo form_open(base_url() . 'index.php?admin/subjects/edit/' . $param2, array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data')); ?>

                <div class="form-group">
                    <label for="subject_code" class="col-sm-3 control-label"><?php echo get_phrase('subject_code'); ?> </label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control" id="subject_code" name="subject_code" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" value="<?php echo $row['subject_code']; ?>" autofocus>
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="partner_code" class="col-sm-3 control-label"><?php echo get_phrase('partner_code'); ?> </label>

                    <div class="col-sm-7">
                        <input type="text" class="form-control" id="partner_code" name="partner_code" value="<?php echo $row['partner_code']; ?>" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                    </div> 
                </div>
                
                <div class="form-group">
                    <label for="subject_name" class="col-sm-3 control-label"><?php echo get_phrase('subject_name'); ?> </label>

                    <div class="col-sm-7">
                        <input type="text" class="form-control" id="subject_name" name="subject_name" value="<?php echo $row['subject_name']; ?>" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                    </div> 
                </div>
                
                <div class="form-group">
                    <label for="units" class="col-sm-3 control-label"><?php echo get_phrase('units'); ?> </label>

                    <div class="col-sm-7">
                        <input type="number" class="form-control" id="units" name="units" value="<?php echo $row['units']; ?>" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" placeholder="0.00">
                    </div> 
                </div>
                
                <div class="form-group">
                    <label for="unit_level" class="col-sm-3 control-label"><?php echo get_phrase('unit_level'); ?></label>

                    <div class="col-sm-7">
                        <input type="text" class="form-control" id="unit_level" name="unit_level" value="<?php echo $row['unit_level']; ?>">
                    </div> 
                </div>
                

                <div class="form-group">
                    <label for="department_id" class="col-sm-3 control-label"><?php echo get_phrase('school_code'); ?>  </label>

                    <div class="col-sm-7">
                        <select name="department_id" class="form-control" id="department_id" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                            <option value=""><?php echo get_phrase('select'); ?></option>
                            <?php
                            $access = $this->db->get('sisfu_departments')->result_array();
                            foreach ($access as $row2):
                                ?>
                                <option value="<?php echo $row2['department_id']; ?>" <?php echo ($row['department_id'] == $row2['department_id'] ? 'selected' :  '');?>
                                        >
                                    <?php echo $row2['department_code'] . ' - ' . $row2['department_name'] ?>
                                </option>
                                <?php
                            endforeach;
                            ?>
                        </select>
                    </div> 
                </div>
                
                

                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-default"><?php echo get_phrase('save'); ?></button>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
<?php endforeach;?>