<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary" data-collapsed="0">
            <div class="panel-heading">
                <div class="panel-title"><span class="primary-color">
                        <i class="entypo-plus-circled"></i>
                        <?php echo get_phrase('add_subject_to_curriculum'); ?></span>
                </div>
            </div>
            <div class="panel-body">

                <?php echo form_open(base_url() . 'index.php?admin/curriculum/add_subject/' . $param2, array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data')); ?>

                <table class="table table-bordered datatable" id="subjs_table">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th><?php echo get_phrase('subject_name'); ?></th>
                            <th><?php echo get_phrase('subject_code'); ?></th>
                            <th><?php echo get_phrase('options'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $result = $this->db->get('sisfu_subjects')->result_array();
                        foreach ($result as $row):
                            ?>
                            <tr>
                                <td><?php echo $row['subject_id'] ?> </td>
                                <td><?php echo $row['subject_name'] ?> </td>
                                <td><?php echo $row['subject_code'] ?> </td>
                                <td><a href="<?php echo base_url() . 'index.php?admin/curriculum/add_subject/' . $param2 . "/" . $row['subject_id']?>" class="btn btn-default btn-sm"><i class="fa fa-plus-square"></i> Add</a></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>


                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
<script>
    jQuery(document).ready(function ($)
    {
        var datatable = $("#subjs_table").dataTable({
            "sPaginationType": "bootstrap"
        });

        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });
    });
</script>