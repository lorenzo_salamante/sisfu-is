<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary" data-collapsed="0">
            <div class="panel-heading">
                <div class="panel-title"><span class="primary-color">
                    <i class="entypo-plus-circled"></i>
                    <?php echo get_phrase('add_new_subject'); ?></span>
                </div>
            </div>
            <div class="panel-body">

                <?php echo form_open(base_url() . 'index.php?admin/subjects/create/', array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data')); ?>

                <div class="form-group">
                    <label for="subject_code" class="col-sm-3 control-label"><?php echo get_phrase('subject_code'); ?></label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control" id="subject_code" name="subject_code" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" value="" autofocus>
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="partner_code" class="col-sm-3 control-label"><?php echo get_phrase('partner_code'); ?></label>

                    <div class="col-sm-7">
                        <input type="text" class="form-control" id="partner_code" name="partner_code" value="" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                    </div> 
                </div>
                
                <div class="form-group">
                    <label for="subject_name" class="col-sm-3 control-label"><?php echo get_phrase('subject_name'); ?></label>

                    <div class="col-sm-7">
                        <input type="text" class="form-control" id="subject_name" name="subject_name" value="" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                    </div> 
                </div>
                
                <div class="form-group">
                    <label for="units" class="col-sm-3 control-label"><?php echo get_phrase('units'); ?></label>

                    <div class="col-sm-7">
                        <input type="number" class="form-control" id="units" name="units" value="" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" placeholder="0.00" min="0">
                    </div> 
                </div>
                
                <div class="form-group">
                    <label for="unit_level" class="col-sm-3 control-label"><?php echo get_phrase('unit_level'); ?></label>

                    <div class="col-sm-7">
                        <input type="text" class="form-control" id="unit_level" name="unit_level" value="" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                    </div> 
                </div>
                

                <div class="form-group">
                    <label for="department_id" class="col-sm-3 control-label"><?php echo get_phrase('school_code'); ?> </label>

                    <div class="col-sm-7">
                        <select name="department_id" class="form-control" id="department_id" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                            <option value=""><?php echo get_phrase('select'); ?></option>
                            <?php
                            $access = $this->db->get('sisfu_departments')->result_array();
                            foreach ($access as $row):
                                ?>
                                <option value="<?php echo $row['department_id']; ?>">
                                    <?php echo $row['department_code'] . ' - ' . $row['department_name'] ?>
                                </option>
                                <?php
                            endforeach;
                            ?>
                        </select>
                    </div> 
                </div>
                
                

                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-default"><?php echo get_phrase('add_subject'); ?></button>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        var password = $('#password');

        function randomString(length, chars) {
            var result = '';
            for (var i = length; i > 0; --i)
                result += chars[Math.floor(Math.random() * chars.length)];
            return result;
        }

        $('.generate-password').on('click', function () {
            var newpass = randomString(8, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
            password.val(newpass);
        });

    });
</script>