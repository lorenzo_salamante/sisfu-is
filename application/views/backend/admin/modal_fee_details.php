<?php $student_id = $this->uri->segment(4, 0); ?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary" data-collapsed="0">
            <div class="panel-body">

                <?php echo form_open(base_url() . 'index.php?admin/student/do_checklist/' . $student_id, array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data')); ?>

                <div>
                    <div class="row">
                        <div class="col-sm-12">
                            <?php
                            $this->db->where('student_id', $student_id);
                            $result = $this->db->get('sisfu_students')->result_array();
                            foreach ($result as $row):

                            endforeach;
                            ?>
                            <div class="panel-heading">
                                <div class="panel-title" >
                                    <span class="primary-color"><i class="fa fa-info-circle"></i>
                                        Breakdown of fees</span>
                                </div>
                            </div>
                            <div class="form-group" style="padding:0">
                            </div>
                            <hr>
                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label">No. of subjects enrolled</label>

                                <div class="col-sm-2 col-sm-offset-4">
                                    <input type="text" class="form-control" name="username" readonly
                                           value="<?php echo $row['username']; ?>123">
                                </div>
                            </div>
                            <hr>

                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label">Tuition Fee</label>

                                <div class="col-sm-3">
                                    <select class="form-control" readonly>
                                        <option>USD</option>
                                        <option>GBP</option>
                                        <option>PHP</option>
                                    </select>
                                </div>
                                <div class="col-sm-3 ">
                                    <input type="text" class="form-control right-text" name="username" readonly
                                           value="<?php echo $row['username']; ?>2500">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label">Net tuition fee</label>

                                <div class="col-sm-3 col-sm-offset-3">
                                    <input type="text" class="form-control right-text" name="username" readonly
                                           value="<?php echo $row['username']; ?>25000">
                                </div>
                            </div>
                            <hr>

                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label">Other fees:</label>

                            </div>
                            <div class="form-group">
                                <label for="field-1" class="col-sm-3  control-label">Registration Fee:</label>

                                <div class="col-sm-3 col-sm-offset-3 ">
                                    <input type="text" class="form-control right-text" name="username" readonly
                                           value="<?php echo $row['username']; ?>25000">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="field-1" class="col-sm-3  control-label">Miscellaneous Fee:</label>

                                <div class="col-sm-3 col-sm-offset-3">
                                    <input type="text" class="form-control right-text" name="username" readonly
                                           value="<?php echo $row['username']; ?>25000">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="field-1" class="col-sm-3  control-label">Application Fee:</label>

                                <div class="col-sm-3 col-sm-offset-3">
                                    <input type="text" class="form-control right-text" name="username" readonly
                                           value="<?php echo $row['username']; ?>25000">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="field-1" class="col-sm-3  control-label">Admin Fee:</label>

                                <div class="col-sm-3 col-sm-offset-3">
                                    <input type="text" class="form-control right-text" name="username" readonly
                                           value="<?php echo $row['username']; ?>25000">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="field-1" class="col-sm-3  control-label">Math Enrichment:</label>

                                <div class="col-sm-3 col-sm-offset-3">
                                    <input type="text" class="form-control right-text" name="username" readonly
                                           value="<?php echo $row['username']; ?>25000">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="field-1" class="col-sm-3  control-label">RSWL:</label>

                                <div class="col-sm-3 col-sm-offset-3">
                                    <input type="text" class="form-control right-text" name="username" readonly
                                           value="<?php echo $row['username']; ?>25000">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="field-1" class="col-sm-3  control-label">Tutorial Fee:</label>

                                <div class="col-sm-3 col-sm-offset-3">
                                    <input type="text" class="form-control right-text" name="username" readonly
                                           value="<?php echo $row['username']; ?>25000">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="field-1" class="col-sm-3  control-label">ESL:</label>

                                <div class="col-sm-3 col-sm-offset-3">
                                    <input type="text" class="form-control right-text" name="username" readonly
                                           value="<?php echo $row['username']; ?>25000">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="field-1" class="col-sm-3  control-label">Other:</label>

                                <div class="col-sm-3 col-sm-offset-3">
                                    <input type="text" class="form-control right-text" name="username" readonly
                                           value="<?php echo $row['username']; ?>25000">
                                </div>
                            </div>
                            <div class="form-group">

                                <div class="col-sm-5 col-sm-offset-4">
                                    <input type="text" class="form-control right-text" name="username" readonly
                                           value="<?php echo $row['username']; ?>25000">
                                </div>
                            </div>
                            <hr>

                            <div class="form-group">
                                <label for="field-1" class="col-sm-5 control-label">Grad/Intl. Registration Fee:</label>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-3">
                                    <select class="form-control ">
                                        <option>Intl. Reg Fee</option>
                                        <option>Grad Fee</option>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <select class="form-control">
                                        <option>PHP</option>
                                        <option>GBP</option>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control right-text" name="username" readonly
                                           value="<?php echo $row['username']; ?>25000">
                                </div>
                            </div>

                            <hr>
                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label">Add On:</label>
                            </div>

                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label">Plan A:</label>

                                <div class="col-sm-2">
                                    <input type="text" class="form-control right-text" name="username" readonly
                                           value="<?php echo $row['username']; ?>62.15">
                                </div>
                                <label for="field-1" class="col-sm-2 control-label">Plan B:</label>

                                <div class="col-sm-2">
                                    <input type="text" class="form-control right-text" name="username" readonly
                                           value="<?php echo $row['username']; ?>25.1">
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label"><strong>Grand Total Fees</strong></label>

                                <div class="col-sm-5 col-sm-offset-1">
                                    <input type="text" class="form-control right-text" name="username" readonly
                                           value="<?php echo $row['username']; ?>25000">
                                </div>
                            </div>
                        </div>
                    </div>


                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

</script>