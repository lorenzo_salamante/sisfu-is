
/* 
 * @Author: Lorenzo Salamante - lorenzosalamante@gmail.com
 * Team Istruktura - http://www.istruktura.com
 */

<div class="row" style="margin-bottom: -18px;">
    <div class="col-md-12">
        <div class="panel panel-primary" data-collapsed="0">
            <div class="panel-body" style="padding: 7px;">
                <div class="form-group">

                    <label for="curriculum_year" class="col-sm-1 control-label small-pad-top"><?php echo get_phrase('curriculum_year'); ?></label>
                    <div class="col-sm-2">
                        <select name="curriculum_year" class="form-control" data-validate="required" id="curriculum_year">
                            <option value="">
                                2015-2016
                            </option>
                        </select>
                    </div> 

                    <label for="active_term" class="col-sm-1 control-label small-pad-top"><?php echo get_phrase('year_term'); ?></label>
                    <div class="col-sm-1">
                        <?php
                        $active_term = $this->db->get_where('settings', array(
                                    'type' => 'active_term'
                                ))->row()->description;
                        ?>    
                        <select name="active_term" class="form-control center-text" id="active_term" disabled="">
                            <option value="1" <?php echo ($active_term == 1 ? 'selected' : "") ?> >1</option>
                            <option value="2" <?php echo ($active_term == 2 ? 'selected' : "") ?> >2</option>
                            <option value="3" <?php echo ($active_term == 3 ? 'selected' : "") ?> >3</option>
                        </select>
                        </select>
                    </div> 

                    <label for="level" class="col-sm-1 control-label small-pad-top"><?php echo get_phrase('level'); ?></label>
                    <div class="col-sm-1">
                        <select name="level" class="form-control" data-validate="required" id="level" disabled="">
                            <option value="">
                                2nd
                            </option>
                        </select>
                    </div> 

                    <label for="academic_year" class="col-sm-1 control-label small-pad-top"><?php echo get_phrase('academic_year'); ?></label>
                    <div class="col-sm-2">
                        <select name="academic_year" class="form-control" data-validate="required" id="academic_year" disabled="">
                            <option value="">
                                2015-2016
                            </option>
                        </select>
                    </div> 

                    <div class="col-sm-2">
                        <a class="btn btn-primary pull-right" id="get_subjects"> <i class="fa fa-plus-square"></i> <?php echo get_phrase('get_subjects'); ?></a>
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>