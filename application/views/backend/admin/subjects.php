<a href="javascript:;" onclick="showAjaxModal('<?php echo base_url(); ?>index.php?modal/popup/modal_subject_add/');" 
   class="btn btn-primary pull-right">
    <i class="entypo-plus-circled"></i>
    <?php echo get_phrase('add_new_subject'); 
    $base_url = base_url();
    ?>
</a> 
<br><br>
<table class="table table-bordered datatable table-hover" id="table_export" >
    <thead>
        <tr>
            <th>ID</th>
            <th><div><?php echo get_phrase('subject_code'); ?></div></th>
<th><div><?php echo get_phrase('partner_code'); ?></div></th>
<th><div><?php echo get_phrase('subject_name'); ?></div></th>
<th><?php echo get_phrase('school_code'); ?></th>
<th><?php echo get_phrase('units'); ?></th>
<th><?php echo get_phrase('unit_level'); ?></th>
<th><?php echo get_phrase('options'); ?></th>
</tr>
</thead>
<tbody>
    <?php
    $subjects = $this->db->get('sisfu_subjects')->result_array();
    foreach ($subjects as $row):
        ?>
        <tr>
            <td><?php echo $subject_id = $row['subject_id'] ?></td>
            <td><?php echo $row['subject_code']; ?></td>
            <td><?php echo $row['partner_code']; ?></td>
            <td><?php echo $row['subject_name']; ?></td>
            <?php
            $this->db->where('department_id', $row['department_id']);
            $departments = $this->db->get('sisfu_departments')->result_array();
            foreach ($departments as $desc):
                ?>
                <td><?php echo $desc['department_code']; ?></td>
            <?php endforeach; ?>
            <td><?php echo $row['units']; ?></td>
            <td><?php echo $row['unit_level']; ?></td>
            <td>
                <div class="btn-group">
                    <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                        Action <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-default pull-right" role="menu">

                        <!--user EDITING LINK--> 
                        <li>
                            <a href="#" onclick="showAjaxModal('<?php echo base_url(); ?>index.php?modal/popup/modal_subject_edit/<?php echo $subject_id ?>');">
                                <i class="fa fa-pencil"></i>
                                <?php // echo get_phrase('edit'); ?>
                                Edit
                            </a>
                        </li>
                        <li class="divider"></li>

                        <!--user DELETION LINK--> 
                        <li>
                            <a href="#" onclick="confirm_modal('<?php echo $base_url; ?>index.php?admin/subjects/delete/<?php echo $subject_id ?>');">
                                <i class="fa fa-trash-o"></i>
                                <?php // echo get_phrase('delete'); ?>
                                Delete
                            </a>
                        </li>
                    </ul>
                </div>
            </td>

        </tr>

    <?php endforeach; ?>
</tbody>
</table>


<!-----  DATA TABLE EXPORT CONFIGURATIONS ---->                      
<script type="text/javascript">

    jQuery(document).ready(function ($)
    {
        var datatable = $("#table_export").dataTable({
            "sPaginationType": "bootstrap",
            "sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-3 col-left'i><'col-xs-9 col-right'p>>",
            "oTableTools": {
                "aButtons": [
                    {
                        "sExtends": "xls",
                        "mColumns": [1, 2, 3, 4, 5]
                    },
                    {
                        "sExtends": "pdf",
                        "mColumns": [1, 2, 3, 4, 5]
                    },
                    {
                        "sExtends": "print",
                        "fnSetText": "Press 'esc' to return",
                        "fnClick": function (nButton, oConfig) {
                            datatable.fnSetColumnVis(5, false);

                            this.fnPrint(true, oConfig);

                            window.print();

                            $(window).keyup(function (e) {
                                if (e.which == 27) {
                                    datatable.fnSetColumnVis(5, true);
                                }
                            });
                        },
                    },
                ]
            },
        });

        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });
    });

</script>