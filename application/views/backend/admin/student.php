<?php
//if ($access_level < 2)
if (false) {
    ?> <a href="<?php echo base_url(); ?>index.php?admin/student/do_post_all"  target="_blank"
       class="btn btn-primary pull-right">
        <i class="fa fa-paper-plane"></i>
        <?php // echo get_phrase('admit_new_student');  ?>
        Post all students
    </a> <?php } ?>
<br><br>
<table class="table table-bordered datatable table-hover" id="table_export">
    <thead>
        <tr>
            <th>ID</th>
            <th><div>Student no.</div></th>
<th><div>Full name</div></th>
<th><div>School</div></th>
<th><div>Status</div></th>
<th><div>Nationality</div></th>
<th><div>Admission date</div></th>
<th>Options</th>
</tr>
</thead>
<tbody>
    <?php
    $base_url = base_url();
    foreach ($student as $row):
        $student_id = $row['student_id'];
        ?>
        <tr>
            <td><?php echo $student_id ?></td>
            <td><?php echo $row['student_no']; ?></td>
            <td><?php echo $row['last_name']; ?>, <?php echo $row['first_name']; ?> <?php echo $row['middle_name']; ?> <?php echo $row['suffix']; ?></td>
            <td><?php echo $row['department_code']; ?></td>
            <td><?php echo $row['status_name']; ?></td>
            <td><?php echo $row['nationality_name']; ?></td>
            <td><?php echo $row['admission_date']; ?></td>
            <td>

                <div class="btn-group">
                    <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                        Action <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-default pull-right" role="menu">
                        <!--user EDITING LINK--> 
                        <li>
                            <a href="<?php echo $base_url; ?>index.php?admin/student_edit/<?php echo $student_id ?>" target="_blank">
                                <i class="fa fa-pencil"></i>
                                Manage Student
                            </a>
                        </li>
                        <?php if ($access_level < 2): ?> 
                            <li class="divider"></li>
                            <!--user DELETION LINK--> 
                            <li>
                                <a href="#" onclick="confirm_modal('<?php echo $base_url ?>index.php?admin/student/delete/<?php echo $student_id; ?>');">
                                    <i class="fa fa-trash-o"></i>
                                    Delete
                                </a>
                            </li>
                        <?php endif ?>
                        <?php if ($access_level < 3): ?>
                            <li class="divider"></li>
                            <li>
                                <a href="#" onclick="showAjaxModal('<?php echo $base_url ?>index.php?modal/popup/modal_requirements_checklist/<?php echo $student_id; ?>');">
                                    <i class="fa fa-list"></i>
                                    Requirements Checklist
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo $base_url ?>index.php?admin/student_assess/<?php echo $student_id; ?>" target="_blank">
                                    <i class="fa fa-pencil-square-o"></i>
                                    Assess Student
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo $base_url ?>index.php?admin/student_transcript/<?php echo $student_id; ?>" target="_blank">
                                    <i class="entypo-vcard"></i>
                                    Manage Grades
                                </a>
                            </li>
                        <?php endif; ?>
                    </ul>
                </div>
                <?php
                $subject_empty = false;

                if (strlen($row['enrolled_subjects']) > 0) {
                    $subject_empty = true;
                }
                ?>

                <?php if ($subject_empty): ?>
                    <a class="btn btn-default btn-sm btn-white" data-toggle="tooltip" data-placement="top" title="This student has subjects that are yet to be posted. Please assess the student for more info."><i class="fa fa-question-circle"></i> Assessed</a>
                <?php endif; ?>
            </td>
        </tr>
    <?php endforeach; ?>
</tbody>
</table>



<!-----  DATA TABLE EXPORT CONFIGURATIONS ---->                      
<script type="text/javascript">

    jQuery(document).ready(function ($)
    {
        var datatable = $("#table_export").dataTable({
            aaSorting: [[0, "desc"]],
            "sPaginationType": "bootstrap",
            "sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-3 col-left'i><'col-xs-9 col-right'p>>",
            "oTableTools": {
                "aButtons": [
                    {
                        "sExtends": "xls",
                        "mColumns": [0, 1, 2, 3, 4, 5, 6]
                    },
                    {
                        "sExtends": "pdf",
                        "mColumns": [0, 1, 2, 3, 4, 5, 6]
                    },
                    {
                        "sExtends": "print",
                        "fnSetText": "Press 'esc' to return",
                        "fnClick": function (nButton, oConfig) {
                            datatable.fnSetColumnVis(5, false);

                            this.fnPrint(true, oConfig);

                            window.print();

                            $(window).keyup(function (e) {
                                if (e.which == 27) {
                                    datatable.fnSetColumnVis(5, true);
                                }
                            });
                        },
                    },
                ]
            },
        });

        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    });

</script>