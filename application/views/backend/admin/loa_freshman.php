<style>
    .print-heading{
        height:155px;
    }
    .sisfu-qsf{
        font-size: 7pt;
        margin-top: -20px;
        left: 47%;
        position: absolute;
    }
    .sisfu-qsf2{
        font-size: 7pt;
        margin-top: -10px;
        left: 47.25%;
        position: absolute;
    }
    .date-div{
        margin-left: 70px;
        font-size: 10pt;
        margin-top: 0px;
        margin-bottom: -10px;
    }
    .reference_no-div{
        margin-left: 70%;
        position: absolute;
        font-size: 10pt;
        margin-top:-5px
    }
    .student-name{
        margin-left: 70px;
        margin-top: 10px;
        font-weight: bold;
        font-size: 10pt;
    }
    .permanent-address{
        margin-left: 70px;
        font-weight: bold;
        font-size: 10pt;
    }
    .main-heading{
        text-align:center;
        font-weight: bold;
        font-size: 10pt;
        margin-top:4px
    }
    .dear-parent{
        margin-left: 70px;
        font-size: 10pt;
        margin-top:4px
    }
    .congratulations{
        text-align: justify;
        margin-left: 70px;
        width: 83%;
        font-size: 10pt;
        margin-top:10px
    }
    .bullet-list{
        text-align: justify;
        margin-left: 70px;
        width: 80%;
        font-size: 10pt;
        margin-top:18px
    }
    .bottom-paragraph{
        text-align: justify;
        margin-left: 70px;
        width: 83%;
        font-size: 10pt;
        margin-top:18px
    }
    .bottom-left-column{
        margin-left: 70px;
        margin-top: 10px;
        font-size: 10pt;
    }
    .bottom-right-column{
        margin-top: -65px;
        font-size: 10pt;
        margin-left: 65%
    }
    .print-footer{
        font-size: 11px;
        line-height: 3px;
        text-align: right;
        margin-right: 0px;
        bottom: 0px;
        margin-bottom: -74px;
        position: absolute;
        right: 0%;
    }
    .print-footing{
        height: 12px;
        margin-bottom: 6px;
    }
    .footer-text{
        font-size:7pt;
        font-family: 'France';
        line-height: 0.5px;
    }
    .strong-underline{
        font-weight: bold;
        text-decoration: underline;
    }
    .form-group {
        margin-bottom: 45px;
    }
    .col-sm-offset-1 {
        margin-left: 1.333333%;
    }
</style>
<div class="print-body-loa visible-print">
    <!--Hello CY-->
    <!--Wag m nalang pansinin tong mga PHP dito naka comment lang yan hehe xD-->
    <!--Eto useful shortcuts sa netbeans-->
    <!--Comment line CTRL + / -->
    <!--Delete line CTRL + E-->
    <!--Move line upwards/downwards SHIFT + ALT + UP or DOWN arrow-->
    <!--Duplicate line CTRL + SHIFT UP or DOWN-->
    <!--Format spacing (favorite ko to) SHIFT + ALT + F-->

    <?php
    #INITS
    $student_id = $this->uri->segment(3, 0);

    $commencement_date = $this->db->get_where('settings', array(
                'type' => 'commencement_date'
            ))->row()->description;
    $requirements_due = $this->db->get_where('settings', array(
                'type' => 'requirements_due'
            ))->row()->description;
    $orientation_date = $this->db->get_where('settings', array(
                'type' => 'orientation_date'
            ))->row()->description;

    $this->db->where('student_id', $student_id);
    $result = $this->db->get('sisfu_students')->result_array();
    foreach ($result as $row):
        ?>

        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 print-header">
                    <center>
                        <!--CY, ganito tumawag ng image mula sa assets/images folder--> 
                        <!--copy paste mo nalang to-->
                        <!-- Inside the src property <?php // echo base_url() . 'assets/images/letter-heading.jpg'                                            ?> -->
                        <!--Make sure to remove the leading -> //-->
                        <img class="print-heading" src="<?php echo base_url() . 'assets/images/letter-heading.jpg' ?>"/>
                    </center>
                </div>
                <div class='sisfu-qsf'>
                    <!--SISFU/QSF-REG-032-->
                    <?php
                    $this->db->where('form_code', 'UCLFP');
                    $iso_form = $this->db->get('sisfu_forms')->result_array();

                    foreach ($iso_form as $form):
                        echo $form['form_control'];
                        ?>
                    </div>
                    <div class='sisfu-qsf2'>
                        <?php
                        echo $form['form_revision'];
                    endforeach;
                    ?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="date-div">
                        <?php echo date('d F Y') ?>
                        <br>
                        <!--31 December 2036-->
                    </div>
                    <div class="reference_no-div">
                        <?php
                        $ref_no = $this->db->get_where('sisfu_reference', array(
                                    'ref_code' => 'LOA'
                                ))->row()->ref_no;
                        ?>

                        Office of the Registrar<br>
                        Ref No. <?php echo $ref_no ?>
                    </div>
                    <div class="student-name">
                        <?php
                        if ($row['sex'] == 'M') {
                            echo 'Mr';
                        } else {
                            echo 'Ms';
                        }
                        echo '. ' . $row['first_name'] . ' ' . $row['middle_name'] . ' ' . $row['last_name'];
                        ?>
                        <!--Mr. This is a test-->
                    </div>
                    <div class="permanent-address">
                        <?php
                        $address_arr = explode(' ', $row['permanent_address']);
                        $arr_length = count($address_arr);
                        for ($i = 0; $i < $arr_length; $i++) {

                            if ($i == 0)
                                echo ($address_arr[$i] . ' ' );
                            else if (($i % ($arr_length / 2) ) == 0)
                                echo '<br>' . ($address_arr[$i] . ' ' );
                            else
                                echo ($address_arr[$i] . ' ' );
                        }
                        ?>
                        <!--221B Baker Street,<br>London, England-->
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="main-heading">
                        CONDITIONAL LETTER OF ACCEPTANCE
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="dear-parent">
                        <?php
                        echo 'Dear  <strong>Mr. / Mrs. ' . $row['last_name'] . ':</strong>';
                        ?>
                        <!--Dear Mr. Parent-->
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="congratulations">
                        Congratulations! It is my distinct pleasure to inform you that you have been accepted into Southville 
                        International School affiliated with Foreign Universities for undergraduate study in 

                        <?php
                        $this->db->where('course_id', $row['course_id']);
                        $result = $this->db->get('sisfu_courses')->result_array();
                        foreach ($result as $course):
                            ?>
                            <span class="strong-underline">
                                <!--BACHELOR OF THIS IS A TEST--> 
                                <?php echo $course['degree_name'] ?> </span>
                            for the School Year <span class="strong-underline">
                                <?php
                                $this_year = date('Y');
                                $last_year = date("Y", strtotime("-1 year", time()));
                                ?>
                                <?php echo $last_year . '-' . $this_year ?>
                                <span class="strong-underline">
                                    <?php echo $_SESSION['active_term'] ?>
                                    <superscript>
                                        <?php
                                        $this->db->where('type', 'active_term');
                                        $q = $this->db->get('settings')->result_array();
                                        foreach ($q as $row) {
                                            switch ($row['description']) {
                                                case 1:
                                                    echo $row['description'] . 'st';
                                                    break;
                                                case 2:
                                                    echo $row['description'] . 'nd';
                                                    break;
                                                case 3:
                                                    echo $row['description'] . 'rd';
                                                    break;
                                                case 4:
                                                    echo $row['description'] . 'th';
                                                    break;
                                            }
                                        }
                                        ?>
                                    </superscript>
                                </span>
                                trimester,
                            </span>
                            commencing on 
                            <?php
                            echo "<span class='strong-underline' id='body-commencement_date'>" . $commencement_date . "</span>";
                            ?>
                            , conditional upon the completion of the following requirements:
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="bullet-list">
                        <ul>
                            <li>Submission of the following academic records</li>
                            <ul>
                                <li>Recommendation Letter from School Principal or Guidance Counselor</li>
                                <li>Recommendation Letter from a Professor or Teacher</li>
                                <li>Official Transcript of Secondary Education (F137)</li>
                                <li>High School Report Card (F138) </li>
                                <li>Copy of High School Diploma</li>
                            </ul>
                            <li>Submission of the following:</li>
                            <ul>
                                <li>Four (4) 2” x  2” pictures</li>
                                <li>Copy of birth certificate and/or passport</li>
                                <li>500-word essay</li>
                            </ul>
                            <li>Payment of admission and application fees</li>

                        </ul>
                    </div>
                    <div class="bottom-paragraph">
                        <p>
                            All requirements must be submitted on or before
                            <?php
                            echo "<span class='strong-underline' id='body-requirements_due'>" . $requirements_due . "</span>";
                            ?>
                            . Once these have been received by Southville 
                            you will receive an Unconditional Letter of Acceptance. International Registration Fees (IRFs) therefore 
                            will be paid by all students. The period of registration is valid for five years from date of enrolment.
                        </p>
                        <p>
                            The academic program is taught at British University standard. You will have the opportunity to learn with 
                            students from all around the world in one of the most focused academic institutions in the Philippines and it 
                            is our hope that you will contribute your special talents to the rich diversity that defines the Southville 
                            student body. You are required to attend the College Preparatory Program, scheduled on 
                            <?php
                            echo "<span class='strong-underline' id='body-orientation_date'>" . $orientation_date . "</span>";
                            ?>
                            as a 
                            requirement for all incoming students.
                        </p>
                        <p>
                            It is essential that you take the TOEIC test for appropriate placement and assistance. The Marketing 
                            Department shall coordinate with you for the scheduled testing date.
                        </p>
                        <p>
                            On behalf of the entire faculty and management, congratulations on your conditional admission to Southville 
                            International School affiliated with Foreign Universities.
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 bottom-left-column">
                    <p>Sincerely,</p>

                    <p><strong>Ms. Caroline F. Mediodia</strong><br>
                        Registrar</p>
                </div>
                <div class="col-md-6 bottom-right-column">
                    <p>Noted by:</p>

                    <p><strong>Mr. Amir Tohid</strong><br>
                        Administrative Head<br>
                        School of Business and Computing
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="print-footer col-sm-12">
                    <img class="print-footing" src="<?php echo base_url() . 'assets/images/letter-footer.png' ?>"/>

                    <p class="footer-text">Lima corner Luxembourg Street BF International, Las Piñas City, Philippines 1741</p>
                    <p class="footer-text">+632-820-918   |   +632-820-6774</p>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>

<div class='hidden-print'>
    <div class="panel panel-primary" >

        <div class="panel-heading">
            <div class="panel-title">
                <?php echo get_phrase('custom_edits'); ?> for <?php
                $this->db->where('student_id', $student_id);
                $result = $this->db->get('sisfu_students')->result_array();
                foreach ($result as $row):
                    echo $row['first_name'] . ' ' . $row['middle_name'] . ' ' . $row['last_name'];
                endforeach;
                ?>
            </div>
        </div>
        <div class="panel-body">
            <div class="form-group">
                <div class="col-sm-9">
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo get_phrase('commencement_date'); ?></label>
                        <div class="col-sm-4">
                            <input class='form-control' type='date' name='commencement_date' id='commencement_date' value='<?php echo $commencement_date ?>' />
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-9">
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo get_phrase('requirements_due'); ?></label>
                        <div class="col-sm-4">
                            <input class='form-control' type='date' name='requirements_due' id='requirements_due' value='<?php echo $requirements_due ?>' />
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-9">
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo get_phrase('orientation_date'); ?></label>
                        <div class="col-sm-4">
                            <input class='form-control' type='date' name='orientation_date' id='orientation_date' value='<?php echo $orientation_date ?>' />
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-1 col-sm-3">
                    <a href='#' class="btn btn-primary" id="print"><i class='fa fa-print'></i> <span style='font-family: Arial;'><?php echo get_phrase('print_preview'); ?></span></a>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('document').ready(function () {
        var print_btn = $('#print');

        var commencement_date = $('#commencement_date');
        var orientation_date = $('#orientation_date');
        var requirements_due = $('#requirements_due');

        var body_commencement_date = $('#body-commencement_date');
        var body_orientation_date = $('#body-orientation_date');
        var body_requirements_due = $('#body-requirements_due');

        commencement_date.blur(function (event) {
            body_commencement_date.html(commencement_date.val());
        });

        orientation_date.blur(function (event) {
            body_orientation_date.html(orientation_date.val());
        });

        requirements_due.blur(function (event) {
            body_requirements_due.html(requirements_due.val());
        });

        print_btn.on('click', function () {
            window.print();
        });

        var beforePrint = function () {
            console.log('Functionality to run before printing.');
        };
        var afterPrint = function () {
            $.ajax({
                url: 'index.php?Admin/ajax_increment_ref_no', // define here controller then function name
                type: 'post',
                data: {data: 0}, // pass here your date variable into controller
                success: function (data) {
                    console.log('ajax request successful');
                    console.log(data);
                }
            });


            console.log('Functionality to run after printing');
        };

        if (window.matchMedia) {
            var mediaQueryList = window.matchMedia('print');
            mediaQueryList.addListener(function (mql) {
                if (mql.matches) {
                    beforePrint();
                } else {
                    afterPrint();
                }
            });
        }

        window.onbeforeprint = beforePrint;
        window.onafterprint = afterPrint;
    });
</script>
