
<a href="javascript:;" onclick="showAjaxModal('<?php echo base_url(); ?>index.php?modal/popup/modal_user_add/');" 
   class="btn btn-primary pull-right">
    <i class="entypo-plus-circled"></i>
    <?php echo get_phrase('add_new_user'); ?>
</a> 
<br><br>
<table class="table table-bordered datatable table-hover" id="table_export">
    <thead>
        <tr>
            <th>ID</th>
            <th><div><?php echo get_phrase('user'); ?></div></th>
<th><div><?php echo get_phrase('email'); ?></div></th>
<th><div><?php echo get_phrase('access_level'); ?></div></th>
<th>Options</th>
</tr>
</thead>
<tbody>
    <?php
    $users = $this->db->get('admin')->result_array();
    foreach ($users as $row):
        ?>
        <?php if ($row['access_level'] == -1) { ?>

        <?php } else {
            ?>
            <tr>
                <td><?php echo $row['admin_id'] ?></td>
                <td><?php echo $row['username']; ?></td>
                <td><?php echo $row['email']; ?></td>
                <?php
                $this->db->where('access_level', $row['access_level']);
                $access_level = $this->db->get('sisfu_access_levels')->result_array();
                foreach ($access_level as $desc):
                    ?>
                <td><?php echo $row['access_level'] . ' - ' . $desc['definition']; ?></td>
                    <?php endforeach; ?>
                <td>

                    <div class="btn-group">
                        <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                            Action <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-default pull-right" role="menu">

                            <!--user EDITING LINK--> 
                            <li>
                                <a href="#" onclick="showAjaxModal('<?php echo base_url(); ?>index.php?modal/popup/modal_user_edit/<?php echo $row['admin_id']; ?>');">
                                    <i class="fa fa-pencil"></i>
                                    <?php echo get_phrase('edit'); ?>
                                </a>
                            </li>
                            <li class="divider"></li>

                            <!--user DELETION LINK--> 
                            <li>
                                <a href="#" onclick="confirm_modal('<?php echo base_url(); ?>index.php?admin/users/delete/<?php echo $row['admin_id']; ?>');">
                                    <i class="fa fa-trash-o"></i>
                                    <?php echo get_phrase('delete'); ?>
                                </a>
                            </li>
                        </ul>
                    </div>

                </td>
            </tr>

        <?php }
        ?>




    <?php endforeach; ?>
</tbody>
</table>



<!-----  DATA TABLE EXPORT CONFIGURATIONS ---->                      
<script type="text/javascript">

    jQuery(document).ready(function ($)
    {
        var datatable = $("#table_export").dataTable({
            "sPaginationType": "bootstrap",
            "sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-3 col-left'i><'col-xs-9 col-right'p>>",
            "oTableTools": {
                "aButtons": [
                    {
                        "sExtends": "xls",
                        "mColumns": [1, 2, 3, 4, 5]
                    },
                    {
                        "sExtends": "pdf",
                        "mColumns": [1, 2, 3, 4, 5]
                    },
                    {
                        "sExtends": "print",
                        "fnSetText": "Press 'esc' to return",
                        "fnClick": function (nButton, oConfig) {
                            datatable.fnSetColumnVis(5, false);

                            this.fnPrint(true, oConfig);

                            window.print();

                            $(window).keyup(function (e) {
                                if (e.which == 27) {
                                    datatable.fnSetColumnVis(5, true);
                                }
                            });
                        },
                    },
                ]
            },
        });

        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });
    });

</script>