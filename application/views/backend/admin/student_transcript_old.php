<?php
$total_grades_arr = array();
$total_credits_arr = array();
$student_id = $this->uri->segment(3, 0);
$student_no = $this->db->get_where('sisfu_students', array('student_id' => $student_id))->row()->student_no;

$department_id = $this->db->get_where('sisfu_students', array('student_id' => $student_id))->row()->department_id;
$course_id = $this->db->get_where('sisfu_students', array('student_id' => $student_id))->row()->course_id;

$department_name = $this->db->get_where('sisfu_departments', array('department_id' => $department_id))->row()->department_name;
$course_name = $this->db->get_where('sisfu_courses', array('course_id' => $course_id))->row()->course_name;

$this->db->where('student_id', $student_id);
$stud = $this->db->get('sisfu_students')->result_array();
foreach ($stud as $row):
    $full_name = $row['last_name'] . ', ' . $row['first_name'] . ' ' . $row['middle_name'];
endforeach;
?>
<div class="row hidden-print" >
    <div class="col-sm-7">
        <div class="well well-transcript" >
            <div class="row">
                <div class="col-sm-6 bold">Student ID:</div>
                <div class="col-sm-6"><?php echo $student_no; ?></div>
            </div>
            <div class="row">
                <div class="col-sm-6 bold">Student Name:</div>
                <div class="col-sm-6"><?php echo $full_name; ?></div>
            </div>
            <div class="row">
                <div class="col-sm-6 bold">School Name:</div>
                <div class="col-sm-6"><?php echo $department_name; ?></div>
            </div>
            <div class="row">
                <div class="col-sm-6 bold">Course:</div>
                <div class="col-sm-6"><?php echo $course_name; ?></div>
            </div>
            <div class="row">
                <div class="col-sm-6 bold">Cumulative GPA:</div>
                <div class="col-sm-6 cum-gpa"></div>
            </div>
        </div>
    </div>
    <div class="col-sm-5">
        <div class='well well-info-transcript'>
            <div class="row">
                <div class="col-sm-12"><i class="fa fa-info-circle"></i> <?php echo get_phrase('print_config'); ?>
                    <br>
                    <br>
                    <strong>Paper size:</strong> 8 1/2 x 14in (LEGAL)<br>
                    <strong>Margins:</strong> Default
                </div>
                <div class="col-sm-12"> &nbsp;</div>
                <div class="col-sm-3 text-center"> <a href="#" class="btn btn-primary" id="sfu-print"> <i class="fa fa-print"></i> Print: SFU</a></div>
                <div class="col-sm-3 text-center"> <a href="#" class="btn btn-primary" id="ichm-print" disabled> <i class="fa fa-print"></i> Print: ICHM</a></div>
                <div class="col-sm-6 text-center"> <a href="<?php echo base_url() . 'index.php?admin/student_sog/' . $student_id ?>" disabled class="btn btn-primary" id="sog-print" > <i class="fa fa-pencil-square-o"></i> Prepare SOG</a></div>
            </div>
        </div>
    </div>
</div>
<?php
$this->db->where('student_no', $student_no);
$this->db->group_by(array("acad_year", "term_code"));
$r = $this->db->get('sisfu_grades')->result_array();

$r_length = $this->db->query("SELECT COUNT(*) as num_rows FROM sisfu_grades WHERE `student_no` = '" . $student_no . "' GROUP BY `acad_year`, `term_code`")->row()->num_rows;
foreach ($r as $row):
    $i = 0;
    ?>
    <div class="row hidden-print">
        <div class="col-md-12">
            <div class="panel panel-primary" data-collapsed="0">
                <div class="panel-heading">
                    <div class="panel-title" style="float:none;">
                        <div class="row">
                            <div class="col-sm-11">
                                <span class="primary-color"><i class="entypo-plus-circled"></i>
                                    <?php
                                    switch ($row['term_code']) {
                                        case 1:
                                            echo $row['term_code'] . 'st Trimester, ';
                                            break;
                                        case 2:
                                            echo $row['term_code'] . 'nd Trimester, ';
                                            break;
                                        case 3:
                                            echo $row['term_code'] . 'rd Trimester, ';
                                            break;
                                        case 4:
                                            echo $row['term_code'] . 'th Trimester, ';
                                            break;
                                        case 'S':
                                            echo 'Summer, ';
                                            break;
                                    }
                                    echo 'School Year ' . $row['acad_year'];
                                    ?>     
                                </span>
                            </div>
                            <div class="col-sm-1 pull-right">
                                <!--<a class="btn btn-sm btn-primary"><i class="fa fa-eye"></i> View fees </a>-->
                            </div>
                        </div>

                    </div>
                </div>
                <div class="panel-body">
                    <table class="table table-bordered datatable table-hover">
                        <thead>
                        <th><?php echo get_phrase('subject_code') ?></th>
                        <th><?php echo get_phrase('subject_name') ?></th>
                        <th><?php echo get_phrase('grade') ?></th>
                        <th><?php echo get_phrase('qty_pts') ?></th>
                        <th><?php echo get_phrase('credit') ?></th>
                        <th><?php echo get_phrase('level') ?></th>
                        <th><?php echo get_phrase('options') ?></th>
                        </thead>
                        <tbody>
                            <?php
                            $this->db->where('acad_year', $row['acad_year']);
                            $this->db->where('term_code', $row['term_code']);
                            $this->db->where('student_no', $row['student_no']);
                            $r2 = $this->db->get('sisfu_grades')->result_array();
                            $grades_arr = array();
                            $credits_arr = array();
                            foreach ($r2 as $row2):
                                $grade_id = $row2['grade_id'];
                                $subject_code = $row2['subject_code'];
                                $subject_name = $this->db->get_where('sisfu_subjects', array('subject_code' => $row2['subject_code']))->row()->subject_name;
                                $grade_alpha = $row2['grade_alpha'];
                                $posted_by = $row2['posted_by'];
                                $post_date = $row2['post_date'];
                                $qty_pts = $this->db->get_where('sisfu_grades_meta', array('grade_alpha' => $grade_alpha))->row()->qty_pts;
                                $units = $this->db->get_where('sisfu_subjects', array('subject_code' => $row2['subject_code']))->row()->units;
                                $unit_level = $this->db->get_where('sisfu_subjects', array('subject_code' => $row2['subject_code']))->row()->unit_level;
                                echo form_open(base_url() . 'index.php?admin/student/update_grades/' . $student_id, array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data'));
                                ?>    
                            <input type='hidden' name='grade_id<?php echo $i ?>' value='<?php echo $grade_id ?>' />
                            <tr>
                                <td>
                                    <?php echo $subject_code ?>
                                </td>
                                <td>
                                    <?php // echo form_open(base_url() . 'index.php?admin/student/replace_grade/' . $student_id . '/' . $grade_id, array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data')); ?>
                                    <select name='new_subject' class='form-control new_subject' >
                                        <?php
                                        $this->db->order_by('subject_name', 'asc');
                                        $subjects = $this->db->get('sisfu_subjects')->result_array();
                                        foreach ($subjects as $s):
                                            $s_code = $s['subject_code'];
                                            ?>
                                            <option value="<?php echo $s_code ?>" <?php echo ($subject_code == $s_code) ? 'selected' : ''; ?>><?php echo $s['subject_name'] . ' - ' . $s_code ?></option>
                                        <?php endforeach; ?>
                                    </select>

                                </td>
                                <td>
                                    <select class='form-control grade-alpha' name='grade_alpha<?php echo $i ?>'>
                                        <?php
                                        $this->db->order_by('grade_alpha', 'asc');
                                        $g_meta = $this->db->get('sisfu_grades_meta')->result_array();
                                        foreach ($g_meta as $g):
                                            $g_alpha = $g['grade_alpha'];
                                            ?>
                                            <option value='<?php echo $g_alpha ?>' <?php echo ($grade_alpha == $g_alpha) ? 'selected' : ''; ?>> <?php echo $g_alpha ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </td>
                                <td><?php
                                    echo ($qty_pts <= 0) ? '...' : $qty_pts;
                                    array_push($grades_arr, $qty_pts * $units);
                                    ?></td>
                                <td><?php
                                    echo ($qty_pts <= 0) ? '...' : $units;
                                    array_push($credits_arr, $units);
                                    ?></td>
                                <td><?php echo $unit_level ?></td>
                                <td>
                                    <?php if ($access_level < 2): ?>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                                Action <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu dropdown-default pull-right" role="menu">
                                                <li>
                                                    <a href="#" onclick="confirm_modal('<?php echo base_url(); ?>index.php?admin/student/delete_grade/<?php echo $student_id . '/' . $grade_id ?>');" class=""><i class="fa fa-trash"></i>
                                                        Delete row
                                                    </a>

                                                </li>
                                                <li>
                                                    <a href="#" onclick="replace_modal('<?php echo base_url(); ?>index.php?admin/student/replace_grade/<?php echo $student_id . '/' . $grade_id ?>');" class=""><i class="fa fa-refresh"></i>
                                                        Replace subject with current

                                                    </a>
                                                </li>
                                            </ul>
                                        </div>

                                        <a class="btn btn-white" data-toggle="tooltip" data-placement="top" title="Last edited by: <?php echo $posted_by ?> on <?php echo $post_date ?>"><i class="fa fa-question-circle"></i></a>
                                    <?php endif; ?>
                                    <?php // echo form_close(); ?>
                                </td>
                            </tr>

                            <?php
                            $i++;
                        endforeach;
                        ?>
                        <tr>
                            <td>&nbsp;</td>
                            <td>**********************************GPA:</td>
                            <td>&nbsp;</td>
                            <td><?php
                                $gpa = number_format(round(array_sum($grades_arr) / array_sum($credits_arr), 2), 2);

                                if ($gpa == -1) {
                                    echo "NGY";
                                } else {
                                    echo $gpa;
                                    array_push($total_grades_arr, array_sum($grades_arr));
                                    array_push($total_credits_arr, array_sum($credits_arr));
                                }
                                unset($grades_arr);
                                unset($credits_arr);
                                ?></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="7">
                                    <div class='pull-right'>
                                        <?php if ($access_level < 2): ?>
                                            <button type='submit' class='btn btn-primary form-control'><i class='fa fa-save'></i> Save</button>
                                        <?php endif; ?>
                                    </div>
                                </td>
                            </tr>
                        </tfoot>
                        <input type='hidden' name='r_length' value='<?php echo $i ?>' />
                        <?php echo form_close(); ?>

                    </table>
                </div>
            </div>
        </div>
    </div>
<?php endforeach; ?>

<div id="grade-body" class="hidden-print hidden">
    <img src="<?php echo base_url() . 'assets\images\logo_mini_with_shadow.png' ?>" class="transcript-logo"/>
    <table class="" id="grade-tbl">
        <tbody>
            <tr>
                <td colspan="2">SOUTHVILLE INTERNATIONAL SCHOOL AFFILIATED WITH FOREIGN UNIVERSITIES</td>
            </tr>
            <tr>
                <td>Student ID:</td>
                <td><?php echo $student_no; ?></td>
            </tr>
            <tr>
                <td>Student Name:</td>
                <td><?php echo $full_name; ?></td>
            </tr>
            <tr>
                <td>School Name:</td>
                <td><?php echo $department_name; ?></td>
            </tr>
            <tr>
                <td>Course:</td>
                <td><?php echo $course_name; ?></td>
            </tr>
            <tr>
                <td>Cumulative GPA:</td>
                <td class="cum-gpa"></td>
            </tr>
        </tbody>
    </table>
    <center>
        <br>
        <hr style="border-color:black; width: 88%;">
        <br>
        <br>
    </center>


    <?php
    $this->db->where('student_no', $student_no);
    $this->db->group_by(array("acad_year", "term_code"));
    $r = $this->db->get('sisfu_grades')->result_array();
    foreach ($r as $row):
        ?>
        <span class="trimester">
            <?php
            switch ($row['term_code']) {
                case 1:
                    echo $row['term_code'] . 'st Trimester, ';
                    break;
                case 2:
                    echo $row['term_code'] . 'nd Trimester, ';
                    break;
                case 3:
                    echo $row['term_code'] . 'rd Trimester, ';
                    break;
                case 4:
                    echo $row['term_code'] . 'th Trimester, ';
                    break;
                case 'S':
                    echo 'Summer, ';
                    break;
            }
            echo 'School Year ' . $row['acad_year'];
            ?>     

        </span>
        <style>
            table { page-break-inside:auto }
            tr    { page-break-inside:avoid; page-break-after:auto }
            thead { display:table-header-group }
            tfoot { display:table-footer-group }
        </style>
        <table class="grades">
            <thead >
            <th></th>
            <th></th>
            <th><?php echo get_phrase('grade') ?></th>
            <th><?php echo get_phrase('qty_pts') ?></th>
            <th><?php echo get_phrase('credit') ?></th>
            <th><?php echo get_phrase('level') ?></th>
            <!--<th><?php // echo get_phrase('options')                                               ?></th>-->
            </thead>
            <tbody>
                <?php
                $this->db->where('acad_year', $row['acad_year']);
                $this->db->where('term_code', $row['term_code']);
                $this->db->where('student_no', $row['student_no']);
                $r2 = $this->db->get('sisfu_grades')->result_array();
                $credits_arr = array();
                $grades_arr = array();
                foreach ($r2 as $row2):
                    $subject_code = $row2['subject_code'];
                    $subject_name = $this->db->get_where('sisfu_subjects', array('subject_code' => $row2['subject_code']))->row()->subject_name;
                    $grade_alpha = $row2['grade_alpha'];
                    $qty_pts = $this->db->get_where('sisfu_grades_meta', array('grade_alpha' => $grade_alpha))->row()->qty_pts;
                    $units = $this->db->get_where('sisfu_subjects', array('subject_code' => $row2['subject_code']))->row()->units;
                    $unit_level = $this->db->get_where('sisfu_subjects', array('subject_code' => $row2['subject_code']))->row()->unit_level;
                    ?>    
                    <tr>
                        <td><?php echo $subject_code ?></td>
                        <td>
                            <?php
                            if (strlen($subject_name) > 39) {
                                $conc = substr($subject_name, 0, 36);
                                echo $conc . '...';
                            } else {
                                echo $subject_name;
                            }
                            ?>
                        </td>
                        <td class="text-right">
                            <?php
                            if ($grade_alpha == 'DS') {
                                echo 'D';
                            } else {
                                echo $grade_alpha;
                            }
                            ?>
                        </td>
                        <td><?php
                    echo ($qty_pts <= 0) ? '...' : $qty_pts;
                    array_push($grades_arr, $qty_pts * $units);
                            ?></td>
                        <td><?php
                            echo ($qty_pts <= 0) ? '...' : $units;
                            array_push($credits_arr, $units);
                            ?></td>
                        <td><?php echo $unit_level ?></td>
                        <!--<td>options</td>-->
                    </tr>

    <?php endforeach; ?>
                <tr>
                    <td>&nbsp;</td>
                    <td>*************************************GPA:</td>
                    <td>&nbsp;</td>
                    <td><?php
    $gpa = number_format(round(array_sum($grades_arr) / array_sum($credits_arr), 2), 2);
    unset($grades_arr);
    unset($credits_arr);
    if ($gpa == -1) {
        echo "NGY";
    } else {
        echo $gpa;
    }
    ?></td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </tbody>
        </table>
<?php endforeach; ?>
    <br>
    <span class="trimester">*************************** Nothing Follows ***********************************</span>
    <br>
    <br>
    <br>
    <span class="trimester">Prepared by: <?php echo $_SESSION['username'] ?></span>
</div>
<script>
    $(document).ready(function () {
        $('#sfu-print').on('click', function () {
            window.print();
        });

        var grade_body = $('#grade-body');
        var sog_body = $('#sog-body');
        var ichm_body = $('#ichm-body');

        function reveal(element) {
            element.addClass('visible-print');
            element.removeClass('hidden');
            element.removeClass('hidden-print');
        }
        function hide(element) {
            element.removeClass('visible-print');
            element.addClass('hidden');
            element.addClass('hidden-print');
        }

        $('#sfu-print').on('mouseover', function () {
            reveal(grade_body);

            hide(ichm_body);
        });


        $('.cum-gpa').html('<?php echo number_format(array_sum($total_grades_arr) / array_sum($total_credits_arr), 4) ?> out of 4.0000');

        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
        $('.new_subject').change(function () {
            $.ajax({
                url: 'index.php?Admin/ajax_new_subject', // define here controller then function name
                type: 'post',
                data: {new_subject: $(this).val()}, // pass here your date variable into controller
                success: function (data) {
                    console.log('new subject set');
                }
            });
        });
    });
</script>