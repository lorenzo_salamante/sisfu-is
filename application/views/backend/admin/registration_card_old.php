<style>
    /*bootstrap styles */
    .btn {
        display: inline-block;
        margin-bottom: 0;
        font-weight: 400;
        text-align: center;
        vertical-align: middle;
        cursor: pointer;
        background-image: none;
        border: 1px solid transparent;
        white-space: nowrap;
        padding: 6px 12px;
        font-size: 12px;
        line-height: 1.42857143;
        border-radius: 3px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        -o-user-select: none;
        user-select: none;
    }
    .btn:focus {
        outline: thin dotted #333;
        outline: 5px auto -webkit-focus-ring-color;
        outline-offset: -2px;
    }
    .btn:hover,
    .btn:focus {
        color: #303641;
        text-decoration: none;
        outline: none;
    }
    .btn:active,
    .btn.active {
        outline: none;
        background-image: none;
        -moz-box-shadow: inset 0 0px 7px rgba(0, 0, 0, 0.225);
        -webkit-box-shadow: inset 0 0px 7px rgba(0, 0, 0, 0.225);
        box-shadow: inset 0 0px 7px rgba(0, 0, 0, 0.225);
        -moz-box-shadow: inset 0 0px 4px rgba(0, 0, 0, 0.2);
        -webkit-box-shadow: inset 0 0px 4px rgba(0, 0, 0, 0.2);
        box-shadow: inset 0 0px 4px rgba(0, 0, 0, 0.2);
    }
    .btn.disabled,
    .btn[disabled],
    fieldset[disabled] .btn {
        cursor: not-allowed;
        pointer-events: none;
        -webkit-opacity: 0.65;
        -moz-opacity: 0.65;
        opacity: 0.65;
        filter: alpha(opacity=65);
        -moz-box-shadow: none;
        -webkit-box-shadow: none;
        box-shadow: none;
    }
    .btn.btn-icon {
        position: relative;
    }
    .btn.btn-icon i {
        position: absolute;
        right: 0;
        top: 0;
        height: 100%;
    }
    .btn-default {
        color: #303641;
        background-color: #f0f0f1;
        border-color: #f0f0f1;
    }
    .btn-default:hover,
    .btn-default:focus,
    .btn-default:active,
    .btn-default.active,
    .open .dropdown-toggle.btn-default {
        color: #303641;
        background-color: #dbdbdd;
        border-color: #d0d0d3;
    }
    .btn-default:active,
    .btn-default.active,
    .open .dropdown-toggle.btn-default {
        background-image: none;
    }
    .btn-default.disabled,
    .btn-default[disabled],
    fieldset[disabled] .btn-default,
    .btn-default.disabled:hover,
    .btn-default[disabled]:hover,
    fieldset[disabled] .btn-default:hover,
    .btn-default.disabled:focus,
    .btn-default[disabled]:focus,
    fieldset[disabled] .btn-default:focus,
    .btn-default.disabled:active,
    .btn-default[disabled]:active,
    fieldset[disabled] .btn-default:active,
    .btn-default.disabled.active,
    .btn-default[disabled].active,
    fieldset[disabled] .btn-default.active {
        background-color: #f0f0f1;
        border-color: #f0f0f1;
    }
    .btn-default .badge {
        color: #f0f0f1;
        background-color: #303641;
    }
    .btn-default > .caret {
        border-top-color: #303641;
        border-bottom-color: #303641 !important;
    }
    .btn-default.dropdown-toggle {
        border-left-color: #dedee0;
    }
    .btn-default.btn-icon {
        position: relative;
        padding-right: 39px;
        border: none;
    }
    .btn-default.btn-icon i {
        background-color: #dbdbdd;
        padding: 6px 6px;
        font-size: 12px;
        line-height: 1.42857143;
        border-radius: 3px;
        -webkit-border-radius: 0 3px 3px 0;
        -webkit-background-clip: padding-box;
        -moz-border-radius: 0 3px 3px 0;
        -moz-background-clip: padding;
        border-radius: 0 3px 3px 0;
        background-clip: padding-box;
    }
    .btn-default.btn-icon.icon-left {
        padding-right: 12px;
        padding-left: 39px;
    }
    .btn-default.btn-icon.icon-left i {
        float: left;
        right: auto;
        left: 0;
        -webkit-border-radius: 3px 0 0 3px !important;
        -webkit-background-clip: padding-box;
        -moz-border-radius: 3px 0 0 3px !important;
        -moz-background-clip: padding;
        border-radius: 3px 0 0 3px !important;
        background-clip: padding-box;
    }
    .btn-default.btn-icon.btn-lg {
        padding-right: 55px;
    }
    .btn-default.btn-icon.btn-lg.icon-left {
        padding-right: 16px;
        padding-left: 55px;
    }
    .btn-default.btn-icon.btn-lg i {
        padding: 10px 10px;
        font-size: 15px;
        line-height: 1.33;
        border-radius: 3px;
    }
    .btn-default.btn-icon.btn-sm {
        padding-right: 36px;
    }
    .btn-default.btn-icon.btn-sm.icon-left {
        padding-right: 10px;
        padding-left: 36px;
    }
    .btn-default.btn-icon.btn-sm i {
        padding: 5px 6px;
        font-size: 11px;
        line-height: 1.5;
        border-radius: 2px;
    }
    .btn-default.btn-icon.btn-xs {
        padding-right: 32px;
    }
    .btn-default.btn-icon.btn-xs.icon-left {
        padding-right: 10px;
        padding-left: 32px;
    }
    .btn-default.btn-icon.btn-xs i {
        padding: 2px 6px;
        font-size: 10px;
        line-height: 1.5;
        border-radius: 2px;
    }
    .btn-primary {
        color: #ffffff;
        background-color: #303641;
        border-color: #303641;
    }
    .btn-primary:hover,
    .btn-primary:focus,
    .btn-primary:active,
    .btn-primary.active,
    .open .dropdown-toggle.btn-primary {
        color: #ffffff;
        background-color: #1f232a;
        border-color: #16191e;
    }
    .btn-primary:active,
    .btn-primary.active,
    .open .dropdown-toggle.btn-primary {
        background-image: none;
    }
    .btn-primary.disabled,
    .btn-primary[disabled],
    fieldset[disabled] .btn-primary,
    .btn-primary.disabled:hover,
    .btn-primary[disabled]:hover,
    fieldset[disabled] .btn-primary:hover,
    .btn-primary.disabled:focus,
    .btn-primary[disabled]:focus,
    fieldset[disabled] .btn-primary:focus,
    .btn-primary.disabled:active,
    .btn-primary[disabled]:active,
    fieldset[disabled] .btn-primary:active,
    .btn-primary.disabled.active,
    .btn-primary[disabled].active,
    fieldset[disabled] .btn-primary.active {
        background-color: #303641;
        border-color: #303641;
    }
    .btn-primary .badge {
        color: #303641;
        background-color: #ffffff;
    }
    .btn-primary > .caret {
        border-top-color: #ffffff;
        border-bottom-color: #ffffff !important;
    }
    .btn-primary.dropdown-toggle {
        border-left-color: #21252c;
    }
    .btn-primary.btn-icon {
        position: relative;
        padding-right: 39px;
        border: none;
    }
    .btn-primary.btn-icon i {
        background-color: #1f232a;
        padding: 6px 6px;
        font-size: 12px;
        line-height: 1.42857143;
        border-radius: 3px;
        -webkit-border-radius: 0 3px 3px 0;
        -webkit-background-clip: padding-box;
        -moz-border-radius: 0 3px 3px 0;
        -moz-background-clip: padding;
        border-radius: 0 3px 3px 0;
        background-clip: padding-box;
    }
    .btn-primary.btn-icon.icon-left {
        padding-right: 12px;
        padding-left: 39px;
    }
    .btn-primary.btn-icon.icon-left i {
        float: left;
        right: auto;
        left: 0;
        -webkit-border-radius: 3px 0 0 3px !important;
        -webkit-background-clip: padding-box;
        -moz-border-radius: 3px 0 0 3px !important;
        -moz-background-clip: padding;
        border-radius: 3px 0 0 3px !important;
        background-clip: padding-box;
    }
    .btn-primary.btn-icon.btn-lg {
        padding-right: 55px;
    }
    .btn-primary.btn-icon.btn-lg.icon-left {
        padding-right: 16px;
        padding-left: 55px;
    }
    .btn-primary.btn-icon.btn-lg i {
        padding: 10px 10px;
        font-size: 15px;
        line-height: 1.33;
        border-radius: 3px;
    }
    .btn-primary.btn-icon.btn-sm {
        padding-right: 36px;
    }
    .btn-primary.btn-icon.btn-sm.icon-left {
        padding-right: 10px;
        padding-left: 36px;
    }
    .btn-primary.btn-icon.btn-sm i {
        padding: 5px 6px;
        font-size: 11px;
        line-height: 1.5;
        border-radius: 2px;
    }
    .btn-primary.btn-icon.btn-xs {
        padding-right: 32px;
    }
    .btn-primary.btn-icon.btn-xs.icon-left {
        padding-right: 10px;
        padding-left: 32px;
    }
    .btn-primary.btn-icon.btn-xs i {
        padding: 2px 6px;
        font-size: 10px;
        line-height: 1.5;
        border-radius: 2px;
    }
    .btn-blue {
        color: #ffffff;
        background-color: #0072bc;
        border-color: #0072bc;
    }
    .btn-blue:hover,
    .btn-blue:focus,
    .btn-blue:active,
    .btn-blue.active,
    .open .dropdown-toggle.btn-blue {
        color: #ffffff;
        background-color: #005993;
        border-color: #004d7f;
    }
    .btn-blue:active,
    .btn-blue.active,
    .open .dropdown-toggle.btn-blue {
        background-image: none;
    }
    .btn-blue.disabled,
    .btn-blue[disabled],
    fieldset[disabled] .btn-blue,
    .btn-blue.disabled:hover,
    .btn-blue[disabled]:hover,
    fieldset[disabled] .btn-blue:hover,
    .btn-blue.disabled:focus,
    .btn-blue[disabled]:focus,
    fieldset[disabled] .btn-blue:focus,
    .btn-blue.disabled:active,
    .btn-blue[disabled]:active,
    fieldset[disabled] .btn-blue:active,
    .btn-blue.disabled.active,
    .btn-blue[disabled].active,
    fieldset[disabled] .btn-blue.active {
        background-color: #0072bc;
        border-color: #0072bc;
    }
    .btn-blue .badge {
        color: #0072bc;
        background-color: #ffffff;
    }
    .btn-blue > .caret {
        border-top-color: #ffffff;
        border-bottom-color: #ffffff !important;
    }
    .btn-blue.dropdown-toggle {
        border-left-color: #005c98;
    }
    .btn-blue.btn-icon {
        position: relative;
        padding-right: 39px;
        border: none;
    }
    .btn-blue.btn-icon i {
        background-color: #005993;
        padding: 6px 6px;
        font-size: 12px;
        line-height: 1.42857143;
        border-radius: 3px;
        -webkit-border-radius: 0 3px 3px 0;
        -webkit-background-clip: padding-box;
        -moz-border-radius: 0 3px 3px 0;
        -moz-background-clip: padding;
        border-radius: 0 3px 3px 0;
        background-clip: padding-box;
    }
    .btn-blue.btn-icon.icon-left {
        padding-right: 12px;
        padding-left: 39px;
    }
    .btn-blue.btn-icon.icon-left i {
        float: left;
        right: auto;
        left: 0;
        -webkit-border-radius: 3px 0 0 3px !important;
        -webkit-background-clip: padding-box;
        -moz-border-radius: 3px 0 0 3px !important;
        -moz-background-clip: padding;
        border-radius: 3px 0 0 3px !important;
        background-clip: padding-box;
    }
    .btn-blue.btn-icon.btn-lg {
        padding-right: 55px;
    }
    .btn-blue.btn-icon.btn-lg.icon-left {
        padding-right: 16px;
        padding-left: 55px;
    }
    .btn-blue.btn-icon.btn-lg i {
        padding: 10px 10px;
        font-size: 15px;
        line-height: 1.33;
        border-radius: 3px;
    }
    .btn-blue.btn-icon.btn-sm {
        padding-right: 36px;
    }
    .btn-blue.btn-icon.btn-sm.icon-left {
        padding-right: 10px;
        padding-left: 36px;
    }
    .btn-blue.btn-icon.btn-sm i {
        padding: 5px 6px;
        font-size: 11px;
        line-height: 1.5;
        border-radius: 2px;
    }
    .btn-blue.btn-icon.btn-xs {
        padding-right: 32px;
    }
    .btn-blue.btn-icon.btn-xs.icon-left {
        padding-right: 10px;
        padding-left: 32px;
    }
    .btn-blue.btn-icon.btn-xs i {
        padding: 2px 6px;
        font-size: 10px;
        line-height: 1.5;
        border-radius: 2px;
    }
    .btn-red {
        color: #ffffff;
        background-color: #d42020;
        border-color: #d42020;
    }
    .btn-red:hover,
    .btn-red:focus,
    .btn-red:active,
    .btn-red.active,
    .open .dropdown-toggle.btn-red {
        color: #ffffff;
        background-color: #b11b1b;
        border-color: #9f1818;
    }
    .btn-red:active,
    .btn-red.active,
    .open .dropdown-toggle.btn-red {
        background-image: none;
    }
    .btn-red.disabled,
    .btn-red[disabled],
    fieldset[disabled] .btn-red,
    .btn-red.disabled:hover,
    .btn-red[disabled]:hover,
    fieldset[disabled] .btn-red:hover,
    .btn-red.disabled:focus,
    .btn-red[disabled]:focus,
    fieldset[disabled] .btn-red:focus,
    .btn-red.disabled:active,
    .btn-red[disabled]:active,
    fieldset[disabled] .btn-red:active,
    .btn-red.disabled.active,
    .btn-red[disabled].active,
    fieldset[disabled] .btn-red.active {
        background-color: #d42020;
        border-color: #d42020;
    }
    .btn-red .badge {
        color: #d42020;
        background-color: #ffffff;
    }
    .btn-red > .caret {
        border-top-color: #ffffff;
        border-bottom-color: #ffffff !important;
    }
    .btn-red.dropdown-toggle {
        border-left-color: #b51b1b;
    }
    .btn-red.btn-icon {
        position: relative;
        padding-right: 39px;
        border: none;
    }
    .btn-red.btn-icon i {
        background-color: #b11b1b;
        padding: 6px 6px;
        font-size: 12px;
        line-height: 1.42857143;
        border-radius: 3px;
        -webkit-border-radius: 0 3px 3px 0;
        -webkit-background-clip: padding-box;
        -moz-border-radius: 0 3px 3px 0;
        -moz-background-clip: padding;
        border-radius: 0 3px 3px 0;
        background-clip: padding-box;
    }
    .btn-red.btn-icon.icon-left {
        padding-right: 12px;
        padding-left: 39px;
    }
    .btn-red.btn-icon.icon-left i {
        float: left;
        right: auto;
        left: 0;
        -webkit-border-radius: 3px 0 0 3px !important;
        -webkit-background-clip: padding-box;
        -moz-border-radius: 3px 0 0 3px !important;
        -moz-background-clip: padding;
        border-radius: 3px 0 0 3px !important;
        background-clip: padding-box;
    }
    .btn-red.btn-icon.btn-lg {
        padding-right: 55px;
    }
    .btn-red.btn-icon.btn-lg.icon-left {
        padding-right: 16px;
        padding-left: 55px;
    }
    .btn-red.btn-icon.btn-lg i {
        padding: 10px 10px;
        font-size: 15px;
        line-height: 1.33;
        border-radius: 3px;
    }
    .btn-red.btn-icon.btn-sm {
        padding-right: 36px;
    }
    .btn-red.btn-icon.btn-sm.icon-left {
        padding-right: 10px;
        padding-left: 36px;
    }
    .btn-red.btn-icon.btn-sm i {
        padding: 5px 6px;
        font-size: 11px;
        line-height: 1.5;
        border-radius: 2px;
    }
    .btn-red.btn-icon.btn-xs {
        padding-right: 32px;
    }
    .btn-red.btn-icon.btn-xs.icon-left {
        padding-right: 10px;
        padding-left: 32px;
    }
    .btn-red.btn-icon.btn-xs i {
        padding: 2px 6px;
        font-size: 10px;
        line-height: 1.5;
        border-radius: 2px;
    }
    .btn-orange {
        color: #ffffff;
        background-color: #ff9600;
        border-color: #ff9600;
    }
    .btn-orange:hover,
    .btn-orange:focus,
    .btn-orange:active,
    .btn-orange.active,
    .open .dropdown-toggle.btn-orange {
        color: #ffffff;
        background-color: #d67e00;
        border-color: #c27200;
    }
    .btn-orange:active,
    .btn-orange.active,
    .open .dropdown-toggle.btn-orange {
        background-image: none;
    }
    .btn-orange.disabled,
    .btn-orange[disabled],
    fieldset[disabled] .btn-orange,
    .btn-orange.disabled:hover,
    .btn-orange[disabled]:hover,
    fieldset[disabled] .btn-orange:hover,
    .btn-orange.disabled:focus,
    .btn-orange[disabled]:focus,
    fieldset[disabled] .btn-orange:focus,
    .btn-orange.disabled:active,
    .btn-orange[disabled]:active,
    fieldset[disabled] .btn-orange:active,
    .btn-orange.disabled.active,
    .btn-orange[disabled].active,
    fieldset[disabled] .btn-orange.active {
        background-color: #ff9600;
        border-color: #ff9600;
    }
    .btn-orange .badge {
        color: #ff9600;
        background-color: #ffffff;
    }
    .btn-orange > .caret {
        border-top-color: #ffffff;
        border-bottom-color: #ffffff !important;
    }
    .btn-orange.dropdown-toggle {
        border-left-color: #db8100;
    }
    .btn-orange.btn-icon {
        position: relative;
        padding-right: 39px;
        border: none;
    }
    .btn-orange.btn-icon i {
        background-color: #d67e00;
        padding: 6px 6px;
        font-size: 12px;
        line-height: 1.42857143;
        border-radius: 3px;
        -webkit-border-radius: 0 3px 3px 0;
        -webkit-background-clip: padding-box;
        -moz-border-radius: 0 3px 3px 0;
        -moz-background-clip: padding;
        border-radius: 0 3px 3px 0;
        background-clip: padding-box;
    }
    .btn-orange.btn-icon.icon-left {
        padding-right: 12px;
        padding-left: 39px;
    }
    .btn-orange.btn-icon.icon-left i {
        float: left;
        right: auto;
        left: 0;
        -webkit-border-radius: 3px 0 0 3px !important;
        -webkit-background-clip: padding-box;
        -moz-border-radius: 3px 0 0 3px !important;
        -moz-background-clip: padding;
        border-radius: 3px 0 0 3px !important;
        background-clip: padding-box;
    }
    .btn-orange.btn-icon.btn-lg {
        padding-right: 55px;
    }
    .btn-orange.btn-icon.btn-lg.icon-left {
        padding-right: 16px;
        padding-left: 55px;
    }
    .btn-orange.btn-icon.btn-lg i {
        padding: 10px 10px;
        font-size: 15px;
        line-height: 1.33;
        border-radius: 3px;
    }
    .btn-orange.btn-icon.btn-sm {
        padding-right: 36px;
    }
    .btn-orange.btn-icon.btn-sm.icon-left {
        padding-right: 10px;
        padding-left: 36px;
    }
    .btn-orange.btn-icon.btn-sm i {
        padding: 5px 6px;
        font-size: 11px;
        line-height: 1.5;
        border-radius: 2px;
    }
    .btn-orange.btn-icon.btn-xs {
        padding-right: 32px;
    }
    .btn-orange.btn-icon.btn-xs.icon-left {
        padding-right: 10px;
        padding-left: 32px;
    }
    .btn-orange.btn-icon.btn-xs i {
        padding: 2px 6px;
        font-size: 10px;
        line-height: 1.5;
        border-radius: 2px;
    }
    .btn-gold {
        color: #846e20;
        background-color: #fcd036;
        border-color: #fcd036;
    }
    .btn-gold:hover,
    .btn-gold:focus,
    .btn-gold:active,
    .btn-gold.active,
    .open .dropdown-toggle.btn-gold {
        color: #846e20;
        background-color: #fbc70e;
        border-color: #f1bc04;
    }
    .btn-gold:active,
    .btn-gold.active,
    .open .dropdown-toggle.btn-gold {
        background-image: none;
    }
    .btn-gold.disabled,
    .btn-gold[disabled],
    fieldset[disabled] .btn-gold,
    .btn-gold.disabled:hover,
    .btn-gold[disabled]:hover,
    fieldset[disabled] .btn-gold:hover,
    .btn-gold.disabled:focus,
    .btn-gold[disabled]:focus,
    fieldset[disabled] .btn-gold:focus,
    .btn-gold.disabled:active,
    .btn-gold[disabled]:active,
    fieldset[disabled] .btn-gold:active,
    .btn-gold.disabled.active,
    .btn-gold[disabled].active,
    fieldset[disabled] .btn-gold.active {
        background-color: #fcd036;
        border-color: #fcd036;
    }
    .btn-gold .badge {
        color: #fcd036;
        background-color: #846e20;
    }
    .btn-gold > .caret {
        border-top-color: #846e20;
        border-bottom-color: #846e20 !important;
    }
    .btn-gold.dropdown-toggle {
        border-left-color: #fbc813;
    }
    .btn-gold.btn-icon {
        position: relative;
        padding-right: 39px;
        border: none;
    }
    .btn-gold.btn-icon i {
        background-color: #fbc70e;
        padding: 6px 6px;
        font-size: 12px;
        line-height: 1.42857143;
        border-radius: 3px;
        -webkit-border-radius: 0 3px 3px 0;
        -webkit-background-clip: padding-box;
        -moz-border-radius: 0 3px 3px 0;
        -moz-background-clip: padding;
        border-radius: 0 3px 3px 0;
        background-clip: padding-box;
    }
    .btn-gold.btn-icon.icon-left {
        padding-right: 12px;
        padding-left: 39px;
    }
    .btn-gold.btn-icon.icon-left i {
        float: left;
        right: auto;
        left: 0;
        -webkit-border-radius: 3px 0 0 3px !important;
        -webkit-background-clip: padding-box;
        -moz-border-radius: 3px 0 0 3px !important;
        -moz-background-clip: padding;
        border-radius: 3px 0 0 3px !important;
        background-clip: padding-box;
    }
    .btn-gold.btn-icon.btn-lg {
        padding-right: 55px;
    }
    .btn-gold.btn-icon.btn-lg.icon-left {
        padding-right: 16px;
        padding-left: 55px;
    }
    .btn-gold.btn-icon.btn-lg i {
        padding: 10px 10px;
        font-size: 15px;
        line-height: 1.33;
        border-radius: 3px;
    }
    .btn-gold.btn-icon.btn-sm {
        padding-right: 36px;
    }
    .btn-gold.btn-icon.btn-sm.icon-left {
        padding-right: 10px;
        padding-left: 36px;
    }
    .btn-gold.btn-icon.btn-sm i {
        padding: 5px 6px;
        font-size: 11px;
        line-height: 1.5;
        border-radius: 2px;
    }
    .btn-gold.btn-icon.btn-xs {
        padding-right: 32px;
    }
    .btn-gold.btn-icon.btn-xs.icon-left {
        padding-right: 10px;
        padding-left: 32px;
    }
    .btn-gold.btn-icon.btn-xs i {
        padding: 2px 6px;
        font-size: 10px;
        line-height: 1.5;
        border-radius: 2px;
    }
    .btn-black {
        color: #ffffff;
        background-color: #000000;
        border-color: #000000;
    }
    .btn-black:hover,
    .btn-black:focus,
    .btn-black:active,
    .btn-black.active,
    .open .dropdown-toggle.btn-black {
        color: #ffffff;
        background-color: #000000;
        border-color: #000000;
    }
    .btn-black:active,
    .btn-black.active,
    .open .dropdown-toggle.btn-black {
        background-image: none;
    }
    .btn-black.disabled,
    .btn-black[disabled],
    fieldset[disabled] .btn-black,
    .btn-black.disabled:hover,
    .btn-black[disabled]:hover,
    fieldset[disabled] .btn-black:hover,
    .btn-black.disabled:focus,
    .btn-black[disabled]:focus,
    fieldset[disabled] .btn-black:focus,
    .btn-black.disabled:active,
    .btn-black[disabled]:active,
    fieldset[disabled] .btn-black:active,
    .btn-black.disabled.active,
    .btn-black[disabled].active,
    fieldset[disabled] .btn-black.active {
        background-color: #000000;
        border-color: #000000;
    }
    .btn-black .badge {
        color: #000000;
        background-color: #ffffff;
    }
    .btn-black > .caret {
        border-top-color: #ffffff;
        border-bottom-color: #ffffff !important;
    }
    .btn-black.dropdown-toggle {
        border-left-color: #000000;
    }
    .btn-black.btn-icon {
        position: relative;
        padding-right: 39px;
        border: none;
    }
    .btn-black.btn-icon i {
        background-color: #000000;
        padding: 6px 6px;
        font-size: 12px;
        line-height: 1.42857143;
        border-radius: 3px;
        -webkit-border-radius: 0 3px 3px 0;
        -webkit-background-clip: padding-box;
        -moz-border-radius: 0 3px 3px 0;
        -moz-background-clip: padding;
        border-radius: 0 3px 3px 0;
        background-clip: padding-box;
    }
    .btn-black.btn-icon.icon-left {
        padding-right: 12px;
        padding-left: 39px;
    }
    .btn-black.btn-icon.icon-left i {
        float: left;
        right: auto;
        left: 0;
        -webkit-border-radius: 3px 0 0 3px !important;
        -webkit-background-clip: padding-box;
        -moz-border-radius: 3px 0 0 3px !important;
        -moz-background-clip: padding;
        border-radius: 3px 0 0 3px !important;
        background-clip: padding-box;
    }
    .btn-black.btn-icon.btn-lg {
        padding-right: 55px;
    }
    .btn-black.btn-icon.btn-lg.icon-left {
        padding-right: 16px;
        padding-left: 55px;
    }
    .btn-black.btn-icon.btn-lg i {
        padding: 10px 10px;
        font-size: 15px;
        line-height: 1.33;
        border-radius: 3px;
    }
    .btn-black.btn-icon.btn-sm {
        padding-right: 36px;
    }
    .btn-black.btn-icon.btn-sm.icon-left {
        padding-right: 10px;
        padding-left: 36px;
    }
    .btn-black.btn-icon.btn-sm i {
        padding: 5px 6px;
        font-size: 11px;
        line-height: 1.5;
        border-radius: 2px;
    }
    .btn-black.btn-icon.btn-xs {
        padding-right: 32px;
    }
    .btn-black.btn-icon.btn-xs.icon-left {
        padding-right: 10px;
        padding-left: 32px;
    }
    .btn-black.btn-icon.btn-xs i {
        padding: 2px 6px;
        font-size: 10px;
        line-height: 1.5;
        border-radius: 2px;
    }
    .btn-white {
        color: #303641;
        background-color: #ffffff;
        border-color: #ffffff;
        border-color: #ebebeb;
    }
    .btn-white:hover,
    .btn-white:focus,
    .btn-white:active,
    .btn-white.active,
    .open .dropdown-toggle.btn-white {
        color: #303641;
        background-color: #ebebeb;
        border-color: #e0e0e0;
    }
    .btn-white:active,
    .btn-white.active,
    .open .dropdown-toggle.btn-white {
        background-image: none;
    }
    .btn-white.disabled,
    .btn-white[disabled],
    fieldset[disabled] .btn-white,
    .btn-white.disabled:hover,
    .btn-white[disabled]:hover,
    fieldset[disabled] .btn-white:hover,
    .btn-white.disabled:focus,
    .btn-white[disabled]:focus,
    fieldset[disabled] .btn-white:focus,
    .btn-white.disabled:active,
    .btn-white[disabled]:active,
    fieldset[disabled] .btn-white:active,
    .btn-white.disabled.active,
    .btn-white[disabled].active,
    fieldset[disabled] .btn-white.active {
        background-color: #ffffff;
        border-color: #ffffff;
    }
    .btn-white .badge {
        color: #ffffff;
        background-color: #303641;
    }
    .btn-white > .caret {
        border-top-color: #303641;
        border-bottom-color: #303641 !important;
    }
    .btn-white.dropdown-toggle {
        border-left-color: #ededed;
    }
    .btn-white.btn-icon {
        position: relative;
        padding-right: 39px;
        border: none;
    }
    .btn-white.btn-icon i {
        background-color: #ebebeb;
        padding: 6px 6px;
        font-size: 12px;
        line-height: 1.42857143;
        border-radius: 3px;
        -webkit-border-radius: 0 3px 3px 0;
        -webkit-background-clip: padding-box;
        -moz-border-radius: 0 3px 3px 0;
        -moz-background-clip: padding;
        border-radius: 0 3px 3px 0;
        background-clip: padding-box;
    }
    .btn-white.btn-icon.icon-left {
        padding-right: 12px;
        padding-left: 39px;
    }
    .btn-white.btn-icon.icon-left i {
        float: left;
        right: auto;
        left: 0;
        -webkit-border-radius: 3px 0 0 3px !important;
        -webkit-background-clip: padding-box;
        -moz-border-radius: 3px 0 0 3px !important;
        -moz-background-clip: padding;
        border-radius: 3px 0 0 3px !important;
        background-clip: padding-box;
    }
    .btn-white.btn-icon.btn-lg {
        padding-right: 55px;
    }
    .btn-white.btn-icon.btn-lg.icon-left {
        padding-right: 16px;
        padding-left: 55px;
    }
    .btn-white.btn-icon.btn-lg i {
        padding: 10px 10px;
        font-size: 15px;
        line-height: 1.33;
        border-radius: 3px;
    }
    .btn-white.btn-icon.btn-sm {
        padding-right: 36px;
    }
    .btn-white.btn-icon.btn-sm.icon-left {
        padding-right: 10px;
        padding-left: 36px;
    }
    .btn-white.btn-icon.btn-sm i {
        padding: 5px 6px;
        font-size: 11px;
        line-height: 1.5;
        border-radius: 2px;
    }
    .btn-white.btn-icon.btn-xs {
        padding-right: 32px;
    }
    .btn-white.btn-icon.btn-xs.icon-left {
        padding-right: 10px;
        padding-left: 32px;
    }
    .btn-white.btn-icon.btn-xs i {
        padding: 2px 6px;
        font-size: 10px;
        line-height: 1.5;
        border-radius: 2px;
    }
    .btn-warning {
        color: #ffffff;
        background-color: #fad839;
        border-color: #fad839;
    }
    .btn-warning:hover,
    .btn-warning:focus,
    .btn-warning:active,
    .btn-warning.active,
    .open .dropdown-toggle.btn-warning {
        color: #ffffff;
        background-color: #f9d011;
        border-color: #f0c706;
    }
    .btn-warning:active,
    .btn-warning.active,
    .open .dropdown-toggle.btn-warning {
        background-image: none;
    }
    .btn-warning.disabled,
    .btn-warning[disabled],
    fieldset[disabled] .btn-warning,
    .btn-warning.disabled:hover,
    .btn-warning[disabled]:hover,
    fieldset[disabled] .btn-warning:hover,
    .btn-warning.disabled:focus,
    .btn-warning[disabled]:focus,
    fieldset[disabled] .btn-warning:focus,
    .btn-warning.disabled:active,
    .btn-warning[disabled]:active,
    fieldset[disabled] .btn-warning:active,
    .btn-warning.disabled.active,
    .btn-warning[disabled].active,
    fieldset[disabled] .btn-warning.active {
        background-color: #fad839;
        border-color: #fad839;
    }
    .btn-warning .badge {
        color: #fad839;
        background-color: #ffffff;
    }
    .btn-warning > .caret {
        border-top-color: #ffffff;
        border-bottom-color: #ffffff !important;
    }
    .btn-warning.dropdown-toggle {
        border-left-color: #f9d116;
    }
    .btn-warning.btn-icon {
        position: relative;
        padding-right: 39px;
        border: none;
    }
    .btn-warning.btn-icon i {
        background-color: #f9d011;
        padding: 6px 6px;
        font-size: 12px;
        line-height: 1.42857143;
        border-radius: 3px;
        -webkit-border-radius: 0 3px 3px 0;
        -webkit-background-clip: padding-box;
        -moz-border-radius: 0 3px 3px 0;
        -moz-background-clip: padding;
        border-radius: 0 3px 3px 0;
        background-clip: padding-box;
    }
    .btn-warning.btn-icon.icon-left {
        padding-right: 12px;
        padding-left: 39px;
    }
    .btn-warning.btn-icon.icon-left i {
        float: left;
        right: auto;
        left: 0;
        -webkit-border-radius: 3px 0 0 3px !important;
        -webkit-background-clip: padding-box;
        -moz-border-radius: 3px 0 0 3px !important;
        -moz-background-clip: padding;
        border-radius: 3px 0 0 3px !important;
        background-clip: padding-box;
    }
    .btn-warning.btn-icon.btn-lg {
        padding-right: 55px;
    }
    .btn-warning.btn-icon.btn-lg.icon-left {
        padding-right: 16px;
        padding-left: 55px;
    }
    .btn-warning.btn-icon.btn-lg i {
        padding: 10px 10px;
        font-size: 15px;
        line-height: 1.33;
        border-radius: 3px;
    }
    .btn-warning.btn-icon.btn-sm {
        padding-right: 36px;
    }
    .btn-warning.btn-icon.btn-sm.icon-left {
        padding-right: 10px;
        padding-left: 36px;
    }
    .btn-warning.btn-icon.btn-sm i {
        padding: 5px 6px;
        font-size: 11px;
        line-height: 1.5;
        border-radius: 2px;
    }
    .btn-warning.btn-icon.btn-xs {
        padding-right: 32px;
    }
    .btn-warning.btn-icon.btn-xs.icon-left {
        padding-right: 10px;
        padding-left: 32px;
    }
    .btn-warning.btn-icon.btn-xs i {
        padding: 2px 6px;
        font-size: 10px;
        line-height: 1.5;
        border-radius: 2px;
    }
    .btn-danger {
        color: #ffffff;
        background-color: #cc2424;
        border-color: #cc2424;
    }
    .btn-danger:hover,
    .btn-danger:focus,
    .btn-danger:active,
    .btn-danger.active,
    .open .dropdown-toggle.btn-danger {
        color: #ffffff;
        background-color: #a91e1e;
        border-color: #981b1b;
    }
    .btn-danger:active,
    .btn-danger.active,
    .open .dropdown-toggle.btn-danger {
        background-image: none;
    }
    .btn-danger.disabled,
    .btn-danger[disabled],
    fieldset[disabled] .btn-danger,
    .btn-danger.disabled:hover,
    .btn-danger[disabled]:hover,
    fieldset[disabled] .btn-danger:hover,
    .btn-danger.disabled:focus,
    .btn-danger[disabled]:focus,
    fieldset[disabled] .btn-danger:focus,
    .btn-danger.disabled:active,
    .btn-danger[disabled]:active,
    fieldset[disabled] .btn-danger:active,
    .btn-danger.disabled.active,
    .btn-danger[disabled].active,
    fieldset[disabled] .btn-danger.active {
        background-color: #cc2424;
        border-color: #cc2424;
    }
    .btn-danger .badge {
        color: #cc2424;
        background-color: #ffffff;
    }
    .btn-danger > .caret {
        border-top-color: #ffffff;
        border-bottom-color: #ffffff !important;
    }
    .btn-danger.dropdown-toggle {
        border-left-color: #ae1f1f;
    }
    .btn-danger.btn-icon {
        position: relative;
        padding-right: 39px;
        border: none;
    }
    .btn-danger.btn-icon i {
        background-color: #a91e1e;
        padding: 6px 6px;
        font-size: 12px;
        line-height: 1.42857143;
        border-radius: 3px;
        -webkit-border-radius: 0 3px 3px 0;
        -webkit-background-clip: padding-box;
        -moz-border-radius: 0 3px 3px 0;
        -moz-background-clip: padding;
        border-radius: 0 3px 3px 0;
        background-clip: padding-box;
    }
    .btn-danger.btn-icon.icon-left {
        padding-right: 12px;
        padding-left: 39px;
    }
    .btn-danger.btn-icon.icon-left i {
        float: left;
        right: auto;
        left: 0;
        -webkit-border-radius: 3px 0 0 3px !important;
        -webkit-background-clip: padding-box;
        -moz-border-radius: 3px 0 0 3px !important;
        -moz-background-clip: padding;
        border-radius: 3px 0 0 3px !important;
        background-clip: padding-box;
    }
    .btn-danger.btn-icon.btn-lg {
        padding-right: 55px;
    }
    .btn-danger.btn-icon.btn-lg.icon-left {
        padding-right: 16px;
        padding-left: 55px;
    }
    .btn-danger.btn-icon.btn-lg i {
        padding: 10px 10px;
        font-size: 15px;
        line-height: 1.33;
        border-radius: 3px;
    }
    .btn-danger.btn-icon.btn-sm {
        padding-right: 36px;
    }
    .btn-danger.btn-icon.btn-sm.icon-left {
        padding-right: 10px;
        padding-left: 36px;
    }
    .btn-danger.btn-icon.btn-sm i {
        padding: 5px 6px;
        font-size: 11px;
        line-height: 1.5;
        border-radius: 2px;
    }
    .btn-danger.btn-icon.btn-xs {
        padding-right: 32px;
    }
    .btn-danger.btn-icon.btn-xs.icon-left {
        padding-right: 10px;
        padding-left: 32px;
    }
    .btn-danger.btn-icon.btn-xs i {
        padding: 2px 6px;
        font-size: 10px;
        line-height: 1.5;
        border-radius: 2px;
    }
    .btn-success,
    .btn-green {
        color: #ffffff;
        background-color: #00a651;
        border-color: #00a651;
    }
    .btn-success:hover,
    .btn-green:hover,
    .btn-success:focus,
    .btn-green:focus,
    .btn-success:active,
    .btn-green:active,
    .btn-success.active,
    .btn-green.active,
    .open .dropdown-toggle.btn-success,
    .open .dropdown-toggle.btn-green {
        color: #ffffff;
        background-color: #007d3d;
        border-color: #006933;
    }
    .btn-success:active,
    .btn-green:active,
    .btn-success.active,
    .btn-green.active,
    .open .dropdown-toggle.btn-success,
    .open .dropdown-toggle.btn-green {
        background-image: none;
    }
    .btn-success.disabled,
    .btn-green.disabled,
    .btn-success[disabled],
    .btn-green[disabled],
    fieldset[disabled] .btn-success,
    fieldset[disabled] .btn-green,
    .btn-success.disabled:hover,
    .btn-green.disabled:hover,
    .btn-success[disabled]:hover,
    .btn-green[disabled]:hover,
    fieldset[disabled] .btn-success:hover,
    fieldset[disabled] .btn-green:hover,
    .btn-success.disabled:focus,
    .btn-green.disabled:focus,
    .btn-success[disabled]:focus,
    .btn-green[disabled]:focus,
    fieldset[disabled] .btn-success:focus,
    fieldset[disabled] .btn-green:focus,
    .btn-success.disabled:active,
    .btn-green.disabled:active,
    .btn-success[disabled]:active,
    .btn-green[disabled]:active,
    fieldset[disabled] .btn-success:active,
    fieldset[disabled] .btn-green:active,
    .btn-success.disabled.active,
    .btn-green.disabled.active,
    .btn-success[disabled].active,
    .btn-green[disabled].active,
    fieldset[disabled] .btn-success.active,
    fieldset[disabled] .btn-green.active {
        background-color: #00a651;
        border-color: #00a651;
    }
    .btn-success .badge,
    .btn-green .badge {
        color: #00a651;
        background-color: #ffffff;
    }
    .btn-success > .caret,
    .btn-green > .caret {
        border-top-color: #ffffff;
        border-bottom-color: #ffffff !important;
    }
    .btn-success.dropdown-toggle,
    .btn-green.dropdown-toggle {
        border-left-color: #008240;
    }
    .btn-success.btn-icon,
    .btn-green.btn-icon {
        position: relative;
        padding-right: 39px;
        border: none;
    }
    .btn-success.btn-icon i,
    .btn-green.btn-icon i {
        background-color: #007d3d;
        padding: 6px 6px;
        font-size: 12px;
        line-height: 1.42857143;
        border-radius: 3px;
        -webkit-border-radius: 0 3px 3px 0;
        -webkit-background-clip: padding-box;
        -moz-border-radius: 0 3px 3px 0;
        -moz-background-clip: padding;
        border-radius: 0 3px 3px 0;
        background-clip: padding-box;
    }
    .btn-success.btn-icon.icon-left,
    .btn-green.btn-icon.icon-left {
        padding-right: 12px;
        padding-left: 39px;
    }
    .btn-success.btn-icon.icon-left i,
    .btn-green.btn-icon.icon-left i {
        float: left;
        right: auto;
        left: 0;
        -webkit-border-radius: 3px 0 0 3px !important;
        -webkit-background-clip: padding-box;
        -moz-border-radius: 3px 0 0 3px !important;
        -moz-background-clip: padding;
        border-radius: 3px 0 0 3px !important;
        background-clip: padding-box;
    }
    .btn-success.btn-icon.btn-lg,
    .btn-green.btn-icon.btn-lg {
        padding-right: 55px;
    }
    .btn-success.btn-icon.btn-lg.icon-left,
    .btn-green.btn-icon.btn-lg.icon-left {
        padding-right: 16px;
        padding-left: 55px;
    }
    .btn-success.btn-icon.btn-lg i,
    .btn-green.btn-icon.btn-lg i {
        padding: 10px 10px;
        font-size: 15px;
        line-height: 1.33;
        border-radius: 3px;
    }
    .btn-success.btn-icon.btn-sm,
    .btn-green.btn-icon.btn-sm {
        padding-right: 36px;
    }
    .btn-success.btn-icon.btn-sm.icon-left,
    .btn-green.btn-icon.btn-sm.icon-left {
        padding-right: 10px;
        padding-left: 36px;
    }
    .btn-success.btn-icon.btn-sm i,
    .btn-green.btn-icon.btn-sm i {
        padding: 5px 6px;
        font-size: 11px;
        line-height: 1.5;
        border-radius: 2px;
    }
    .btn-success.btn-icon.btn-xs,
    .btn-green.btn-icon.btn-xs {
        padding-right: 32px;
    }
    .btn-success.btn-icon.btn-xs.icon-left,
    .btn-green.btn-icon.btn-xs.icon-left {
        padding-right: 10px;
        padding-left: 32px;
    }
    .btn-success.btn-icon.btn-xs i,
    .btn-green.btn-icon.btn-xs i {
        padding: 2px 6px;
        font-size: 10px;
        line-height: 1.5;
        border-radius: 2px;
    }
    .btn-info {
        color: #ffffff;
        background-color: #21a9e1;
        border-color: #21a9e1;
    }
    .btn-info:hover,
    .btn-info:focus,
    .btn-info:active,
    .btn-info.active,
    .open .dropdown-toggle.btn-info {
        color: #ffffff;
        background-color: #1a8fbf;
        border-color: #1782ad;
    }
    .btn-info:active,
    .btn-info.active,
    .open .dropdown-toggle.btn-info {
        background-image: none;
    }
    .btn-info.disabled,
    .btn-info[disabled],
    fieldset[disabled] .btn-info,
    .btn-info.disabled:hover,
    .btn-info[disabled]:hover,
    fieldset[disabled] .btn-info:hover,
    .btn-info.disabled:focus,
    .btn-info[disabled]:focus,
    fieldset[disabled] .btn-info:focus,
    .btn-info.disabled:active,
    .btn-info[disabled]:active,
    fieldset[disabled] .btn-info:active,
    .btn-info.disabled.active,
    .btn-info[disabled].active,
    fieldset[disabled] .btn-info.active {
        background-color: #21a9e1;
        border-color: #21a9e1;
    }
    .btn-info .badge {
        color: #21a9e1;
        background-color: #ffffff;
    }
    .btn-info > .caret {
        border-top-color: #ffffff;
        border-bottom-color: #ffffff !important;
    }
    .btn-info.dropdown-toggle {
        border-left-color: #1a92c4;
    }
    .btn-info.btn-icon {
        position: relative;
        padding-right: 39px;
        border: none;
    }
    .btn-info.btn-icon i {
        background-color: #1a8fbf;
        padding: 6px 6px;
        font-size: 12px;
        line-height: 1.42857143;
        border-radius: 3px;
        -webkit-border-radius: 0 3px 3px 0;
        -webkit-background-clip: padding-box;
        -moz-border-radius: 0 3px 3px 0;
        -moz-background-clip: padding;
        border-radius: 0 3px 3px 0;
        background-clip: padding-box;
    }
    .btn-info.btn-icon.icon-left {
        padding-right: 12px;
        padding-left: 39px;
    }
    .btn-info.btn-icon.icon-left i {
        float: left;
        right: auto;
        left: 0;
        -webkit-border-radius: 3px 0 0 3px !important;
        -webkit-background-clip: padding-box;
        -moz-border-radius: 3px 0 0 3px !important;
        -moz-background-clip: padding;
        border-radius: 3px 0 0 3px !important;
        background-clip: padding-box;
    }
    .btn-info.btn-icon.btn-lg {
        padding-right: 55px;
    }
    .btn-info.btn-icon.btn-lg.icon-left {
        padding-right: 16px;
        padding-left: 55px;
    }
    .btn-info.btn-icon.btn-lg i {
        padding: 10px 10px;
        font-size: 15px;
        line-height: 1.33;
        border-radius: 3px;
    }
    .btn-info.btn-icon.btn-sm {
        padding-right: 36px;
    }
    .btn-info.btn-icon.btn-sm.icon-left {
        padding-right: 10px;
        padding-left: 36px;
    }
    .btn-info.btn-icon.btn-sm i {
        padding: 5px 6px;
        font-size: 11px;
        line-height: 1.5;
        border-radius: 2px;
    }
    .btn-info.btn-icon.btn-xs {
        padding-right: 32px;
    }
    .btn-info.btn-icon.btn-xs.icon-left {
        padding-right: 10px;
        padding-left: 32px;
    }
    .btn-info.btn-icon.btn-xs i {
        padding: 2px 6px;
        font-size: 10px;
        line-height: 1.5;
        border-radius: 2px;
    }
    .btn-link {
        color: #373e4a;
        font-weight: normal;
        cursor: pointer;
        border-radius: 0;
    }
    .btn-link,
    .btn-link:active,
    .btn-link[disabled],
    fieldset[disabled] .btn-link {
        background-color: transparent;
        -moz-box-shadow: none;
        -webkit-box-shadow: none;
        box-shadow: none;
    }
    .btn-link,
    .btn-link:hover,
    .btn-link:focus,
    .btn-link:active {
        border-color: transparent;
    }
    .btn-link:hover,
    .btn-link:focus {
        color: #818da2;
        text-decoration: underline;
        background-color: transparent;
    }
    .btn-link[disabled]:hover,
    fieldset[disabled] .btn-link:hover,
    .btn-link[disabled]:focus,
    fieldset[disabled] .btn-link:focus {
        color: #999999;
        text-decoration: none;
    }
    .btn-lg {
        padding: 10px 16px;
        font-size: 15px;
        line-height: 1.33;
        border-radius: 3px;
    }
    .btn-sm,
    .btn-xs {
        padding: 5px 10px;
        font-size: 11px;
        line-height: 1.5;
        border-radius: 2px;
    }
    .btn-xs {
        padding: 1px 5px;
    }
    .btn-block {
        display: block;
        width: 100%;
        padding-left: 0;
        padding-right: 0;
    }
    .btn-block + .btn-block {
        margin-top: 5px;
    }
</style>

<style>
    .card1{
        margin-top: 100px;
        margin-bottom: 160px;
    }
    .card2{
        margin-bottom: 50px;
        margin-top: 50px;
    }
    .southville{
        display: none;
        color: #b41818;
        margin-top: 40px;
        margin-left: 25px;
        font-size: 13px;
        font-weight: bold;
    }
    .copy-type{
        display: none;
        color: #b41818;
        font-weight: bold;
        margin-left: 80%;
        position: absolute;
        font-size: 13px;
        margin-top:-15px;
        margin-bottom: 0px;    
    }
    .sisfu-qsf{
        display: none;
        margin-top: -5px;
        margin-left: 25px;
        font-size: 10px;
        font-size: 8px;
    }
    .student-name{
        margin-left: 40px;
        margin-top: 10px;
        font-size: 10px;
    }
    .student-number{
        margin-left: 40px;
        margin-top: 3px;
        font-size: 10px;
    }
    .student-status{
        margin-left: 40px;
        margin-top: 3px;
        font-size: 10px;
    }
    .student-level{
        margin-left: 50%;
        position: absolute;
        font-size: 10px;
        margin-top:-10px
    }
    .registration-number{
        margin-left: 80%;
        position: absolute;
        font-size: 10px;
        margin-top:-10px
    }
    .student-year{
        margin-left: 50%;
        position: absolute;
        font-size: 10px;
        margin-top: -10px
    }
    .student-course{
        margin-left: 50%;
        position: absolute;
        font-size: 10px;
        margin-top:-10px
    }


    .table{
        position: absolute;
        margin-left: 25px;
        margin-top: 14px;
        display: table;
    }
    .table2{
        position: absolute;
        margin-left: 46%;
        margin-top: 23px;
        display: block;
    }
    .table3{
        padding-bottom: 38;
        padding-top: 5px;
        padding-left: 5px;
        margin-top: 15px;
        margin-left: 71%;
        width: 185px;
        border: 1px solid;
        font-size: 10px;
    }
    .heading{
        display: table-row;
        font-size: 10px;
        text-align: center;
    }
    .row{
        display: table-row;
        text-align: center;
    }
    .cell-1{
        display: table-cell;
        border: solid;
        border-width: thin;
        padding-left: 5px;
        padding-right: 5px;
        padding-bottom: 5px;
        padding-top: 5px;
        font-size: 7px;
    }
    .cell-2{
        display: table-cell;
        border: solid;
        border-width: 0;
        padding-left: 5px;
        padding-right: 5px;
        padding-bottom: 5px;
        padding-top: 5px;
        font-size: 7px;
        text-align:right;
    }

    .type-payment
    {
        font-size: 10px;
        position: absolute;
        margin-left: 46%;
        margin-top: 170px;
    }
    .check{
        font-size: 10px;
        margin-right: 70%;
    }
    .total{
        font-weight: bold;
        margin-left: 40px;
    }
    .price{
        font-weight: bold;
        margin-left: 46px;
    }
    .installment{
        margin-left: 30px;
    }
    .installment-price{
        margin-left: 25px;
    }
    .note-due{
        margin-top: -4px;
        position: absolute;
    }
    .note{
        font-size: 9px;
        margin-left: 5px;
    }
    .duedate{
        font-size: 8px;
        margin-left: 25px;
    }
    .approved-by{
        display: none;
        margin-left: -60%;
        font-size: 10px;
        position: absolute;
        margin-top: 65px;
        text-decoration: overline;
        font-weight: bold;
    }
    .student-sig{
        display: none;
        margin-left: -34%;
        position: absolute;
        font-size: 10px;
        margin-top: 65px;
        text-decoration: overline;
        font-weight: bold;
    }
    .OR-no{
        display: none;
        margin-left: -5%;
        position: absolute;
        font-size: 10px;
        text-decoration: overline;
        font-weight: bold;
        margin-top: 65px;
    }
    .reminder{
        display: none;
        position: absolute;
        margin-top: 75px;
        margin-left: -67%;
        font-size: 10px;
        font-weight: bold;
    }
    .visible-print{
        /*display:none;*/
    }
    .page-header {
        color: #fff;
        text-align: center;
        background-color: #BA001F;
        background-image: linear-gradient(120deg, #155799, #c);
        padding: 3rem 4rem;
        height: 85.8vh;
    }
    body{
        margin:0px;
        
    }
    @media print{
        .visible-print{
            display:block;
        }
        .hidden-print{
            display:none;
        }
    }
</style>
<div class="visible-print">
    <?php
    $this->db->where('student_id', $_SESSION['student_id']);
    $r = $this->db->get('sisfu_students')->result_array();
    foreach ($r as $row):
        ?>
        <div class="card1"> 
            <div>
                <div class="southville">
                    <p>SOUTHVILLE INTERNATIONAL SCHOOL</p>
                    <p style="margin-bottom: 0px; margin-top: -15px;">AFFILIATED WITH FOREIGN UNIVERSITIES</p>
                </div>
                <div class="copy-type">Student's Copy</div>
                <div class='sisfu-qsf'>
                    <p>Registration Card</p>
                    <p style="margin-bottom: -3px; margin-top: -8px;">SISFU/QSF-REG-004</p>
                </div>
                <div class="student-name">
                    Student Name: 
                    <?php
                    $full_name = $row['last_name'] . ', ' . $row['first_name'] . ' ' . $row['middle_name'];
                    echo strtoupper($full_name);
                    ?>
                </div>
                <span class="student-level">
                    Year Level- Term: 
                    <?php
                    $level = $this->db->get_where('sisfu_levels_meta', array(
                                'level_id' => $row['level_id']
                            ))->row()->description;

                    if ($row['level_id'] == 0) {
                        echo $level = 'New Enrollee';
                    } else {

                        echo $level;
                    }
                    ?>
                </span>
                <span class="registration-number">
                    Reg No. 
                    <?php
                    echo $reg_no = $this->db->get_where('sisfu_reference', array('ref_code' => 'REGCARD'))->row()->ref_no;
                    ?>
                </span>
                <div class="student-number">
                    Student Number: <?php echo $row['student_no'] ?>
                </div>
                <span class="student-year">
                    School Year: 
                    <?php
                    $this_year = date('Y');
                    $last_year = date("Y", strtotime("-1 year", time()));
                    echo $school_year = $last_year . '-' . $this_year;
                    ?>
                </span>
                <div class="student-status">
                    Status/Scholarship: <?php
                    $scholarship = $this->db->get_where('sisfu_scholarships_meta', array('scholarship_id' => $row['scholarship_id']))->row()->scholarship_name;
                    if ($row['scholarship_id'] == 0) {
                        echo $scholarship = 'No Scholarship';
                    } else {

                        echo $scholarship;
                    }
                    ?>
                </div>
                <span class="student-course">
                    Course: <?php
                    echo $course = $this->db->get_where('sisfu_courses', array('course_id' => $row['course_id']))->row()->course_name;
                    ?>
                </span>
            </div>

            <!--Table-->
<!--            <table>
                <thead>
                <tr>
                    <th>Subjects Enrolled</th><th>Units</th><th>Day</th><th>Time</th><th>Rm</th><th>Sig</th>
                </thead>
                <tbody>
                    
                </tbody>
            </table>-->
            
            <div class="table" style="width:42% !important">

                <div class="heading">
                    <div class="cell-1" style="padding-left: 40px; padding-right: 40px; font-weight: bold; font-size: 10px;">
                        <p>Subjects Enrolled</p>
                    </div>
                    <div class="cell-1">
                        <p>Units</p>
                    </div>
                    <div class="cell-1">
                        <p>Day</p>
                    </div>
                    <div class="cell-1">
                        <p>Time</p>
                    </div>
                    <div class="cell-1">
                        <p>Rm</p>
                    </div>
                    <div class="cell-1">
                        <p>Sig</p>
                    </div>
                </div>
                <?php
                for ($x = 1; $x < 11; $x++):

                    $subjs_arr = explode(",", $row['enrolled_subjects']);
                    if ($subjs_arr[0] == '') {
                        $subjs_len = 0;
                    } else {
                        $subjs_len = count($subjs_arr);
                    }
                    $this->db->where('subject_id', $subjs_arr[$x - 1]);
                    $q = $this->db->get('sisfu_subjects');
                    $result = $q->result_array();

                    if ($q->num_rows() == 0) {
                        ?>
                        <div class="row">
                            <div class="cell-1" style="text-align: left;">
                                <p></p>
                            </div>
                            <div class="cell-1">
                                <p></p>
                            </div>
                            <div class="cell-1">
                                <p></p>
                            </div>
                            <div class="cell-1">
                                <p></p>
                            </div>
                            <div class="cell-1">
                                <p></p>
                            </div>
                            <div class="cell-1">
                                <p></p>
                            </div>
                        </div>
                        <?php
                    } else {

                        foreach ($result as $row2):
                            ?>
                            <div class="row">
                                <div class="cell-1" style="text-align: left;">
                                    <p><?php echo $row2['subject_name'] ?></p>
                                </div>
                                <div class="cell-1">
                                    <p><?php echo $row2['units'] ?></p>
                                </div>
                                <div class="cell-1">
                                    <p></p>
                                </div>
                                <div class="cell-1">
                                    <p></p>
                                </div>
                                <div class="cell-1">
                                    <p></p>
                                </div>
                                <div class="cell-1">
                                    <p></p>
                                </div>
                            </div>
                            <?php
                        endforeach;
                    }
                endfor;
                ?>

            </div>
            <div class="table2">

                <div class="heading">
                    <div class="cell-2" style="text-decoration: underline; padding-right: 30px; font-weight: bold; font-size: 10px;">
                        <p>Breakdown of fees:</p>
                    </div>
                    <div class="cell-2">
                        <p></p>
                    </div>
                </div>
                <div class="row">
                    <div class="cell-2" style="text-align: left;">
                        <p>Tuition Fee: US$</p>
                    </div>
                    <div class="cell-2">
                        <p><?php echo $_SESSION['tuition'] ?></p>
                    </div>

                </div>
                <div class="row">
                    <div class="cell-2" style="text-align: left;">
                        <p>Net Total Tuition Fee:</p>
                    </div>
                    <div class="cell-2">
                        <p><?php echo $_SESSION['a_total'] ?></p>
                    </div>

                </div>
                <div class="row">
                    <div class="cell-2" style="text-align: left;">
                        <p>Registration Fee:</p>
                    </div>
                    <div class="cell-2">
                        <p><?php echo $_SESSION['ir_fee'] ?></p>
                    </div>

                </div>
                <div class="row">
                    <div class="cell-2" style="text-align: left;">
                        <p>Miscellaneous Fee:</p>
                    </div>
                    <div class="cell-2">
                        <p><?php echo $_SESSION['misc'] ?></p>
                    </div>

                </div>
                <div class="row">
                    <div class="cell-2" style="text-align: left;">
                        <p>Other: Late Enrolment Fee</p>
                    </div>
                    <div class="cell-2">
                        <p><?php echo $_SESSION['other'] ?></p>
                    </div>

                </div>
                <div class="row">
                    <div class="cell-2" style="text-align: left;">
                        <p>Add On (Plan B): US$</p>
                    </div>
                    <div class="cell-2">
                        <p><?php echo $_SESSION['b_add_on'] ?></p>
                    </div>

                </div>
                <div class="row">
                    <div class="cell-2" style="text-align: left;">
                        <p>Add On (Plan C): US$</p>
                    </div>
                    <div class="cell-2">
                        <p><?php echo $_SESSION['c_add_on'] ?></p>
                    </div>

                </div>
            </div>
            <div class="type-payment">
                <p>
                    Type of Payment
                </p>
                <label><input type="checkbox" id="cbox1" value="cash">Cash</label>
                <label><input type="checkbox" id="cbox2" value="check">Check</label>
                <label><input type="checkbox" id="cbox3" value="credit">Credit Card</label>
            </div>


            <div class="table3">

                <div class="heading">
                    <div class="cell-3" style="padding-left: 5px; margin-bottom: -8px; font-weight: bold; font-size: 10px;">
                        <p>Schedule of Payment</p>
                    </div>
                    <div class="cell-3">
                        <p></p>
                    </div>
                </div>
                <div>
                    <label class="check"><input type="checkbox" id="cbox1" value="PA">Plan A</label>
                </div>
                <div class='total'>
                    total:
                    <span class='price'><?php echo $_SESSION['a_total']; ?></span>
                </div>
                <div>

                    <label class="check"><input type="checkbox" id="cbox1" value="PB">Plan B</label>
                </div>
                <div class='installment'>1st inst:
                    <span class='installment-price' style="margin-left:45;"><?php echo $_SESSION['b_down']; ?></span>
                </div>
                <div class='installment'>2nd inst:
                    <span class='installment-price' style="margin-left:42;"><?php echo $_SESSION['b_give']; ?></span>
                </div>
                <div class='total'>
                    total:
                    <span class='price'><?php echo $_SESSION['b_total']; ?></span>
                </div>
                <div>

                    <label class="check"><input type="checkbox" id="cbox1" value="PC">Plan C</label>
                </div>
                <div class='installment'>1st inst:
                    <span class='installment-price' style="margin-left:45;"><?php echo $_SESSION['c_down']; ?></span>
                </div>
                <div class='installment'>2nd inst:
                    <span class='installment-price' style="margin-left:42;"><?php echo $_SESSION['c_give']; ?></span>
                </div>
                <div class='installment' >3rd inst:
                    <span class='installment-price' style="margin-left:44;"><?php echo $_SESSION['c_give']; ?></span>
                </div>
                <div class='total'>
                    total:
                    <span class='price'><?php echo $_SESSION['c_total']; ?></span>
                </div>
                <hr>
                <div class="note-due">
                    <div class='note' style="font-weight: bold; text-decoration: underline;">Note:
                        <span class='duedate' style="margin-left:60; font-weight: bold; text-decoration: underline;">Due Date:</span>
                    </div>
                    <div class='note'>Plan A / 1st Inst.
                        <span class='duedate' style="margin-left:18;">Upon Enrollment</span>
                    </div>
                    <div class='note' >2nd Inst.

                        <?php
                        $active_term = $this->db->get_where('settings', array(
                                    'type' => 'active_term'
                                ))->row()->description;
                        $due_first = $this->db->get_where('sisfu_dues', array(
                                    'active_term' => $active_term
                                ))->row()->due_first;
                        $due_second = $this->db->get_where('sisfu_dues', array(
                                    'active_term' => $active_term
                                ))->row()->due_second;
                        ?>

                        <span class='duedate' style="margin-left:47;"><?php
                            $d1 = new DateTime($due_first);
                            $d1 = $d1->format('d M Y');
                            echo $d1;
                            ?></span>
                    </div>
                    <div class='note' >3rd Inst.
                        <span class='duedate' style="margin-left:49;"><?php
                            $d2 = new DateTime($due_second);
                            $d2 = $d2->format('d M Y');
                            echo $d2;
                            ?></span>
                    </div>
                </div>
                <div class="approved-by">
                         APPROVED BY/ DATE     
                </div>
                <span class="student-sig">
                     STUDENT’S SIGNATURE / DATE 
                </span>
                <span class="OR-no">
                     O.R. NO. / DATE / AMOUNT PAID 
                </span>
                <div class='reminder'>
                    <p>REMINDER: LATE PAYMENT FEE OF USD 150.00 SHALL BE CHARGED WHEN A STUDENT ENROLLS (PAYS TUITION FEES) AFTER                     </p>
                    <p>THE FIRST DAY OF CLASS FOR THE TERM</p>
                    <hr>
                </div>
            </div>
        </div>









        <!-- SECOND CARD --> 









        <div class="card2"> 
            <div>
                <div class="southville">
                    <p>SOUTHVILLE INTERNATIONAL SCHOOL</p>
                    <p style="margin-bottom: 0px; margin-top: -15px;">AFFILIATED WITH FOREIGN UNIVERSITIES</p>
                </div>
                <div class="copy-type">Registrar's</div>
                <div class='sisfu-qsf'>
                    <p>Registration Card</p>
                    <p style="margin-bottom: -3px; margin-top: -8px;">SISFU/QSF-REG-004</p>
                </div>
                <div class="student-name">
                    <?php
//                        echo 'Mr. ' . $row['first_name'] . ' ' . $row['middle_name'] . ' ' . $row['last_name'];
                    ?>
                    Student Name: <?php echo strtoupper($full_name) ?>
                </div>
                <span class="student-level">
                    Year Level- Term: <?php echo $level; ?>
                </span>
                <span class="registration-number">
                    Reg No. <?php echo $reg_no ?>
                </span>
                <div class="student-number">
                    Student Number: <?php echo $row['student_no'] ?>
                </div>
                <span class="student-year">
                    School Year: <?php
                    $this_year = date('Y');
                    $last_year = date("Y", strtotime("-1 year", time()));
                    echo $last_year . '-' . $this_year;
                    ?>
                </span>
                <div class="student-status">
                    Status/Scholarship: <?php echo $scholarship ?>
                </div>
                <span class="student-course">
                    Course: <?php
                    echo $course;
                    ?>
                </span>
            </div>

            <!--Table-->

            <div class="table">

                <div class="heading">
                    <div class="cell-1" style="padding-left: 100px; padding-right: 100px; font-weight: bold; font-size: 10px;">
                        <p>Subjects Enrolled</p>
                    </div>
                    <div class="cell-1" >
                        <p>Units</p>
                    </div>
                    <div class="cell-1" style="padding-left: 20px; padding-right: 20px;">
                        <p>Day</p>
                    </div>
                    <div class="cell-1" style="padding-left: 10px; padding-right: 10px;">
                        <p>Time</p>
                    </div>
                    <div class="cell-1" style="padding-left: 20px; padding-right: 20px;">
                        <p>Rm</p>
                    </div>
                    <div class="cell-1" style="padding-left: 20px; padding-right: 20px;">
                        <p>Sig</p>
                    </div>
                </div>
                <?php
                for ($x = 1; $x < 11; $x++):

                    $subjs_arr = explode(",", $row['enrolled_subjects']);
                    if ($subjs_arr[0] == '') {
                        $subjs_len = 0;
                    } else {
                        $subjs_len = count($subjs_arr);
                    }
                    $this->db->where('subject_id', $subjs_arr[$x - 1]);
                    $q = $this->db->get('sisfu_subjects');
                    $result = $q->result_array();

                    if ($q->num_rows() == 0) {
                        ?>
                        <div class="row">
                            <div class="cell-1" style="text-align: left;">
                                <p></p>
                            </div>
                            <div class="cell-1">
                                <p></p>
                            </div>
                            <div class="cell-1">
                                <p></p>
                            </div>
                            <div class="cell-1">
                                <p></p>
                            </div>
                            <div class="cell-1">
                                <p></p>
                            </div>
                            <div class="cell-1">
                                <p></p>
                            </div>
                        </div>
                        <?php
                    } else {

                        foreach ($result as $row2):
                            ?>
                            <div class="row">
                                <div class="cell-1" style="text-align: left;">
                                    <p><?php echo $row2['subject_name'] ?></p>
                                </div>
                                <div class="cell-1">
                                    <p><?php echo $row2['units'] ?></p>
                                </div>
                                <div class="cell-1">
                                    <p></p>
                                </div>
                                <div class="cell-1">
                                    <p></p>
                                </div>
                                <div class="cell-1">
                                    <p></p>
                                </div>
                                <div class="cell-1">
                                    <p></p>
                                </div>
                            </div>
                            <?php
                        endforeach;
                    }
                endfor;
                ?>

            </div>

            <div class="table3">

                <div class="heading">
                    <div class="cell-3" style="padding-left: 5px; margin-bottom: -8px; font-weight: bold; font-size: 10px;">
                        <p>Schedule of Payment</p>
                    </div>
                    <div class="cell-3">
                        <p></p>
                    </div>
                </div>
                <div>
                    <label class="check"><input type="checkbox" id="cbox1" value="PA">Plan A</label>
                </div>
                <div class='total'>
                    total:
                    <span class='price'><?php echo $_SESSION['a_total']; ?></span>
                </div>
                <div>

                    <label class="check"><input type="checkbox" id="cbox1" value="PB">Plan B</label>
                </div>
                <div class='installment'>1st inst:
                    <span class='installment-price' style="margin-left:45;"><?php echo $_SESSION['b_down']; ?></span>
                </div>
                <div class='installment'>2nd inst:
                    <span class='installment-price' style="margin-left:42;"><?php echo $_SESSION['b_give']; ?></span>
                </div>
                <div class='total'>
                    total:
                    <span class='price'><?php echo $_SESSION['b_total']; ?></span>
                </div>
                <div>

                    <label class="check"><input type="checkbox" id="cbox1" value="PC">Plan C</label>
                </div>
                <div class='installment'>1st inst:
                    <span class='installment-price' style="margin-left:45;"><?php echo $_SESSION['c_down']; ?></span>
                </div>
                <div class='installment'>2nd inst:
                    <span class='installment-price' style="margin-left:42;"><?php echo $_SESSION['c_give']; ?></span>
                </div>
                <div class='installment' >3rd inst:
                    <span class='installment-price' style="margin-left:44;"><?php echo $_SESSION['c_give']; ?></span>
                </div>
                <div class='total'>
                    total:
                    <span class='price'><?php echo $_SESSION['c_total']; ?></span>
                </div>
                <hr>
                <div class="note-due">
                    <div class='note' style="font-weight: bold; text-decoration: underline;">Note:
                        <span class='duedate' style="margin-left:60; font-weight: bold; text-decoration: underline;">Due Date:</span>
                    </div>
                    <div class='note'>Plan A / 1st Inst.
                        <span class='duedate' style="margin-left:18;">Upon Enrollment</span>
                    </div>
                    <div class='note' >2nd Inst.
                        <span class='duedate' style="margin-left:47;"><?php echo $d1 ?></span>
                    </div>
                    <div class='note' >3rd Inst.
                        <span class='duedate' style="margin-left:49;"><?php echo $d2 ?></span>
                    </div>
                </div>
                <div class="reminder" style="margin-top: 20px; ">
                    <p style="margin-bottom: 25px;">                                                                                                                                                                                                                                          
                    </p>
                    <hr>
                </div>
            </div>
        </div>








        <!-- THIRD CARD --> 








        <div class="card1" style="margin-top: 100px;"> 
            <div>
                <div class="southville">
                    <p>SOUTHVILLE INTERNATIONAL SCHOOL</p>
                    <p style="margin-bottom: 0px; margin-top: -15px;">AFFILIATED WITH FOREIGN UNIVERSITIES</p>
                </div>
                <div class="copy-type">Accounting's</div>
                <div class='sisfu-qsf'>
                    <p>Registration Card</p>
                    <p style="margin-bottom: -3px; margin-top: -8px;">SISFU/QSF-REG-004</p>
                </div>
                <div class="student-name">
                    Student Name: <?php echo strtoupper($full_name) ?>
                </div>
                <span class="student-level">
                    Year Level- Term: <?php echo $level ?>
                </span>
                <span class="registration-number">
                    Reg No.  <?php echo $reg_no ?>
                </span>
                <div class="student-number">
                    Student Number: <?php echo $row['student_no'] ?>
                </div>
                <span class="student-year">
                    School Year: <?php echo $school_year ?>
                </span>
                <div class="student-status">
                    Status/Scholarship: <?php echo $scholarship ?>
                </div>
                <span class="student-course">
                    Course: <?php echo $course ?>
                </span>
            </div>

            <!--Table-->

            <div class="table" style="width:42% !important">

                <div class="heading">
                    <div class="cell-1" style="padding-left: 100px; padding-right: 100px; font-weight: bold; font-size: 10px;">
                        <p>Subjects Enrolled</p>
                    </div>
                    <div class="cell-1" >
                        <p>Units</p>
                    </div>
                </div>
                <div class="row">
                    <div class="cell-1" style="text-align: left;">
                        <p>1. Research Project</p>
                    </div>
                    <div class="cell-1">
                        <p>3</p>
                    </div>
                </div>
                <?php
                for ($x = 1; $x < 11; $x++):

                    $subjs_arr = explode(",", $row['enrolled_subjects']);
                    if ($subjs_arr[0] == '') {
                        $subjs_len = 0;
                    } else {
                        $subjs_len = count($subjs_arr);
                    }
                    $this->db->where('subject_id', $subjs_arr[$x - 1]);
                    $q = $this->db->get('sisfu_subjects');
                    $result = $q->result_array();

                    if ($q->num_rows() == 0) {
                        ?>
                        <div class="row">
                            <div class="cell-1" style="text-align: left;">
                                <p></p>
                            </div>
                            <div class="cell-1">
                                <p></p>
                            </div>
                        </div>
                        <?php
                    } else {

                        foreach ($result as $row2):
                            ?>
                            <div class="row">
                                <div class="cell-1" style="text-align: left;">
                                    <p><?php echo $row2['subject_name'] ?></p>
                                </div>
                                <div class="cell-1">
                                    <p><?php echo $row2['units'] ?></p>
                                </div>
                            </div>
                            <?php
                        endforeach;
                    }
                endfor;
                ?>



            </div>
            <div class="table2">

                <div class="heading">
                    <div class="cell-2" style="text-decoration: underline; padding-right: 30px; font-weight: bold; font-size: 10px;">
                        <p>Breakdown of fees:</p>
                    </div>
                    <div class="cell-2">
                        <p></p>
                    </div>
                </div>
                <div class="row">
                    <div class="cell-2" style="text-align: left;">
                        <p>Tuition Fee: US$</p>
                    </div>
                    <div class="cell-2">
                        <p><?php echo $_SESSION['tuition'] ?></p>
                    </div>

                </div>
                <div class="row">
                    <div class="cell-2" style="text-align: left;">
                        <p>Net Total Tuition Fee:</p>
                    </div>
                    <div class="cell-2">
                        <p><?php echo $_SESSION['a_total'] ?></p>
                    </div>

                </div>
                <div class="row">
                    <div class="cell-2" style="text-align: left;">
                        <p>Registration Fee:</p>
                    </div>
                    <div class="cell-2">
                        <p><?php echo $_SESSION['ir_fee'] ?></p>
                    </div>

                </div>
                <div class="row">
                    <div class="cell-2" style="text-align: left;">
                        <p>Miscellaneous Fee:</p>
                    </div>
                    <div class="cell-2">
                        <p><?php echo $_SESSION['misc'] ?></p>
                    </div>

                </div>
                <div class="row">
                    <div class="cell-2" style="text-align: left;">
                        <p>Other: Late Enrolment Fee</p>
                    </div>
                    <div class="cell-2">
                        <p><?php echo $_SESSION['other'] ?></p>
                    </div>

                </div>
                <div class="row">
                    <div class="cell-2" style="text-align: left;">
                        <p>Add On (Plan B): US$</p>
                    </div>
                    <div class="cell-2">
                        <p><?php echo $_SESSION['b_add_on'] ?></p>
                    </div>

                </div>
                <div class="row">
                    <div class="cell-2" style="text-align: left;">
                        <p>Add On (Plan C): US$</p>
                    </div>
                    <div class="cell-2">
                        <p><?php echo $_SESSION['c_add_on'] ?></p>
                    </div>

                </div>
            </div>
            <div class="type-payment">
                <p>
                    Type of Payment
                </p>
                <label><input type="checkbox" id="cbox1" value="cash">Cash</label>
                <label><input type="checkbox" id="cbox2" value="check">Check</label>
                <label><input type="checkbox" id="cbox3" value="credit">Credit Card</label>
            </div>


            <div class="table3">

                <div class="heading">
                    <div class="cell-3" style="padding-left: 5px; margin-bottom: -8px; font-weight: bold; font-size: 10px;">
                        <p>Schedule of Payment</p>
                    </div>
                    <div class="cell-3">
                        <p></p>
                    </div>
                </div>
                <div>
                    <label class="check"><input type="checkbox" id="cbox1" value="PA">Plan A</label>
                </div>
                <div class='total'>
                    total:
                    <span class='price'><?php echo $_SESSION['a_total']; ?></span>
                </div>
                <div>

                    <label class="check"><input type="checkbox" id="cbox1" value="PB">Plan B</label>
                </div>
                <div class='installment'>1st inst:
                    <span class='installment-price' style="margin-left:45;"><?php echo $_SESSION['b_down']; ?></span>
                </div>
                <div class='installment'>2nd inst:
                    <span class='installment-price' style="margin-left:42;"><?php echo $_SESSION['b_give']; ?></span>
                </div>
                <div class='total'>
                    total:
                    <span class='price'><?php echo $_SESSION['b_total']; ?></span>
                </div>
                <div>

                    <label class="check"><input type="checkbox" id="cbox1" value="PC">Plan C</label>
                </div>
                <div class='installment'>1st inst:
                    <span class='installment-price' style="margin-left:45;"><?php echo $_SESSION['c_down']; ?></span>
                </div>
                <div class='installment'>2nd inst:
                    <span class='installment-price' style="margin-left:42;"><?php echo $_SESSION['c_give']; ?></span>
                </div>
                <div class='installment' >3rd inst:
                    <span class='installment-price' style="margin-left:44;"><?php echo $_SESSION['c_give']; ?></span>
                </div>
                <div class='total'>
                    total:
                    <span class='price'><?php echo $_SESSION['c_total']; ?></span>
                </div>
                <hr>
                <div class="note-due">
                    <div class='note' style="font-weight: bold; text-decoration: underline;">Note:
                        <span class='duedate' style="margin-left:60; font-weight: bold; text-decoration: underline;">Due Date:</span>
                    </div>
                    <div class='note'>Plan A / 1st Inst.
                        <span class='duedate' style="margin-left:18;">Upon Enrollment</span>
                    </div>
                    <div class='note' >2nd Inst.
                        <span class='duedate' style="margin-left:47;"><?php echo $d1 ?></span>
                    </div>
                    <div class='note' >3rd Inst.
                        <span class='duedate' style="margin-left:49;"><?php echo $d2 ?></span>
                    </div>
                </div>
                <div class="approved-by">
                         STUDENT’S SIGNATURE / DATE     
                </div>
                <span class="OR-no">
                        VERIFIED BY/ DATE   
                </span>
            </div>
        </div>   
    </div>

    <div class="hidden-print page-header">
        <a href="<?php echo base_url() . 'index.php?admin/student_assess/' . $_SESSION['student_id'] ?>">Back to <?php echo $full_name ?>'s assessment</a>
        <br>
        <button id="print" class="btn btn-primary">Print</button>
    </div>
<?php endforeach; ?>
<!-- Bottom Scripts -->
<script src="assets/js/jquery-1.11.0.min.js"></script>

<script src="assets/js/custom.js"></script>
<script>
    $(document).ready(function () {
        $('#print').on('click', function () {
            window.print();
        });
    });
</script>