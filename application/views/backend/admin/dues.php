
<a href="javascript:;" onclick="showAjaxModal('<?php echo base_url(); ?>index.php?modal/popup/modal_due_add/');" 
   class="btn btn-primary pull-right hidden" >
    <i class="entypo-plus-circled"></i>
    <?php echo get_phrase('add_new_due'); ?>
</a> 
<br><br>
<table class="table table-bordered datatable table-hover" id="table_export">
    <thead>
        <tr>
            <th>ID</th>
<th><div><?php echo get_phrase('term_number'); ?></div></th>
<th><div><?php echo get_phrase('due_first'); ?></div></th>
<th><div><?php echo get_phrase('due_second'); ?></div></th>
<th><div><?php echo get_phrase('options'); ?></div></th>
</tr>
</thead>
<tbody>
    <?php
    foreach ($result as $row):
        ?>
        <tr>
            <td><?php echo $row['due_id']; ?></td>
            <td><?php echo $row['active_term']; ?></td>
            <td><?php echo $row['due_first'];  ?></td>
            <td><?php echo $row['due_second'];  ?></td>
           
            <td>
                <div class="btn-group">
                    <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                        Action <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-default pull-right" role="menu">

                        <!--user EDITING LINK--> 
                        <li>
                            <a href="#" onclick="showAjaxModal('<?php echo base_url(); ?>index.php?modal/popup/modal_due_edit/<?php echo $row['due_id']; ?>');">
                                <i class="fa fa-pencil"></i>
                                <?php echo get_phrase('edit'); ?>
                            </a>
                        </li>
                        <li class="divider"></li>

                        <!--user DELETION LINK--> 
                        <li>
                            <a href="#" onclick="confirm_modal('<?php echo base_url(); ?>index.php?admin/dues/delete/<?php echo $row['due_id']; ?>');">
                                <i class="fa fa-trash-o"></i>
                                <?php echo get_phrase('delete'); ?>
                            </a>
                        </li>
                    </ul>
                </div>
            </td>

        </tr>

    <?php endforeach; ?>
</tbody>
</table>



<!-----  DATA TABLE EXPORT CONFIGURATIONS ---->                      
<script type="text/javascript">

    jQuery(document).ready(function ($)
    {
        var datatable = $("#table_export").dataTable({
            "sPaginationType": "bootstrap",
            "sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-3 col-left'i><'col-xs-9 col-right'p>>",
            "oTableTools": {
                "aButtons": [
                    {
                        "sExtends": "xls",
                        "mColumns": [0, 1, 2, 3]
                    },
                    {
                        "sExtends": "pdf",
                        "mColumns": [0, 1, 2, 3]
                    },
                    {
                        "sExtends": "print",
                        "fnSetText": "Press 'esc' to return",
                        "fnClick": function (nButton, oConfig) {
                            datatable.fnSetColumnVis(5, false);

                            this.fnPrint(true, oConfig);

                            window.print();

                            $(window).keyup(function (e) {
                                if (e.which == 27) {
                                    datatable.fnSetColumnVis(5, true);
                                }
                            });
                        },
                    },
                ]
            },
        });

        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });
    });

</script>