<?php
$edit_data = $this->db->get_where('sisfu_scholarships_meta', array('scholarship_id' => $param2))->result_array();
foreach ($edit_data as $row):
    ?>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary" data-collapsed="0">
                <div class="panel-heading">
                    <div class="panel-title"><span class="primary-color">
                        <i class="entypo-plus-circled"></i>
                        <?php echo get_phrase('edit_discount'); ?></span>
                    </div>
                </div>
                <div class="panel-body">

                    <?php echo form_open(base_url() . 'index.php?admin/scholarships/edit/' . $param2, array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data')); ?>

                    <div class="form-group">
                        <label for="scholarship_name" class="col-sm-3 control-label"><?php echo get_phrase('scholarship_name'); ?></label>

                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="scholarship_name" name="scholarship_name" value="<?php echo $row['scholarship_name'] ?>" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                        </div> 
                    </div>

                    <div class="form-group">
                        <label for="discount_value" class="col-sm-3 control-label"><?php echo get_phrase('discount_value'); ?></label>

                        <div class="col-sm-7">
                            <div class="input-group">
                                <input type="number" class="form-control" id="discount_value" name="discount_value" value="<?php echo $row['discount_value'] ?>" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" min="0" step="0.01">
                                <span class="input-group-addon">%</span>
                            </div> 
                        </div> 
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-5">
                            <button type="submit" class="btn btn-default"><?php echo get_phrase('save'); ?></button>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
<?php endforeach; ?>