<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary" data-collapsed="0">
            <div class="panel-heading">
                <div class="panel-title"><span class="primary-color">
                    <i class="entypo-plus-circled"></i>
                    <?php echo get_phrase('add_user'); ?></span>
                </div>
            </div>
            <div class="panel-body">

                <?php echo form_open(base_url() . 'index.php?admin/users/create/', array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data')); ?>

                <!--NAMES-->
                <!--USERNAME-->
                <div class="form-group">
                    <label for="username" class="col-sm-3 control-label"><?php echo get_phrase('username'); ?></label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control" id="username" name="username" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" value="" autofocus>
                    </div>
                </div>
                <!--EMAIL-->
                <div class="form-group">
                    <label for="email" class="col-sm-3 control-label"><?php echo get_phrase('email'); ?></label>

                    <div class="col-sm-7">
                        <input type="email" class="form-control" id="email" name="email" value="" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                    </div> 
                </div>
                <!--password-->
                <div class="form-group">
                    <label for="password" class="col-sm-3 control-label"><?php echo get_phrase('password'); ?></label>

                    <div class="col-sm-7">
                        <div class="input-group">
                            <input type="text" class="form-control" id="password" name="password" value="" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" >
                            <span class="input-group-addon generate-password"><i class="fa fa-cog"></i> Generate Password</span>
                        </div> 
                    </div>
                </div>
                <!--ACCESS LEVEL-->
                <div class="form-group">
                    <label for="access_level" class="col-sm-3 control-label"><?php echo get_phrase('access_level'); ?> </label>

                    <div class="col-sm-7">
                        <select name="access_level" class="form-control" id="access_level" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                            <option value=""><?php echo get_phrase('select'); ?></option>
                            <?php
                            $access = $this->db->get('sisfu_access_levels')->result_array();
                            foreach ($access as $row):
                                ?>
                                <option value="<?php echo $row['access_level']; ?>">
                                    <?php echo $row['access_level'] . ' - ' . $row['definition']; ?>
                                </option>
                                <?php
                            endforeach;
                            ?>
                        </select>
                    </div> 
                </div>
                
                

                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-default"><?php echo get_phrase('add_user'); ?></button>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        var password = $('#password');

        function randomString(length, chars) {
            var result = '';
            for (var i = length; i > 0; --i)
                result += chars[Math.floor(Math.random() * chars.length)];
            return result;
        }

        $('.generate-password').on('click', function () {
            var newpass = randomString(8, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
            password.val(newpass);
        });

    });
</script>