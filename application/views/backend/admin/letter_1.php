<style>
    .print-heading{
        height:155px;
    }
    .sisfu-qsf{
        text-align:center;
        font-size: 10px;
        margin-top: -20px;
    }
    .date-div{
        margin-left: 70px;
        font-size: 13px;
        margin-top: 0px;
        margin-bottom: -10px;
    }
    .reference_no-div{
        margin-left: 70%;
        position: absolute;
        font-size: 13px;
        margin-top:-10px
    }
    .student-name{
        margin-left: 70px;
        margin-top: 10px;
        font-weight: bold;
        font-size: 13px;
    }
    .permanent-address{
        margin-left: 70px;
        font-weight: bold;
        font-size: 13px;
    }
    .main-heading{
        text-align:center;
        font-weight: bold;
        font-size: 13px;
        margin-top:4px
    }
    .dear-parent{
        margin-left: 70px;
        font-size: 13px;
        margin-top:4px
    }
    .congratulations{
        text-align: justify;
        margin-left: 70px;
        width: 83%;
        font-size: 13px;
        margin-top:10px
    }
    .discount-percent{
        text-decoration:underline;
        font-weight:bold
    }
    .degree-name{
        text-decoration:underline;
        font-weight:bold
    }
    .quarters{
        text-decoration:underline;
        font-weight:bold
    }
    .commencement{
        text-decoration:underline;
        font-weight:bold
    }
    .bullet-list{
        text-align: justify;
        margin-left: 70px;
        width: 80%;
        font-size: 13px;
        margin-top:18px
    }
    .bottom-paragraph{
        text-align: justify;
        margin-left: 70px;
        width: 83%;
        font-size: 13px;
        margin-top:10px
    }
    .submission-deadline{
        text-decoration:underline;
        font-weight:bold
    }
    .schedule-date{
        text-decoration:underline;
        font-weight:bold
    }
    .bottom-left-column{
        margin-left: 70px;
        margin-top: 10px;
        font-size: 13px;
    }
    .bottom-right-column{
        margin-top: -65px;
        font-size: 13px;
        margin-left: 65%
    }
    .print-footer{
        font-size: 11px;
        line-height: 3px;
        text-align: right;
        margin-right: 10px;
        bottom: 8px;
        position: absolute;
        right: 1%;
    }
    .print-footing{
        height: 12px;
        margin-bottom: -5px;
    }
</style>
<body >

    <?php
//    $this->db->where('student_id', $student_id);
//    $result = $this->db->get('sisfu_students')->result_array();
//    foreach ($result as $row):
    ?>

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 print-header">
                <center>
                    
                    <img class="print-heading" src="<?php echo base_url() . 'assets/images/letter-heading.jpg' ?>"/>
                </center>
            </div>
        </div>
        <div class='sisfu-qsf'>
            SISFU/QSF-REG-032
        </div>
        <div class='sisfu-qsf'>
            Rev 003 01/12/15
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="date-div">
                    <?php // echo date('d F Y') ?>
                    31 December 2036
                </div>
                <div class="reference_no-div">
                    Office of the Registrar<br>
                    Ref No. ################
                </div>
                <div class="student-name">
                    <?php
//                        echo 'Mr. ' . $row['first_name'] . ' ' . $row['middle_name'] . ' ' . $row['last_name'];
                    ?>
                    Mr. This is a test
                </div>
                <div class="permanent-address">
                    <?php
//                     echo $row['current_address'];
                    ?>
                    221B Baker Street,<br>London, England
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="main-heading">
                    CONDITIONAL LETTER OF ACCEPTANCE
                </div>
            </div>
            <div class="col-sm-12">
                <div class="dear-parent">
                    <?php
//                        echo 'Dear Mr. <strong>' . $row['last_name'] . '</strong>';
                    ?>
                    Dear Mr. Parent
                </div>
            </div>
            <div class="col-sm-12">
                <div class="congratulations">
                    Congratulations! It is my distinct pleasure to inform you that you have been accepted into Southville International School 
                    affiliated with Foreign Universities for an undergraduate study with a  

                    <?php
//                        $this->db->where('course_id', $row['course_id']);
//                        $result = $this->db->get('sisfu_courses')->result_array();
//                        foreach ($result as $course):
                    ?>
                    <span class="discount-percent">25% discount on tuition fee only<?php // echo $course['discount_percent']         ?> </span>
                    in
                    <span class="degree-name">Higher National Diploma in Business (Management)<?php // echo $course['degree_name']         ?> </span>
                    for the School Year 
                    <?php
                    ?>
                    2015-2016
                    <?php
                    ?>
                    <span class="quarters"> 3<superscript>rd</superscript> trimester,</span> commencing on 
                    <span class="commencement">#########</span>, conditional upon the completion of the following requirements:
                    <?php // endforeach; ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="bullet-list">
                    <ul>
                        <li>Submission of the following academic records</li>
                        <ul>
                            <li>Recommendation Letter from School Principal or Guidance Counselor</li>
                            <li>Recommendation Letter from a Professor or Teacher</li>
                            <li>Official Transcript of of Tertiary Education (F137))</li>
                            <li>Transfer Credentials/Honorable Dismissal</li>
                            <li>Course Description</li>
                        </ul>
                        <li>Submission of the following:</li>
                        <ul>
                            <li>Four (4) 2” x  2” pictures with white background</li>
                            <li>Original NSO birth certificate (if applicable)</li>
                            <li>Photocopy of passport information page and entry visa page</li>
                            <li>500-word essay</li>
                            <li>Copy of Alien Certificate of Registration(ACR)</li>
                            <li>Copy of Student Visa and/or Special Study Permit</li>
                        </ul>
                        <li>Payment of admission and application fees</li>

                    </ul>
                </div>
                <div class="bottom-paragraph">
                    <p>
                        All requirements must be submitted on or before 
                        <span class="submission-deadline">12 February 2016.</span> 
                        Unconditional Letter of Acceptance will be issued upon completion. International Registration Fees (IRF’s) are 
                        set by the overseas institutions and are different from tuition fees. IRF’s therefore will be paid by all students. 
                        <strong>The period of registration for the Higher National Diploma is only for two (2) years from date of registration.</strong>
                    </p>
                    <p>
                        The academic programmes are taught at British and Australian university standards. As such the 
                        <strong>College Preparatory Program, scheduled</strong> on
                        <span class="schedule-date">June 2016</span>
                        is a requirement for all incoming freshmen. You will have the opportunity to learn with students from all around the world in one of 
                        the most focused academic institutions in the Philippines and it is our hope that you will contribute your special talents to the rich 
                        diversity that defines the Southville student body.
                    </p>
                    <p>
                        It is essential that you take the <strong>TOEIC Exams</strong> for appropriate placement and assistance. The Marketing Department shall 
                        coordinate with you for the test schedules.
                    </p>
                    <p>
                        On behalf of the entire faculty and management, congratulations on your conditional admission to Southville International School 
                        affiliated with Foreign Universities.
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 bottom-left-column">
                <p>Yours sincerely,</p>

                <p><strong>Ms. Caroline F. Mediodia</strong><br>
                    Registrar</p>
            </div>
            <div class="col-md-6 bottom-right-column">
                <p>Noted by:</p>

                <p><strong>Mr. Amir Tohid</strong><br>
                    Administrative Head<br>
                    School of Business and Computing
                </p>
            </div>
        </div>
        <div class="row">
            <div class="print-footer col-sm-12">
                <img class="print-footing" src="<?php echo base_url() . 'assets/images/letter-footer.png' ?>"/>

                <p>Lima corner Luxembourg Street BF International, Las Piñas City, Philippines 1741</p>
                <p>+632-820-918   |   +632-820-6774</p>
            </div>
        </div>
    </div>
    <?php // endforeach; ?>
</body>

<script>
//    $('document').ready(function () {
//        window.print();
//    });
</script>
