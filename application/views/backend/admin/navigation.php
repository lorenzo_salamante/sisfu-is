
<div class="sidebar-menu">
    <header class="logo-env" >

        <!-- logo -->
        <div class="logo logo-container" style="">
            <a href="<?php echo base_url(); ?>">
                <img src="assets/images/logo_mini_with_shadow.png"  style="max-height:60px;"/>
            </a>
        </div>

        <!-- logo collapse icon -->
        <div class="sidebar-collapse" style="">
            <a href="#" class="sidebar-collapse-icon with-animation">

                <i class="entypo-menu"></i>
            </a>
        </div>

        <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
        <div class="sidebar-mobile-menu visible-xs">
            <a href="#" class="with-animation">
                <i class="entypo-menu"></i>
            </a>
        </div>
    </header>
    <div style=""></div>
    <?php
    if ($access_level < 0) {
        //ORIGINAL NAV LIST
        ?>
        <ul id="main-menu" class="">
            <!-- add class "multiple-expanded" to allow multiple submenus to open -->
            <!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->
            <!-- DASHBOARD -->
            <li class="<?php if ($page_name == 'dashboard') echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>index.php?admin/dashboard">
                    <i class="entypo-gauge"></i>
                    <span><?php echo get_phrase('dashboard'); ?></span>
                </a>
            </li>

            <!-- REGISTER STUDENT -->
            <li class="<?php if ($page_name == 'student_add') echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>index.php?admin/student_add">
                    <i class="fa fa-group"></i>
                    <span><?php echo get_phrase('admit_student'); ?></span>
                </a>
            </li>

            <!-- STUDENT -->
            <li class="<?php
            if ($page_name == 'student_add' ||
                    $page_name == 'student_bulk_add' ||
                    $page_name == 'student_information' ||
                    $page_name == 'student_marksheet')
                echo 'opened active has-sub';
            ?> ">
                <a href="#">
                    <i class="fa fa-group"></i>
                    <span><?php echo get_phrase('student'); ?></span>
                </a>
                <ul>
                    <!--STUDENT ADMISSION--> 
                    <li class="<?php if ($page_name == 'student_add') echo 'active'; ?> ">
                        <a href="<?php echo base_url(); ?>index.php?admin/student_add">
                            <span><i class="entypo-dot"></i> <?php echo get_phrase('admit_student'); ?></span>
                        </a>
                    </li>

                    <!--STUDENT BULK ADMISSION--> 
                    <li class="<?php if ($page_name == 'student_bulk_add') echo 'active'; ?> ">
                        <a href="<?php echo base_url(); ?>index.php?admin/student_bulk_add">
                            <span><i class="entypo-dot"></i> <?php echo get_phrase('admit_bulk_student'); ?></span>
                        </a>
                    </li>

                    <!--STUDENT INFORMATION--> 
                    <li class="<?php if ($page_name == 'student_information') echo 'opened active'; ?> ">
                        <a href="#">
                            <span><i class="entypo-dot"></i> <?php echo get_phrase('student_information'); ?></span>
                        </a>
                        <ul>
                            <?php
                            $classes = $this->db->get('class')->result_array();
                            foreach ($classes as $row):
                                ?>
                                <li class="<?php if ($page_name == 'student_information' && $class_id == $row['class_id']) echo 'active'; ?>">
                                    <a href="<?php echo base_url(); ?>index.php?admin/student_information/<?php echo $row['class_id']; ?>">
                                        <span><?php echo get_phrase('class'); ?> <?php echo $row['name']; ?></span>
                                    </a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </li>

                    <!--STUDENT MARKSHEET--> 
                    <li class="<?php if ($page_name == 'student_marksheet') echo 'opened active'; ?> ">
                        <a href="#">
                            <span><i class="entypo-dot"></i> <?php echo get_phrase('student_marksheet'); ?></span>
                        </a>
                        <ul>
                            <?php
                            $classes = $this->db->get('class')->result_array();
                            foreach ($classes as $row):
                                ?>
                                <li class="<?php if ($page_name == 'student_marksheet' && $class_id == $row['class_id']) echo 'active'; ?>">
                                    <a href="<?php echo base_url(); ?>index.php?admin/student_marksheet/<?php echo $row['class_id']; ?>">
                                        <span><?php echo get_phrase('class'); ?> <?php echo $row['name']; ?></span>
                                    </a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </li>
                </ul>
            </li>

            <!-- TEACHER -->
            <li class="<?php if ($page_name == 'teacher') echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>index.php?admin/teacher">
                    <i class="entypo-users"></i>
                    <span><?php echo get_phrase('teacher'); ?></span>
                </a>
            </li>

            <!-- PARENTS -->
            <li class="<?php if ($page_name == 'parent') echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>index.php?admin/parent">
                    <i class="entypo-user"></i>
                    <span><?php echo get_phrase('parents'); ?></span>
                </a>
            </li>

            <!-- CLASS -->
            <li class="<?php
            if ($page_name == 'class' ||
                    $page_name == 'section')
                echo 'opened active';
            ?> ">
                <a href="#">
                    <i class="entypo-flow-tree"></i>
                    <span><?php echo get_phrase('class'); ?></span>
                </a>
                <ul>
                    <li class="<?php if ($page_name == 'class') echo 'active'; ?> ">
                        <a href="<?php echo base_url(); ?>index.php?admin/classes">
                            <span><i class="entypo-dot"></i> <?php echo get_phrase('manage_classes'); ?></span>
                        </a>
                    </li>
                    <li class="<?php if ($page_name == 'section') echo 'active'; ?> ">
                        <a href="<?php echo base_url(); ?>index.php?admin/section">
                            <span><i class="entypo-dot"></i> <?php echo get_phrase('manage_sections'); ?></span>
                        </a>
                    </li>
                </ul>
            </li>

            <!-- SUBJECT -->
            <li class="<?php if ($page_name == 'subject') echo 'opened active'; ?> ">
                <a href="#">
                    <i class="entypo-docs"></i>
                    <span><?php echo get_phrase('subject'); ?></span>
                </a>
                <ul>
                    <?php
                    $classes = $this->db->get('class')->result_array();
                    foreach ($classes as $row):
                        ?>
                        <li class="<?php if ($page_name == 'subject' && $class_id == $row['class_id']) echo 'active'; ?>">
                            <a href="<?php echo base_url(); ?>index.php?admin/subject/<?php echo $row['class_id']; ?>">
                                <span><?php echo get_phrase('class'); ?> <?php echo $row['name']; ?></span>
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </li>

            <!-- CLASS ROUTINE -->
            <li class="<?php if ($page_name == 'class_routine') echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>index.php?admin/class_routine">
                    <i class="entypo-target"></i>
                    <span><?php echo get_phrase('class_routine'); ?></span>
                </a>
            </li>

            <!-- DAILY ATTENDANCE -->
            <li class="<?php if ($page_name == 'manage_attendance') echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>index.php?admin/manage_attendance/<?php echo date("d/m/Y"); ?>">
                    <i class="entypo-chart-area"></i>
                    <span><?php echo get_phrase('daily_attendance'); ?></span>
                </a>

            </li>

            <!-- EXAMS -->
            <li class="<?php
            if ($page_name == 'exam' ||
                    $page_name == 'grade' ||
                    $page_name == 'marks' ||
                    $page_name == 'exam_marks_sms')
                echo 'opened active';
            ?> ">
                <a href="#">
                    <i class="entypo-graduation-cap"></i>
                    <span><?php echo get_phrase('exam'); ?></span>
                </a>
                <ul>
                    <li class="<?php if ($page_name == 'exam') echo 'active'; ?> ">
                        <a href="<?php echo base_url(); ?>index.php?admin/exam">
                            <span><i class="entypo-dot"></i> <?php echo get_phrase('exam_list'); ?></span>
                        </a>
                    </li>
                    <li class="<?php if ($page_name == 'grade') echo 'active'; ?> ">
                        <a href="<?php echo base_url(); ?>index.php?admin/grade">
                            <span><i class="entypo-dot"></i> <?php echo get_phrase('exam_grades'); ?></span>
                        </a>
                    </li>
                    <li class="<?php if ($page_name == 'marks') echo 'active'; ?> ">
                        <a href="<?php echo base_url(); ?>index.php?admin/marks">
                            <span><i class="entypo-dot"></i> <?php echo get_phrase('manage_marks'); ?></span>
                        </a>
                    </li>
                    <li class="<?php if ($page_name == 'exam_marks_sms') echo 'active'; ?> ">
                        <a href="<?php echo base_url(); ?>index.php?admin/exam_marks_sms">
                            <span><i class="entypo-dot"></i> <?php echo get_phrase('send_marks_by_sms'); ?></span>
                        </a>
                    </li>
                </ul>
            </li>

            <!-- PAYMENT -->
            <li class="<?php if ($page_name == 'invoice') echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>index.php?admin/invoice">
                    <i class="entypo-credit-card"></i>
                    <span><?php echo get_phrase('payment'); ?></span>
                </a>
            </li>

            <!-- ACCOUNTING -->
            <li class="<?php
            if ($page_name == 'income' ||
                    $page_name == 'expense' ||
                    $page_name == 'expense_category')
                echo 'opened active';
            ?> ">
                <a href="#">
                    <i class="entypo-suitcase"></i>
                    <span><?php echo get_phrase('accounting'); ?></span>
                </a>
                <ul>
                    <li class="<?php if ($page_name == 'income') echo 'active'; ?> ">
                        <a href="<?php echo base_url(); ?>index.php?admin/income">
                            <span><i class="entypo-dot"></i> <?php echo get_phrase('income'); ?></span>
                        </a>
                    </li>
                    <li class="<?php if ($page_name == 'expense') echo 'active'; ?> ">
                        <a href="<?php echo base_url(); ?>index.php?admin/expense">
                            <span><i class="entypo-dot"></i> <?php echo get_phrase('expense'); ?></span>
                        </a>
                    </li>
                    <li class="<?php if ($page_name == 'expense_category') echo 'active'; ?> ">
                        <a href="<?php echo base_url(); ?>index.php?admin/expense_category">
                            <span><i class="entypo-dot"></i> <?php echo get_phrase('expense_category'); ?></span>
                        </a>
                    </li>
                </ul>
            </li>

            <!-- LIBRARY -->
            <li class="<?php if ($page_name == 'book') echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>index.php?admin/book">
                    <i class="entypo-book"></i>
                    <span><?php echo get_phrase('library'); ?></span>
                </a>
            </li>

            <!-- TRANSPORT -->
            <li class="<?php if ($page_name == 'transport') echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>index.php?admin/transport">
                    <i class="entypo-location"></i>
                    <span><?php echo get_phrase('transport'); ?></span>
                </a>
            </li>

            <!-- DORMITORY -->
            <li class="<?php if ($page_name == 'dormitory') echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>index.php?admin/dormitory">
                    <i class="entypo-home"></i>
                    <span><?php echo get_phrase('dormitory'); ?></span>
                </a>
            </li>

            <!-- NOTICEBOARD -->
            <li class="<?php if ($page_name == 'noticeboard') echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>index.php?admin/noticeboard">
                    <i class="entypo-doc-text-inv"></i>
                    <span><?php echo get_phrase('noticeboard'); ?></span>
                </a>
            </li>

            <!-- MESSAGE -->
            <li class="<?php if ($page_name == 'message') echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>index.php?admin/message">
                    <i class="entypo-mail"></i>
                    <span><?php echo get_phrase('message'); ?></span>
                </a>
            </li>

            <!-- SETTINGS -->
            <li class="<?php
            if ($page_name == 'system_settings' ||
                    $page_name == 'manage_language' ||
                    $page_name == 'sms_settings')
                echo 'opened active';
            ?> ">
                <a href="#">
                    <i class="entypo-lifebuoy"></i>
                    <span><?php echo get_phrase('settings'); ?></span>
                </a>
                <ul>
                    <li class="<?php if ($page_name == 'system_settings') echo 'active'; ?> ">
                        <a href="<?php echo base_url(); ?>index.php?admin/system_settings">
                            <span><i class="entypo-dot"></i> <?php echo get_phrase('general_settings'); ?></span>
                        </a>
                    </li>
                    <li class="<?php if ($page_name == 'sms_settings') echo 'active'; ?> ">
                        <a href="<?php echo base_url(); ?>index.php?admin/sms_settings">
                            <span><i class="entypo-dot"></i> <?php echo get_phrase('sms_settings'); ?></span>
                        </a>
                    </li>
                    <li class="<?php if ($page_name == 'manage_language') echo 'active'; ?> ">
                        <a href="<?php echo base_url(); ?>index.php?admin/manage_language">
                            <span><i class="entypo-dot"></i> <?php echo get_phrase('language_settings'); ?></span>
                        </a>
                    </li>
                </ul>
            </li>

            <!-- ACCOUNT -->
            <li class="<?php if ($page_name == 'manage_profile') echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>index.php?admin/manage_profile">
                    <i class="entypo-lock"></i>
                    <span><?php echo get_phrase('account'); ?></span>
                </a>
            </li>

        </ul>

        <?php
    }
    else if ($access_level == 0) {
        //all modules
        ?>
        <ul id="main-menu" class="">
            <!-- add class "multiple-expanded" to allow multiple submenus to open -->
            <!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->

            <!-- DASHBOARD -->
            <li class="<?php if ($page_name == 'dashboard') echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>index.php?admin/dashboard">
                    <i class="entypo-gauge"></i>
                    <span><?php echo get_phrase('dashboard'); ?></span>
                </a>
            </li>

            <!-- MANAGE STUDENTS -->
    <!--            <li class="<?php // if ($page_name == 'students') echo 'active';     ?> ">
                <a href="<?php // echo base_url();     ?>index.php?admin/students">
                    <i class="fa fa-group"></i>
                    <span><?php // echo get_phrase('manage_students');     ?></span>
                </a>
            </li>-->

            <li class="<?php
            if ($page_name == 'student' ||
                    $page_name == 'student_add' ||
                    $page_name == 'student_edit' ||
                    $page_name == 'student_transcript' ||
                    $page_name == 'student_assess'
            )
                echo 'opened active';
            ?> ">
                <a href="#">
                    <i class="fa fa-group"></i>
                    <span><?php echo get_phrase('students'); ?></span>
                </a>
                <ul>
                    <li class="<?php if ($page_name == 'student' || $page_name == 'student_edit' || $page_name == 'student_assess' || $page_name == 'student_transcript') echo 'active'; ?> ">
                        <a href="<?php echo base_url(); ?>index.php?admin/student">
                            <span><i class="fa fa-book"></i>
                                <?php echo get_phrase('manage_students'); ?></span>
                        </a>
                    </li>
                    <li class="<?php if ($page_name == 'student_add') echo 'active'; ?> ">
                        <a href="<?php echo base_url(); ?>index.php?admin/student_add" target="_blank">
                            <span><i class="fa fa-plus-circle"></i>
                                <?php echo get_phrase('admit_new_student'); ?></span>
                        </a>
                    </li>
                </ul>
            </li>

            <!-- SETTINGS -->
            <li class="<?php
            if ($page_name == 'system_settings' ||
                    $page_name == 'manage_language' ||
                    $page_name == 'sms_settings' ||
                    $page_name == 'add_users' ||
                    $page_name == 'subjects' ||
                    $page_name == 'departments' ||
                    $page_name == 'discounts' ||
                    $page_name == 'scholarships' ||
                    $page_name == 'nationalities' ||
                    $page_name == 'status' ||
                    $page_name == 'curriculum' ||
                    $page_name == 'forms' ||
                    $page_name == 'dues' ||
                    $page_name == 'noticeboard' ||
                    $page_name == 'users')
                echo 'opened active';
            ?> ">
                <a href="#">
                    <i class="entypo-tools"></i>
                    <span><?php echo get_phrase('settings'); ?></span>
                </a>
                <ul>
                    <li class="<?php if ($page_name == 'users') echo 'active'; ?> ">
                        <a href="<?php echo base_url(); ?>index.php?admin/users">
                            <span><i class="fa fa-user"></i> <?php echo get_phrase('manage_users'); ?></span>
                        </a>
                    </li>
                    <li class="<?php if ($page_name == 'subjects') echo 'active'; ?> ">
                        <a href="<?php echo base_url(); ?>index.php?admin/subjects">
                            <span><i class="fa fa-file-text"></i> <?php echo get_phrase('manage_subjects'); ?></span>
                        </a>
                    </li>
                    <li class="<?php if ($page_name == 'departments') echo 'active'; ?> ">
                        <a href="<?php echo base_url(); ?>index.php?admin/departments">
                            <span><i class="fa fa-building"></i> <?php echo get_phrase('manage_schools'); ?></span>
                        </a>
                    </li>
                    <li class="<?php if ($page_name == 'discounts') echo 'active'; ?> ">
                        <a href="<?php echo base_url(); ?>index.php?admin/discounts">
                            <span><i class="fa fa-percent"></i> <?php echo get_phrase('manage_discounts'); ?></span>
                        </a>
                    </li>
                    <li class="<?php if ($page_name == 'scholarships') echo 'active'; ?> ">
                        <a href="<?php echo base_url(); ?>index.php?admin/scholarships">
                            <span><i class="fa fa-graduation-cap"></i> <?php echo get_phrase('manage_scholarships'); ?></span>
                        </a>
                    </li>
                    <li class="<?php if ($page_name == 'nationalities') echo 'active'; ?> ">
                        <a href="<?php echo base_url(); ?>index.php?admin/nationalities">
                            <span><i class="fa fa-globe"></i> <?php echo get_phrase('manage_nationalities'); ?></span>
                        </a>
                    </li>
                    <li class="<?php if ($page_name == 'status') echo 'active'; ?> ">
                        <a href="<?php echo base_url(); ?>index.php?admin/status">
                            <span><i class="fa fa-paperclip"></i> <?php echo get_phrase('manage_statuses'); ?></span>
                        </a>
                    </li>
                    <li class="<?php if ($page_name == 'curriculum') echo 'active'; ?> ">
                        <a href="<?php echo base_url(); ?>index.php?admin/curriculum">
                            <span><i class="fa fa-list-alt"></i> <?php echo get_phrase('manage_curricula'); ?></span>
                        </a>
                    </li>
                    <li class="<?php if ($page_name == 'forms') echo 'active'; ?> ">
                        <a href="<?php echo base_url(); ?>index.php?admin/forms">
                            <span><i class="fa fa-calendar-check-o"></i> <?php echo get_phrase('manage_ISO_forms'); ?></span>
                        </a>
                    </li>
                    <li class="<?php if ($page_name == 'dues') echo 'active'; ?> ">
                        <a href="<?php echo base_url(); ?>index.php?admin/dues">
                            <span><i class="fa fa-line-chart"></i> <?php echo get_phrase('manage_dues'); ?></span>
                        </a>
                    </li>
                    <li class="<?php if ($page_name == 'noticeboard') echo 'active'; ?> ">
                        <a href="<?php echo base_url(); ?>index.php?admin/noticeboard">
                            <i class="fa fa-bullhorn"></i>
                            <span><?php echo get_phrase('noticeboard'); ?></span>
                        </a>
                    </li>
                    <li class="<?php if ($page_name == 'system_settings') echo 'active'; ?> ">
                        <a href="<?php echo base_url(); ?>index.php?admin/system_settings">
                            <span><i class="fa fa-cogs"></i> <?php echo get_phrase('system_settings'); ?></span>
                        </a>
                    </li>
                </ul>
            </li>
            <!--/SETTINGS-->

        </ul>

        <?php
    }
    else if ($access_level == 1) {
        //student modules level
        ?>
        <ul id="main-menu" class="">
            <!-- add class "multiple-expanded" to allow multiple submenus to open -->
            <!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->

            <!-- DASHBOARD -->
            <li class="<?php if ($page_name == 'dashboard') echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>index.php?admin/dashboard">
                    <i class="entypo-gauge"></i>
                    <span><?php echo get_phrase('dashboard'); ?></span>
                </a>
            </li>

            <!-- MANAGE STUDENTS -->
    <!--            <li class="<?php // if ($page_name == 'students') echo 'active';     ?> ">
                <a href="<?php // echo base_url();     ?>index.php?admin/students">
                    <i class="fa fa-group"></i>
                    <span><?php // echo get_phrase('manage_students');     ?></span>
                </a>
            </li>-->

            <li class="<?php
            if ($page_name == 'student' ||
                    $page_name == 'student_add' ||
                    $page_name == 'student_edit' ||
                    $page_name == 'student_assess'
            )
                echo 'opened active';
            ?> ">
                <a href="#">
                    <i class="fa fa-group"></i>
                    <span><?php echo get_phrase('students'); ?></span>
                </a>
                <ul>
                    <li class="<?php if ($page_name == 'student' || $page_name == 'student_edit' || $page_name == 'student_assess') echo 'active'; ?> ">
                        <a href="<?php echo base_url(); ?>index.php?admin/student">
                            <span><i class="fa fa-book"></i>
                                <?php echo get_phrase('manage_students'); ?></span>
                        </a>
                    </li>
                    <li class="<?php if ($page_name == 'student_add') echo 'active'; ?> ">
                        <a href="<?php echo base_url(); ?>index.php?admin/student_add" target="_blank">
                            <span><i class="fa fa-plus-circle"></i>
                                <?php echo get_phrase('admit_new_student'); ?></span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="<?php if ($page_name == 'noticeboard') echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>index.php?admin/noticeboard">
                    <i class="fa fa-bullhorn"></i>
                    <span><?php echo get_phrase('noticeboard'); ?></span>
                </a>
            </li>
        </ul>

        <?php
    }
    else if ($access_level == 2) {
        //assessment module
        ?>

        <ul id="main-menu" class="">
            <!-- add class "multiple-expanded" to allow multiple submenus to open -->
            <!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->

            <!-- DASHBOARD -->
            <li class="<?php if ($page_name == 'dashboard') echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>index.php?admin/dashboard">
                    <i class="entypo-gauge"></i>
                    <span><?php echo get_phrase('dashboard'); ?></span>
                </a>
            </li>

            <!-- MANAGE STUDENTS -->
    <!--            <li class="<?php // if ($page_name == 'students') echo 'active';     ?> ">
                <a href="<?php // echo base_url();     ?>index.php?admin/students">
                    <i class="fa fa-group"></i>
                    <span><?php // echo get_phrase('manage_students');     ?></span>
                </a>
            </li>-->

            <li class="<?php
            if ($page_name == 'student' ||
                    $page_name == 'student_add' ||
                    $page_name == 'student_edit' ||
                    $page_name == 'student_assess'
            )
                echo 'opened active';
            ?> ">
                <a href="#">
                    <i class="fa fa-group"></i>
                    <span><?php echo get_phrase('students'); ?></span>
                </a>
                <ul>
                    <li class="<?php if ($page_name == 'student' || $page_name == 'student_edit' || $page_name == 'student_assess') echo 'active'; ?> ">
                        <a href="<?php echo base_url(); ?>index.php?admin/student">
                            <span><i class="fa fa-book"></i>
                                <?php echo get_phrase('manage_students'); ?></span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>



        <?php
    }
    else if ($access_level == 3) {
        //student modules level
        ?>

        <ul id="main-menu" class="">
            <!-- add class "multiple-expanded" to allow multiple submenus to open -->
            <!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->

            <!-- DASHBOARD -->
            <li class="<?php if ($page_name == 'dashboard') echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>index.php?admin/dashboard">
                    <i class="entypo-gauge"></i>
                    <span><?php echo get_phrase('dashboard'); ?></span>
                </a>
            </li>



            <li class="<?php
            if ($page_name == 'student' ||
                    $page_name == 'student_add' ||
                    $page_name == 'student_edit' ||
                    $page_name == 'student_assess'
            )
                echo 'opened active';
            ?> ">
                <a href="#">
                    <i class="fa fa-group"></i>
                    <span><?php echo get_phrase('students'); ?></span>
                </a>
                <ul>
                    <li class="<?php if ($page_name == 'student') echo 'active'; ?> ">
                        <a href="<?php echo base_url(); ?>index.php?admin/student">
                            <i class="fa fa-group"></i>
                            <span><?php echo get_phrase('manage_students'); ?></span>
                        </a>
                    </li>
                    <li class="<?php if ($page_name == 'student_add') echo 'active'; ?> ">
                        <a href="<?php echo base_url(); ?>index.php?admin/student_add" target="_blank">
                            <span><i class="fa fa-plus-circle"></i>
                                <?php echo get_phrase('admit_new_student'); ?></span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>

        <?php
    }
    else if ($access_level == 4) {
//view only
        ?>
        <ul id="main-menu" class="">
            <!-- add class "multiple-expanded" to allow multiple submenus to open -->
            <!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->

            <!-- DASHBOARD -->
            <li class="<?php if ($page_name == 'dashboard') echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>index.php?admin/dashboard">
                    <i class="entypo-gauge"></i>
                    <span><?php echo get_phrase('dashboard'); ?></span>
                </a>
            </li>

            <!-- MANAGE STUDENTS -->
    <!--            <li class="<?php // if ($page_name == 'students') echo 'active';     ?> ">
                <a href="<?php // echo base_url();     ?>index.php?admin/students">
                    <i class="fa fa-group"></i>
                    <span><?php // echo get_phrase('manage_students');     ?></span>
                </a>
            </li>-->

            <li class="<?php
            if ($page_name == 'student' ||
                    $page_name == 'student_add' ||
                    $page_name == 'student_edit' ||
                    $page_name == 'student_assess'
            )
                echo 'opened active';
            ?> ">
                <a href="#">
                    <i class="fa fa-group"></i>
                    <span><?php echo get_phrase('students'); ?></span>
                </a>
                <ul>
                    <li class="<?php if ($page_name == 'student' || $page_name == 'student_edit' || $page_name == 'student_assess') echo 'active'; ?> ">
                        <a href="<?php echo base_url(); ?>index.php?admin/student">
                            <span><i class="fa fa-book"></i>
                                <?php echo get_phrase('manage_students'); ?></span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    <?php } ?>


    <div class="watermark-container">
        <img src="assets/images/goldwatermark.png" class="watermark" id="#goldwatermark">
    </div>
</div>