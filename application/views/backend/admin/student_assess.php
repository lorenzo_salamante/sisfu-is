<?php
$student_id = $this->uri->segment(3, 0);
$student_no = $this->db->get_where('sisfu_students', array('student_id' => $student_id))->row()->student_no;
$tuition = $this->db->get_where('settings', array('type' => 'tuition'))->row()->description;
$scholarship_id = $this->db->get_where('sisfu_students', array('student_id' => $student_id))->row()->scholarship_id;
$discount_pct = $this->db->get_where('sisfu_scholarships_meta', array('scholarship_id' => $scholarship_id))->row()->discount_value;
?>
<?php
$this_year = date('Y');
$last_year = date("Y", strtotime("-1 year", time()));
?>
<div class="row hidden-print">
    <div class="col-md-12">
        <div class="panel panel-primary" data-collapsed="0">
            <div class="panel-heading">
                <div class="panel-title" >
                    <span class="primary-color"><i class="entypo-plus-circled"></i>
                        <?php echo get_phrase('student_information'); ?></span>
                </div>
            </div>
            <div class="panel-body">
                <?php echo form_open(base_url() . 'index.php?admin/student/assess/' . $student_id, array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data')); ?>

                <?php
                $this->db->where('type', 'active_term');
                $settings = $this->db->get('settings')->result_array();
                foreach ($settings as $row):
                    $active_term = $row['description'];
                endforeach;
                ?>

                <?php
                $query = 'SELECT a.student_id, a.student_no, a.last_name, a.first_name, a.middle_name, a.suffix, a.nickname,
                            a.department_id, a.course_id, a.scholarship_id, a.status_id, a.nationality_id, a.visa_id,
                            a.degree_id, a.other_school_id, a.high_school_id, a.discount_id, a.level_id,
                            a.degree_year_completed, a.other_school_year_completed, a.high_school_year_completed,
                            a.admission_date, a.admitted_by, a.sex, a.birth_date, a.current_address, a.permanent_address, a.email, a.contact_no,
                            a.visa_expiration, a.remarks, a.photo, a.enrolled_subjects,
                            b.department_name, b.department_code,
                            c.course_name, 
                            d.scholarship_name,
                            f.status_name,
                            g.nationality_name,
                            h.visa_name,
                            i.degree_name
                            FROM sisfu_students as a
                            LEFT JOIN sisfu_departments as b
                            ON a.department_id = b.department_id
                            LEFT JOIN sisfu_courses as c
                            ON a.course_id = c.course_id
                            LEFT JOIN sisfu_scholarships_meta as d
                            ON a.scholarship_id = d.scholarship_id
                            LEFT JOIN sisfu_status_meta as f
                            ON a.status_id = f.status_id
                            LEFT JOIN sisfu_nationalities_meta as g
                            ON a.nationality_id = g.nationality_id
                            LEFT JOIN sisfu_visa_meta as h
                            ON a.visa_id = h.visa_id
                            LEFT JOIN sisfu_degrees_meta as i
                            ON a.degree_id = i.degree_id
                            LEFT JOIN sisfu_other_schools_meta as j
                            ON a.other_school_id = j.other_school_id
                            WHERE a.student_id = ' . $student_id . '
                            ORDER BY a.student_id';
                $student_info = $this->db->query($query)->result_array();
                foreach ($student_info as $student):
                    $temp_sch = $student['scholarship_id'];
                    $temp_raw = $student['discount_id'];
                    #REGS
                    $temp_level_id = $student['level_id'];
                    $temp_enrolled_subjects = $student['enrolled_subjects'];
                    ?>
                    <!--HIDDEN LEVEL ID!!--> 
                    <div class="form-group">
                        <!--FULL NAME-->
                        <label for="full_name" class="col-sm-1 control-label"><?php echo get_phrase('full_name'); ?></label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control static" id="full_name" name="full_name" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" value="<?php echo $student['last_name'] . ', ' . $student['first_name'] . ' ' . $student['middle_name']; ?>" autofocus>
                        </div>
                        <!--DEPARTMENT-->
                        <label for="department_id" class="col-sm-1 control-label"><?php echo get_phrase('department'); ?></label>

                        <div class="col-sm-3">
                            <select name="department_id" class="form-control static" data-validate="required" id="department_id" 
                                    data-message-required="<?php echo get_phrase('value_required'); ?>">
                                <option value=""><?php echo get_phrase('select'); ?></option>
                                <?php
                                $departments = $this->db->get('sisfu_departments')->result_array();
                                foreach ($departments as $row):
                                    ?>
                                    <option value="<?php echo $row['department_id']; ?>" <?php echo ($student['department_id'] == $row['department_id'] ? 'selected' : '' ); ?> >
                                        <?php echo $row['department_name']; ?>
                                    </option>
                                    <?php
                                endforeach;
                                ?>
                            </select>
                        </div> 
                        <!--COURSE-->
                        <label for="course_id" class="col-sm-1 control-label"><?php echo get_phrase('course'); ?></label>
                        <div class="col-sm-3">
                            <select name="course_id" class="form-control static" data-validate="required" id="course_id" 
                                    data-message-required="<?php echo get_phrase('value_required'); ?>">
                                <option value=""><?php echo get_phrase('select'); ?></option>
                                <?php
                                $courses = $this->db->get('sisfu_courses')->result_array();
                                foreach ($courses as $row):
                                    ?>
                                    <option value="<?php echo $row['course_id']; ?>" <?php echo ($student['course_id'] == $row['course_id'] ? 'selected' : '' ); ?>>
                                        <?php echo $row['course_name']; ?>
                                    </option>
                                    <?php
                                endforeach;
                                ?>
                            </select>
                        </div> 
                    </div>

                    <div class="form-group">
                        <!--STATUS/ENTRY LEVEL-->
                        <label for="status_id" class="col-sm-1 control-label"><?php echo get_phrase('status'); ?></label>

                        <div class="col-sm-2">
                            <select name="status_id" class="form-control static" data-validate="required" id="status_id" 
                                    data-message-required="<?php echo get_phrase('value_required'); ?>">
                                <option value=""><?php echo get_phrase('select'); ?></option>
                                <?php
                                $status = $this->db->get('sisfu_status_meta')->result_array();
                                foreach ($status as $row):
                                    ?>
                                    <option value="<?php echo $row['status_id']; ?>" <?php echo ($student['status_id'] == $row['status_id'] ? 'selected' : '' ); ?>>
                                        <?php echo $row['status_name']; ?>
                                    </option>
                                    <?php
                                endforeach;
                                ?>
                            </select>
                        </div> 

                        <label for="level_id" class="col-sm-1 control-label"><?php echo get_phrase('year_level'); ?></label>
                        <!--level-->
                        <div class="col-sm-2">
                            <select name="level_id" class="form-control static" id="level_id">
                                <option value=""><?php echo get_phrase('new_enrollee'); ?></option>
                                <?php
                                $max_id = $this->db->query('SELECT tuition_id FROM sisfu_tuition WHERE student_no = "' . $student_no . '" ORDER BY tuition_id DESC LIMIT 1')->row()->tuition_id;
                                $this->db->where('tuition_id', $max_id);
                                $year_level = $this->db->get('sisfu_tuition')->row()->year_level;
                                ?>
                                <?php
                                $result = $this->db->get('sisfu_levels_meta')->result_array();
                                foreach ($result as $row):
                                    ?>
                                    <option value="<?php echo $row['level_id']; ?>" <?php echo ($year_level == $row['level_id'] ? 'selected' : '' ); ?>>
                                        <?php
                                        echo $row['description'];
                                        ?>
                                    </option>
                                    <?php
                                endforeach;
                                ?>
                            </select>
                        </div>

                        <label for="scholarship_id" class="col-sm-1 control-label"><?php echo get_phrase('scholarship_discount'); ?></label>
                        <!--SCHOLARSHIPS-->
                        <div class="col-sm-2">
                            <select name="scholarship_id" class="form-control static" id="scholarship_id">
                                <option value=""><?php echo get_phrase('no_discount'); ?></option>
                                <?php
                                $scholarships = $this->db->get('sisfu_scholarships_meta')->result_array();
                                foreach ($scholarships as $row):
                                    ?>
                                    <option value="<?php echo $row['scholarship_id']; ?>" <?php echo ($student['scholarship_id'] == $row['scholarship_id'] ? 'selected' : '' ); ?>>
                                        <?php
                                        echo ceil($row['discount_value']) . "%";
                                        ?>
                                    </option>
                                    <?php
                                endforeach;
                                ?>
                            </select>
                        </div> 
                        <label for="discount_id" class="col-sm-1 control-label"><?php echo get_phrase('raw_discount'); ?></label>
                        <!--RAW DISCOUNT-->
                        <div class="col-sm-2">
                            <select name="discount_id" class="form-control static"  id="discount_id">
                                <option value=""><?php echo get_phrase('no_discount'); ?></option>
                                <?php
                                $discounts = $this->db->get('sisfu_discounts_meta')->result_array();
                                foreach ($discounts as $row):
                                    ?>
                                    <option value="<?php echo $row['discount_id']; ?>" <?php echo ($student['discount_id'] == $row['discount_id'] ? 'selected' : '' ); ?>>
                                        <?php
                                        echo ceil($row['discount_value']) . "%";
                                        ?>
                                    </option>
                                    <?php
                                endforeach;
                                ?>
                            </select>
                        </div> 
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>

<?php
if ($temp_sch == 0) {
    $scholarship_discount = 0;
}
if ($temp_raw == 0) {
    $raw_discount = 0;
}
$this->db->where('scholarship_id', $temp_sch);
$s = $this->db->get('sisfu_scholarships_meta')->result_array();
foreach ($s as $row):
    $scholarship_discount = $row['discount_value'];
endforeach;
$this->db->where('discount_id', $temp_raw);
$d = $this->db->get('sisfu_discounts_meta')->result_array();
foreach ($d as $row):
    $raw_discount = $row['discount_value'];
endforeach;
?>

<div class="row hidden-print" style="margin-bottom: -18px;">
    <div class="col-md-12">
        <div class="panel panel-primary" data-collapsed="0">
            <div class="panel-body" style="padding: 7px;">
                <div class="form-group">
                    <!--SUBJECT NAME-->
                    <label for="curriculum_name" class="col-sm-1 control-label small-pad-top"><?php echo get_phrase('curriculum_name'); ?></label>
                    <div class="col-sm-4">
                        <select name="curriculum_name" class="form-control" id="curriculum_name">
                            <?php
                            $curriculum = $this->db->get('sisfu_curriculum')->result_array();
                            foreach ($curriculum as $row):
                                ?>
                                <option value="<?php echo $row['curriculum_id']; ?>" id="curriculum_name-<?php echo $row['curriculum_id']; ?>">
                                    <?php echo $row['curriculum_name']; ?> - Term <?php echo $row['curriculum_term']; ?>
                                </option>
                                <?php
                            endforeach;
                            ?>
                        </select>
                    </div> 

                    <!--SUBJECT CODE-->
                    <label for="curriculum_code" class="col-sm-1 control-label small-pad-top"><?php echo get_phrase('curriculum_code'); ?></label>
                    <div class="col-sm-3">
                        <select name="curriculum_code" class="form-control" data-validate="required" id="curriculum_code">
                            <?php
                            $curriculum = $this->db->get('sisfu_curriculum')->result_array();
                            foreach ($curriculum as $row):
                                ?>
                                <option value="<?php echo $row['curriculum_id']; ?>" id="curriculum_code-<?php echo $row['curriculum_id']; ?>">
                                    <?php echo $row['curriculum_code']; ?> - Term <?php echo $row['curriculum_term']; ?>
                                </option>
                                <?php
                            endforeach;
                            ?>
                        </select>
                    </div> 

                    <div class="col-sm-2">
                        <a class="btn btn-primary pull-right col-sm-offset-1" id="reload"> <i class="fa fa-retweet"></i> <?php echo get_phrase('reload'); ?></a>
                        <a class="btn btn-primary pull-right " id="get_subjects" > <i class="fa fa-plus-square"></i> <?php echo get_phrase('get_subjects'); ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<table class="table table-bordered datatable hidden-print" id="">
    <thead>
        <tr>
            <th><div><?php echo get_phrase('subject_code') ?></div></th>
<th><div><?php echo get_phrase('partner_code'); ?></div></th>
<th><div><?php echo get_phrase('subjects_enrolled'); ?></div></th>
<th><div><?php echo get_phrase('credits'); ?></div></th>
<th><div><?php echo get_phrase('unit_level'); ?></div></th>
</tr>
</thead>
<tbody>
    <?php for ($x = 1; $x < 10; $x++): ?>
        <tr>
            <?php
            $subjs_arr = explode(",", $student['enrolled_subjects']);
            if ($subjs_arr[0] == '') {
                $subjs_len = 0;
            } else {
                $subjs_len = count($subjs_arr);
            }
            $this->db->where('subject_id', $subjs_arr[$x - 1]);
            $q = $this->db->get('sisfu_subjects');
            $result = $q->result_array();

            if ($q->num_rows() == 0) {
                ?>

        <input type="hidden" name="hidden_subj_id-<?php echo $x ?>" id="hidden_subj_id-<?php echo $x ?>" value="">
        <td>
            <input type="text" id='col1-<?php echo $x ?>' name='col1-<?php echo $x ?>' readonly class="zero-border" value=""/></td>
        <td>
            <input type="text" id='col2-<?php echo $x ?>' name='col2-<?php echo $x ?>' readonly class="zero-border" value=""/></td>
        <td>
            <input type="text" id='col3-<?php echo $x ?>' name='col3-<?php echo $x ?>' readonly class="zero-border" value=""/></td>
        <td>
            <input type="text" id='col4-<?php echo $x ?>' name='col4-<?php echo $x ?>' readonly class="zero-border" value=""/></td>
        <td>
            <input type="text" id='col5-<?php echo $x ?>' name='col5-<?php echo $x ?>' readonly class="zero-border" value=""/></td>
        <?php
    } else {

        foreach ($result as $row):
            ?>

            <input type="hidden" name="hidden_subj_id-<?php echo $x ?>" id="hidden_subj_id-<?php echo $x ?>" value="<?php echo $row['subject_id']; ?>">
            <td>
                <input type="text" id='col1-<?php echo $x ?>' name='col1-<?php echo $x ?>' readonly class="zero-border" value="<?php echo $row['subject_code']; ?>"/></td>
            <td>
                <input type="text" id='col2-<?php echo $x ?>' name='col2-<?php echo $x ?>' readonly class="zero-border" value="<?php echo $row['partner_code']; ?>"/></td>
            <td>
                <input type="text" id='col3-<?php echo $x ?>' name='col3-<?php echo $x ?>' readonly class="zero-border" value="<?php echo $row['subject_name']; ?>"/></td>
            <td>
                <input type="text" id='col4-<?php echo $x ?>' name='col4-<?php echo $x ?>' readonly class="zero-border" value="<?php echo $row['units']; ?>"/></td>
            <td>
                <input type="text" id='col5-<?php echo $x ?>' name='col5-<?php echo $x ?>' readonly class="zero-border" value="<?php echo $row['unit_level']; ?>"/></td>
            <?php
        endforeach;
    }
    ?>


    </tr>
<?php endfor; ?>

</tbody>
</table>

<div class="row hidden-print" style="margin-top: -17px;">
    <div class="col-md-12">
        <div class="panel panel-primary" data-collapsed="0">
            <div class="panel-body" style="padding: 7px;">
                <div class="form-group">
                    <!--SUBJECT NAME-->
                    <label for="subject_name" class="col-sm-1 control-label small-pad-top"><?php echo get_phrase('subject_name'); ?></label>
                    <div class="col-sm-3">
                        <select name="subject_name" class="form-control" data-validate="required" id="subject_name">
                            <?php
                            $this->db->order_by("subject_name", "asc");
                            $subjects = $this->db->get('sisfu_subjects')->result_array();
                            foreach ($subjects as $row):
                                ?>
                                <option value="<?php echo $row['subject_id']; ?>" id="subject_name-<?php echo $row['subject_id']; ?>">
                                    <?php echo $row['subject_name']; ?> - <?php echo $row['subject_code']; ?>
                                </option>
                                <?php
                            endforeach;
                            ?>
                        </select>
                    </div> 

                    <!--SUBJECT CODE-->
                    <label for="subject_code" class="col-sm-1 control-label small-pad-top"><?php echo get_phrase('subject_code'); ?></label>
                    <div class="col-sm-2">
                        <select name="subject_code" class="form-control" data-validate="required" id="subject_code">
                            <?php
                            $subjects = $this->db->get('sisfu_subjects')->result_array();
                            foreach ($subjects as $row):
                                ?>
                                <option value="<?php echo $row['subject_id']; ?>" id="subject_code-<?php echo $row['subject_id']; ?>">
                                    <?php echo $row['subject_code']; ?>
                                </option>
                                <?php
                            endforeach;
                            ?>
                        </select>
                    </div> 

                    <!--UNITS-->
                    <label for="units" class="col-sm-1 control-label small-pad-top"><?php echo get_phrase('units'); ?></label>
                    <div class="col-sm-2">
                        <select name="units" class="form-control" data-validate="required" id="units" readonly>
                            <?php
                            $subjects = $this->db->get('sisfu_subjects')->result_array();
                            foreach ($subjects as $row):
                                ?>
                                <option value="<?php echo $row['subject_id']; ?>" id="units-<?php echo $row['subject_id']; ?>">
                                    <?php echo $row['units']; ?>
                                </option>
                                <?php
                            endforeach;
                            ?>
                        </select>
                    </div> 

                    <!--PARTNER CODE-->
                    <div class='hidden'>
                        <select name="partner_code" class="form-control" data-validate="required" id="units" readonly>
                            <?php
                            $subjects = $this->db->get('sisfu_subjects')->result_array();
                            foreach ($subjects as $row):
                                ?>
                                <option value="<?php echo $row['subject_id']; ?>" id="partner_code-<?php echo $row['subject_id']; ?>">
                                    <?php echo $row['partner_code']; ?>
                                </option>
                                <?php
                            endforeach;
                            ?>
                        </select>
                    </div>
                    <!--UNIT LEVEL-->
                    <div class='hidden'>
                        <select name="unit_level" class="form-control" data-validate="required" id="units" readonly>
                            <?php
                            $subjects = $this->db->get('sisfu_subjects')->result_array();
                            foreach ($subjects as $row):
                                ?>
                                <option value="<?php echo $row['subject_id']; ?>" id="unit_level-<?php echo $row['subject_id']; ?>">
                                    <?php echo $row['unit_level']; ?>
                                </option>
                                <?php
                            endforeach;
                            ?>
                        </select>
                    </div>

                    <input type="hidden" id="subject_id" name="subject_id" value="">
                    <input type="hidden" id="unit_level" name="unit_level" value="">

                    <div class="col-sm-1">
                        <a class="btn btn-primary pull-right" id="add_subject"> <i class="fa fa-plus-square"></i> <?php echo get_phrase('add'); ?></a>
                    </div>
                    <div class="col-sm-1">
                        <a class="btn btn-primary pull-right" id="btn_clear"> <i class="fa fa-refresh"></i> <?php echo get_phrase('clear'); ?></a>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>

<input type='hidden' id='' class="form-control subject_count" value='<?php echo $subjs_len ?>' name='subject_count' readonly/>

<div class="row visible-print-block">
    <div class="col-md-6" style='margin-bottom:-18px'>
        <div class="panel panel-primary" data-collapsed="0">
            <div class="panel-body" style="padding: 7px;">
                <div class="form-group">
                    <div class="col-sm-2">
                        <button type="submit" class="btn btn-primary pull-right hidden-print" id="save"><i class="fa fa-floppy-o"></i> <?php echo get_phrase('save'); ?></button> 
                    </div> 
                    <?php echo form_close() ?>

                    <div class="col-sm-3">
                        <button class="btn btn-primary pull-right hidden-print" id="print" > <i class="fa fa-print"></i> <?php echo get_phrase('print'); ?></button>
                    </div>
                    <div class="col-sm-7">
                        <a class="btn btn-primary pull-right hidden-print" href="#" id="breakdown" >
                            <i class="fa fa-eye"></i> Hide/View Breakdown of Fees 
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div  class="col-sm-12" style="padding:0px">
            <div class="panel panel-primary" data-collapsed="0">
                <div class="panel-body" style="    padding: 8px;">
                    <div class="form-group">
                        <?php echo form_open(base_url() . 'index.php?admin/student/do_post/' . $student_id, array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data')); ?>
                        <div class="col-sm-2">
                                                <!--<input type="hidden" name="post_level_id" id="post_level_id" value="" data-validate="required" />-->
                            <!--<input type="hidden" name="post_term_code" id="term_code" value="<?php // echo $active_term  ?>" data-validate="required" />-->
                            <input type="hidden" name="post_tuition" id="post_tuition" value="<?php echo $tuition ?>" data-validate="required" />
                            <input type="hidden" name="post_discount_pct" id="post_discount_pct" value="<?php echo $discount_pct ?>" data-validate="required" />

                            <input type="hidden" name="post_ir_fee" id="post_ir_fee" value="" data-validate="required" />
                            <input type="hidden" name="post_misc_fee" id="post_misc_fee" value="" data-validate="required" />
                            <input type="hidden" name="post_application_fee" id="post_application_fee" value="" data-validate="required" />
                            <input type="hidden" name="post_admin_fee" id="post_admin_fee" value="" data-validate="required" />
                            <input type="hidden" name="post_math_fee" id="post_math_fee" value="" data-validate="required" />
                            <input type="hidden" name="post_tutorial_fee" id="post_tutorial_fee" value="" data-validate="required" />
                            <input type="hidden" name="post_rswl_fee" id="post_rswl_fee" value="" data-validate="required" />
                            <input type="hidden" name="post_esl_fee" id="post_esl_fee" value="" data-validate="required" />
                            <input type="hidden" name="post_other_fee" id="post_other_fee" value="" data-validate="required" />
                            <input type="hidden" name="post_other_text" id="post_other_text" value="" data-validate="required" />
                            <input type="hidden" name="post_grad_fee" id="post_grad_fee" value="" data-validate="required" />
                            <input type="hidden" name="post_b_add_on" id="post_b_add_on" value="" data-validate="required" />
                            <input type="hidden" name="post_c_add_on" id="post_c_add_on" value="" data-validate="required" />
                            <input type="hidden" name="post_grand_total" id="post_grand_total" value="" data-validate="required"  />
                            <input type="hidden" name="post_currency" id="post_currency" value="" data-validate="required"  />

                            <input type="hidden" name="post_enrolled_subjects" id="post_enrolled_subjects" value="" data-validate="required"  data-message-required="<?php echo get_phrase('cannot_post_yet'); ?>" />

                            <button type="submit" class="btn btn-primary pull-right hidden-print" id="post"> <i class="fa fa-paper-plane"></i> <?php echo get_phrase('post'); ?></button>
                        </div>
                        <label for="" class="col-sm-1 control-label"><?php echo get_phrase('level'); ?></label>
                        <div class="col-sm-3">
                            <?php
                            $max_id = $this->db->query('SELECT tuition_id FROM sisfu_tuition WHERE student_no = "' . $student_no . '" ORDER BY tuition_id DESC LIMIT 1')->row()->tuition_id;
//                        echo $max_id;
                            if ($max_id == NULL) {
                                echo '<input type="number" min="0" max="5" class="form-control" name="post_year_level" id="post_year_level" value="0" data-validate="required" data-toggle="tooltip" data-placement="top" title="Incoming year level of the student"/>';
                            } else {
                                $this->db->where('tuition_id', $max_id);
                                $year_level = $this->db->get('sisfu_tuition')->row()->year_level;
                                echo '<input type="number" min="0" max="5" class="form-control" name="post_year_level" id="post_year_level" value="' . $year_level . '" data-validate="required" data-toggle="tooltip" data-placement="top" title="Incoming year level of the student"/>';
                            }
                            ?>
                        </div>
                        <label for="" class="col-sm-1 control-label"><?php echo get_phrase('term'); ?></label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" value="<?php echo $active_term ?>" name="post_term_code" data-toggle="tooltip" data-placement="top" title="Values accepted: S, 1, 2, 3 (etc.)" data-validate="required" />
                        </div>
                        <label for="" class="col-sm-2 col-sm-offset-1 control-label" style="margin-top:10px">Acad Year</label>
                        <div class="col-sm-3" style="margin-top:10px">
                            <input type="text" class="form-control" name="post_acad_year" id="acad_year" value="<?php echo $last_year . '-' . $this_year ?>" data-validate="required" />
                        </div>
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>


    </div>
</div>
<div class='row hidden-print'>
    <div class='col-md-6'>

        <table class="table table-bordered datatable" id="payments_tbl">
            <thead>
                <tr>
                    <th><div>&nbsp;</div></th>
            <th><div>Plan A</div></th>
            <th><div>Plan B</div></th>
            <th><div>Plan C</div></th>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <td class='grey-bg'>1st Payment</td>
                    <td id='plan_a-1' >&nbsp;</td>
                    <td id='plan_b-1'>&nbsp;</td>
                    <td id='plan_c-1'>&nbsp;</td>
                </tr>
                <tr>
                    <td class='grey-bg'>2nd Payment</td>
                    <td >&nbsp;</td>
                    <td id='plan_b-2'>&nbsp;</td>
                    <td id='plan_c-2'>&nbsp;</td>
                </tr>
                <tr>
                    <td class='grey-bg'>3rd Payment</td>
                    <td >&nbsp;</td>
                    <td >&nbsp;</td>
                    <td id='plan_c-3'>&nbsp;</td>
                </tr>
                <tr>
                    <td class='grey-bg'>Add-On</td>
                    <td >&nbsp;</td>
                    <td id='add_on-b'>&nbsp;</td>
                    <td id='add_on-c'>&nbsp;</td>
                </tr>
                <tr>
                    <td class='grey-bg'>Total Payment</td>
                    <td id='total-a'>&nbsp;</td>
                    <td id='total-b'>&nbsp;</td>
                    <td id='total-c'>&nbsp;</td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class='col-md-6 breakdown'>
        <div class="row">
            <div class="col-sm-12">
                <?php
                $this->db->where('student_id', $student_id);
                $result = $this->db->get('sisfu_students')->result_array();
                foreach ($result as $row):

                endforeach;
                ?>
                <div class="panel-heading" >
                    <div class="panel-title" style="width:100%">
                        <div class="row">
                            <div class="col-sm-10">
                                <span class="primary-color"><i class="fa fa-info-circle"></i>
                                    Breakdown of fees</span>

                            </div>
                            <div class="col-sm-2">
                                <a class="btn btn-primary" style='margin-top:-5px;' id='reset_fees'>Set fees to <span style="font-family: 'Consolas';">0</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="field-1" class="col-sm-4 control-label">No. of subjects enrolled</label>

                    <div class="col-sm-3 col-sm-offset-5">
                        <input type="text" class="form-control subject_count" readonly value="<?php echo $subjs_len ?>">
                    </div>
                </div>
            </div>
        </div>
        <hr>

        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="field-1" class="col-sm-4 control-label">Tuition Fee</label>

                    <div class="col-sm-4">
                        <select class="form-control" id="currency">
                            <option value="usd">USD</option>
                            <option value="gbp">GBP</option>
                            <option value="php">PHP</option>
                        </select>
                    </div>
                    <div class="col-sm-4 ">
                        <input type="number" min="0" class="form-control right-text on_tuition_change" id="modal_tuition" 
                               value="">
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Net tuition fee</label>

                    <div class="col-sm-4 col-sm-offset-5">
                        <input type="number" min="0" class="form-control right-text" disabled name="" id="modal_net"
                               value="0">
                    </div>
                </div>
            </div>
        </div>
        <hr>

        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Other fees:</label>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="field-1" class="col-sm-3  control-label">Registration Fee:</label>

                    <div class="col-sm-4 col-sm-offset-5">
                        <!--<div class="input-group">-->
                        <!--<div class="input-group-addon" id="reg_addon"></div>-->
                        <input type="number" min="0" class="form-control right-text on_blur" id="modal_reg"
                               value="0">
                        <!--</div>-->
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="field-1" class="col-sm-3   control-label">Miscellaneous Fee:</label>

                    <div class="col-sm-4 col-sm-offset-5">
                        <!--                        <div class="input-group">
                                                    <div class="input-group-addon" id="misc_addon"></div>-->
                        <input type="number" min="0" class="form-control right-text on_blur" id="modal_misc" 
                               value="0">
                        <!--</div>-->
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="field-1" class="col-sm-3  control-label">Application Fee:</label>

                    <div class="col-sm-4 col-sm-offset-5">
                        <input type="number" min="0" class="form-control right-text on_blur" id="modal_app"
                               value="0">
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="field-1" class="col-sm-3  control-label">Admin Fee:</label>

                    <div class="col-sm-4 col-sm-offset-5">
                        <input type="number" min="0" class="form-control right-text on_blur" id="modal_admin"
                               value="0">
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="field-1" class="col-sm-3  control-label">Math Enrichment:</label>

                    <div class="col-sm-4 col-sm-offset-5">
                        <input type="number" min="0" class="form-control right-text on_blur" id="modal_math"
                               value="0">
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="field-1" class="col-sm-3  control-label">RSWL:</label>

                    <div class="col-sm-4 col-sm-offset-5">
                        <input type="number" min="0" class="form-control right-text on_blur" id="modal_rswl" 
                               value="0">
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="field-1" class="col-sm-3  control-label">Tutorial Fee:</label>

                    <div class="col-sm-4 col-sm-offset-5">
                        <input type="number" min="0" class="form-control right-text on_blur" id="modal_tut"
                               value="0">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="field-1" class="col-sm-3  control-label">ESL:</label>

                    <div class="col-sm-4 col-sm-offset-5">
                        <input type="number" min="0" class="form-control right-text on_blur" id="modal_esl"
                               value="0">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="field-1" class="col-sm-3  control-label">Other:</label>

                    <div class="col-sm-4 col-sm-offset-5">
                        <input type="number" min="0" class="form-control right-text on_blur" id="modal_other"
                               value="0">
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="field-1" class="col-sm-3  control-label">Please specify</label>

                    <div class="col-sm-5 col-sm-offset-4">
                        <input type="text" class="form-control on_blur" id="modal_other_text"
                               value="" placeholder="one time charges, other fees, etc.">
                    </div>
                </div>
            </div>
        </div>
        <hr>

        <div class="row">
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="field-1" class="col-sm-5 control-label">
                                Grad
                                /Intl. Registration Fee:</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <div class="col-sm-3">
                        <select class="form-control ">
                            <option>Grad Fee</option>
                            <!--<option>Intl. Reg Fee</option>-->
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <select class="form-control">
                            <option>PHP</option>
                            <!--<option>GBP</option>-->
                        </select>
                    </div>
                    <div class="col-sm-4 col-sm-offset-2">
                        <input type="number" min="0" class="form-control right-text on_blur" id="modal_grad"
                               value="0">
                    </div>
                </div>
            </div>
        </div>


        <hr>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Add On:</label>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Plan B:</label>

                    <div class="col-sm-3">
                        <input type="text" class="form-control right-text" id="modal_planB" readonly
                               value="0">
                    </div>
                    <label for="field-1" class="col-sm-3 control-label">Plan C:</label>

                    <div class="col-sm-3">
                        <input type="text" class="form-control right-text" id="modal_planC" readonly
                               value="0">
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label"><strong>Grand Total Fees</strong></label>

                    <div class="col-sm-4 col-sm-offset-5">
                        <!--                        <div class="input-group">
                                                    <div class="input-group-addon" >$</div>-->
                        <input type="text" class="form-control right-text" id="modal_total" readonly
                               value="0">
                        <!--</div>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    function get_class_sections(class_id) {

        $.ajax({
            url: '<?php echo base_url(); ?>index.php?admin/get_class_section/' + class_id,
            success: function (response)
            {
                jQuery('#section_selector_holder').html(response);
            }
        });
    }

</script>

<script>
    $(document).ready(function () {
//ASSESSMENTS
        var level_id = $('#level_id');
        var post_level_id = <?php echo $temp_level_id; ?>;
        var post_enrolled_subjects = '<?php echo $temp_enrolled_subjects; ?>';
        var student_id = <?php echo $student_id; ?>;
        var department_id = $('#department_id');
        var subject_name = $('#subject_name');
        var subject_code = $('#subject_code');
        var curriculum_name = $('#curriculum_name');
        var curriculum_code = $('#curriculum_code');
        var partner_code = $('#partner_code');
        var subject_id = $('#subject_id');
        var add_subject = $('#add_subject');
        var get_subjects = $('#get_subjects');
        var units = $('#units');
        var unit_level = $('#unit_level');
        var tr_count = Number($('.subject_count').attr('value')) + 1;
        var sc_id = $('#subject_code-1');
        var sn_id = $('#subject_name-1');
        var u_id = $('#units-1');
        var ul_id = $('#unit_level-1');
        var pc_id = $('#partner_code-1');
        var s_id = '1';
        var subjects_arr = [];
        var btn_clear = $('#btn_clear');
        var subject_count = $('.subject_count');
        var MAX_SIZE = 9;
        var curriculum_arr;
        //MODAL
        // NO MODAL ANYMORE JUST THE SIDE DIV
        var modal_tuition = $('#modal_tuition');
        var modal_net = $('#modal_net');
        var modal_reg = $('#modal_reg');
        var modal_misc = $('#modal_misc');
        var modal_appl = $('#modal_app');
        var modal_admin = $('#modal_admin');
        var modal_math = $('#modal_math');
        var modal_rswl = $('#modal_rswl');
        var modal_tut = $('#modal_tut');
        var modal_esl = $('#modal_esl');
        var modal_other = $('#modal_other');
        var modal_other_text = $('#modal_other_text');
        var modal_grad = $('#modal_grad');
        var modal_planB = $('#modal_planB');
        var modal_planC = $('#modal_planC');
        var modal_total = $('#modal_total');

        var reg_addon = $('#reg_addon');
        var misc_addon = $('#misc_addon');
        var on_blur = $('.on_blur');
        var on_tuition_change = $('.on_tuition_change');

//PAYMENTS
        var scholarship_discount = <?php echo $scholarship_discount; ?>;
        var raw_discount = <?php echo $raw_discount; ?>;
        var net_discount = scholarship_discount + raw_discount;
        var discount_pct = net_discount / 100;
//        var add_on_b = 2.5252525252525 / 100;
//        var add_on_c = 4.2087542087542 / 100;
//        var b_down_pct = 62.26654661184 / 100;
//        var b_give_pct = 37.73345338816 / 100;
//        var c_down_pct = 49.429255480268 / 100;
//        var c_give_pct = 27.230177468143 / 100;
        var ir_fee = 0; //init
        var application_fee = 0.00; //init
        var admin_fee = 0.00; //init
        var dissertation_fee = 0.00; //init

        var b_rate = 1.03;
        var c_rate = 1.05;

        var tuition = Number(<?php echo $tuition ?>);
        var misc = 135.00;
        //RENAME
        var a_total = 0;
        var b_total = 0;
        var c_total = 0;
        var c_down = 0;
        var c_give = 0;
        var c_add_on = 0;
        var b_give = 0;
        var b_down = 0;
        var b_add_on = 0;
        //IF SoHM
        if (department_id.val() == 9) {
            ir_fee = 400;
        }
        else {
            if (level_id.val() < 2) {
                ir_fee = 400;

            }
            else if (level_id.val() > 1) {
                ir_fee = 100;
            }
        }


//        if (level_id.attr('value') == 0) {
//        if (level_id.val() == 0) {
//            application_fee = 30.00;
//            admin_fee = 300.00;
//            dissertation_fee = 100.00;
//        }
//        else {
        application_fee = 0.00;
        admin_fee = 0.00;
        dissertation_fee = 0.00;
//        }

        var one_time_charge = application_fee + admin_fee + dissertation_fee;
        var net_tuition = 0;
        var additional = 0;
        var ajax_net = 0;
        var ajax_reg = 0;
        var ajax_misc = 0;
        var ajax_appl = 0;
        var ajax_admin = 0;
        var ajax_math = 0;
        var ajax_rswl = 0;
        var ajax_tut = 0;
        var ajax_esl = 0;
        var ajax_other = 0;
        var ajax_other_text = "";
        var ajax_grad = 0;
//END DECLARATIONS

//INITS
        $('.static').prop('disabled', true);
        $('#post_currency').attr('value', $('#currency').val());
        set_curriculum_id();
        set_payments();
        check_post();
//        modal_total.val();
//        modal_total.attr('value', 0);

        function set_curriculum_id() {
            $.ajax({
                url: 'index.php?Admin/ajax', // define here controller then function name
                type: 'post',
                data: {curriculum_id: curriculum_code.val()}, // pass here your date variable into controller
                success: function (data) {
                    curriculum_arr = (JSON.parse(data));
                    console.log(JSON.parse(data));
                }
            });
        }
        function prepare_print() {
            $.ajax({
                url: 'index.php?Admin/ajax_reg_card', // define here controller then function name
                type: 'post',
                data: {planA_total: a_total,
                    planB_total: b_total,
                    planC_total: c_total,
                    student_id: student_id,
                    b_down: b_down,
                    b_give: b_give,
                    c_down: c_down,
                    c_give: c_give,
                    tuition: tuition,
                    ajax_grad: ajax_grad,
                    misc: misc + Number(ajax_misc),
                    ir_fee: ir_fee + Number(ajax_reg),
                    other: ajax_other,
                    modal_other_text: modal_other_text.val(),
                    b_add_on: b_add_on,
                    c_add_on: c_add_on,
                    net_tuition: net_tuition,
                    tr_count: tr_count

                }, // pass here your date variable into controller
                success: function (data) {
                    //                    window.open('<?php // echo base_url() .'index.php?admin/reg_card'                                       ?>', '_blank');
                    location.href = '<?php echo base_url() . 'index.php?admin/reg_card' ?>';
                }
            });
        }
        curriculum_name.on('change', function () {
            $('#curriculum_code-'.concat(curriculum_name.val())).prop('selected', true);
            set_curriculum_id();
        });
        curriculum_code.on('change', function () {
            $('#curriculum_name-'.concat(curriculum_code.val())).prop('selected', true);
            set_curriculum_id();
        });
        get_subjects.on('click', function () {
            subjects_arr.length = 0;
            //RESET 
            var col1 = '';
            var col2 = '';
            var col3 = '';
            var col4 = '';
            var col5 = '';
            var hidden_subj_id = '';
//
            for (var i = 1; i <= curriculum_arr.length; i++) {
//
                col1 = $('#col1-'.concat(i));
                col2 = $('#col2-'.concat(i));
                col3 = $('#col3-'.concat(i));
                col4 = $('#col4-'.concat(i));
                col5 = $('#col5-'.concat(i));
                hidden_subj_id = $('#hidden_subj_id-'.concat(i));
////                
                col1.val(curriculum_arr[i - 1]['0'].subject_code);
                col2.val(curriculum_arr[i - 1]['0'].partner_code);
                col3.val(curriculum_arr[i - 1]['0'].subject_name);
                col4.val(curriculum_arr[i - 1]['0'].units);
                col5.val(curriculum_arr[i - 1]['0'].unit_level);
                subjects_arr.push(curriculum_arr[i - 1]['0'].subject_id);
                hidden_subj_id.attr('value', curriculum_arr[i - 1]['0'].subject_id);
                col1.attr('value', curriculum_arr[i - 1]['0'].subject_code);
                col2.attr('value', curriculum_arr[i - 1]['0'].partner_code);
                col3.attr('value', curriculum_arr[i - 1]['0'].subject_name);
                col4.attr('value', curriculum_arr[i - 1]['0'].units);
                col5.attr('value', curriculum_arr[i - 1]['0'].unit_level);
//
            }

            for (var i = curriculum_arr.length + 1; i <= MAX_SIZE; i++) {
                col1 = $('#col1-'.concat(i));
                col2 = $('#col2-'.concat(i));
                col3 = $('#col3-'.concat(i));
                col4 = $('#col4-'.concat(i));
                col5 = $('#col5-'.concat(i));
                hidden_subj_id = $('#hidden_subj_id-'.concat(i));
                col1.val('');
                col2.val('');
                col3.val('');
                col4.val('');
                col5.val('');
                col1.attr('value', '');
                col2.attr('value', '');
                col3.attr('value', '');
                col4.attr('value', '');
                col5.attr('value', '');
                hidden_subj_id.attr('value', '');
            }

            subject_count.val(curriculum_arr.length);
            subject_count.attr('value', curriculum_arr.length);
            tr_count = curriculum_arr.length + 1;
        });
        subject_name.on('change', function () {
            sn_id = $('#subject_name-'.concat(subject_name.val()));
            pc_id = $('#partner_code-'.concat(subject_name.val()));
            u_id = $('#units-'.concat(subject_name.val()));
            ul_id = $('#unit_level-'.concat(subject_name.val()));
            s_id = subject_name.val();
            sc_id = $('#subject_code-'.concat(subject_name.val()));
            sc_id.prop('selected', true);
            u_id.prop('selected', true);
            ul_id.prop('selected', true);
            pc_id.prop('selected', true);
        });
        subject_code.on('change', function () {
            sc_id = $('#subject_code-'.concat(subject_code.val()));
            pc_id = $('#partner_code-'.concat(subject_code.val()));
            u_id = $('#units-'.concat(subject_code.val()));
            ul_id = $('#unit_level-'.concat(subject_code.val()));
            s_id = subject_code.val();
            sn_id = $('#subject_name-'.concat(subject_code.val()));
            sn_id.prop('selected', true);
            u_id.prop('selected', true);
            ul_id.prop('selected', true);
            pc_id.prop('selected', true);
        });
        add_subject.on('click', function () {
            if ($.inArray(s_id, subjects_arr, 0) != -1) {
            }
            else {
                var col1 = $('#col1-'.concat(tr_count));
                var col2 = $('#col2-'.concat(tr_count));
                var col3 = $('#col3-'.concat(tr_count));
                var col4 = $('#col4-'.concat(tr_count));
                var col5 = $('#col5-'.concat(tr_count));
                var hidden_subj_id = $('#hidden_subj_id-'.concat(tr_count));
                subject_count.val(tr_count);
                subject_count.attr('value', tr_count);
                subjects_arr.push(s_id);
                col1.val(sc_id.text().trim());
                col2.val(pc_id.text().trim());
                col3.val(sn_id.text().trim());
                col4.val(u_id.text().trim());
                col5.val(ul_id.text().trim());
                hidden_subj_id.attr('value', s_id);
                col1.attr('value', sc_id.text().trim());
                col2.attr('value', pc_id.text().trim());
                col3.attr('value', sn_id.text().trim());
                col4.attr('value', u_id.text().trim());
                col5.attr('value', ul_id.text().trim());
                tr_count++;
            }
        });
        btn_clear.on('click', function () {
            var col1 = '';
            var col2 = '';
            var col3 = '';
            var col4 = '';
            var col5 = '';
            var hidden_subj_id = '';
            for (var i = 1; i <= MAX_SIZE; i++) {
                col1 = $('#col1-'.concat(i));
                col2 = $('#col2-'.concat(i));
                col3 = $('#col3-'.concat(i));
                col4 = $('#col4-'.concat(i));
                col5 = $('#col5-'.concat(i));
                hidden_subj_id = $('#hidden_subj_id-'.concat(i));
                col1.val('');
                col2.val('');
                col3.val('');
                col4.val('');
                col5.val('');
                col1.attr('value', '');
                col2.attr('value', '');
                col3.attr('value', '');
                col4.attr('value', '');
                col5.attr('value', '');
                hidden_subj_id.attr('value', '');
            }
            subject_count.val(0);
            subject_count.attr('value', 0);
            tr_count = 1;
            subjects_arr.length = 0;
        });
        function get_net(value, discount_pct) {
            return Math.abs((value * discount_pct) - value).toFixed(2);
        }
        function set_payments() {
            net_tuition = get_net(tuition, discount_pct);
            //PLAN A
            var other_fees = Number(misc) + Number(ir_fee) + Number(additional);
            a_total = (Number(net_tuition) + other_fees).toFixed(2);

            $('#plan_a-1').html(a_total);
            $('#total-a').html('$'.concat(a_total));

            b_total = Number(net_tuition * b_rate + other_fees).toFixed(2);
            var b_net = Number(net_tuition * b_rate);
            b_down = Number((b_net / 2) + other_fees);
            b_give = Number(b_net / 2);
            b_add_on = b_total - a_total;

            $('#plan_b-1').html(b_down.toFixed(2));
            $('#plan_b-2').html(b_give.toFixed(2));
            $('#add_on-b').html(b_add_on.toFixed(2));
            $('#total-b').html('$'.concat(b_total));

            c_total = Number(net_tuition * c_rate + other_fees).toFixed(2);
            var c_net = Number(net_tuition * c_rate);
            c_down = Number((c_net / 3) + other_fees);
            c_give = Number(c_net / 3);
            c_add_on = c_total - a_total;

            $('#plan_c-1').html(c_down.toFixed(2));
            $('#plan_c-2').html(c_give.toFixed(2));
            $('#plan_c-3').html(c_give.toFixed(2));
            $('#add_on-c').html(c_add_on.toFixed(2));
            $('#total-c').html('$'.concat(c_total));

            modal_tuition.val(tuition.toFixed(2));
            modal_tuition.attr('value', tuition);

            modal_net.val(net_tuition);
            modal_net.attr('value', net_tuition);
            modal_reg.val(ir_fee + ajax_reg);
            modal_reg.attr('value', ir_fee + ajax_reg);
            modal_misc.val(misc + ajax_misc);
            modal_misc.attr('value', misc + ajax_misc);

            modal_planB.val(b_add_on.toFixed(2));
            modal_planB.attr('value', b_add_on.toFixed(2));
            modal_planC.val(c_add_on.toFixed(2));
            modal_planC.attr('value', c_add_on.toFixed(2));
            modal_total.val(a_total);
            modal_total.attr('value', a_total);

            $('#post_ir_fee').attr('value', ajax_reg);
            $('#post_misc_fee').attr('value', ajax_misc);
            $('#post_application_fee').attr('value', ajax_appl);
            $('#post_admin_fee').attr('value', ajax_admin);
            $('#post_math_fee').attr('value', ajax_math);
            $('#post_tutorial_fee').attr('value', ajax_tut);
            $('#post_rswl_fee').attr('value', ajax_rswl);
            $('#post_esl_fee').attr('value', ajax_esl);
            $('#post_other_fee').attr('value', ajax_other);
            $('#post_other_text').attr('value', ajax_other_text);
            $('#post_grad_fee').attr('value', ajax_grad);
            $('#post_b_add_on').attr('value', b_add_on);
            $('#post_c_add_on').attr('value', c_add_on);
            $('#post_grand_total').attr('value', a_total);

            $('#post_enrolled_subjects').attr('value', post_enrolled_subjects);
        }
//        function set_payments_old() {
//            net_tuition = get_net(tuition, discount_pct);
//            //PLAN A
//            a_total = (Number(net_tuition) + Number(misc)).toFixed(2);
//            a_total = Number(Number(a_total) + Number(one_time_charge) + Number(ir_fee) + Number(additional)).toFixed(2);
//            $('#plan_a-1').html(a_total);
//            $('#total-a').html('$'.concat(a_total));
//            //PLAN B
//            b_give = (net_tuition - get_net(net_tuition, b_give_pct)).toFixed(2);
//            b_down = (net_tuition - get_net(net_tuition, b_down_pct)).toFixed(2);
//            b_add_on = (net_tuition - get_net(net_tuition, add_on_b)).toFixed(2);
//            b_total = Number(b_down) + Number(b_give) + Number(b_add_on) + misc;
//            b_total = Number(Number(b_total) + Number(one_time_charge) + Number(ir_fee) + Number(additional)).toFixed(2);
//            var b_misc = Number((misc + one_time_charge + ir_fee + Number(additional)) / 2).toFixed(2);
//            b_give = Number(Number(b_give) + Number(b_misc)).toFixed(2);
//            b_down = Number(Number(b_down) + Number(b_misc)).toFixed(2);
//            $('#plan_b-1').html(b_down);
//            $('#plan_b-2').html(b_give);
//            $('#add_on-b').html(b_add_on);
//            $('#total-b').html('$'.concat(b_total));
//            c_down = (net_tuition - get_net(net_tuition, c_down_pct)).toFixed(2);
//            c_give = (net_tuition - get_net(net_tuition, c_give_pct)).toFixed(2);
//            c_add_on = (net_tuition - get_net(net_tuition, add_on_c)).toFixed(2);
//            c_total = (Number(c_down) + Number(c_give * 2) + Number(c_add_on) + Number(misc)).toFixed(2);
//            c_total = Number(Number(c_total) + Number(one_time_charge) + Number(ir_fee) + Number(additional)).toFixed(2);
//            var c_misc = Number((misc + one_time_charge + ir_fee + Number(additional)) / 3).toFixed(2);
//            c_give = Number(Number(c_give) + Number(c_misc)).toFixed(2);
//            c_down = Number(Number(c_down) + Number(c_misc)).toFixed(2);
//            $('#plan_c-1').html(c_down);
//            $('#plan_c-2').html(c_give);
//            $('#plan_c-3').html(c_give);
//            $('#add_on-c').html(c_add_on);
//            $('#total-c').html('$'.concat(c_total));
//            //PLAN C
//
//            modal_tuition.val(tuition.toFixed(2));
//            modal_tuition.attr('value', tuition);
//
//            modal_net.val(net_tuition);
//            modal_net.attr('value', net_tuition);
//            modal_reg.val(ir_fee + ajax_reg);
//            modal_reg.attr('value', ir_fee + ajax_reg);
//            modal_misc.val(misc + ajax_misc);
//            modal_misc.attr('value', misc + ajax_misc);
//
//            modal_planB.val(b_add_on);
//            modal_planB.attr('value', b_add_on);
//            modal_planC.val(c_add_on);
//            modal_planC.attr('value', c_add_on);
//            modal_total.val(a_total);
//            modal_total.attr('value', a_total);
//
//            $('#post_ir_fee').attr('value', ajax_reg);
//            $('#post_misc_fee').attr('value', ajax_misc);
//            $('#post_application_fee').attr('value', ajax_appl);
//            $('#post_admin_fee').attr('value', ajax_admin);
//            $('#post_math_fee').attr('value', ajax_math);
//            $('#post_tutorial_fee').attr('value', ajax_tut);
//            $('#post_rswl_fee').attr('value', ajax_rswl);
//            $('#post_esl_fee').attr('value', ajax_esl);
//            $('#post_other_fee').attr('value', ajax_other);
//            $('#post_other_text').attr('value', ajax_other_text);
//            $('#post_grad_fee').attr('value', ajax_grad);
//            $('#post_grad_fee').attr('value', ajax_grad);
//            $('#post_b_add_on').attr('value', b_add_on);
//            $('#post_c_add_on').attr('value', c_add_on);
//            $('#post_grand_total').attr('value', a_total);
//
//            $('#post_enrolled_subjects').attr('value', post_enrolled_subjects);
//        }
        function reload() {
            location.reload();
        }
        $('#reload').on('click', function () {
            reload();
        });
        $('#breakdown').on('click', function (e) {
            e.preventDefault();
            $('.breakdown').toggle('fast');
        });
        on_blur.blur(set_ajax);
        on_tuition_change.blur(function () {
            tuition = Number(modal_tuition.val());
            set_ajax();
        });
        function set_ajax() {
            ajax_reg = Number(modal_reg.val() - ir_fee);
            ajax_misc = Number(modal_misc.val() - misc);
            ajax_appl = Number(modal_appl.val());
            ajax_admin = Number(modal_admin.val());
            ajax_math = Number(modal_math.val());
            ajax_tut = Number(modal_tut.val());
            ajax_rswl = Number(modal_rswl.val());
            ajax_esl = Number(modal_esl.val());
            ajax_other = Number(modal_other.val());
            ajax_other_text = modal_other_text.val();
            ajax_grad = Number(modal_grad.val());

            additional = Number(ajax_reg) + Number(ajax_misc)
                    + Number(ajax_appl) + Number(ajax_admin)
                    + Number(ajax_math) + Number(ajax_tut)
                    + Number(ajax_rswl) + Number(ajax_esl)
                    + Number(ajax_other);
            set_payments();
        }
        $('#print').on('click', function () {
            prepare_print();
        });
        function check_post() {
            if (subject_count.attr('value') == 0) {
                $('#post').prop('disabled', true);
                $('#print').prop('disabled', true);
            }
        }
        $('#currency').on('change', function () {
            $('#post_currency').attr('value', $(this).val());
        });
        $('#reset_fees').on('click', function() {
            tuition = Number(0);
            modal_reg.attr('value', 0);
            modal_misc.attr('value', 0);
            modal_admin.attr('value', 0);
            modal_math.attr('value', 0);
            modal_rswl.attr('value', 0);
            modal_tut.attr('value', 0);
            modal_esl.attr('value', 0);
            modal_other.attr('value', 0);
            modal_other_text.attr('value', 0);
            modal_grad.attr('value', 0);
            
            modal_reg.val( 0);
            modal_misc.val( 0);
            modal_admin.val( 0);
            modal_math.val( 0);
            modal_rswl.val( 0);
            modal_tut.val( 0);
            modal_esl.val( 0);
            modal_other.val( 0);
            modal_other_text.val( 0);
            modal_grad.val( 0);

            set_ajax();
        });
    }); //END OF DOCUMENT READY
</script>