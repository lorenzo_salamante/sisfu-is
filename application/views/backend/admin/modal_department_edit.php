<?php
$edit_data = $this->db->get_where('sisfu_departments', array('department_id' => $param2))->result_array();
foreach ($edit_data as $row):
    ?>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary" data-collapsed="0">
            <div class="panel-heading">
                <div class="panel-title"><span class="primary-color">
                    <i class="entypo-plus-circled"></i>
                    <?php echo get_phrase('edit_school'); ?></span>
                </div>
            </div>
            <div class="panel-body">

                <?php echo form_open(base_url() . 'index.php?admin/departments/edit/' . $param2, array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data')); ?>

                <div class="form-group">
                    <label for="department_name" class="col-sm-3 control-label"><?php echo get_phrase('school_name'); ?></label>

                    <div class="col-sm-7">
                        <input type="text" class="form-control" id="department_name" name="department_name" value="<?php echo $row['department_name'] ?>" data-validate="required" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                    </div> 
                </div>
                
                <div class="form-group">
                    <label for="department_code" class="col-sm-3 control-label"><?php echo get_phrase('school_code'); ?></label>

                    <div class="col-sm-7">
                        <input type="text" class="form-control" id="department_code" name="department_code" value="<?php echo $row['department_code'] ?>" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                    </div> 
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-default"><?php echo get_phrase('save'); ?></button>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
<?php endforeach;?>