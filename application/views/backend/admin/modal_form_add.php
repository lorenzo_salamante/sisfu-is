<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary" data-collapsed="0">
            <div class="panel-heading">
                <div class="panel-title"><span class="primary-color">
                    <i class="entypo-plus-circled"></i>
                    <?php echo get_phrase('add_new_ISO_form'); ?></span>
                </div>
            </div>
            <div class="panel-body">

                <?php echo form_open(base_url() . 'index.php?admin/forms/create/', array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data')); ?>
                
                <div class="form-group">
                    <label for="form_name" class="col-sm-3 control-label"><?php echo get_phrase('form_name'); ?> </label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control" id="form_name" name="form_name" value="" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                    </div> 
                </div>
                
                <div class="form-group">
                    <label for="form_code" class="col-sm-3 control-label"><?php echo get_phrase('form_code'); ?> </label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control" id="form_code" name="form_code" value="" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                    </div> 
                </div>
                
                <div class="form-group">
                    <label for="form_control" class="col-sm-3 control-label"><?php echo get_phrase('form_control'); ?> </label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control" id="form_control" name="form_control" value="SISFU/QSF-REG-###" data-validate="required" placeholder=""data-message-required="<?php echo get_phrase('value_required'); ?>">
                    </div> 
                </div>
                
                <div class="form-group">
                    <label for="form_revision" class="col-sm-3 control-label"><?php echo get_phrase('form_revision'); ?> </label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control" id="form_revision" name="form_revision" value="Rev ### MM/DD/YY" placeholder="" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                    </div> 
                </div>
                
                
                
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-default"><?php echo get_phrase('add_ISO_form'); ?></button>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>