<?php /*
 * @Author: Lorenzo Salamante - lorenzosalamante@gmail.com
 * Team Istruktura - http://www.istruktura.com
 */ ?>

<!--MENTOR-->
<label for="mentor_id" class="col-sm-1 control-label"><?php echo get_phrase('mentor'); ?></label>

<div class="col-sm-2">
    <select name="mentor_id" class="form-control" id="mentor_id" 
            >
        <option value=""><?php echo get_phrase('select'); ?></option>
        <?php
        $mentors = $this->db->get('sisfu_mentors_meta')->result_array();
        foreach ($mentors as $row):
            ?>
            <option value="<?php echo $row['mentor_id']; ?>">
                <?php echo $row['mentor_name']; ?>
            </option>
            <?php
        endforeach;
        ?>
    </select>
</div> 