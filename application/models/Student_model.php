<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Student_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    # FORMS

    function get_all_students() {
        $query = 'SELECT a.student_id, a.student_no, a.last_name, a.first_name, a.middle_name, a.enrolled_subjects, a.suffix, a.nickname, a.admission_date, a.admitted_by, a.sex, a.birth_date, a.current_address, a.permanent_address, a.email, a.contact_no, a.visa_expiration, a.remarks, a.photo,
        b.department_name, b.department_code, c.course_name, d.scholarship_name, f.status_name, g.nationality_name, h.visa_name 
        FROM sisfu_students as a
        LEFT JOIN sisfu_departments as b
        ON a.department_id = b.department_id
        LEFT JOIN sisfu_courses as c
        ON a.course_id = c.course_id
        LEFT JOIN sisfu_scholarships_meta as d
        ON a.scholarship_id = d.scholarship_id
        LEFT JOIN sisfu_status_meta as f
        ON a.status_id = f.status_id
        LEFT JOIN sisfu_nationalities_meta as g
        ON a.nationality_id = g.nationality_id
        LEFT JOIN sisfu_visa_meta as h
        ON a.visa_id = h.visa_id
        ORDER BY a.student_id';
        $q = $this->db->query($query);
        return $q->result_array();
//f.status_category
//WHERE f.status_category = 1
    }

    function get_student($id) {
        $this->db->where('student_id', $id);
        $q = $this->db->get('sisfu_students');
        return $q->result_array();
    }

    function add_new_student() {
        $data['student_name'] = $this->input->post('student_name');
        $data['student_code'] = $this->input->post('student_code');
        $data['student_control'] = $this->input->post('student_control');
        $data['student_revision'] = $this->input->post('student_revision');

        $this->db->insert('sisfu_students', $data);
        return $this->db->affected_rows();
    }

    function edit_student($id) {
        $data['student_name'] = $this->input->post('student_name');
        $data['student_code'] = $this->input->post('student_code');
        $data['student_control'] = $this->input->post('student_control');
        $data['student_revision'] = $this->input->post('student_revision');

        $this->db->where('student_id', $id);
        $this->db->update('sisfu_students', $data);
        return $this->db->affected_rows();
    }

    function delete_student($id) {
        $this->db->where('student_id', $id);
        $this->db->delete('sisfu_students');
        return $this->db->affected_rows();
    }

    # /FORMS
}
